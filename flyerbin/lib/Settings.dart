

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'Utils/SharedPrefrence.dart';
import 'Utils/Urls.dart';

class Settings extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
   return state();
  }

}
class state extends State<Settings>{
  bool not_flag = true;

  @override
  void initState() {
    super.initState();
    Future token = SharedPrefrence().getNotificationFlag();
    token.then((data) async {
     setState(() {
       not_flag = data;
     });
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        centerTitle: false,
        iconTheme: new IconThemeData(color: Color(0xff00ADEE)),
        title: Text(
          "Settings",
          style: TextStyle(color: Color(0xff00ADEE), fontSize: 13),
        ),
        backgroundColor: Colors.white,
      ),
      body: ListView(
        children: [
          Row(
            children: [
              Expanded(
                child: Icon(Icons.notifications,color: Colors.blue,),
                flex: 1,
              ),
              Expanded(
                child: Text('Notifications'),
                flex: 3,
              ),
              Expanded(
                child: Checkbox(
                  value: not_flag,
                  onChanged: (bool value) {
                    setState(() {
                     // this.showvalue = value;
                      not_flag = value;
                      SharedPrefrence().setNotificationFLag(not_flag);
                    });
                  },
                ),
                flex: 1,
              ),
            ],
          ),
          Divider(),
          Row(
            children: [
              Expanded(
                child: Icon(Icons.refresh,color: Colors.blue,),
                flex: 1,
              ),
              Expanded(
                child: Text('Clear Cache'),
                flex: 4,
              ),
            ],
          ),
          Divider(),
          GestureDetector(
            child: Row(
              children: [
                Expanded(
                  child: Icon(Icons.help,color: Colors.blue,),
                  flex: 1,
                ),
                Expanded(
                  child: Text('Help'),
                  flex: 4,
                ),
              ],
            ),
            onTap: () async {
              await launch(Urls.HelpUrl);
            },
          ),
          Divider(),
         GestureDetector(
           child:  Row(
             children: [
               Expanded(
                 child: Icon(Icons.warning_outlined,color: Colors.blue,),
                 flex: 1,
               ),
               Expanded(
                 child: Text('Terms and Conditions'),
                 flex: 4,
               ),
             ],
           ),
           onTap: ()async{
             await launch(Urls.TermsUrl);
           },
         ),
          Divider(),
          GestureDetector(
            child: Row(
              children: [
                Expanded(
                  child: Icon(Icons.privacy_tip_rounded,color: Colors.blue,),
                  flex: 1,
                ),
                Expanded(
                  child: Text('Privacy Policy'),
                  flex: 4,
                ),
              ],
            ),
            onTap: ()async{
              await launch(Urls.PrivacyPolicyUrl);
            },
          )
        ],
      ),
    );
  }
}