

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:group_radio_button/group_radio_button.dart';

import 'BottomnavBarScreen.dart';
import 'Utils/SharedPrefrence.dart';

class Languages extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return state();
  }

}

class state extends State<Languages>{
  List<String> lang = ["English", "Arabic"];
  String _singleValue = "Text alignment right";
  String _verticalGroupValue = "Pending";
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: SingleChildScrollView(
        child: Center(
          child: Container(
            //color: Colors.white,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            decoration: new BoxDecoration(
              image: new DecorationImage(
                image: new AssetImage("assets/images/login_bg.png"),
                fit: BoxFit.fill,
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(32.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 130.0,
                    child: SvgPicture.asset(
                      "assets/images/flyerbin_logo.svg",
                      fit: BoxFit.contain,
                    ),
                  ),
                  SizedBox(height: 20.0),
                  Text('Select Your Language'),
                  SizedBox(height: 10,),
                  RadioGroup<String>.builder(
                    groupValue: _verticalGroupValue,
                    onChanged: (value) => setState(() {
                      _verticalGroupValue = value;
                      print(_verticalGroupValue);

                    }),
                    items: lang,
                    itemBuilder: (item) => RadioButtonBuilder(
                      item,
                    ),
                  ),
                  SizedBox(height: 25,),
                  GestureDetector(
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 10),
                      padding: EdgeInsets.all(20),
                      width: MediaQuery.of(context).size.width,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(55)),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: Colors.grey.shade200,
                                offset: Offset(5, 4),
                                blurRadius: 20,
                                spreadRadius: 2)
                          ],
                          gradient: LinearGradient(
                              begin: Alignment.centerLeft,
                              end: Alignment.centerRight,
                             // colors: [Color(0xff00ADEE),Color(0xff00ADEE)]
                          )
                      ),
                      child: Text(
                        'Save',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 15, color: Colors.white),
                      ),
                    ),
                    onTap: (){
                      if(_verticalGroupValue == "English"){
                        SharedPrefrence().setLanguage('en');
                      }else{
                        SharedPrefrence().setLanguage("ar");
                      }
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DashboardScreen()),
                          ModalRoute.withName("/login"));
                    },
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}