

import 'dart:convert';
import 'dart:io';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mime/mime.dart';
import 'Loginscreen.dart';
import 'PasswordChange.dart';
import 'UpdateMobile.dart';
import 'Utils/CustomDialogs.dart';
import 'Utils/SharedPrefrence.dart';
import 'Utils/Urls.dart';
import 'package:http_parser/http_parser.dart';
class ProfileScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new ProfileScreenState();
  }
  
}

class ProfileScreenState extends State<ProfileScreen>{
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 15.0);
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String otp,userToken,user_email = "",password_label = "";
  File image;
  bool hasPassword = false;
  final Imagepicker = ImagePicker();
  final TextEditingController _textEditingNameController =
  new TextEditingController();
  final TextEditingController _textEditingMailController =
  new TextEditingController();
  final TextEditingController _textEditingPasswordController =
  new TextEditingController();
  final TextEditingController _textEditingConfirmPasswordController =
  new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future email = SharedPrefrence().getUserEmail();
    Future name = SharedPrefrence().getUserName();
    email.then((data) async {
      setState(() {
        user_email = data;
      });

      _textEditingMailController.text = user_email;

    });
    name.then((data) async {
      _textEditingNameController.text = data;
    });
    Future token = SharedPrefrence().getToken();
    token.then((data) async {
    userToken = data;
    });
    Future password = SharedPrefrence().getHasPassword();
    password.then((password) async {
      hasPassword = password;
     if(password == true){
       password_label = "Change Password";
     }else{
       password_label = "Add Password";
     }
    });

  }

  @override
  Widget build(BuildContext context) {

    final nameField = TextField(
      keyboardType: TextInputType.text,
      obscureText: false,
      style: style,
      controller: _textEditingNameController,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(10.0, 15.0, 20.0, 10.0),
        //hintText: "Name",
        labelText: "Name"
        //border:
        // OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))
      ),
    );

    final emailField = TextField(
      keyboardType: TextInputType.emailAddress,
      obscureText: false,
      style: style,
      controller: _textEditingMailController,
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.mail,color: Colors.blue,),
        contentPadding: EdgeInsets.fromLTRB(10.0, 15.0, 20.0, 10.0),
        //hintText: "Email",
        labelText: "Email"
        //border:
        // OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))
      ),
    );



     final privatefiled = Align(alignment: Alignment.topLeft,child: Text('Private Information',style: TextStyle(color: Colors.blue),),);

    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff00ADEE),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(10.0, 15.0, 20.0, 10.0),
        onPressed: () {
          showLogoutConfirmationDialog();
        },
        child: Text("Log out",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );


    return Scaffold(
      resizeToAvoidBottomInset: false,
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          "Profile",
          style: TextStyle(fontSize: 15, color: Colors.lightBlue),
        ),
        automaticallyImplyLeading: true,
        iconTheme: IconThemeData(
          color: Colors.lightBlue,
        ),
        backgroundColor: Colors.white,
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: GestureDetector(
              child: Icon(
                Icons.done,
                color: Color(0xff00ADEE),
              ),
              onTap: (){
                if(_textEditingNameController.text.isEmpty){
                  final snackBar = SnackBar(content: Text('Name Is Mandatory',style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
                  _scaffoldKey.currentState.showSnackBar(snackBar);
                }else{
                  updateProfile(_textEditingNameController.text, _textEditingMailController.text);
                }
              },
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Container(
            //color: Colors.white,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: const EdgeInsets.all(32.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                      width: 70,
                      height: 70,
                      decoration: BoxDecoration(
                        //color: Colors.white,
                        shape: BoxShape.circle,
                        // image: DecorationImage(image:AssetImage("assets/images/user_palceholder.png")),

                      ),
                      padding: const EdgeInsets.all(3.0),
                      child: Padding(
                        padding: EdgeInsets.all(0),
                        child:ClipRRect(
                          //borderRadius: BorderRadius.circular(50.0),
                            child:  image == null ? Image.asset( "assets/images/user_palceholder.png") : Image.file(image)),
                      )
                  ),
                  Padding(padding: EdgeInsets.all(10),child: GestureDetector(
                    child: Text("Edit Photo",style: TextStyle(color: Color(0xff00ADEE)),textAlign:TextAlign.center ,),
                    onTap: (){
                      Pickimage();
                    },
                  ) ,),
                 /* SizedBox(
                    height: 30.0,
                    *//*child: SvgPicture.asset(
                      "assets/images/flyerbin_logo.svg",
                      fit: BoxFit.contain,
                    ),*//*
                    child:Align(
                      child:Text("Sign Up",style: TextStyle(color: Color(0xff00ADEE),fontSize: 20),textAlign: TextAlign.left,), alignment: Alignment.centerLeft,
                    )


                  ),*/
                  SizedBox(height: 20.0),
                  nameField,
                  SizedBox(height: 20.0),
                  privatefiled,
                  SizedBox(height: 20.0),
                  emailField,
                  SizedBox(height: 30.0),

                  loginButon,
                  SizedBox(height: 20.0),
                  SizedBox(height: 5,),
                  Padding(padding: EdgeInsets.all(10),child:GestureDetector(
                    child: Text(password_label,style: TextStyle(color: Color(0xff00ADEE)),textAlign:TextAlign.center ,),
                    onTap: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => PasswordChange(
                            hasPassword),
                        ),
                      );
                    },
                  ) ,),
                  SizedBox(height: 5,),
                  Padding(padding: EdgeInsets.all(10),child:GestureDetector(
                    child: Text("Update Mobile",style: TextStyle(color: Color(0xff00ADEE)),textAlign:TextAlign.center ,),
                    onTap: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => UpdateMobile(),
                        ),
                      );
                    },
                  ) ,),
                  SizedBox(
                    height: 15.0,
                  ),
                 // SocialLoginLayout(),
                 // RegisterLayout()
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void updatProPic(File image) async{
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    Map<String, String> headers = {
      "Accept": "application/json",
      "Authorization": "Bearer ${userToken}"
    };
    // await pr.show();
    final mimeTypeData =
    lookupMimeType(image.path, headerBytes: [0xFF, 0xD8]).split('/');
    final request =
    new http.MultipartRequest("POST", Uri.parse(Urls.baseUrl+Urls.UpdateProfilePhoto));
    final file = await http.MultipartFile.fromPath('profile_photo', image.path,
        contentType: MediaType(mimeTypeData[0], mimeTypeData[1]));
    request.files.add(file);
    request.headers.addAll(headers);
    StreamedResponse response = await request.send();

    response.stream.transform(utf8.decoder).listen((value) {
      try {
        print("resp_upimage : " + value);
        Map<String, dynamic> map = jsonDecode(value);
        var status = map['status'];
        if (response.statusCode == 200) {

        } else {
          final snackBar = SnackBar(content: Text('Try Again',style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
          _scaffoldKey.currentState.showSnackBar(snackBar);
        }

        setState(() {});
      } on SocketException catch (e) {
        dialog.dismissProgressDialog(context);
        e.toString();
        final snackBar = SnackBar(content: Text("No Internet connection 😑",style: TextStyle(color: Colors.white),),backgroundColor: Colors.red,);
        _scaffoldKey.currentState.showSnackBar(snackBar);
    }});
  }

  void Pickimage() async {
    PickedFile _image =
    await Imagepicker.getImage(source: ImageSource.gallery, maxHeight: 500);

      setState(() {
        image = File(_image.path);

      });
      print(image.path);
      updatProPic(image);


  }

  void updateProfile(String name,String email)async{
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = name;
    data['email'] = email;
    var bodydata = json.encode(data);// important
    print(bodydata);
    var response = await http.post(
        "${Urls.baseUrl}${Urls.UpdateProfile}",
        headers: {
          "Authorization": "Bearer ${userToken}",
          "Content-Type": "application/json"
        },
        body:bodydata);
     Map<String, dynamic> value = json.decode(response.body);
     if(response.statusCode == 200){
        dialog.dismissProgressDialog(context);
        try{
          print(response.body);
           if(value['success']){
             final snackBar = SnackBar(content: Text('Success',style: TextStyle(color: Colors.white)),backgroundColor: Colors.green,);
             _scaffoldKey.currentState.showSnackBar(snackBar);
             var data_obj = value['data'];
             var user_obj = data_obj['user'];
             SharedPrefrence().setUserName(user_obj['name']);
             SharedPrefrence().setHasPassword(user_obj['hasPassword']);
             if(user_obj['mobile'] == null){
               SharedPrefrence().setMobile("");
             }else{
               SharedPrefrence().setMobile(user_obj['mobile']);
             }
             if(user_obj['email'] == null){

               SharedPrefrence().setUserEmail('');
             }else{
               SharedPrefrence().setUserEmail(user_obj['email']);
             }

           }
        }catch(e){
          e.toString();
        }
     }else{
          dialog.dismissProgressDialog(context);
          final snackBar = SnackBar(content: Text('Try Again',style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
          _scaffoldKey.currentState.showSnackBar(snackBar);
     }
  }

  void showLogoutConfirmationDialog(){
    showDialog(
        context: context,
        builder: (_) => new AlertDialog(
          title: new Text("CONFIRM"),
          content: new Text("Are You Sure To Logout?"),
          actions: <Widget>[
            FlatButton(
              child: Text('Yes'),
              onPressed: () {
                Navigator.of(context).pop();
                SharedPrefrence().setLoggedIn(false);
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                        builder: (context) => LoginScreen()),
                    ModalRoute.withName("/profile"));
              },
            ),
            FlatButton(
              child: Text('No,Go Back'),
              onPressed: () {
                Navigator.of(context).pop();

              },
            )
          ],
        ));
  }
}
