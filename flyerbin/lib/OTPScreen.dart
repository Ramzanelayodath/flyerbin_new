import 'dart:async';
import 'dart:convert';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:http/http.dart' as http;
import 'package:pin_code_text_field/pin_code_text_field.dart';

import 'RegisterScreen.dart';
import 'Utils/CustomDialogs.dart';
import 'Utils/SharedPrefrence.dart';
import 'Utils/Urls.dart';

class OTPScreen extends StatefulWidget {
  String referal;

  OTPScreen(this.referal);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new OTPScreeState(referal);
  }
}

class OTPScreeState extends State<OTPScreen> {
  TextEditingController controller = TextEditingController(text: "");
  String OTP = "";
  String mobile_number = "";
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String thisText = "";
  int pinLength = 6;
  bool hasError = false;
  String errorMessage;
  String referal;

  OTPScreeState(this.referal);

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  void initState() {
    Future mobilenumber = SharedPrefrence().getMobile();
    mobilenumber.then((data) async {
      mobile_number = data;
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white,
      key: _scaffoldKey,
      body: SafeArea(
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: new Container(
            margin: EdgeInsets.symmetric(horizontal: 16),
            color: Colors.white,
            child: Column(
              children: <Widget>[
                new Expanded(
                  child: _logo(),
                  flex: 2,
                ),
                new Expanded(
                  child: _textVerifyOTP(),
                  flex: 1,
                ),
                new Expanded(
                  child: _otpTextField(),
                  flex: 1,
                ),
                new Expanded(
                  child: _btnVerify(),
                  flex: 2,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Column _btnVerify() {
    return new Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        _verifyButton(),
      ],
    );
  }

  Widget _verifyButton() {
    return GestureDetector(
      onTap: () {
        if (controller.text.toString().length == 0) {
          final snackBar = SnackBar(content: Text("Enter OTP"));
          _scaffoldKey.currentState.showSnackBar(snackBar);
        }
        else{
          print(OTP);
         // Navigator.pushReplacementNamed(context, "/register_screen");
          doVarifyOtp(mobile_number, OTP);
         /* Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                  builder: (context) => RegisterScreen()),
              ModalRoute.withName("/login"));*/

        }

      },
      child: new Container(
        margin: EdgeInsets.symmetric(horizontal: 20),
        padding: EdgeInsets.all(20),
        width: MediaQuery.of(context).size.width,
        alignment: Alignment.topCenter,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(55)),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.grey.shade200,
                  offset: Offset(5, 4),
                  blurRadius: 20,
                  spreadRadius: 2)
            ],
            gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
               // colors: [Color(0xff00ADEE), Color(0xff00ADEE)]
            )),
        child: Text(
          'VERIFY',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 15,
              color: Colors.black,
              letterSpacing: 1,
              fontWeight: FontWeight.w700),
        ),
      ),
    );
  }

  Center _logo() {
    return Center(
      child: SvgPicture.asset(
        'assets/images/flyerbin_logo.svg',
        fit: BoxFit.fitWidth,
        width: 200,
      ),
    );
  }

  Column _textVerifyOTP() {
    return new Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          "VERIFY OTP",
          style: TextStyle(fontWeight: FontWeight.w500, letterSpacing: 1),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 5.0, right: 2.0),
              child: Container(
                height: 2.0,
                width: 25.0,
                color: Color(0xff00ADEE),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 5.0, left: 2.0),
              child: Container(
                height: 2.0,
                width: 25.0,
                color: Colors.grey,
              ),
            ),
          ],
        ),
        SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "We have send the OTP on ",
              style: TextStyle(
                  color: Colors.grey, fontSize: 12, letterSpacing: 0.5),
            ),
            Text(
              mobile_number,
              style: TextStyle(color: Colors.grey, fontSize: 12),
            ),
          ],
        ),
      ],
    );
  }

  Widget _otpTextField() {
    return SingleChildScrollView(
      child: new Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            PinCodeTextField(
              autofocus: false,
              controller: controller,
              hideCharacter: false,
              highlight: true,
              highlightColor: CupertinoColors.activeBlue,
              defaultBorderColor: CupertinoColors.black,
              hasTextBorderColor:Color(0xff00ADEE),
              maxLength: pinLength,
              hasError: hasError,
              maskCharacter: "🐶",
              onTextChanged: (text) {
                setState(() {
                  hasError = false;
                  thisText = text;
                });
              },
              isCupertino: true,
              onDone: (text) {
                print("DONE $text");
                OTP = text;
              },
              wrapAlignment: WrapAlignment.end,
              pinBoxHeight: 60,
              pinBoxWidth: 40,
              pinBoxDecoration:
              ProvidedPinBoxDecoration.underlinedPinBoxDecoration,
              pinTextStyle: TextStyle(fontSize: 30.0),
              pinTextAnimatedSwitcherTransition:
              ProvidedPinBoxTextAnimation.scalingTransition,
              pinTextAnimatedSwitcherDuration: Duration(milliseconds: 300),
              highlightAnimation: true,
              highlightAnimationBeginColor: Colors.black,
              highlightAnimationEndColor: Colors.white12,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "If you don't receive code ?",
                    style: TextStyle(
                        color: Colors.grey, fontSize: 12, letterSpacing: 0.5),
                  ),
                  Text(
                    "Resend",
                    style: TextStyle(
                        color: Color(0xff00ADEE), fontSize: 12, letterSpacing: 0.5),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

//Verify OTP

  void doVarifyOtp(String userMob, String OTP) async {
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var response = await http.post(Urls.baseUrl+Urls.Varify_OTP,
        headers: {"Content-Type": "application/json",
                   "Accept":"application/json"},
        body: json.encode({"mobile": userMob, "otp": OTP}));

    Map<String, dynamic> value = json.decode(response.body);
    if (response.statusCode == 200) {
      dialog.dismissProgressDialog(context);
      try {
        print("response OTP " + response.body.toString());
        Map<String, dynamic> value = json.decode(response.body);
        var success = value["success"];
        var message = value["message"].toString();
        if(success){
          SharedPrefrence().setMobileOTP(OTP);
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                  builder: (context) => RegisterScreen(referal)),
              ModalRoute.withName("/login"));
         // Navigator.pushReplacementNamed(context, "/register_screen");
        }
        else{
          final snackBar =
          SnackBar(content: Text(message));
          _scaffoldKey.currentState.showSnackBar(snackBar);
        }
      } catch (e) {
        print(e.toString());
      }
    } else if (response.statusCode == 404) {
      dialog.dismissProgressDialog(context);
      var message = value['message'];
      CustomDialogs().showErrorAlert(context, message);
    } else if (response.statusCode == 208) {
      dialog.dismissProgressDialog(context);
//      progressDialog.dismiss();
    } else {
      dialog.dismissProgressDialog(context);
      CustomDialogs().showErrorAlert(context, "Try Again...");
    }
  }
}