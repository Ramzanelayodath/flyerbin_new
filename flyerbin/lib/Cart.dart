


import 'dart:convert';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'FlyerLoadScreen.dart';
import 'Model/CartInsideModel.dart';
import 'Model/CartModel.dart';
import 'Utils/CustomDialogs.dart';
import 'Utils/SharedPrefrence.dart';
import 'Utils/Urls.dart';
import 'package:http/http.dart' as http;
class Cart extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
   return state();
  }
}

class state extends State<Cart>{
  String userToken = "",lan = "en";
  var cart_items = List<CartModel>();
  var coupon_items = List<CartModel>();
  var list_count = 0,copunitems_count = 0;
  bool isShowClipItem = true,isShowCopounItem = true,isShowlist = true;
  @override
  void initState() {
    super.initState();
    Future lng = SharedPrefrence().getLanguage();
    lng.then((data) async {
      lan = data;
    });

    Future token = SharedPrefrence().getToken();
    token.then((data) async {
      userToken = data;
      getCartItems();
     
    });

  }
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
   return Scaffold(
       key: _scaffoldKey,
       resizeToAvoidBottomInset: false,
       appBar :AppBar(
         centerTitle: false,
         iconTheme: new IconThemeData(color: Color(0xff00ADEE)),
         title: Text(
           "Cart",
           style: TextStyle(color: Color(0xff00ADEE), fontSize: 13),
         ),
         backgroundColor: Colors.white,
       ),
       body: SingleChildScrollView(
         child: isShowlist ? Container(
             child :Column(
               children: [
                 Visibility(
                   child: Container(
                     child: Column(
                       children: [
                         Padding(padding: EdgeInsets.all(8),
                           child: Align(
                             alignment: Alignment.topLeft,
                             child: Text('My Clippings',style: TextStyle(color: Colors.black),),
                           ),),
                         ListView.builder(
                             physics: const NeverScrollableScrollPhysics(),
                             itemCount: list_count,
                             shrinkWrap: true,
                             scrollDirection: Axis.vertical,
                             itemBuilder: (context, index) {
                               return Column(
                                 children: [
                                   Container(
                                     width: double.infinity,
                                     color: Colors.grey[200],
                                     child :Align(
                                       alignment: Alignment.topLeft,
                                       child:  Padding(
                                         padding: EdgeInsets.all(5),
                                         child: Image.network(Urls.baseImageUrl+cart_items[index].logo,height: 25,width: 25,),
                                       ),
                                     ),
                                   ),
                                   Container(
                                     height: 200,
                                     child: Align(
                                       alignment: Alignment.topLeft,
                                       child: ListView.builder(
                                           itemCount: cart_items[index].inside_items.length,
                                           shrinkWrap: true,
                                           scrollDirection: Axis.horizontal,
                                           itemBuilder: (context, i) {
                                             return GestureDetector(
                                               child: Container(
                                                 height: 50,
                                                 width: 150,
                                                 child: Card(
                                                   elevation: 2,
                                                   shape: RoundedRectangleBorder(
                                                     borderRadius: BorderRadius.circular(2),
                                                   ),
                                                   child:  Container(
                                                       width: 150,
                                                       child:Stack(
                                                         children: [
                                                           Column(
                                                             crossAxisAlignment: CrossAxisAlignment.start,
                                                             mainAxisAlignment: MainAxisAlignment.start,
                                                             children: [
                                                               Padding(
                                                                 padding: EdgeInsets.all(2),
                                                                 child: Image.network(
                                                                   cart_items[index].inside_items[i].imagelink,
                                                                   fit: BoxFit.fitWidth,
                                                                   height: 110,
                                                                   width: 180,
                                                                 ),
                                                               ),
                                                               Padding(
                                                                 padding: const EdgeInsets.all(8),
                                                                 child: Text(cart_items[index].inside_items[i].name,style: TextStyle(fontSize: 11),),
                                                               ),
                                                               GestureDetector(
                                                                 child: Container(
                                                                   width: double.infinity,
                                                                   height: 30,
                                                                   color: Colors.green,
                                                                   child: Padding(padding: EdgeInsets.all(2),
                                                                     child: Center(child: Text('Mark as Collected',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),),),),
                                                                 ),
                                                                 onTap: (){
                                                                   markasCollected(cart_items[index].inside_items[i].id.toString());
                                                                 },
                                                               )

                                                             ],
                                                           ),
                                                           Align(alignment: Alignment.topRight,child:  Visibility(
                                                             child: GestureDetector(
                                                               child: Icon(Icons.delete ,color: Colors.blue,),
                                                               onTap: (){
                                                                 deleteItem(cart_items[index].inside_items[i].id.toString());
                                                               },
                                                             ),),
                                                           )
                                                         ],
                                                       )),
                                                 ),
                                               ),
                                               onTap: (){
                                                 Navigator.push(
                                                   context,
                                                   MaterialPageRoute(
                                                       builder: (context) => FlyerLoadScreen(cart_items[index].inside_items[i].flyer_id.toString(),cart_items[index].brandname,cart_items[index].inside_items[i].valid_from,
                                                           cart_items[index].inside_items[i].valid_to,cart_items[index].logo,true,cart_items[index].inside_items[i].id.toString())),
                                                 );
                                               },
                                             );
                                           }),
                                     ),
                                   )
                                 ],
                               ) ;
                             })
                       ],
                     ),
                   ),
                   visible: isShowClipItem,
                 ),
                 Visibility(
                   child: Container(
                     child:Column(
                       children: [
                         Padding(padding: EdgeInsets.all(8),
                           child: Align(
                             alignment: Alignment.topLeft,
                             child: Text('coupons',style: TextStyle(color: Colors.black),),
                           ),),
                         ListView.builder(
                             itemCount: copunitems_count,
                             physics: const NeverScrollableScrollPhysics(),
                             shrinkWrap: true,
                             scrollDirection: Axis.vertical,
                             itemBuilder: (context, index) {
                               return Column(
                                 children: [
                                   Container(
                                     width: double.infinity,
                                     color: Colors.grey[200],
                                     child :Align(
                                       alignment: Alignment.topLeft,
                                       child:  Padding(
                                         padding: EdgeInsets.all(5),
                                         child: Image.network(Urls.baseImageUrl+coupon_items[index].logo,height: 25,width: 25,),
                                       ),
                                     ),
                                   ),
                                   Container(
                                     height: 200,
                                     child: Align(
                                       alignment: Alignment.topLeft,
                                       child: ListView.builder(
                                           itemCount: coupon_items[index].inside_items.length,
                                           shrinkWrap: true,
                                           scrollDirection: Axis.horizontal,
                                           itemBuilder: (context, i) {
                                             return Container(
                                               height: 50,
                                               width: 150,
                                               child: Card(
                                                 elevation: 2,
                                                 shape: RoundedRectangleBorder(
                                                   borderRadius: BorderRadius.circular(2),
                                                 ),
                                                 child:  Container(
                                                     width: 150,
                                                     child:Stack(
                                                       children: [
                                                         Column(
                                                           crossAxisAlignment: CrossAxisAlignment.start,
                                                           mainAxisAlignment: MainAxisAlignment.start,
                                                           children: [
                                                             Padding(
                                                               padding: EdgeInsets.all(2),
                                                               child: Image.network(
                                                                 coupon_items[index].inside_items[i].imagelink,
                                                                 fit: BoxFit.fitWidth,
                                                                 height: 110,
                                                                 width: 180,
                                                               ),
                                                             ),
                                                             Padding(
                                                               padding: const EdgeInsets.all(8),
                                                               child: Text(coupon_items[index].inside_items[i].name,style: TextStyle(fontSize: 11),),
                                                             ),
                                                            GestureDetector(
                                                              child:  Container(
                                                                width: double.infinity,
                                                                height: 30,
                                                                color: Colors.green,
                                                                child: Padding(padding: EdgeInsets.all(2),
                                                                  child: Center(child: Text('Mark as Collected',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),),),),
                                                              ),
                                                              onTap: (){
                                                                markCopunAsCollected(coupon_items[index].inside_items[i].id);
                                                              },
                                                            )

                                                           ],
                                                         ),
                                                         Align(alignment: Alignment.topRight,child:  Visibility(
                                                           child: GestureDetector(
                                                             child: Icon(Icons.delete ,color: Colors.blue,),
                                                             onTap: (){
                                                               deleteCopuns(coupon_items[index].inside_items[i].id.toString());
                                                             },
                                                           ),),
                                                         )
                                                       ],
                                                     )),
                                               ),
                                             );
                                           }),
                                     ),
                                   )
                                 ],
                               ) ;
                             })
                       ],
                     ) ,
                   ),
                   visible: isShowCopounItem,
                 )
               ],
             )
         ) : SizedBox(
           height: MediaQuery.of(context).size.height,
           width: MediaQuery.of(context).size.width,
           child: Column(

             mainAxisAlignment: MainAxisAlignment.center,
             children: [
               Text("Your Shopping List is Empty"),
               Padding(
                 padding: EdgeInsets.only(top: 10),
                 child: Image.asset('assets/images/list_empty.png'),
               ),
             ],
           ),
         ),
       )
   );
  }
  
  void getCartItems()async{
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var response = await http.get(
        "${Urls.baseUrl}${Urls.GetCart}?locale="+lan,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print(response.body);
    if(response.statusCode == 200){
      dialog.dismissProgressDialog(context);
      try{
          var data = value['data'];
          var brands_array = data['brands'];
          for(int i=0 ; i< brands_array.length ; i++){
              var obj = brands_array[i];
              var items_array = obj['items'];
              var items_list = List<CartInsideModel>();
              for(int j = 0 ; j< items_array.length ; j++){
                var obj = items_array[j];
                items_list.add(CartInsideModel(obj['id'], obj['name'], obj['description'], Urls.baseImageUrl+obj['item_path'], obj['brand_id'], obj['flyer_id'], obj['valid_from'], obj['valid_to'], obj['flyer_slug']));
              }
              cart_items.add(CartModel(obj['id'],obj['name'],obj['logo'],items_list));
          }
          var shops_array = data['shops'];
          for(int i = 0; i < shops_array.length ; i++){
              var obj = shops_array[i];
              var items_array = obj['items'];
              var insidelist = List<CartInsideModel>();
              for(int k = 0 ;  k < items_array.length ; k++){
                 var obj = items_array[k];
                 insidelist.add(CartInsideModel(obj['id'], obj['name'], obj['description'], Urls.baseImageUrl+obj['item_path'], obj['brand_id'], obj['flyer_id'], obj['valid_from'], obj['valid_to'], obj['flyer_slug']));
              }
              cart_items.add(CartModel(obj['id'],obj['name'],obj['logo'],insidelist));
          }
          var copuns_array = data['coupons'];
         if(copuns_array.length > 0){
           var copuns_inside_list = List<CartInsideModel>();
           for(int i =0; i < copuns_array.length ; i++){
             var obj = copuns_array[i];
             copuns_inside_list.add(CartInsideModel(obj['id'], obj['title'], obj['description'], Urls.baseImageUrl+obj['image'],"", "", obj['valid_from'], obj['valid_to'], ''));
           }
           coupon_items.add(CartModel('','','',copuns_inside_list));
         }
          setState(() {
            list_count  = cart_items.length;
            copunitems_count = coupon_items.length;
            if(list_count == 0){
              isShowClipItem = false;
            }
            if(copunitems_count ==0){
              isShowCopounItem = false;
            }
            if(list_count == 0 && copunitems_count == 0){
              isShowlist = false;
            }else{
              isShowlist = true;
            }
            print(list_count.toString());
          });
      }catch(e){
        e.toString();
      }
    }else{
      dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(content: Text("Try Again",style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  void deleteItem(String id)async{
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var response = await http.post(
        "${Urls.baseUrl}${Urls.RemoveFlyerFromCart}?cartitem_id="+id,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print(response.body);
    if(response.statusCode == 200){
      dialog.dismissProgressDialog(context);
      try{
           if(value['success']){
             for(int i = 0 ; i < cart_items.length ; i++){
               for(int j = 0 ; j < cart_items[i].inside_items.length ; j++){
                 if(cart_items[i].inside_items[j].id.toString() == id){
                   setState(() {
                     cart_items[i].inside_items.removeAt(j);
                     if(cart_items[i].inside_items.length == 0){
                       cart_items.removeAt(i);
                     }
                     list_count = cart_items.length;
                     if(list_count == 0){
                       isShowClipItem = false;
                     }
                   });

                   break;
                 }
               }
               setState(() {
                 if(list_count == 0 && copunitems_count == 0){
                   isShowlist = false;
                 }else{
                   isShowlist = true;
                 }
               });
               final snackBar = SnackBar(content: Text("Item Removed From Your Cart",style: TextStyle(color: Colors.white)),backgroundColor: Colors.green,);
               _scaffoldKey.currentState.showSnackBar(snackBar);
             }
           }
      }catch(e){

      }
    }else{
      dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(content: Text("Try Again",style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }


  void deleteCopuns(String id)async{
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var response = await http.post(
        "${Urls.baseUrl}${Urls.clipUnclip}?coupon_id="+id,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print(response.body);
    if(response.statusCode == 200){
      dialog.dismissProgressDialog(context);
      try{
        if(value['success']){
          for(int i = 0 ; i < coupon_items.length ; i++){
            for(int j = 0 ; j < coupon_items[i].inside_items.length ; j++){
              if(coupon_items[i].inside_items[j].id.toString() == id){
                setState(() {
                  coupon_items[i].inside_items.removeAt(j);
                  if(coupon_items[i].inside_items.length == 0){
                    coupon_items.removeAt(i);
                  }
                  copunitems_count = coupon_items.length;
                  if(copunitems_count == 1){
                    isShowClipItem = false;
                  }
                });

                break;
              }
            }
            setState(() {
              if(list_count == 0 && copunitems_count == 0){
                isShowlist = false;
              }else{
                isShowlist = true;
              }
            });
            final snackBar = SnackBar(content: Text("Item Removed From Your Cart",style: TextStyle(color: Colors.white)),backgroundColor: Colors.green,);
            _scaffoldKey.currentState.showSnackBar(snackBar);
          }
        }
      }catch(e){

      }
    }else{
      dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(content: Text("Try Again",style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  void markasCollected(String id)async{
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var response = await http.post(
        "${Urls.baseUrl}${Urls.GetMarkItemCollected}?cartitem_id="+id,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print(response.body);
    if(response.statusCode == 200){
      dialog.dismissProgressDialog(context);
      try{
        if(value['success']){
          for(int i = 0 ; i < cart_items.length ; i++){
            for(int j = 0 ; j < cart_items[i].inside_items.length ; j++){
              if(cart_items[i].inside_items[j].id.toString() == id){
                setState(() {
                  cart_items[i].inside_items.removeAt(j);
                  if(cart_items[i].inside_items.length == 0){
                    cart_items.removeAt(i);
                  }
                  list_count = cart_items.length;
                  if(list_count == 0){
                    isShowClipItem = false;
                  }
                });
                final snackBar = SnackBar(content: Text("Item Marked",style: TextStyle(color: Colors.white)),backgroundColor: Colors.green,);
                _scaffoldKey.currentState.showSnackBar(snackBar);
                break;
              }
            }
            setState(() {
              if(list_count == 0 && copunitems_count == 0){
                isShowlist = false;
              }else{
                isShowlist = true;
              }
            });
          }
        }
      }catch(e){

      }
    }else{
      dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(content: Text("Try Again",style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  void markCopunAsCollected(String id)async{
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var response = await http.post(
        "${Urls.baseUrl}${Urls.GetMarkCouponCollected}?coupon_id="+id,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print(response.body);
    if(response.statusCode == 200){
      dialog.dismissProgressDialog(context);
      try{
        if(value['success']){
          for(int i = 0 ; i < coupon_items.length ; i++){
            for(int j = 0 ; j < coupon_items[i].inside_items.length ; j++){
              if(coupon_items[i].inside_items[j].id.toString() == id){
                setState(() {
                  coupon_items[i].inside_items.removeAt(j);
                  if(coupon_items[i].inside_items.length == 0){
                    coupon_items.removeAt(i);
                  }
                  copunitems_count = coupon_items.length;
                  if(copunitems_count == 1){
                    isShowClipItem = false;
                  }
                });
                final snackBar = SnackBar(content: Text("Item Marked",style: TextStyle(color: Colors.white)),backgroundColor: Colors.green,);
                _scaffoldKey.currentState.showSnackBar(snackBar);
                break;
              }
            }
          }
          setState(() {
            if(list_count == 0 && copunitems_count == 0){
              isShowlist = false;
            }else{
              isShowlist = true;
            }
          });
        }
      }catch(e){

      }
    }else{
      dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(content: Text("Try Again",style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }
}
