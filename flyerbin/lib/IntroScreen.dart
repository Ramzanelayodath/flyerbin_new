import 'package:flutter/material.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';

import 'Loginscreen.dart';


class IntroScreen extends StatefulWidget {
  @override
  _IntroScreenState createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  List<Slide> slides = new List();

  @override
  void initState() {
    super.initState();

    slides.add(
      new Slide(
        title: "",
        styleTitle: TextStyle(
            color: Colors.black, fontSize: 17, fontWeight: FontWeight.bold),
        description: "",
        styleDescription: TextStyle(color: Colors.black),
        pathImage: "assets/images/intro1.png",
        widthImage: 600,
        heightImage: 500,
        foregroundImageFit: BoxFit.contain,
        marginTitle: EdgeInsets.all(15),
        backgroundColor: Color(0xffffffff),
      ),
    );
    slides.add(
      new Slide(
        title: "",
        styleTitle: TextStyle(
            color: Colors.black, fontSize: 17, fontWeight: FontWeight.bold),
        description: "",
        styleDescription: TextStyle(color: Colors.black),
        pathImage: "assets/images/intro2.png",
        widthImage: 600,
        heightImage: 500,
        foregroundImageFit: BoxFit.contain,
        marginTitle: EdgeInsets.all(15),
        backgroundColor: Color(0xffffffff),
      ),
    );
    slides.add(
      new Slide(
        title: "",
        styleTitle: TextStyle(
            color: Colors.black, fontSize: 17, fontWeight: FontWeight.bold),
        description: "",
        styleDescription: TextStyle(color: Colors.black),
        pathImage: "assets/images/intro3.png",
        widthImage: 600,
        heightImage: 500,
        foregroundImageFit: BoxFit.contain,
        marginTitle: EdgeInsets.all(15),
        backgroundColor: Color(0xffffffff),
      ),
    );
  }

  void onDonePress() {
    //SharedPrefrence().setShowIntro(true);
    Navigator.pop(context);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => LoginScreen(),
      ),
    );
  }

  void onSkipPress() {}

  @override
  Widget build(BuildContext context) {
    return new IntroSlider(
      slides: this.slides,
      onSkipPress: this.onSkipPress,
      onDonePress: this.onDonePress,
      styleNamePrevBtn: TextStyle(color: Colors.black),
      styleNameDoneBtn: TextStyle(color: Colors.black),
    );
  }
}
