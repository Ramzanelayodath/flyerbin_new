import 'dart:async';
import 'dart:convert';

import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';
import 'Model/NearByMapModel.dart';
import 'NearbyListItemDetailsScreen.dart';
import 'Utils/Constants.dart';
import 'Utils/SharedPrefrence.dart';
import 'Utils/Urls.dart';

class DashboardNearByScreen extends StatefulWidget {
  @override
  _DashboardNearByScreenState createState() => _DashboardNearByScreenState();
}

class _DashboardNearByScreenState extends State<DashboardNearByScreen> {
  bool map_view_visibility = false;
  bool maplist_view_visibility = true;

  double latti = 25.2048493, long = 55.2707828;
  Completer<GoogleMapController> _controller = Completer();
  Iterable markers = [];

  final List<NearByMapModel> mapLists = List();
  TextEditingController searchController = TextEditingController();
  String userToken,lan = 'en';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future lng = SharedPrefrence().getLanguage();
    lng.then((data) async {
      lan = data;
    });
    Future token = SharedPrefrence().getToken();
    token.then((data) async {
      userToken = data;
      Mapdata();

      //  getTab();
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.white,
          title: _SearchText(),
          bottom: TabBar(
              indicatorColor: Colors.lightBlue[800],
              labelColor: Colors.lightBlue[800],
              unselectedLabelColor: Colors.black,
              tabs: [
                Tab(
                  icon: null,
                  text: "LIST",
                ),
                Tab(
                  icon: null,
                  text: "MAP",
                ),
              ]),
        ),
        body: DoubleBackToCloseApp(
    snackBar: const SnackBar(
    content: Text('Tap back again to exit'),
    ),child: TabBarView(physics: NeverScrollableScrollPhysics(), children: [
          ListViewData(),
          MapViewData(),
        ]),
      ),
    ),);
  }

  Widget _SearchText() {
    return Container(
      width: 400,
      height: 35,
      child: TextField(
        onChanged: (value) {
          setState(() {});
        },
        controller: searchController,
        autofocus: false,
        decoration: InputDecoration(
            prefixIcon: Icon(Icons.search),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(25.0),
              ),
            ),
            hintText: "Search...",
            contentPadding: EdgeInsets.all(5),
            hintStyle: TextStyle(color: Colors.blueGrey, fontSize: 14),
            filled: true,
            fillColor: Colors.white),
      ),
    );
  }

  Widget MapViewData() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: GoogleMap(
        markers: Set.from(markers),
        initialCameraPosition:
            CameraPosition(target: LatLng(latti, long), tilt: 30, zoom: 14),
        mapType: MapType.normal,
        onMapCreated: (GoogleMapController controller) {
          _controller = controller as Completer<GoogleMapController>;
        },
      ),
    );
  }

  Widget ListViewData() {
    return Container(
        //children: <Widget>[
        child:  mapLists.length != 0 ?ListView.builder(
            itemCount: mapLists.length,
            shrinkWrap: true,
            itemBuilder: (BuildContext context, int index) {
              NearByMapModel data = mapLists[index];
              if (searchController.text.isEmpty) {
                return Card(
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(5),
                      side: BorderSide(color: Colors.white)),
                  child: Container(
                    height: 98,
                    margin: EdgeInsets.all(5),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => NearbyListItemDetailsScreen(
                                data.id.toString(), data.mall_id.toString(),data.name.toString()),
                          ),
                        );
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Expanded(
                                  flex: 0,
                                  child: Image.network(
                                    data.original_logo.toString(),
                                    height: 70,
                                    width: 70,
                                    fit: BoxFit.fill,
                                  )),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                MainAxisAlignment.spaceEvenly,
                                children: [
                                  Expanded(
                                      flex: 0,
                                      child: Container(
                                        width: 200,
                                        child: Text(
                                          data.name,
                                          style: TextStyle(
                                              fontSize: 13,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      )),
                                  Expanded(
                                      flex: 0,
                                      child: SizedBox(
                                        height: 6,
                                      )),
                                  Expanded(
                                      flex: 0,
                                      child: Container(
                                          width: 230,
                                          child: Text(data.address,softWrap: true,style: TextStyle(fontSize: 12.8),))),
                                ],
                              ),
                              GestureDetector(
                                onTap: () {
                                  openMap(
                                      double.parse(data.latitude),
                                      double.parse(data.longitude),
                                      Uri.encodeFull(data.name));
                                },
                                child: Icon(
                                  Icons.location_on,
                                  color: Colors.blueAccent,
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              } else if ((data.name
                      .toLowerCase()
                      .contains(searchController.text) ||
                  data.address
                      .toLowerCase()
                      .contains(searchController.text))) {
                return Card(
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(5),
                      side: BorderSide(color: Colors.white)),
                  child: Container(
                    height: 98,
                    margin: EdgeInsets.all(5),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => NearbyListItemDetailsScreen(
                                data.id.toString(), data.mall_id.toString(),data.name.toString()),
                          ),
                        );
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Expanded(
                                  flex: 0,
                                  child: Image.network(
                                    data.original_logo.toString(),
                                    height: 70,
                                    width: 70,
                                    fit: BoxFit.fill,
                                  )),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Expanded(
                                      flex: 0,
                                      child: Text(
                                        data.name,
                                        style: TextStyle(
                                            fontSize: 13,
                                            fontWeight: FontWeight.bold),
                                      )),
                                  Expanded(
                                      flex: 0,
                                      child: SizedBox(
                                        height: 6,
                                      )),
                                  Expanded(
                                      flex: 0,
                                      child: Container(
                                          width: 230,
                                          child: Text(data.address,softWrap: true,style: TextStyle(fontSize: 12.8),))),
                                ],
                              ),
                              GestureDetector(
                                onTap: () {
                                  openMap(
                                      double.parse(data.latitude),
                                      double.parse(data.longitude),
                                      Uri.encodeFull(data.name));
                                },
                                child: Icon(
                                  Icons.location_on,
                                  color: Colors.blueAccent,
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              } else {
                return Container();
              }
            }):Center(child: CircularProgressIndicator(),)

        //],
        );
  }

  Future<void> Mapdata() async {
    var response = await http.get(
        "${Urls.baseUrl}${Urls.GetMapData}?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=100&locale="+lan+"&page=1",
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print("resp_foodcoupn " + response.body);
    if (response.statusCode == 200) {
      try {
        var data = value['data'];
        var array = data['shops'];
        for (int i = 0; i < array.length; i++) {
          var obj = array[i];
          // print("Image link " + obj['name']);
          mapLists.add(NearByMapModel(
            obj['id'],
            obj['owner_id'],
            obj['brand_id'],
            obj['region_id'],
            obj['country_id'],
            obj['mall_id'],
            obj['name'],
            Urls.baseImageUrl + obj['logo'],
            Urls.baseImageUrl + obj['original_logo'],
            obj['address'],
            obj['latitude'],
            obj['longitude'],
          ));

          Iterable _markers = Iterable.generate(mapLists.length, (index) {
            Map result = array[index];
            latti = double.parse(obj["latitude"]);
            long = double.parse(obj["longitude"]);
            LatLng latLngMarker = LatLng(double.parse(result["latitude"]),
                double.parse(result["longitude"]));

            return Marker(
                markerId: MarkerId("marker$index"),
                position: latLngMarker,
                infoWindow: InfoWindow(
                    title: result["name"], snippet: result["address"]));
          });
          setState(() {
            mapLists.length;
            markers = _markers;
            this.latti = latti;
            this.long = long;
          });
        }
      } catch (e) {
        e.toString();
      }
    }
  }

  static Future<void> openMap(
      double latitude, double longitude, String name) async {
    String googleUrl =
        Urls.GoogleMapNavigationUrl + '$latitude,$longitude?q=$name';
    if (await canLaunch(googleUrl)) {
      await launch(googleUrl);
    } else {
      throw 'Could not open the map.';
    }
  }
}
