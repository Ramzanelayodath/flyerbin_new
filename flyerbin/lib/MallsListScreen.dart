

import 'dart:convert';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'Model/NearByMapModel.dart';
import 'NearbyListItemDetailsScreen.dart';
import 'Utils/CustomDialogs.dart';
import 'Utils/SharedPrefrence.dart';
import 'Utils/Urls.dart';

class MallsListScreen extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new MallsListScreenState();
  }

}

class MallsListScreenState extends State<MallsListScreen>{
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 15.0);
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String otp,lan = 'en';
  String user_token,default_latitude,default_longitude;
  final List<NearByMapModel> mapLists = List();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future lng = SharedPrefrence().getLanguage();
    lng.then((data) async {
      lan = data;
    });
    Future token = SharedPrefrence().getToken();
    Future latitude = SharedPrefrence().getLatitude();
    Future longitude = SharedPrefrence().getLongitude();
    token.then((data) async {
      latitude.then((lat_data)async{
        longitude.then((lon_data)async{
          user_token = data;
          default_latitude = lat_data;
          default_longitude = lon_data;
          Mapdata();
        });
      });

      //  getTab();
    });
    //_webViewHandler();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          "Malls",
          style: TextStyle(fontSize: 15, color: Colors.lightBlue),
        ),
        automaticallyImplyLeading: true,
        iconTheme: IconThemeData(
          color: Colors.lightBlue,
        ),
        backgroundColor: Colors.white,
      ),
      body:
          Container(
            //color: Colors.white,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child:
                  mapLists.length != 0 ?ListView.builder(
                      itemCount: mapLists.length,
                      shrinkWrap: true,
                      itemBuilder: (BuildContext context, int index) {
                        NearByMapModel data = mapLists[index];
                          return Card(
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(5),
                                side: BorderSide(color: Colors.white)),
                            child: Container(
                              height: 98,
                              margin: EdgeInsets.all(5),
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => NearbyListItemDetailsScreen(
                                          data.id.toString(), data.mall_id.toString(),data.name.toString()),
                                    ),
                                  );
                                },
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        Expanded(
                                            flex: 0,
                                            child: Image.network(
                                              data.logo.toString(),
                                              height: 70,
                                              width: 70,
                                              fit: BoxFit.fill,
                                            )),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                          children: [
                                            Expanded(
                                                flex: 0,
                                                child: Container(
                                                  width: 200,
                                                  child: Text(
                                                    data.name,
                                                    style: TextStyle(
                                                        fontSize: 13,
                                                        fontWeight: FontWeight.bold),
                                                  ),
                                                )),
                                            Expanded(
                                                flex: 0,
                                                child: SizedBox(
                                                  height: 6,
                                                )),
                                            Expanded(
                                                flex: 0,
                                                child: Container(
                                                    width: 230,
                                                    child: Text(data.address,softWrap: true,style: TextStyle(fontSize: 12.8),))),
                                          ],
                                        ),
                                        GestureDetector(
                                          onTap: () {

                                          },
                                          child: Text(" ")
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );

                      }):Center(child: CircularProgressIndicator(),)
              ),
            ),


    );
  }

  Future<void> Mapdata() async {
    var response = await http.get(
        "${Urls.baseUrl}${Urls.GetMalls}?latitude=${default_latitude}&longitude=${default_longitude}&radius=100&locale="+lan,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${user_token}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print("resp mall " + response.body);
    if (response.statusCode == 200) {
      try {
        var data = value['data'];
        //var array = data['shops'];
        for (int i = 0; i < data.length; i++) {
          var obj = data[i];
          // print("Image link " + obj['name']);
          mapLists.add(NearByMapModel(
            obj['id'],
            "",
            "",
            "",
            obj['country_id'],
            obj['id'],
            obj['name'],
            Urls.baseImageUrl + obj['thumbs']['lg'],
            "",
            obj['address'],
            obj['latitude'],
            obj['longitude'],
          ));
          setState(() {
            mapLists.length;
          });
        }
      } catch (e) {
        e.toString();
      }
    }
  }

}