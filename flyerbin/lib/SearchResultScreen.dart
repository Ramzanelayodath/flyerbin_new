import 'dart:convert';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher.dart';
import 'Cart.dart';
import 'FlyerLoadScreen.dart';
import 'Model/PrefrenceModel.dart';
import 'SimiliarProductScreen.dart';
import 'Utils/Constants.dart';
import 'Utils/CustomDialogs.dart';
import 'Utils/SharedPrefrence.dart';
import 'Utils/Urls.dart';
import 'package:share/share.dart';

class SearchResultScreen extends StatefulWidget {
  String slug, name;

  SearchResultScreen(this.slug, this.name);

  @override
  State<StatefulWidget> createState() {
    return state(slug, name);
  }
}

class state extends State<SearchResultScreen> {
  String slug, name, related_image = "", related_image_2 = "",lan = 'en';
  bool show_similiar_prdouct = false;
  var page = 1, last_page = 1, list_count = 0;
  List<PrefrenceModel> search_result_list = [];

  state(this.slug, this.name);

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  var title = "", userToken = "";

  ScrollController _sccontroller = new ScrollController();
  bool isLoading = true;
  @override
  void initState() {
    super.initState();
    Future lng = SharedPrefrence().getLanguage();
    lng.then((data) async {
      lan = data;
    });
    Future token = SharedPrefrence().getToken();
    token.then((data) async {
      userToken = data;
      this.getSearchResult(page);
      _sccontroller.addListener(() {
        if (_sccontroller.position.pixels ==
            _sccontroller.position.maxScrollExtent) {
          if(page==0){

          }
          else{
            getSearchResult(page);
          }

        }
      });
    });
    setState(() {
      title = name;
    });

  }

  @override
  void dispose() {
    _sccontroller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // TODO: implement build
    return Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          centerTitle: false,
          iconTheme: new IconThemeData(color: Color(0xff00ADEE)),
          title: Text(
            title,
            style: TextStyle(color: Color(0xff00ADEE), fontSize: 13),
          ),
          backgroundColor: Colors.white,
          actions: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: GestureDetector(
                child: Icon(
                  Icons.shopping_cart,
                  color: Color(0xff00ADEE),
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Cart(),
                    ),
                  );
                },
              ),
            )
          ],
        ),
        body:isLoading?/*Center(child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("No Products to show...!"),
        ))*/Shimmer.fromColors(
          baseColor: Colors.grey[200],
          highlightColor: Colors.grey,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Container(
              height: 250,
              width: 200,
              //width: 30,
              //height: 30,
              decoration: BoxDecoration(
                color: Color(0xFFFFF29A),
                shape: BoxShape.rectangle,
                /*borderRadius: BorderRadius.all(
                                Radius.circular(15),
                              ),*/
              ),

            ),
          ),
        ):search_result_list.length==0?Center(child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("No Products to show...!"),
        )): GridView.builder(
          controller: _sccontroller,
          itemCount: search_result_list.length,
          physics: const AlwaysScrollableScrollPhysics(),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(childAspectRatio: 5.4/7.5, crossAxisCount: 2,),
          itemBuilder: (BuildContext context, int index) {
            print(search_result_list.length.toString()+" length");
           /* if(search_result_list.length==0){
              return Center(child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("No Products to show...!"),
            ));
          }
            else*/ if (index == search_result_list.length) {
              if(page==0){
                return Center(child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(" "),
                ));
              }
              else{
                return Shimmer.fromColors(
                  baseColor: Colors.grey[200],
                  highlightColor: Colors.grey,
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Container(
                      height: size.height * 0.5,
                      //width: 30,
                      //height: 30,
                      decoration: BoxDecoration(
                        color: Color(0xFFFFF29A),
                        shape: BoxShape.rectangle,
                        /*borderRadius: BorderRadius.all(
                                Radius.circular(15),
                              ),*/
                      ),

                    ),
                  ),
                );//_buildProgressIndicator();
              }

            } else {
              return Container(
                child: Padding(
                  padding: const EdgeInsets.all(5),
                  child: GestureDetector(
                    onTap: () {
                      ClipFoodCoupnAlert(context, search_result_list[index]);
                    },
                    child: Container(
                      // height: 700,
                      child: Card(
                        elevation: 2,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(2),
                        ),
                        child: Container(
                            width: 300,
                            // height: 400,
                            child: Stack(
                              children: [
                                Column(
                                  // mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.all(2),
                                      child: Image.network(
                                        Urls.baseImageUrl +
                                            search_result_list[index]
                                                .thumb_path,
                                        fit: BoxFit.fill,
                                        height: 180,
                                        width: 180,
                                      ),
                                    ),
                                    Padding(
                                        padding: const EdgeInsets.all(1),
                                        child: Text(
                                          search_result_list[index].name,
                                          // maxLines: 2,
                                          style: TextStyle(
                                              fontSize: 11,
                                              fontWeight: FontWeight.bold),

                                        )),
                                    Padding(
                                        padding: const EdgeInsets.only(left: 3),
                                        child: Align(
                                          child: Text(
                                            search_result_list[index].price +
                                                " AED",
                                            style: TextStyle(
                                                fontSize: 12.5,
                                                fontWeight: FontWeight.bold,
                                                color: Color(0xff00ADEE)),
                                            textAlign: TextAlign.left,
                                          ),
                                          alignment: Alignment.centerLeft,
                                        )),
                                  ],
                                ),
                                Align(
                                  alignment: Alignment.topRight,
                                  child: Visibility(
                                    child: Text(
                                      'Clipped',
                                      style: TextStyle(
                                          backgroundColor: Colors.green,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white),
                                    ),
                                    visible:
                                    search_result_list[index].isClipped,
                                  ),
                                )
                              ],
                            )),
                      ),
                    ),
                  ),
                ),
              );
            }
          },
        ));
  }
  Widget _buildProgressIndicator() {
    return new Padding(
      padding: const EdgeInsets.all(8.0),
      child: new Center(
        child: new Opacity(
          opacity: isLoading ? 1.0 : 00,
          child: new CircularProgressIndicator(),
        ),
      ),
    );
  }
  void getSearchResult(int index) async {
    print("size" + index.toString());
   /* if (!isLoading&&page==1) {
      setState(() {
        isLoading = true;
      });*/
      // ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
      List<PrefrenceModel> tList = List();
      print( "${Urls.baseUrl}${Urls.CategorySearch}?latitude=${Constants
          .latitude}&longitude=${Constants
          .longitude}&radius=100&locale="+lan+"&keyword=${slug}&page=${index
          .toString()}&type=item");
      var response = await http.get(
        "${Urls.baseUrl}${Urls.CategorySearch}?latitude=${Constants
            .latitude}&longitude=${Constants
            .longitude}&radius=100&locale="+lan+"&keyword=${slug}&page=${index
            .toString()}&type=item",
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        },
      );

      Map<String, dynamic> value = json.decode(response.body);
      if (response.statusCode == 200) {
        // dialog.dismissProgressDialog(context);
        try {
          print("response search " + response.body.toString());
          Map<String, dynamic> value = json.decode(response.body);

          var status = value['success'];
          var message = value['message'];
          print(message);
          if (status == true) {
            // search_result_list.clear();
            try {
              var data = value['data']['data'];
              if (data.length > 0) {
                for (int i = 0; i < data.length; i++) {
                  var obj = data[i];
                  //var petrol_obj = obj['petrolstation'];
                  search_result_list.add(PrefrenceModel(
                    obj['id'].toString(),
                    obj['description'].toString(),
                    obj['valid_from'].toString(),
                    obj['valid_to'].toString(),
                    obj['thumbs']['md'].toString(),
                    obj['status'].toString(),
                    "",
                    "",
                    obj['name'].toString(),
                    "",
                    obj['flyer_slug'].toString(),
                    obj['type'].toString(),
                    obj['isClipped'],
                    obj['url'].toString(),
                    obj['price'].toString(),
                    obj['flyer_page_id'].toString(),
                    obj['flyer_id'].toString(),
                    obj['shop_name'].toString(),
                    obj['shop_logo'].toString(),
                    obj['brand_name'].toString(),
                    obj['brand_logo'].toString(),
                  ));
                }
                setState(() {
                  isLoading = false;
                  last_page = value['data']['last_page'];
                  if(last_page==1){
                    list_count = search_result_list.length;
                    isLoading = false;
                    search_result_list.addAll(tList);
                    print(search_result_list.length);
                    page=0;
                  }
                  else if(page!=last_page&&page!=0){
                    list_count = search_result_list.length;
                    isLoading = false;
                    search_result_list.addAll(tList);
                    print(search_result_list.length);
                    page++;
                  }
                  else if(page==last_page&&page!=0){
                    list_count = search_result_list.length;
                    isLoading = false;
                    search_result_list.addAll(tList);
                    print(search_result_list.length);
                    page=0;
                  }
                  else{
                    page=0;
                  }

                });
              } else {
                /*final snackBar = SnackBar(content: Text("No Data Available"));
                _scaffoldKey.currentState.showSnackBar(snackBar);*/
                setState(() {
                  isLoading=false;
                });

              }
            } catch (e) {
              e.toString();
            }
          } else {
            print("Error...");
            final snackBar = SnackBar(content: Text(message));
            _scaffoldKey.currentState.showSnackBar(snackBar);
          }
        } catch (e) {
          print(e.toString());
        }
      } else if (response.statusCode == 404) {
        // dialog.dismissProgressDialog(context);
        var message = value['message'];
        final snackBar = SnackBar(content: Text(message));
        _scaffoldKey.currentState.showSnackBar(snackBar);
      } else if (response.statusCode == 208) {
        // dialog.dismissProgressDialog(context);
      } else {
        var message = value['message'];
        // dialog.dismissProgressDialog(context);
        final snackBar = SnackBar(content: Text(message));
        _scaffoldKey.currentState.showSnackBar(snackBar);
      }
    /*}
    else
      {
        setState(() { isLoading = false; });
      }*/
  }

  /*void getTrendingItems(String slug,String page)async{
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var brand_or_shop_name = "";
    var brand_or_shop_id = "";
    var response = await http.get(
        "${Urls.baseUrl}${Urls.TrendindItems}/${slug}?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=100&locale=en&page="+page,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print("resp_trednding_delas "+response.body);
    if(response.statusCode == 200){
      dialog.dismissProgressDialog(context);
      try{
        var data = value['data'];
        last_page = data['last_page'];
        var array = data['data'];
        for(int i = 0 ; i< array.length ; i++){
          var obj = array[i];
          if(obj['brand_id'] == null){
            brand_or_shop_id = obj['shop_id'].toString();
            brand_or_shop_name = obj['shop_name'];
          }else{
            brand_or_shop_id = obj['brand_id'].toString();
            brand_or_shop_name = obj['brand_name'];
          }
          var thumb = obj['thumbs'];
          list.add(OffersInEachItemModel(obj['id'],obj['flyer_page_id'],obj['category_id'],obj['type'].toString(),
              obj['name'],obj['description'],obj['url'],obj['sku_no'],obj['sales_details'],
              obj['offer_details'],obj['valid_from'],obj['valid_to'],obj['offer_description'],
              obj['item_path'],obj['flyer_id'],obj['flyer_slug'],brand_or_shop_id,brand_or_shop_name,obj['isClipped'],obj['brand_logo'],obj['price']));
        }
        setState(() {
          list_count = list.length;
        });
      }catch(e){
        e.toString();
      }
    }else{
      dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(content: Text("Try Again",style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }*/

  Future<void> ClipFoodCoupnAlert(
      BuildContext context, PrefrenceModel data) async {
    setState(() {
      getSimiliarProduct(data.id.toString());
    });

    await showGeneralDialog(
        context: context,
        barrierDismissible: true,
        barrierLabel:
        MaterialLocalizations.of(context).modalBarrierDismissLabel,
        barrierColor: Colors.black45,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (BuildContext buildContext, Animation animation,
            Animation secondaryAnimation) {
          return StatefulBuilder(builder: (context, setState) {
            return Center(
              child: Card(
                  elevation: 6,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        // width: MediaQuery.of(context).size.width - 10,
                        // height: MediaQuery.of(context).size.height -  110,
                        padding: EdgeInsets.all(0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.white,
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(10),
                                  child: Container(
                                    child: Row(
                                      children: [
                                        Image.network(
                                          data.shop_logo == "null"
                                              ? Urls.baseImageUrl +
                                              data.brand_logo
                                              : Urls.baseImageUrl +
                                              data.shop_logo,
                                          fit: BoxFit.fill,
                                          width: 40,
                                          height: 40,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(5),
                                  child: GestureDetector(
                                      onTap: () {
                                        Navigator.pop(context);
                                      },
                                      child: Icon(
                                        Icons.cancel,
                                        color: Colors.blueGrey,
                                      )),
                                )
                              ],
                            ),
                            Align(
                              alignment: Alignment.topRight,
                              child: Padding(
                                padding: EdgeInsets.only(right: 8, top: 8),
                                child: Text('Valid To : ' + data.valid_to),
                              ),
                            ),
                            Divider(),
                            Align(
                              child: Padding(
                                padding: EdgeInsets.all(8),
                                child: Container(
                                  height: 250,
                                  width: 300,
                                  child: Image.network(
                                    Urls.baseImageUrl + data.thumb_path,
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                              alignment: Alignment.center,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                data.name,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 22),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Text(
                                data.price.toString() + " AED",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.lightBlue,
                                    fontSize: 25),
                              ),
                            ),
                            Visibility(
                              child: Padding(padding: EdgeInsets.all(8),child:  Row(
                                children: [
                                  Image.network(related_image,width: 50,height: 50,),
                                  Padding(padding: EdgeInsets.only(left: 5,right: 5),
                                    child: Image.network(related_image_2,width: 50,height: 50,),),
                                  Column(
                                    children: [
                                      Padding(padding: EdgeInsets.all(5),
                                        child:GestureDetector(
                                          child:Text('See More >>',style: TextStyle(color: Colors.blue,fontWeight: FontWeight.bold),),
                                          onTap: (){
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) => SimiliarProductScreen(data.id.toString()),
                                              ),
                                            );
                                          },),
                                      ),
                                      GestureDetector(
                                        child: Container(
                                            padding: EdgeInsets.all(3),
                                            decoration: BoxDecoration(border: Border.all(color: Colors.green,),
                                                borderRadius: BorderRadius.all(Radius.circular(20)),color: Colors.green),
                                            child: Padding(padding: EdgeInsets.all(2),
                                              child: Row(
                                                children: [
                                                  Text('View Flyer',style: TextStyle(color: Colors.white,fontSize: 12),)
                                                ],
                                              ),)
                                        ) ,
                                        onTap: (){
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) => FlyerLoadScreen(
                                                  data.flyer_id.toString(),
                                                  data.shop_name.toString(),
                                                  data.valid_from.toString(),
                                                  data.valid_to.toString(),
                                                  data.brand_logo.toString(),
                                                  true,
                                                  data.id.toString()),
                                            ),
                                          );
                                        },
                                      )

                                    ],
                                  ),
                                ],
                              ),),
                              visible: true,
                            ),
                            Container(
                              width: double.infinity,
                              height: 55,
                              child: Row(
                                children: [
                                  Expanded(
                                    flex: 1,
                                    child: Padding(
                                      padding: EdgeInsets.only(right: 2),
                                      child: GestureDetector(
                                        child: Container(
                                          width: double.infinity,
                                          height: 55,
                                          color: Colors.blue,
                                          child: Center(
                                            child: Row(
                                              children: [
                                                Expanded(
                                                  flex: 1,
                                                  child: Icon(
                                                    Icons.ios_share,
                                                    color: Colors.white,
                                                    size: 20,
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 1,
                                                  child: Text(
                                                    'Share',
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontWeight:
                                                        FontWeight.bold,
                                                        fontSize: 16),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                        onTap: () {
                                          share(data);
                                        },
                                      ),
                                    ),
                                  ),
                                  Visibility(
                                    visible: data.type == "offline" ||
                                        data.type == "both",
                                    child: Expanded(
                                      flex: 1,
                                      child: Padding(
                                        padding: EdgeInsets.only(right: 2),
                                        child: GestureDetector(
                                          child: Container(
                                              width: double.infinity,
                                              height: 55,
                                              color: data.isClipped?Colors.red:Colors.blue,
                                              child: Center(
                                                child: Row(
                                                  children: [
                                                    Expanded(
                                                      flex: 1,
                                                      child: Icon(
                                                        Icons
                                                            .shopping_bag_outlined,
                                                        color: Colors.white,
                                                        size: 20,
                                                      ),
                                                    ),
                                                    Expanded(
                                                      flex: 1,
                                                      child: Text(
                                                        data.isClipped
                                                            ? 'Unclip'
                                                            : 'Clip',
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontWeight:
                                                            FontWeight.bold,
                                                            fontSize: 16),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              )),
                                          onTap: () {
                                            doclipUnclip(data);
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                  Visibility(
                                      visible: data.type == "both",
                                      child: Expanded(
                                        flex: 1,
                                        child: Padding(
                                          padding: EdgeInsets.only(right: 2),
                                          child: GestureDetector(
                                            child: Container(
                                                width: double.infinity,
                                                height: 55,
                                                color: Colors.blue,
                                                child: Center(
                                                  child: Row(
                                                    children: [
                                                      Expanded(
                                                        flex: 1,
                                                        child: Icon(
                                                          Icons.shopping_basket,
                                                          color: Colors.white,
                                                          size: 20,
                                                        ),
                                                      ),
                                                      Expanded(
                                                        flex: 1,
                                                        child: Text(
                                                          'Buy Now',
                                                          style: TextStyle(
                                                              color:
                                                              Colors.white,
                                                              fontWeight:
                                                              FontWeight
                                                                  .bold,
                                                              fontSize: 16),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                )),
                                            onTap: () {
                                              openUrl(data.url);
                                            },
                                          ),
                                        ),
                                      )),
                                  Visibility(
                                    visible: data.type == "link",
                                    child: Expanded(
                                      flex: 1,
                                      child: Padding(
                                        padding: EdgeInsets.only(right: 2),
                                        child: GestureDetector(
                                          child: Container(
                                              width: double.infinity,
                                              height: 55,
                                              color: Colors.blue,
                                              child: Center(
                                                child: Row(
                                                  children: [
                                                    Expanded(
                                                      flex: 1,
                                                      child: Icon(
                                                        Icons.public,
                                                        color: Colors.white,
                                                        size: 20,
                                                      ),
                                                    ),
                                                    Expanded(
                                                      flex: 1,
                                                      child: Text(
                                                        'Visit',
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontWeight:
                                                            FontWeight.bold,
                                                            fontSize: 16),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              )),
                                          onTap: () {
                                            openUrl(data.url);
                                          },
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  )),
            );
          });
        });
  }

  void doclipUnclip(PrefrenceModel model) async {
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var response = await http.post(
        "${Urls.baseUrl}${Urls.RemoveFlyerFromCart}?cartitem_id=" +
            model.id.toString(),
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    if (response.statusCode == 200) {
      dialog.dismissProgressDialog(context);
      print("resp_copun_clip " + response.body);
      var data = value['data'];
      if (data['isClipped']) {
        Future token = SharedPrefrence().getCartCount();
        token.then((data) async {
          SharedPrefrence().setCartCount(data + 1);
        });
        setState(() {
          model.isClipped = true;
        });
        Navigator.pop(context);
        final snackBar = SnackBar(
          content: Text("Clipped", style: TextStyle(color: Colors.white)),
          backgroundColor: Colors.green,duration: Duration(milliseconds: 100),
        );
        _scaffoldKey.currentState.showSnackBar(snackBar);
      } else {
        Future token = SharedPrefrence().getCartCount();
        token.then((data) async {
          if (data > 0) {
            SharedPrefrence().setCartCount(data - 1);
          }
        });
        setState(() {
          model.isClipped = false;
        });
        Navigator.pop(context);
        final snackBar = SnackBar(
          content: Text("Un clipped", style: TextStyle(color: Colors.white)),
          backgroundColor: Colors.green,duration: Duration(milliseconds: 100),
        );
        _scaffoldKey.currentState.showSnackBar(snackBar);
      }
    } else {
      dialog.dismissProgressDialog(context);
      print(response.body);
    }
  }

  void share(PrefrenceModel model) {
    var shareMessage =
        Urls.baseurl + "flyer/" + model.slug + "?item=" + model.id.toString();
    Share.share(shareMessage, subject: 'Flyerbin');
  }

  void openUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void getSimiliarProduct(String id) async {
    var response = await http.get(
        "${Urls.baseUrl}${Urls.SimiliarProducts}?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=100&&count=20&locale="+lan+"&page=1&id=" +
            id,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print("resp_similiar " + response.body);
    if (response.statusCode == 200) {
      try {
        var array = value['data'];
        if (array.length > 0) {
          setState(() {
            show_similiar_prdouct = true;
            related_image = Urls.baseImageUrl + array[0]['item_path'];
            related_image_2 = Urls.baseImageUrl + array[1]['item_path'];
          });
        } else {
          setState(() {
            show_similiar_prdouct = false;
          });
        }
      } catch (e) {
        e.toString();
      }
    }
  }
}
