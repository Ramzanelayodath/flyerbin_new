import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:apple_sign_in/apple_sign_in.dart';
import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:flutter_svg/svg.dart';

import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

import 'ChangeShopPreference.dart';
import 'LoginscreenMobile.dart';
import 'OTPScreen.dart';
import 'Utils/CustomDialogs.dart';
import 'Utils/SharedPrefrence.dart';
import 'Utils/Urls.dart';

GoogleSignIn _googleSignIn = GoogleSignIn(
  scopes: <String>[
    'email',
    'https://www.googleapis.com/auth/contacts.readonly',
  ],
);


class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 15.0);
  static final FacebookLogin facebookSignIn = new FacebookLogin();
  String _message = 'Log in/out by pressing the buttons below.';
  String radioItem = '';
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final TextEditingController _textLoginMailController =
      new TextEditingController();
  final TextEditingController _textLoginPasswordController =
      new TextEditingController();
  GoogleSignInAccount _currentUser;
  String latitude;
  String longitude;
  String language = "";
  String referal = "";
  final TextEditingController _regMobController = new TextEditingController();

  GoogleSignIn googleSignIn = GoogleSignIn();
 /* GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: [
      'email',
      'https://www.googleapis.com/auth/contacts.readonly',
    ],
  );*/
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  FirebaseAnalytics analytics;
  @override
  void initState() {
    super.initState();
    analytics = FirebaseAnalytics();
    _registerOnFirebase();
    initDynamicLinks();
   /* Future landat = SharedPrefrence().getLanguage();
    landat.then((data) async {
      language = data;
    });*/

   // _GetLocationdata();
    _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount account) {
      setState(() {
        _currentUser = account;
      });
      if (_currentUser != null) {
       // _handleGetContact();
      }
    });
    _googleSignIn.signInSilently();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final emailField = TextField(
      obscureText: false,
      style: style,
      controller: _textLoginMailController,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Email",
          //border:
         // OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))
      ),
    );

    final passwordField = TextField(
      obscureText: true,
      style: style,
      controller: _textLoginPasswordController,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Password",
        //  border:
        //  OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))
         ),
    );

    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff00ADEE),
      child: MaterialButton(
          minWidth: MediaQuery.of(context).size.width,
          padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          onPressed: () {
            if (_textLoginMailController.text
                .toString()
                .length ==
                0) {
              final snackBar = SnackBar(
                  content: Text("Enter Mail ID"));
              _scaffoldKey.currentState
                  .showSnackBar(snackBar);
            } else if (_textLoginPasswordController.text
                .toString()
                .length ==
                0) {
              final snackBar = SnackBar(
                  content: Text("Enter Password"));
              _scaffoldKey.currentState
                  .showSnackBar(snackBar);
            } else {
              doLogin(_textLoginMailController.text.toString(), _textLoginPasswordController.text.toString());
            }
          },
          child: Text("Login",
              textAlign: TextAlign.center,
              style: style.copyWith(
                  color: Colors.white, fontWeight: FontWeight.bold)),
        ),
    );

    final loginphoneButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff00ADEE),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          Navigator.pop(context, true);
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => LoginScreenMobile(referal)));
            //doLogin(_textLoginMailController.text.toString(), _textLoginPasswordController.text.toString());
        },
        child: Text("Login Using Mobile number",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );


    /*return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        body: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Center(
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(10),
                            child:SvgPicture.asset('assets/images/flyerbin_logo.svg',height: 100,width: 100,),
                            ),
                          Padding(
                            padding: EdgeInsets.all(30),
                            child: Text(
                              "",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                  fontSize: 18),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 10, right: 10),
                            child: Container(
                              width: double.infinity,
                              height: 40,
                             *//* decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage(
                                      "assets/images/ic_edit_field_bg.png"),
                                  fit: BoxFit.cover,
                                ),
                              ),*//*
                              child: TextField(
                                keyboardType: TextInputType.text,
                                controller: _textLoginMailController,
                                decoration: InputDecoration(
                                    border:null,
                                    hintText: "Email",
                                    contentPadding: EdgeInsets.all(10),
                                    *//*prefixIcon: Icon(
                                      Icons.person_outline,
                                      color: Color.fromRGBO(34, 83, 148, 1),
                                    )*//*),
                              ),
                            ),
                          ),
                          Padding(
                            padding:
                                EdgeInsets.only(left: 10, right: 10, top: 15),
                            child: Container(
                              width: double.infinity,
                              height: 40,
                             *//* decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage(
                                      "assets/images/ic_edit_field_bg.png"),
                                  fit: BoxFit.cover,
                                ),
                              ),*//*
                              child: TextField(
                                keyboardType: TextInputType.text,
                                obscureText: true,
                                controller: _textLoginPasswordController,
                                decoration: InputDecoration(
                                    border: null,
                                    hintText: "Password",
                                    contentPadding: EdgeInsets.all(10),
                                   *//* prefixIcon: Icon(
                                      Icons.phone,
                                      color: Color.fromRGBO(34, 83, 148, 1),
                                    )*//*),
                              ),
                            ),
                          ),
                          Padding(
                            padding:
                                EdgeInsets.only(left: 8, top: 25, bottom: 8,right: 8),
                            child: GestureDetector(
                              child: Container(
                                height: 45,
                               // width: 160,
                                child: Center(
                                  child: Text(
                                    "LOGIN",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Color.fromRGBO(34, 83, 148, 1),
                                ),
                              ),
                              onTap: () {
                                if (_textLoginMailController.text
                                        .toString()
                                        .length ==
                                    0) {
                                  final snackBar = SnackBar(
                                      content: Text("Enter Mail ID"));
                                  _scaffoldKey.currentState
                                      .showSnackBar(snackBar);
                                } else if (_textLoginPasswordController.text
                                        .toString()
                                        .length ==
                                    0) {
                                  final snackBar = SnackBar(
                                      content: Text("Enter Password"));
                                  _scaffoldKey.currentState
                                      .showSnackBar(snackBar);
                                } else {
                                   doLogin(_textLoginMailController.text.toString(), _textLoginPasswordController.text.toString());
                                }
                              },
                            ),
                          ),
                         *//* Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: new Container(
                                      margin: const EdgeInsets.only(
                                          left: 10.0, right: 15.0),
                                      child: Divider(
                                        color: Colors.black,
                                        height: 50,
                                      )),
                                ),

                                Expanded(
                                  child: new Container(
                                    margin: const EdgeInsets.only(
                                        left: 15.0, right: 10.0),
                                    child: Divider(
                                      color: Colors.black,
                                      height: 50,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )*//*
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );*/
    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
        child: Center(
          child: Container(
            //color: Colors.white,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            decoration: new BoxDecoration(
              image: new DecorationImage(
                image: new AssetImage("assets/images/login_bg.png"),
                fit: BoxFit.fill,
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(32.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 130.0,
                    child: SvgPicture.asset(
                      "assets/images/flyerbin_logo.svg",
                      fit: BoxFit.contain,
                    ),
                  ),
                  SizedBox(height: 20.0),
                  emailField,
                  SizedBox(height: 20.0),
                  passwordField,
                  SizedBox(
                    height: 30.0,
                  ),
                  GestureDetector(
                    child: loginButon,
                  ),

                  SizedBox(
                    height: 15.0,
                  ),
                  loginphoneButon,
                  SizedBox(
                    height: 10.0,
                  ),
                  SocialLoginLayout(context),
                  RegisterLayout()
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

   SocialLoginLayout(BuildContext context){
    return Center(
      child: Container(
        height: 50,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            GestureDetector(
              onTap: (){
               // _handleSignIn();
                signInWithGoogle();
                print("signin");
              },
              child:Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    color: Colors.black,
                    shape: BoxShape.circle,
                    // image: DecorationImage(image:AssetImage("assets/images/apple_logo.png")),

                  ),
                  child:Padding(
                    padding: EdgeInsets.all(10),
                    child:Image.asset(
                      "assets/images/ic_login_gmail.png",
                      height: 25,
                      width: 25,
                    ),
                  )
              ),
              ),

             GestureDetector(
               onTap: (){
                 fb_login();
               },
               child:Padding(
     padding: EdgeInsets.only(left: 10),
     child:Container(
                   width: 40,
                   height: 40,
                   decoration: BoxDecoration(
                     color: Colors.black,
                     shape: BoxShape.circle,
                     // image: DecorationImage(image:AssetImage("assets/images/apple_logo.png")),

                   ),
                   child:Padding(
                     padding: EdgeInsets.all(10),
                     child:Image.asset(
                       "assets/images/ic_fb_login.png",
                       height: 25,
                       width: 25,
                     ),
                   )
               ),
             ),
             ),
           GestureDetector(
             onTap: ()async{
               //_signInWithApple(context);
               if(Platform.isIOS) {
                 // FirebaseUser user = await auth.appleSignIn();
                 //appleSignIn();
                 if(await AppleSignIn.isAvailable()) {
                   final AuthorizationResult result = await AppleSignIn.performRequests([
                     AppleIdRequest(requestedScopes: [Scope.email, Scope.fullName])
                   ]);
                   switch (result.status) {
                     case AuthorizationStatus.authorized:
                       print(String.fromCharCodes(result.credential.identityToken));//All the required credentials
                       doLoginApple(String.fromCharCodes(result.credential.identityToken),result.credential.fullName.familyName);
                       break;
                     case AuthorizationStatus.error:
                       print("Sign in failed: ${result.error.localizedDescription}");
                       break;
                     case AuthorizationStatus.cancelled:
                       print('User cancelled');
                       break;
                   }
                 }
               }

             },
             child:  Padding(
               padding: EdgeInsets.only(left: 10),
               child: Container(
                   width: 40,
                   height: 40,
                   decoration: BoxDecoration(
                     color: Colors.black,
                     shape: BoxShape.circle,
                     // image: DecorationImage(image:AssetImage("assets/images/apple_logo.png")),

                   ),
                   child:Padding(
                     padding: EdgeInsets.all(10),
                     child:Image.asset(
                       "assets/images/apple_logo.png",
                       height: 25,
                       width: 25,
                     ),
                   )
               ),
             )
           )

          ],
        ),
      ),
    );
  }

  RegisterLayout(){
    return Center(
      child: Container(
        padding: EdgeInsets.all(15),
        height: 50,
       child: Row(
         mainAxisSize: MainAxisSize.min,
         crossAxisAlignment: CrossAxisAlignment.stretch,
         children: [
           Text("Not a user yet?"),
          // Text("  SIGN UP", style: TextStyle(color: Color(0xff00ADEE),fontWeight: FontWeight.bold),),
           GestureDetector(
             onTap: _regModalSheet,
             child: Text("  SIGN UP", style: TextStyle(color: Color(0xff00ADEE),fontWeight: FontWeight.bold),),
           )
         ],
       ),
      ),
    );
  }

  void _regModalSheet() {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (builder) {
          return Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: const Radius.circular(10),
                topRight: const Radius.circular(10),
              ),
            ),
            height: 500,
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Stack(
              children: <Widget>[
                Image.asset(
                  'assets/images/register_bg.jpg',
                  fit: BoxFit.fill,
                ),
                Column(
                  children: <Widget>[
                    new Expanded(
                      flex: 2,
                      child: _textRegHead(),
                    ),
                    // new Flexible(child: _inputMobReg()),
                    new Expanded(
                      flex: 1,
                      child: _btnReg(),
                    )
                  ],
                ),
              ],
            ),
          );
        });
  }

  Column _btnReg() {
    return new Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        _regButton(),
      ],
    );
  }

  Widget _regButton() {
    return GestureDetector(
      onTap: () {
        if (_regMobController.text.toString().length == 0) {
          final snackBar = SnackBar(content: Text("Enter Mobile Number"));
          _scaffoldKey.currentState.showSnackBar(snackBar);
        } else {
          doRegisterMob(_regMobController.text.toString());
         /* Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                  builder: (context) => OTPScreen()),
              ModalRoute.withName("/login"));*/
          // SharedPrefrence().setMobile(_regMobController.text.toString());
          //Navigator.pushReplacementNamed(context, "/otpscreen");
        }
      },
      child: new Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        padding: EdgeInsets.all(20),
        width: MediaQuery.of(context).size.width,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(55)),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.grey.shade200,
                  offset: Offset(5, 4),
                  blurRadius: 20,
                  spreadRadius: 2)
            ],
            gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                //colors: [Color(0xff00ADEE),Color(0xff00ADEE)]
            )),
        child: Text(
          'REGISTER',
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 15, color: Colors.black),
        ),
      ),
    );
  }

  Widget _textRegHead() {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 35, 0, 10),
            child: Text(
              "REGISTER",
              style: TextStyle(fontSize: 16),
            ),
          ),
          Text(
            "Enter Your Mobile No",
            style: TextStyle(
                fontSize: 12,
                color: Colors.grey,
                letterSpacing: 2.0,
                fontWeight: FontWeight.w500),
          ),
          Padding(
            padding: EdgeInsets.only(top: 50),
            child: _inputMobReg(),
          )
        ],
      ),
    );
  }

  Widget _inputMobReg() {
    return SafeArea(
      child: new Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              child: TextField(
                keyboardType: TextInputType.number,
                style: TextStyle(color: Colors.black),
                inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                controller: _regMobController,
                decoration: new InputDecoration(
                  hintText: "Mobile No",
                  hintStyle: TextStyle(color: Colors.black, fontSize: 15),
                  icon: Icon(Icons.mobile_screen_share, color: Colors.grey),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black12),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Color(0xff00ADEE)),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void doLogin(String usermail,String userpass) async {
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);

    var response = await http.post(Urls.baseUrl+Urls.logIn,
        headers: {"Content-Type": "application/json"},
        body: json.encode({
          "email": usermail,
          "password": userpass,
        }));

    Map<String, dynamic> value = json.decode(response.body);
    if (response.statusCode == 200) {
      dialog.dismissProgressDialog(context);
      try {
        print("response login " + response.body.toString());
        Map<String, dynamic> value = json.decode(response.body);

        var status = value['success'];
        var message = value['message'];
        print(message);
        if (status == true) {
          // SharedPrefrence().setMobile(userMob);
          if(message=='Operation Successfull'){
            SharedPrefrence().setToken(value['data']['token'].toString());
            SharedPrefrence().setUserId(value['data']['id'].toString());
            SharedPrefrence().setUserName(value['data']['name'].toString());
            SharedPrefrence().setHasPassword(value['data']['hasPassword']);
            SharedPrefrence().saveUserPhoto(value['data']['meta_data']['profile_photo'].toString());
            SharedPrefrence().saveUserPassword(userpass);
            SharedPrefrence().setReferralcode(value['data']['referral_code'].toString());
            if(value['data']['email'] != null){
              SharedPrefrence().setUserEmail(value['data']['email'].toString());
            }else{
              SharedPrefrence().setUserEmail("");
            }
            if(value['data']['mobile'] != null){
              SharedPrefrence().setMobile(value['data']['mobile'].toString());
            }else{
              SharedPrefrence().setMobile("");
            }
            SharedPrefrence().setLoggedIn(true);
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                    builder: (context) => ChangeShopPreference()),
                ModalRoute.withName("/login"));
          }

        }else {
          print("Error...");
          final snackBar = SnackBar(content: Text("Please check your credentials"));
          _scaffoldKey.currentState.showSnackBar(snackBar);
        }
      } catch (e) {
        print(e.toString());
      }
    } else if (response.statusCode == 404) {
      dialog.dismissProgressDialog(context);
      var message = value['message'];
      final snackBar = SnackBar(content: Text(message));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    } else if (response.statusCode == 208) {
      dialog.dismissProgressDialog(context);
    } else {
      var message = value['message'];
      dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(content: Text("Please check your credentials"));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  void doLoginSocial(String name,String email,String provider_id,String provider) async {
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);

    var response = await http.post(Urls.baseUrl+Urls.social_login,
        headers: {"Content-Type": "application/json"},
        body: json.encode({
          "name": name,
          "email": email,
          "provider_id": provider_id,
          "provider": provider,
        }));

    Map<String, dynamic> value = json.decode(response.body);
    if (response.statusCode == 200) {
      dialog.dismissProgressDialog(context);
      try {
        print("response social login " + response.body.toString());
        Map<String, dynamic> value = json.decode(response.body);

        var status = value['success'];
        var message = value['message'];
        print(message);
        if (status == true) {
          // SharedPrefrence().setMobile(userMob);
          if(message=='Operation Successfull'){
            SharedPrefrence().setToken(value['data']['token'].toString());
            SharedPrefrence().setUserId(value['data']['id'].toString());
            SharedPrefrence().setUserName(value['data']['name'].toString());
            SharedPrefrence().setUserEmail(value['data']['email'].toString());
            SharedPrefrence().setLoggedIn(true);
            SharedPrefrence().setHasPassword(value['data']['hasPassword']);
            if(value['data']['hasPassword']){
              SharedPrefrence().saveUserPassword('');
            }
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                    builder: (context) => ChangeShopPreference()),
                ModalRoute.withName("/login"));
          }

        } else if (message == 'Already Registered') {
          final snackBar = SnackBar(content: Text(message));
          _scaffoldKey.currentState.showSnackBar(snackBar);
          // SharedPrefrence().setMobile(userMob);
          //  SharedPrefrence().setLoggedIn(true);
          /* Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => HomeScreen()),
              ModalRoute.withName("/login"));*/
        } else if (message == "NOT REGISTERED") {
          final snackBar = SnackBar(content: Text(message));
          _scaffoldKey.currentState.showSnackBar(snackBar);
        } else if (message == 'Network Error.OTP Not Generated') {
          print("Network Error.OTP Not Generated");
          final snackBar = SnackBar(content: Text(message));
          _scaffoldKey.currentState.showSnackBar(snackBar);
        } else {
          print("Error...");
          final snackBar = SnackBar(content: Text(message));
          _scaffoldKey.currentState.showSnackBar(snackBar);
        }
      } catch (e) {
        print(e.toString());
      }
    } else if (response.statusCode == 404) {
      dialog.dismissProgressDialog(context);
      var message = value['message'];
      final snackBar = SnackBar(content: Text(message));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    } else if (response.statusCode == 208) {
      dialog.dismissProgressDialog(context);
    } else {
      var message = value['message'];
      dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(content: Text(message));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  void doLoginApple(String token,String name) async {
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);

    var response = await http.post(Urls.baseUrl+Urls.Apple_login,
        headers: {"Content-Type": "application/json"},
        body: json.encode({
          "token": token,
          "name": name,
        }));

    Map<String, dynamic> value = json.decode(response.body);
    print("response social login " + response.body.toString());
    if (response.statusCode == 200) {
      dialog.dismissProgressDialog(context);
      try {
        print("response social login " + response.body.toString());
        Map<String, dynamic> value = json.decode(response.body);

        var status = value['success'];
        var message = value['message'];
        print(message);
        if (status == true) {
          // SharedPrefrence().setMobile(userMob);
          if(message=='Operation Successfull'){
            SharedPrefrence().setToken(value['data']['token'].toString());
            SharedPrefrence().setUserId(value['data']['id'].toString());
            SharedPrefrence().setUserName(value['data']['name'].toString());
            SharedPrefrence().setUserEmail(value['data']['email'].toString());
            SharedPrefrence().setLoggedIn(true);
            SharedPrefrence().setHasPassword(value['data']['hasPassword']);
            if(value['data']['hasPassword']){
              SharedPrefrence().saveUserPassword('');
            }
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                    builder: (context) => ChangeShopPreference()),
                ModalRoute.withName("/login"));
          }

        } else if (message == 'Already Registered') {
          final snackBar = SnackBar(content: Text(message));
          _scaffoldKey.currentState.showSnackBar(snackBar);
          // SharedPrefrence().setMobile(userMob);
          //  SharedPrefrence().setLoggedIn(true);
          /* Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => HomeScreen()),
              ModalRoute.withName("/login"));*/
        } else if (message == "NOT REGISTERED") {
          final snackBar = SnackBar(content: Text(message));
          _scaffoldKey.currentState.showSnackBar(snackBar);
        } else if (message == 'Network Error.OTP Not Generated') {
          print("Network Error.OTP Not Generated");
          final snackBar = SnackBar(content: Text(message));
          _scaffoldKey.currentState.showSnackBar(snackBar);
        } else {
          print("Error...");
          final snackBar = SnackBar(content: Text(message));
          _scaffoldKey.currentState.showSnackBar(snackBar);
        }
      } catch (e) {
        print(e.toString());
      }
    } else if (response.statusCode == 404) {
      dialog.dismissProgressDialog(context);
      var message = value['message'];
      final snackBar = SnackBar(content: Text(message));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    } else if (response.statusCode == 208) {
      dialog.dismissProgressDialog(context);
    } else {
      var message = value['message'];
      dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(content: Text(message));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  Future<void> _handleSignIn() async {
    try {
      await _googleSignIn.signIn();
      print(_googleSignIn.currentUser.id);
    } catch (error) {
      print("fffff "+error.toString());
    }
  }

  Future<Null> fb_login() async {
    final FacebookLoginResult result =
    await facebookSignIn.logIn(['email']);

    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        final FacebookAccessToken accessToken = result.accessToken;
        print(accessToken.permissions);
        final graphResponse = await http.get(
            'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=${accessToken.token}');
        final profile = json.decode(graphResponse.body);
        print(profile);
        doLoginSocial(profile['name'].toString(),profile['email'].toString(),profile['id'].toString(),"facebook");
        _showMessage('''
         Logged in!
         
         Token: ${accessToken.token}
         User id: ${accessToken.userId}
         Expires: ${accessToken.expires}
         Permissions: ${accessToken.permissions}
         Declined permissions: ${accessToken.declinedPermissions}
         ''');
        break;
      case FacebookLoginStatus.cancelledByUser:
        _showMessage('Login cancelled by the user.');
        break;
      case FacebookLoginStatus.error:
        _showMessage('Something went wrong with the login process.\n'
            'Here\'s the error Facebook gave us: ${result.errorMessage}');
        break;
    }
  }

  void _showMessage(String message) {
    setState(() {
      _message = message;
      print(_message);
    });
  }

  void doRegisterMob(String userMob) async {
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var response = await http.post(Urls.baseUrl+Urls.Register_generate_OTP,
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json"
        },
        body: json.encode({
          "mobile": userMob,
        }));

    Map<String, dynamic> value = json.decode(response.body);
    print("response number verify " + response.body.toString());

    if (response.statusCode == 200) {
      dialog.dismissProgressDialog(context);
      try {
        print("response number verify " + response.body.toString());
        Map<String, dynamic> value = json.decode(response.body);

        /*var sectionId = value['sectionId'];
        var branchId = value['branchId'];

        SharedPrefrence().setsectionId(sectionId['_id'].toString());
        SharedPrefrence().setToken(value['token'].toString());
        SharedPrefrence().setbranchId(value['branchId'].toString());
        SharedPrefrence().setLoggedIn(true);
        SharedPrefrence().setUserId(value['_id'].toString());
        SharedPrefrence().setSectionHead(sectionId['Section_Head'].toString());*/
        var status = value['success'];
        if (status == true) {
          SharedPrefrence().setMobile(userMob);
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                  builder: (context) => OTPScreen(referal)),
              ModalRoute.withName("/login"));
        } else {
          var message = value['message'];
          final snackBar = SnackBar(content: Text(message));
          _scaffoldKey.currentState.showSnackBar(snackBar);
        }
      } catch (e) {
        print(e.toString());
      }
    } else if (response.statusCode == 404) {
      dialog.dismissProgressDialog(context);
      // var message = value['message'];
      //  CustomDialogs().showErrorAlert(context, message);
    } else if (response.statusCode == 208) {
      dialog.dismissProgressDialog(context);
//      progressDialog.dismiss();
    }
    else {
      dialog.dismissProgressDialog(context);
      CustomDialogs().showErrorAlert(context, "Try Again...");
    }
  }



 /* /// Sign in with Apple
  Future<FirebaseUser> appleSignIn() async {
    *//*try {
      final AuthorizationResult appleResult = await AppleSignIn.performRequests(
          [
            AppleIdRequest(requestedScopes: [Scope.email, Scope.fullName])
          ]);

      if (appleResult.error != null) {
        // handle errors from Apple here
      }

      final AuthCredential credential = OAuthProvider(providerId: 'apple.com')
          .getCredential(
        accessToken: String.fromCharCodes(
            appleResult.credential.authorizationCode),
        idToken: String.fromCharCodes(appleResult.credential.identityToken),
      );

      AuthResult firebaseResult = await _firebaseAuth.signInWithCredential(
          credential);
      FirebaseUser user = firebaseResult.user;

      // Optional, Update user data in Firestore
      // updateUserData(user);

      return user;
    } catch (error) {
      print(error);
      return null;
    }*//*
    try {

      final AuthorizationResult result = await AppleSignIn.performRequests([
        AppleIdRequest(requestedScopes: [Scope.email, Scope.fullName])
      ]);

      switch (result.status) {
        case AuthorizationStatus.authorized:
          try {
            print("successfull sign in");
            final AppleIdCredential appleIdCredential = result.credential;

            OAuthProvider oAuthProvider =
            new OAuthProvider(providerId: "apple.com");
            final AuthCredential credential = oAuthProvider.getCredential(
              idToken:
              String.fromCharCodes(appleIdCredential.identityToken),
              accessToken:
              String.fromCharCodes(appleIdCredential.authorizationCode),
            );
             print(String.fromCharCodes(appleIdCredential.identityToken));
            doLoginApple(String.fromCharCodes(appleIdCredential.identityToken),"${appleIdCredential.fullName.givenName} ${appleIdCredential.fullName.familyName}");
            final AuthResult _res = await FirebaseAuth.instance
                .signInWithCredential(credential);

            FirebaseAuth.instance.currentUser().then((val) async {
              UserUpdateInfo updateUser = UserUpdateInfo();
              updateUser.displayName =
              "${appleIdCredential.fullName.givenName} ${appleIdCredential.fullName.familyName}";
              updateUser.photoUrl =
              "define an url";
              await val.updateProfile(updateUser);
              print(updateUser.displayName);
            });

          } catch (e) {
            print("error");
          }
          break;
        case AuthorizationStatus.error:
        // do something
          break;

        case AuthorizationStatus.cancelled:
          print('User cancelled');
          break;
      }
    } catch (error) {
      print("error with apple sign in");
    }
  }*/

  Future<String> signInWithGoogle() async {
    await Firebase.initializeApp();
    FirebaseAuth _auth = FirebaseAuth.instance;

    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    final UserCredential authResult = await _auth.signInWithCredential(credential);
    final User user = authResult.user;

    if (user != null) {
      assert(!user.isAnonymous);
      assert(await user.getIdToken() != null);

      final User currentUser = _auth.currentUser;
      assert(user.uid == currentUser.uid);

      print('signInWithGoogle succeeded: $user');
      print(user.uid);
      doLoginSocial(user.displayName, user.email, user.uid, "google");

      return '$user';
    }

    return null;
  }
  void initDynamicLinks() async {
    FirebaseDynamicLinks.instance.onLink(
        onSuccess: (PendingDynamicLinkData dynamicLink) async {
          final PendingDynamicLinkData data = await FirebaseDynamicLinks.instance.getInitialLink();
          final Uri deepLink = data?.link;
          print("deepLink3 : ${deepLink.queryParameters['referral_code']}");
          if(deepLink.queryParameters['referral_code'] != null){
            setState(() {
              referal = deepLink.queryParameters['referral_code'];
            });
          }
        },
        onError: (OnLinkErrorException e) async {
          print('onLinkError');
          print(e.message);
        }
    );


  }
  _registerOnFirebase() {
    _firebaseMessaging.subscribeToTopic('all');
    _firebaseMessaging.getToken().then((token) => print(token));
  }
}



