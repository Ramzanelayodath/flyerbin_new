import 'dart:async';
import 'dart:convert';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';

import 'Cart.dart';
import 'FlyerLoadScreen.dart';
import 'Model/BrowseFlyerModel.dart';
import 'Model/NearbyListitemModel.dart';
import 'Model/NearbyMallListItemModel.dart';
import 'Utils/Constants.dart';
import 'Utils/CustomDialogs.dart';
import 'Utils/SharedPrefrence.dart';
import 'Utils/Urls.dart';
import 'package:http/http.dart' as http;

// ignore: must_be_immutable
class NearbyListItemDetailsScreen extends StatefulWidget {
  String shop_id;
  String mall_id;
  String namep;

  NearbyListItemDetailsScreen(this.shop_id, this.mall_id, this.namep);

  @override
  _NearbyListItemDetailsScreenState createState() =>
      _NearbyListItemDetailsScreenState(this.shop_id, this.mall_id, this.namep);
}

class _NearbyListItemDetailsScreenState
    extends State<NearbyListItemDetailsScreen> {
  String shop_id;
  String mall_id;
  String namep,lan = 'en';
  final List<NearbyListitemModel> nearbylistitems = List();
  final List<NearbyMallListItemModel> nearbymalllistitems = List();
  double latti = 25.2048493, long = 55.2707828;
  Completer<GoogleMapController> _controller = Completer();
  Iterable markers = [];
  String userToken;
  bool shopflyershow = false;
  bool mallflyershow = false;
  final DateFormat formatter = DateFormat('yyyy-MM-dd');
  final DateFormat formatter2 = DateFormat('dd-MM-yyyy');
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  List<BrowseFlyerModel>fav_flyer = List();
  int fav_flyer_list_count=0;
  _NearbyListItemDetailsScreenState(this.shop_id, this.mall_id, this.namep);



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("mall_id=" + mall_id.toString());
    print("Shop_id=" + shop_id.toString());
    Future lng = SharedPrefrence().getLanguage();
    lng.then((data) async {
      lan = data;
    });
    if (mall_id == "null") {
      setState(() {
        shopflyershow = true;
        mallflyershow = false;
      });
      print("shop");
      ShopFlyerData(shop_id);
    } else {
      setState(() {
        shopflyershow = false;
        mallflyershow = true;
      });
      print("Mall");
      MallFlyerData(mall_id);
    }
    Future token = SharedPrefrence().getToken();
    token.then((data) async {
      userToken = data;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          centerTitle: false,
          iconTheme: new IconThemeData(color: Color.fromRGBO(34, 83, 148, 1)),
          title: Text(
            namep.toString(),
            style:
                TextStyle(fontSize: 13, color: Color.fromRGBO(34, 83, 148, 1)),
          ),
          backgroundColor: Colors.white,
          actions: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: GestureDetector(
                child:  Icon(
                  Icons.shopping_cart,
                  color: Color(0xff00ADEE),
                ),
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Cart(),
                    ),
                  );
                },
              ),
            )
          ],
        ),
        body: SingleChildScrollView(
          child: Column( 
            children: [
              Visibility(visible: mallflyershow, child: MallFlyer()),
              Visibility(visible: shopflyershow, child: ShopFlyer())
            ],
          ),
        ));
  }

  Widget MallFlyer() {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListView.builder(
            shrinkWrap: true,
            itemCount: nearbymalllistitems.length,
            itemBuilder: (context, index) {
              NearbyMallListItemModel data = nearbymalllistitems[index];
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                      flex: 0,
                      child: Container(
                        height: 70,
                        width: 100,
                        child: Image.network(
                          data.original_logo.toString(),
                          height: 70,
                          width: 100,
                          fit: BoxFit.fill,
                        ),
                      )),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        data.name.toString(),
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.w500),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Container(
                          width: 250,
                          child: Text(data.address.toString(),
                              style: TextStyle(
                                  fontSize: 13, color: Colors.blueGrey))),
                    ],
                  )
                ],
              );
            },
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 150,
            child: GoogleMap(
              markers: Set.from(markers),
              initialCameraPosition: CameraPosition(
                  target: LatLng(latti, long), tilt: 30, zoom: 14),
              mapType: MapType.normal,
              onMapCreated: (GoogleMapController controller) {
                _controller = controller as Completer<GoogleMapController>;
              },
            ),
          ),
         /* Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Flyers",
              style: TextStyle(fontSize: 15, color: Colors.blueAccent[400]),
            ),
          ),*/
        ],
      ),
    );
  }

  Widget ShopFlyer() {
    return nearbylistitems.length==0?Center(child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Image.asset("assets/images/no_flyer.png"))
    ):
     ListView.builder(
      shrinkWrap: true,
      itemCount: nearbylistitems.length,
      itemBuilder: (context, index) {
        NearbyListitemModel data = nearbylistitems[index];
        return Card(
          child: GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => FlyerLoadScreen(
                        data.id.toString(),
                        data.name.toString(),
                        data.valid_from.toString(),
                        data.valid_to.toString(),
                        data.image_path.toString(),false,"")),
              );
            },
            child: Container(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    height: 160,
                    width: 120,
                    child: Image.network(
                      data.image_path.toString(),
                      height: 160,
                      width: 120,
                      fit: BoxFit.fill,
                    ),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: [
                          Container(
                              width: 150,
                              child: Text(
                                data.name,
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold),
                              )),
                          SizedBox(
                            height: 20,
                          ),
                          Text("Valid From " +
                             data.valid_from),
                          Text("Valid To  " +
                              data.valid_to),
                        ],
                      ),
                      GestureDetector(
                        onTap: (){
                          doFavorUnfav(model :data,isfromlist : true);
                        },
                        child: Icon(Icons.favorite_border,
                            color: data.isFavorited ? Colors.red : Colors.blue),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Future<void> ShopFlyerData(String shop_id) async {
    print(shop_id);
    var response = await http.get(
        "${Urls.baseUrl}${Urls.ShopFlyers}/$shop_id/flyers?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=100&locale="+lan+"&page=1",
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print("response " + response.body);
    if (response.statusCode == 200) {
      try {
        var data = value['data'];
        var current_page = value['current_page'];
        var array = data['data'];
        for (int i = 0; i < array.length; i++) {
          var obj = array[i];
          print("Image link " + obj['name']);

          nearbylistitems.add(NearbyListitemModel(
            obj['id'],
            obj['shop_id'],
            obj['brand_id'],
            obj['name'],
            formatter2.format(formatter.parse(obj['valid_from'])),
            formatter2.format(formatter.parse(obj['valid_to'])),
            obj['favourite_count'],
            Urls.baseImageUrl + obj['image_path'],
            obj['isFavorited'],
          ));

          setState(() {
            nearbylistitems.length;
            print(nearbylistitems.length);
          });
        }
      } catch (e) {
        e.toString();
      }
    }
  }

  Future<void> MallFlyerData(String mall_id) async {
    print(mall_id);
    var response = await http.get(
        "${Urls.baseUrl}${Urls.GetMalldetails}/$mall_id?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=100&locale="+lan+"&page=1",
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print("response mall" + response.body);
    if (response.statusCode == 200) {
      try {
        var data = value['data'];
        print("Mall name " + data['name']);
        nearbymalllistitems.add(NearbyMallListItemModel(
          data['id'],
          data['name'],
          Urls.baseImageUrl + data['original_logo'],
          data['address'],
          data['pincode'],
          data['latitude'],
          data['longitude'],
        ));
        setState(() {
          nearbymalllistitems.length;
          this.latti = latti;
          this.long = long;
          latti = double.parse(data["latitude"]);
          long = double.parse(data["longitude"]);
          LatLng latLngMarker = LatLng(
              double.parse(data["latitude"]), double.parse(data["longitude"]));

          return Marker(
              markerId: MarkerId("marker$data"),
              position: latLngMarker,
              infoWindow:
                  InfoWindow(title: data["name"], snippet: data["address"]));
        });
      } catch (e) {
        e.toString();
      }
    }
  }

  void doFavorUnfav({NearbyListitemModel model,bool isfromlist, String id})async{
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var url;
    if(isfromlist){
      url =  "${Urls.baseUrl}${Urls.AddRemoveFavourite}?flyer_id="+model.id.toString();
    }else{
      url =  "${Urls.baseUrl}${Urls.AddRemoveFavourite}?flyer_id="+model.id;
    }
    var response = await http.post(
        url,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print("response mall" + response.body);
    if(response.statusCode == 200){
      dialog.dismissProgressDialog(context);
      try{
        if(value['message'] == "Favourited"){
          setState(() {
            if(isfromlist) {
              model.isFavorited = true;
            }else{
              model.isFavorited = true;
            }
          });
          final snackBar = SnackBar(content: Text("Added into your Favourite list",style: TextStyle(color: Colors.white)),backgroundColor: Colors.green,);
          _scaffoldKey.currentState.showSnackBar(snackBar);

        }else if(value['message'] == "Unfavourited"){
          setState(() {
            if(isfromlist) {
              model.isFavorited = false;
              // if(isfromfavflayer){
              //   fav_flyer.remove(model);
              //   fav_flyer_list_count = fav_flyer.length;
              //
              // }
            }else{
              model.isFavorited = false;
            }
          });
          final snackBar = SnackBar(content: Text("Removed From your Favourite list",style: TextStyle(color: Colors.white)),backgroundColor: Colors.green,);
          _scaffoldKey.currentState.showSnackBar(snackBar);
        }
      }catch(e){}
    }else{
      dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(content: Text(value['message'],style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }
}
