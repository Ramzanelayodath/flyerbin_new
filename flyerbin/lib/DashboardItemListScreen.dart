import 'dart:convert';
import 'dart:io';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:http/http.dart' as http;
import 'Cart.dart';
import 'ListItems.dart';
import 'Model/AutoCompleteModel.dart';
import 'Model/RecommendedListItemModel.dart';
import 'Model/UserListItem.dart';
import 'Model/UserListModel.dart';
import 'SideDrawerScreen.dart';
import 'Utils/Constants.dart';
import 'Utils/CustomDialogs.dart';
import 'Utils/SharedPrefrence.dart';
import 'Utils/Urls.dart';


class DashboardItemListScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return state();
  }
}

class state extends State<DashboardItemListScreen> {
  String userToken,lan = 'en';
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final TextEditingController ctrl_auto = TextEditingController();
  var list = List<UserListModel>();
  var recommended_list = List<RecommendedListItemModel>();
  var auto_cmplt_list = List<AutoCompleteModel>();
  var list_count = 0, recommended_list_count = 0, auto_list_count = 0;
  var isdeleteButtonVisible = false, isshowlist = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future lng = SharedPrefrence().getLanguage();
    lng.then((data) async {
      lan = data;
    });
    Future token = SharedPrefrence().getToken();
    token.then((data) async {
      userToken = data;
      getCategoryLists();
      getRecommendedList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomInset: false,
      drawer: SideDraweScreen(),
      appBar: AppBar(
        iconTheme: new IconThemeData(color: Color.fromRGBO(34, 83, 148, 1)),
        title: appbarLogo(),
        backgroundColor: Colors.white,
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: GestureDetector(
              child:  Icon(
                Icons.shopping_cart,
                color: Color(0xff00ADEE),
              ),
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Cart(),
                  ),
                );
              },
            ),
          )
        ],
      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: () {
          PopupCategory(context);
        },
        child: const Icon(
          Icons.add,
        ),
      ),
      body: DoubleBackToCloseApp(
        snackBar: const SnackBar(
    content: Text('Tap back again to exit'),
    ),child: SingleChildScrollView(
        child: Column(
          children: [
            isshowlist
                ? Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          flex: 0,
                          child: Container(
                            height: 30,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: GestureDetector(
                                    child: Text(
                                      "Select All",
                                      softWrap: true,
                                      style: TextStyle(
                                          color: Colors.blue[500],
                                          fontSize: 15),
                                    ),
                                    onTap: () {
                                      for (int i = 0; i < list.length; i++) {
                                        for (int j = 0;
                                            j < list[i].items.length;
                                            j++) {
                                          setState(() {
                                            list[i].items[j].isclicked = true;
                                          });
                                        }
                                      }
                                      setState(() {
                                        isdeleteButtonVisible = true;
                                      });
                                    },
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Visibility(
                                    child: GestureDetector(
                                      child: Icon(
                                        Icons.delete,
                                        color: Colors.blue[500],
                                        size: 20,
                                      ),
                                      onTap: () {
                                        deleteItems();
                                      },
                                    ),
                                    visible: isdeleteButtonVisible,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        ListView.builder(
                            itemCount: list_count,
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            //controller: latest_flyer_controller,
                            itemBuilder: (context, index) {
                              return Column(
                                children: [
                                  Padding(
                                      padding: EdgeInsets.all(8),
                                      child: Align(
                                          alignment: Alignment.topLeft,
                                          child: Text(list[index].name,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 13)))),
                                  ListView.builder(
                                      itemCount: list[index].items.length,
                                      physics: NeverScrollableScrollPhysics(),
                                      shrinkWrap: true,
                                      //controller: latest_flyer_controller,
                                      itemBuilder: (context, i) {
                                        return new GestureDetector(
                                          child: Padding(
                                            padding: EdgeInsets.only(
                                                left: 2, right: 2, top: 1),
                                            child: Container(
                                              height: 40,
                                              decoration: BoxDecoration(
                                                  border: Border.all(
                                                      color: Colors.grey[350])),
                                              child: Row(
                                                mainAxisSize: MainAxisSize.max,
                                                children: [
                                                  Expanded(
                                                    child: Padding(
                                                      padding:
                                                          EdgeInsets.all(8),
                                                      child: Checkbox(
                                                        value: list[index]
                                                            .items[i]
                                                            .isclicked,
                                                        onChanged:
                                                            (bool value) {
                                                          setState(() {
                                                            list[index]
                                                                    .items[i]
                                                                    .isclicked =
                                                                value;
                                                          });
                                                          for (int i = 0;
                                                              i < list.length;
                                                              i++) {
                                                            for (int j = 0;
                                                                j <
                                                                    list[i]
                                                                        .items
                                                                        .length;
                                                                j++) {
                                                              if (list[i]
                                                                  .items[j]
                                                                  .isclicked) {
                                                                setState(() {
                                                                  isdeleteButtonVisible =
                                                                      true;
                                                                });
                                                                break;
                                                              }
                                                            }
                                                          }
                                                        },
                                                      ),
                                                    ),
                                                    flex: 0,
                                                  ),
                                                  Expanded(
                                                    child: Padding(
                                                      padding: EdgeInsets.only(
                                                          top: 8, bottom: 8),
                                                      child: Text(
                                                        list[index]
                                                            .items[i]
                                                            .name,
                                                        style: TextStyle(
                                                            decoration: list[
                                                                        index]
                                                                    .items[i]
                                                                    .isclicked
                                                                ? TextDecoration
                                                                    .lineThrough
                                                                : TextDecoration
                                                                    .none),
                                                      ),
                                                    ),
                                                    flex: 2,
                                                  ),
                                                  Expanded(
                                                    child: Visibility(
                                                      child: Padding(
                                                        padding:
                                                            EdgeInsets.only(
                                                                right: 8),
                                                        child: Row(
                                                          children: [
                                                            Padding(
                                                              padding: EdgeInsets
                                                                  .only(
                                                                      top: 8,
                                                                      right: 5),
                                                              child: Text(
                                                                list[index]
                                                                        .items[
                                                                            i]
                                                                        .flyer_items_count
                                                                        .toString() +
                                                                    " Deals",
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .blue,
                                                                ),
                                                              ),
                                                            ),
                                                            Padding(
                                                              padding: EdgeInsets
                                                                  .only(
                                                                      top: 8,
                                                                      right: 8),
                                                              child: Icon(
                                                                list[index]
                                                                        .items[
                                                                            i]
                                                                        .isclicked
                                                                    ? Icons
                                                                        .close
                                                                    : Icons
                                                                        .arrow_forward_ios_sharp,
                                                                color:
                                                                    Colors.grey,
                                                                size: 13,
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                      visible:
                                                          !list[index].iscustom,
                                                    ),
                                                    flex: 0,
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                          onTap: () {
                                            if (!list[index].iscustom) {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) =>
                                                      ListItems(
                                                          list[index]
                                                              .items[i]
                                                              .slug,
                                                          list[index]
                                                              .items[i]
                                                              .name),
                                                ),
                                              );
                                            }
                                          },
                                        );
                                      })
                                ],
                              );
                            })

                        // Expanded(flex: 0, child: Categories()),
                      ],
                    ),
                  )
                : SizedBox(
                    height: MediaQuery.of(context).size.height,
                     width: MediaQuery.of(context).size.width,
                    child: Column(

                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("List it to shop fast"),
                        Padding(
                          padding: EdgeInsets.only(top: 10),
                          child: Image.asset('assets/images/list_empty.png'),
                        ),
                      ],
                    ),
                  ),
          ],
        ),
      ),
      // floatingActionButton: new FloatingActionButton(
      //   onPressed: () {
      //     PopupCategory(context);
      //   },
      //   child: const Icon(
      //     Icons.add,
      //   ),
      // ),
    ),);
  }

  Widget appbarLogo() {
    return SvgPicture.asset(
      "assets/images/flyerbin_logo.svg",
      height: 40,
    );
  }

  Future<void> PopupCategory(BuildContext context) async {
    ctrl_auto.clear();
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (context, setState) {
            return Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0)),
              //this right here
              child: SingleChildScrollView(
                child: Container(
                  height: 450,
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                GestureDetector(
                                  child: Icon(
                                    Icons.cancel,
                                    color: Colors.blueGrey,
                                    size: 20,
                                  ),
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 3),
                                  child: Text(
                                    "Recommended",
                                    style: TextStyle(fontSize: 15),
                                  ),
                                )
                              ],
                            ),
                            GestureDetector(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Icon(
                                    Icons.check_circle,
                                    color: Colors.blue,
                                    size: 20,
                                  ),
                                  Text(
                                    "Done",
                                    style: TextStyle(fontSize: 14),
                                  )
                                ],
                              ),
                              onTap: () {
                                addToList();
                              },
                            ),
                          ],
                        ),
                        Divider(
                          thickness: 0.8,
                        ),
                        Expanded(
                            flex: 0,
                            child: Padding(
                              padding: const EdgeInsets.all(2),
                              child: Container(
                                  height: 30,
                                  width: 300,
                                  child: TypeAheadField(
                                    textFieldConfiguration:
                                        TextFieldConfiguration(
                                            controller: this.ctrl_auto,
                                            autofocus: false,
                                            decoration: InputDecoration(
                                                contentPadding: EdgeInsets.only(top:12,left: 10),
                                              hintText: "Add Item....",
                                                border: OutlineInputBorder())),
                                    suggestionsCallback: (pattern) async {
                                      // ignore: missing_return
                                      print("pattern " + pattern);
                                      try {
                                        final result = await InternetAddress.lookup('google.com');
                                        if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
                                          print('connected');
                                          return await getAutoComplete(pattern);
                                        }
                                      } on SocketException catch (_) {
                                        print('not connected');
                                      }

                                      /*var connectivityResult = await (Connectivity().checkConnectivity());
                                      if (connectivityResult != ConnectivityResult.none) {
                                        return await getAutoComplete(pattern);
                                      }else{
                                        return null;
                                      }*/
                                    },
                                    itemBuilder: (context, suggestion) {
                                      return ListTile(
                                        title: Text(suggestion.name),
                                      );
                                    },
                                    onSuggestionSelected: (suggestion) {
                                      if (suggestion.iscustomTag) {
                                        addCustomTags(
                                            ctrl_auto.text.toString());
                                      } else {
                                        addToListSingleItem(
                                            suggestion.id.toString());
                                      }
                                    },
                                  )),
                            )),
                        Container(
                            child: GridView.count(
                          crossAxisCount: 4,
                          childAspectRatio: MediaQuery.of(context).size.width /
                              (MediaQuery.of(context).size.height / 1.2),
                          crossAxisSpacing: 5.0,
                          mainAxisSpacing: 5.0,
                          shrinkWrap: true,
                          physics: ScrollPhysics(),
                          children:
                              List.generate(recommended_list_count, (index) {
                            return SingleChildScrollView(
                              child: Column(
                                children: [
                                  Expanded(
                                    flex: 0,
                                    child: GestureDetector(
                                      onTap: () {},
                                      child: Container(
                                        height: 90,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Expanded(
                                              flex: 0,
                                              child: GestureDetector(
                                                child: Container(
                                                    width: 45.0,
                                                    height: 45.0,
                                                    decoration:
                                                        new BoxDecoration(
                                                            shape: BoxShape
                                                                .circle,
                                                            border: Border.all(
                                                              color: recommended_list[
                                                                          index]
                                                                      .isclicked
                                                                  ? Colors
                                                                      .red[500]
                                                                  : Colors
                                                                      .white,
                                                            ),
                                                            image: new DecorationImage(
                                                                fit: BoxFit
                                                                    .fill,
                                                                image: new NetworkImage(Urls
                                                                        .baseImageUrl +
                                                                    recommended_list[
                                                                            index]
                                                                        .icon)))),
                                                onTap: () {
                                                  setState(() {
                                                    if (recommended_list[index]
                                                        .isclicked) {
                                                      recommended_list[index]
                                                          .isclicked = false;
                                                    } else {
                                                      recommended_list[index]
                                                          .isclicked = true;
                                                    }
                                                  });
                                                },
                                              ),
                                            ),
                                            Expanded(
                                              flex: 0,
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(4.0),
                                                child: Text(
                                                  recommended_list[index].name,
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontSize: 10),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            );
                          }),
                        )),
                      ],
                    ),
                  ),
                ),
              ),
            );
          });
        });
  }

  Future<void> getCategoryLists() async {
    var name;
    var response = await http.get(
        "${Urls.baseUrl}${Urls.userList}?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=100&locale="+lan,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    print(response.statusCode);
    print("response list " + response.body.toString());
    Map<String, dynamic> value = json.decode(response.body);
    var msg = value['message'];
    if (response.statusCode == 200) {
      try {
        var data = value['data'];
        var custom_array = data['custom_tags'];
        var user_items = List<UserListItem>();
        for (int i = 0; i < custom_array.length; i++) {
          var obj = custom_array[i];
          user_items.add(UserListItem(
              obj['id'], "", obj['name'], obj['icon'], "", "", false));
        }
        if (custom_array.length > 0) {
          list.add(
              UserListModel('', 'Shopping List', '', '', user_items, true));
        }

        var array = data['user_tags'];
        for (int i = 0; i < array.length; i++) {
          var obj = array[i];
          var items = List<UserListItem>();
          var tags = obj['tags'];
          for (int j = 0; j < tags.length; j++) {
            var obj = tags[j];
            if(obj ['translated'] !=null){
              name = obj['translated']['name'];
            }else{
              name = obj['name'];
            }
            items.add(UserListItem(
                obj['id'],
                obj['slug'],
                name,
                obj['icon'],
                obj['tag_category_id'],
                obj['flyer_items_count'],
                false));
          }
          list.add(UserListModel(
              obj['id'], obj['name'], obj['slug'], obj['icon'], items, false));
        }
      } catch (e) {
        e.toString();
      }
      setState(() {
        list_count = list.length;
        if (list_count == 0) {
          isshowlist = false;
        }
        else{
          isshowlist = true;
        }
      });
    } else if (response.statusCode == 401) {
      final snackBar = SnackBar(content: Text(msg.toString()));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    } else {
      final snackBar = SnackBar(content: Text(msg.toString()));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  void deleteItems() async {
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var custom_array = [];
    var user_tags_array = [];
    for (int i = 0; i < list.length; i++) {
      for (int k = 0; k < list[i].items.length; k++) {
        if (list[i].items[k].isclicked) {
          if (list[i].iscustom) {
            custom_array.add(list[i].items[k].id);
          } else {
            user_tags_array.add(list[i].items[k].id);
          }
        }
      }
    }
    var obj = {};
    obj['custom_tags'] = custom_array;
    obj['user_tags'] = user_tags_array;
    print("delete ids " + obj.toString());
    var response = await http.post(
        "${Urls.baseUrl}${Urls.removeUserTag}?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=100&locale=en",
        headers: {
          "Authorization": "Bearer ${userToken}",
          "Content-Type": "application/json"
        },
        body: json.encode({"tag_id": obj}));
    print("response delete " + response.body.toString());
    Map<String, dynamic> value = json.decode(response.body);
    if (response.statusCode == 200) {
      dialog.dismissProgressDialog(context);
      if (value['success']) {
        list_count = 0;
        list.clear();
        getCategoryLists();
      }
    } else {
      dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(
        content: Text("Try Again..", style: TextStyle(color: Colors.white)),
        backgroundColor: Colors.red,
      );
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  void getRecommendedList() async {
    var name;
    var response = await http
        .get("${Urls.baseUrl}${Urls.recomentedList}?locale="+lan, headers: {
      "Authorization": "Bearer ${userToken}",
      "Accept": "application/json"
    });
    Map<String, dynamic> value = json.decode(response.body);
    print("resp_recommend " + response.body);
    if (response.statusCode == 200) {
      try {
        var array = value['data'];
        for (int i = 0; i < array.length; i++) {
          var obj = array[i];
          if(obj['translated'] != null){
            name = obj['translated']['name'];
          }else{
            name = obj['name'];
          }
          var thumbs = obj['thumbs'];
          recommended_list.add(RecommendedListItemModel(obj['id'], obj['slug'],
              name, thumbs['md'], obj['tag_category_id'], false));
        }
        setState(() {
          recommended_list_count = recommended_list.length;
        });
      } catch (e) {
        e.toString();
      }
    }
  }

  void addToList() async {
    var id = [];
    for (int i = 0; i < recommended_list.length; i++) {
      if (recommended_list[i].isclicked) {
        id.add(recommended_list[i].id);
        recommended_list[i].isclicked = false;
      }
    }
    var response = await http.post(
        "${Urls.baseUrl}${Urls.addUserTag}?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=100&locale="+lan,
        headers: {
          "Authorization": "Bearer ${userToken}",
          "Content-Type": "application/json"
        },
        body: json.encode({"tag_ids": id}));
    Map<String, dynamic> value = json.decode(response.body);
    print("resp_Add " + response.body);
    if (response.statusCode == 200) {
      if (value['success']) {
        list_count = 0;
        list.clear();
        getCategoryLists();
        Navigator.pop(context);
      } else {
        final snackBar = SnackBar(
          content: Text(
            "Failed",
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: Colors.red,
        );
        _scaffoldKey.currentState.showSnackBar(snackBar);
      }
    } else {
      final snackBar = SnackBar(
        content: Text(
          "Try Again",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.red,
      );
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  void addToListSingleItem(String id) async {
    var ids = [];
    ids.add(id);
    print("ids " + id);
    var response = await http.post(
        "${Urls.baseUrl}${Urls.addUserTag}?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=100&locale="+lan,
        headers: {
          "Authorization": "Bearer ${userToken}",
          "Content-Type": "application/json"
        },
        body: json.encode({"tag_ids": ids}));
    Map<String, dynamic> value = json.decode(response.body);
    print("resp_Add " + response.body);
    if (response.statusCode == 200) {
      if (value['success']) {
        list_count = 0;
        list.clear();
        getCategoryLists();
        Navigator.pop(context);
      } else {
        final snackBar = SnackBar(
          content: Text(
            "Failed",
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: Colors.red,
        );
        _scaffoldKey.currentState.showSnackBar(snackBar);
      }
    } else {
      final snackBar = SnackBar(
        content: Text(
          "Try Again",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.red,
      );
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  Future<List<AutoCompleteModel>> getAutoComplete(String keyword) async {
    auto_list_count = 0;
    auto_cmplt_list.clear();
    var response = await http.get(
        "${Urls.baseUrl}${Urls.GetItemAutoComplete}?locale="+lan+"&s=" + keyword,
        headers: {
          "Authorization": "Bearer ${userToken}",
          "Content-Type": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print("resp_auto " + response.body);
    if (response.statusCode == 200) {
      try {
        var data = value['data'];
        var array = data['tags'];
        if (array.length > 0) {
          for (int i = 0; i < array.length; i++) {
            var obj = array[i];
            auto_cmplt_list.add(AutoCompleteModel(
                obj['id'], obj['slug'], obj['name'], obj['icon'], "", false));
          }
        } else {
          auto_cmplt_list.add(AutoCompleteModel("", "", keyword, "", "", true));
        }

        setState(() {
          auto_list_count = auto_cmplt_list.length;
        });
      } catch (e) {
        e.toString();
      }
    }else{
      print("resp_code "+response.statusCode.toString());
    }

    return auto_cmplt_list;
  }

  Future<void> addCustomTags(String customTag) async {
    var response = await http.post(
        "${Urls.baseUrl}${Urls.CustomUserTags}?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=100&locale="+lan+"&name=" +
            customTag,
        headers: {
          "Authorization": "Bearer ${userToken}",
          "Content-Type": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print("custom_resp " + response.body);
    if (response.statusCode == 200) {
      try {
        if (value['success']) {
          list_count = 0;
          list.clear();
          getCategoryLists();
          Navigator.pop(context);
        } else {
          final snackBar = SnackBar(
            content: Text(
              "Failed",
              style: TextStyle(color: Colors.white),
            ),
            backgroundColor: Colors.red,
          );
          _scaffoldKey.currentState.showSnackBar(snackBar);
        }
      } catch (e) {}
    } else {
      final snackBar = SnackBar(
        content: Text(
          "Try Again",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.red,
      );
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }
}

/*floatingActionButton: new FloatingActionButton(
        onPressed: () {
          PopupCategory(context);

        },
        child: const Icon(
          Icons.add,
        ),
      ), */
