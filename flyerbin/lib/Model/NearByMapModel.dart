class NearByMapModel {
  var id,
      owner_id,
      brand_id,
      region_id,
      country_id,
      mall_id,
      name,
      logo,
      original_logo,
      address,
      latitude,
      longitude;

  NearByMapModel(
      this.id,
      this.owner_id,
      this.brand_id,
      this.region_id,
      this.country_id,
      this.mall_id,
      this.name,
      this.logo,
      this.original_logo,
      this.address,
      this.latitude,
      this.longitude,);
}
