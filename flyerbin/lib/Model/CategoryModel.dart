class CategoryModel {
  String name,
      imagepath,
      slug,
      is_tab;

  CategoryModel(
      this.name,
      this.imagepath,
      this.slug,
      this.is_tab);
}