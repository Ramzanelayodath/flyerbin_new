

class OffersInEachItemModel{
 var  id,flyer_page_id,category_id,type,name,description,url,sku_no,sales_details,offer_details,valid_from,valid_to,
     offer_description,item_path,flyer_id,flyer_slug,brand_id_or_shop_id,brand_name_or_shop_name,isClipped,brand_logo,price;

 OffersInEachItemModel(
      this.id,
      this.flyer_page_id,
      this.category_id,
      this.type,
      this.name,
      this.description,
      this.url,
      this.sku_no,
      this.sales_details,
      this.offer_details,
      this.valid_from,
      this.valid_to,
      this.offer_description,
      this.item_path,
      this.flyer_id,
      this.flyer_slug,
      this.brand_id_or_shop_id,
      this.brand_name_or_shop_name,
      this.isClipped,
      this.brand_logo,
      this.price);
}