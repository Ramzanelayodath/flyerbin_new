
class NearbyListitemModel {
  var id,
      shop_id,
      brand_id,
      name,
      valid_from,
      valid_to,
      favourite_count,
      image_path,
      isFavorited;

  NearbyListitemModel(
      this.id,
      this.shop_id,
      this.brand_id,
      this.name,
      this.valid_from,
      this.valid_to,
      this.favourite_count,
      this.image_path,
      this.isFavorited,
   );
}
