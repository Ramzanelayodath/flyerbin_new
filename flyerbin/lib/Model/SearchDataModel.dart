import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:flyerbin/Utils/Constants.dart';
import 'package:flyerbin/Utils/Urls.dart';

import 'package:http/http.dart' as http;

class SearchDataModel{
  var item_name,itemImage,itemType,hasSlug,slug,id;

  SearchDataModel(this.item_name, this.itemImage, this.itemType, this.hasSlug,
      this.slug, this.id);

  /*factory SearchDataModel.fromJson(Map<String, dynamic> parsedJson) {
    return SearchDataModel(
        item_name: parsedJson['name'] as String,
        itemImage: parsedJson['thumbs']['md'] as String,
        itemType: Constants.BRAND,
        hasSlug: "false",
        slug: parsedJson['slug'].isNull?"":parsedJson['slug'] as String,
        id: parsedJson['id'] as String
    );
  }*/

}

class SearchViewModel {
  static List<SearchDataModel> search_data;

  static Future loadPlayers(String keyword) async {
    try {
      search_data = new List<SearchDataModel>();
      var response = await http.get(
        "${Urls.baseUrl}${Urls.searchAutoComplt}?s=${keyword}&locale=en",
        headers: {"Content-Type": "application/json",
          //"Authorization": "Bearer ${userToken}",
          "Accept": "application/json"},
        /* body: json.encode({
          "email": usermail,
          "password": userpass,
        })*/);
      print("seaech " +response.body.toString());
      Map<String, dynamic> value = json.decode(response.body);
      var brands = value['data']['brands'] as List;
      var tags = value['data']['tags'] as List;
      for (int i = 0; i < brands.length; i++) {
        search_data.add(new SearchDataModel(brands[i]['name'].toString(),
                       " ",
                       Constants.BRAND.toString(),
                       "false",
                        "",
                        brands[i]['id'].toString()
                        ));
      }
      for (int i = 0; i < tags.length; i++) {
        search_data.add(new SearchDataModel(tags[i]['name'].toString(),
            " ",
            Constants.TAGS.toString(),
            "false",
            tags[i]['slug'].toString(),
            tags[i]['id'].toString()
        ));
      }
      return List.generate(search_data.length, (index) {
        return {'name': search_data[index].item_name, 'slug': search_data[index].slug};
      });
    } catch (e) {
      print(e);
    }
  }

  static List<SearchDataModel> getSuggestions(String query) {
    List<SearchDataModel> matches = List();
    matches.addAll(search_data);

    matches.retainWhere((s) => s.item_name.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}