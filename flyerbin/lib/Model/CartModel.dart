


import 'CartInsideModel.dart';

class CartModel{
  var id,brandname,logo;
  List<CartInsideModel> inside_items;

  CartModel(this.id, this.brandname, this.logo, this.inside_items);
}