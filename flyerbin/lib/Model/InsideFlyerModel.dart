

class InsideFlyerModel{
  var id ,flyer_page_id,category_id,type,name,description,url,
      sku_no,sales_details,offer_details,valid_from, valid_to,
      offer_description,view_count,cart_entry_count,item_path ,lyer_id,isClipped ,price,slug,brand_logo;

  InsideFlyerModel(
      this.id,
      this.flyer_page_id,
      this.category_id,
      this.type,
      this.name,
      this.description,
      this.url,
      this.sku_no,
      this.sales_details,
      this.offer_details,
      this.valid_from,
      this.valid_to,
      this.offer_description,
      this.view_count,
      this.cart_entry_count,
      this.item_path,
      this.lyer_id,
      this.isClipped,
      this.price,
      this.slug,
      this.brand_logo);
}