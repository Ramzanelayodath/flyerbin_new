
import 'dart:convert';

CategoryItemModel categoryItemModelFromJson(String str) =>
    CategoryItemModel.fromJson(json.decode(str));

String categoryItemModelToJson(CategoryItemModel data) =>
    json.encode(data.toJson());
class CategoryItemModel {
  CategoryItemModel({
    this.success,
    this.message,
    this.data,
  });

  bool success;
  String message;
  Data data;

  factory CategoryItemModel.fromJson(Map<String, dynamic> json) =>
      CategoryItemModel(
        success: json["success"],
        message: json["message"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
    "success": success,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    this.customTags,
    this.userTags,
  });

  List<CustomTag> customTags;
  List<UserTag> userTags;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    customTags: List<CustomTag>.from(
        json["custom_tags"].map((x) => CustomTag.fromJson(x))),
    userTags: List<UserTag>.from(
        json["user_tags"].map((x) => UserTag.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "custom_tags": List<dynamic>.from(customTags.map((x) => x.toJson())),
    "user_tags": List<dynamic>.from(userTags.map((x) => x.toJson())),
  };
}

class CustomTag {
  CustomTag({
    this.id,
    this.userId,
    this.name,
    this.deletedAt,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  int userId;
  String name;
  dynamic deletedAt;
  DateTime createdAt;
  DateTime updatedAt;

  factory CustomTag.fromJson(Map<String, dynamic> json) => CustomTag(
    id: json["id"],
    userId: json["user_id"],
    name: json["name"],
    deletedAt: json["deleted_at"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "user_id": userId,
    "name": name,
    "deleted_at": deletedAt,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };
}

class UserTag {
  UserTag({
    this.id,
    this.name,
    this.slug,
    this.icon,
    this.thumbs,
    this.createdAt,
    this.updatedAt,
    this.tags,
    this.translated,
  });

  int id;
  String name;
  String slug;
  String icon;
  ThumbsClass thumbs;
  DateTime createdAt;
  DateTime updatedAt;
  List<Tag> tags;
  dynamic translated;

  factory UserTag.fromJson(Map<String, dynamic> json) => UserTag(
    id: json["id"],
    name: json["name"],
    slug: json["slug"],
    icon: json["icon"],
    thumbs: ThumbsClass.fromJson(json["thumbs"]),
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    tags: List<Tag>.from(json["tags"].map((x) => Tag.fromJson(x))),
    translated: json["translated"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "slug": slug,
    "icon": icon,
    "thumbs": thumbs.toJson(),
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "tags": List<dynamic>.from(tags.map((x) => x.toJson())),
    "translated": translated,
  };
}

class Tag {
  Tag({
    this.id,
    this.slug,
    this.name,
    this.icon,
    this.thumbs,
    this.tagCategoryId,
    this.createdAt,
    this.updatedAt,
    this.flyerItemsCount,
    this.translated,
  });

  int id;
  String slug;
  String name;
  String icon;
  dynamic thumbs;
  int tagCategoryId;
  DateTime createdAt;
  DateTime updatedAt;
  int flyerItemsCount;
  dynamic translated;

  factory Tag.fromJson(Map<String, dynamic> json) => Tag(
    id: json["id"],
    slug: json["slug"],
    name: json["name"],
    icon: json["icon"],
    thumbs: json["thumbs"],
    tagCategoryId: json["tag_category_id"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    flyerItemsCount: json["flyer_items_count"],
    translated: json["translated"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "slug": slug,
    "name": name,
    "icon": icon,
    "thumbs": thumbs,
    "tag_category_id": tagCategoryId,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "flyer_items_count": flyerItemsCount,
    "translated": translated,
  };
}

class ThumbsClass {
  ThumbsClass({
    this.lg,
    this.md,
    this.sm,
    this.xs,
  });

  String lg;
  String md;
  String sm;
  String xs;

  factory ThumbsClass.fromJson(Map<String, dynamic> json) => ThumbsClass(
    lg: json["lg"],
    md: json["md"],
    sm: json["sm"],
    xs: json["xs"],
  );

  Map<String, dynamic> toJson() => {
    "lg": lg,
    "md": md,
    "sm": sm,
    "xs": xs,
  };
}

