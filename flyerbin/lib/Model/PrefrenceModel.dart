
class PrefrenceModel{
  var id,description,valid_from,valid_to,thumb_path,status,isFavourite,logo,name,categories_count,slug,type,isClipped,
      url,price,flyer_page_id,flyer_id,shop_name,shop_logo,brand_name,brand_logo;


  PrefrenceModel(
      this.id,
      this.description,
      this.valid_from,
      this.valid_to,
      this.thumb_path,
      this.status,
      this.isFavourite,
      this.logo,
      this.name,
      this.categories_count,
      this.slug,
      this.type,
      this.isClipped,
      this.url,
      this.price,
      this.flyer_page_id,
      this.flyer_id,
      this.shop_name,
      this.shop_logo,
      this.brand_name,this.brand_logo);
}