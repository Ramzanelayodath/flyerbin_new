

class SimiliarProdcutModel{
  var id,brand_id,brand_logo,brand_name,description,flyer_id,flyer_page_id,flyer_slug,image_path,isClipped,
      item_path,name,price,sales_details,shop_id,shop_logo,shop_name,type,url,validto,valid_from;

  SimiliarProdcutModel(
      this.id,
      this.brand_id,
      this.brand_logo,
      this.brand_name,
      this.description,
      this.flyer_id,
      this.flyer_page_id,
      this.flyer_slug,
      this.image_path,
      this.isClipped,
      this.item_path,
      this.name,
      this.price,
      this.sales_details,
      this.shop_id,
      this.shop_logo,
      this.shop_name,
      this.type,
      this.url,
      this.validto,
      this.valid_from);
}