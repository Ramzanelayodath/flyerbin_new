

class VoucherModel{
  var id,name,date,user_id,image,amount,barcode,shop_id,status,shop,expiry,coupon_status;

  VoucherModel(this.id, this.name, this.date, this.user_id, this.image,
      this.amount, this.barcode, this.shop_id, this.status, this.shop,this.expiry,this.coupon_status);

   /* coupon_status
      1- pending
      2- claimed
      3- approved
      4 - upcoming */
}