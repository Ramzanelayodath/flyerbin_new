

class NearbyMallListItemModel {
  var id,
      name,
      original_logo,
      address,
      pincode,
      latitude,
      longitude;

  NearbyMallListItemModel(
      this.id,
      this.name,
      this.original_logo,
      this.address,
      this.pincode,
      this.latitude,
      this.longitude,);
}
