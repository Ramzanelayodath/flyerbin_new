class FavouriteShopPreferenceModel {
  String id,
      name,
      logo,
      is_favourite;
  var isclicked;

  FavouriteShopPreferenceModel(
      this.id,
      this.name,
      this.logo,
      this.is_favourite,
      this.isclicked);
}