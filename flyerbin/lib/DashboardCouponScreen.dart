import 'dart:convert';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';

import 'ActiveVoucher.dart';
import 'Cart.dart';
import 'Model/FoodCopunModel.dart';
import 'Model/VoucherModel.dart';
import 'Model/VoucherShopModel.dart';
import 'Model/WinnersModel.dart';
import 'SideDrawerScreen.dart';
import 'Utils/Constants.dart';
import 'Utils/CustomDialogs.dart';
import 'Utils/SharedPrefrence.dart';
import 'Utils/Urls.dart';
import 'package:http/http.dart' as http;
import 'package:share/share.dart';

class DashboardCouponScreen extends StatefulWidget {
  @override
  _DashboardCouponScreenState createState() => _DashboardCouponScreenState();
}

class _DashboardCouponScreenState extends State<DashboardCouponScreen>with TickerProviderStateMixin {
  final List<FoodCopunModel> foodcoupnList = List();
  final List<WinnersModel> winnersList = List();
  final List<VoucherModel> voucherList = List();
  final List<VoucherShopModel> shops = List();
  String userToken,lan ='en';
  String referralCode = '' ,totalPoints ='0',selectedShopId='';
  VoucherShopModel selectedShop;
  final DateFormat formatter = DateFormat('yyyy-MM-dd');
  final DateFormat formatter2 = DateFormat('MMM-dd');
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  TabController _controller;
  bool isshowFloatingButton = false,isActiveVouchers = false;
  //List<String> shops = ['abc','gjjg','fjfjjf'];
  int winnner_list_count = 0,voucher_list_count = 0;
  String _scanBarcode = 'Unknown';
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = TabController(vsync: this, length: 2);
    _controller.addListener(updateIndex);
    Future lng = SharedPrefrence().getLanguage();
    lng.then((data) async {
      lan = data;
    });
    Future token = SharedPrefrence().getToken();
    token.then((data) async {
      userToken = data;
      getFoodCopuns();
      getPoints();
      getWinners();
      getVouchers();
      getVoucherShops();

    });
    Future referal = SharedPrefrence().getreferralCode();
    referal.then((data) async {
      setState(() {
        referralCode = data;
      });
    });
  }
  @override
  void dispose() {
    _controller.removeListener(updateIndex);
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      home: DefaultTabController(
        length :2,
        child: Scaffold(
          key:_scaffoldKey,
          resizeToAvoidBottomInset: false,
          drawer: SideDraweScreen(),
          appBar: AppBar(
            bottom: TabBar(
              controller: _controller,
              indicatorColor: Colors.blue,
              labelColor: Colors.blue,
              unselectedLabelColor: Colors.black,
              tabs: [
                Tab(text: 'Coupons'),
                Tab(text: 'Voucher'),

              ],
            ),
            iconTheme: new IconThemeData(color: Color.fromRGBO(34, 83, 148, 1)),
            title: appbarLogo(),
            backgroundColor: Colors.white,

            actions: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child:GestureDetector(
                  child:  Icon(
                    Icons.shopping_cart,
                    color: Color(0xff00ADEE),
                  ),
                  onTap: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Cart(),
                      ),
                    );
                  },
                ),
              )
            ],
          ),
          body:DoubleBackToCloseApp(
              snackBar: const SnackBar(
                content: Text('Tap back again to exit'),
              ),
              child:  TabBarView(
                controller: _controller,
              children: [
                coupon(),
                Voucher()
              ],
            ),
          ),
          floatingActionButton: isshowFloatingButton == true ? Padding(
            padding: EdgeInsets.only(bottom: 50),
            child:  FloatingActionButton.extended(
              label: Text('Refer Friend',style: TextStyle(color: Colors.black),),
              backgroundColor: Colors.white70,
              foregroundColor: Colors.white70,
              onPressed: () => {
                openRefferDialog()
              },
            ),
          ) : null,
          floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        ),
      ),
    );
  }

  Widget appbarLogo() {
    return SvgPicture.asset(
      "assets/images/flyerbin_logo.svg",
      height: 40,
    );
  }

  Widget coupon(){
    return foodcoupnList.length !=0 ? GridView.count(
      crossAxisCount: 2,
      crossAxisSpacing: 10.0,
      mainAxisSpacing: 10.0,
      childAspectRatio: 7.0 / 9.0,
      shrinkWrap: true,
      children: List.generate(
        foodcoupnList.length,
            (index) {
          FoodCopunModel data = foodcoupnList[index];
          return Stack(
            children: [
              GestureDetector(
                onTap: () {
                  CouponDetails(context,data);
                },
                child: Card(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Expanded(
                        flex: 0,
                        child: Container(
                          height: 120,
                          width: 160,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: NetworkImage(data.image.toString()),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 0,
                        child: Text(
                          data.title,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 15),
                        ),
                      ),
                      Expanded(
                        flex: 0,
                        child: Text(
                          data.descritpion,
                          softWrap: true,
                          maxLines: 5,
                          style: TextStyle(
                              fontSize: 11.5, color: Colors.blueGrey),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              data.isclipped ? Padding(
                padding: const EdgeInsets.all(7.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      height: 20,
                      width: 50,
                      child:
                      Text(
                        "Clipped",
                        style: TextStyle(color: Colors.white),
                      )
                      ,
                      color: Colors.green,
                    )
                  ],
                ),
              ):Container(),
            ],
          );
        },
      ),
    ):SizedBox(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Column(

        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("Your have no coupons yet"),
          Padding(
            padding: EdgeInsets.only(top: 10),
            child: Image.asset('assets/images/no_coupons.png'),
          ),
        ],
      ),
    );
  }

  Widget Voucher(){
    return SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(8),
            child: Container(
              width: double.infinity,
              // height: 250,
              child: Column(
                children: [
                  SizedBox(height: 15,),
                  Text(totalPoints,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 50,color: Colors.black),),
                  Text('POINTS',style: TextStyle(color: Colors.grey,fontSize: 18),),
                  SizedBox(height: 2,),
                  Text('USER ID '+referralCode,style: TextStyle(fontSize: 18),),
                  SizedBox(height: 8,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center ,
                    crossAxisAlignment: CrossAxisAlignment.center ,
                    children: [
                      Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                color: isActiveVouchers ? Colors.green : Colors.black,
                              ),
                              borderRadius: BorderRadius.all(Radius.circular(20)),
                              color: isActiveVouchers ? Colors.green : Colors.black
                          ),
                          height: 35,
                          child: Padding(
                            padding: EdgeInsets.all(8),
                            child: GestureDetector(
                              child: Text('Active Vouchers',style: TextStyle(color: Colors.white),),
                              onTap: (){
                                 if(isActiveVouchers){
                                   var activeVoucher = List<VoucherModel>();
                                   for(int i =0 ; i< voucherList.length ; i++){
                                     if(voucherList[i].coupon_status ==3){
                                       activeVoucher.add(voucherList[i]);
                                     }
                                   }
                                   Navigator.push(
                                     context,
                                     MaterialPageRoute(
                                       builder: (context) => ActiveVoucher(activeVoucher),
                                     ),
                                   );
                                 }else{
                                   openRefferDialog();
                                 }
                              },
                            ),
                          )
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      GestureDetector(
                        onTap: (){
                          scanBarcodeNormal();
                       },
                        child:Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.blue,
                              ),
                              borderRadius: BorderRadius.all(Radius.circular(20)),
                              color: Colors.blue
                          ),
                          height: 35,
                          width: 65,
                          child: Padding(
                            padding: EdgeInsets.all(8),
                            child: Icon(Icons.camera_alt,color: Colors.white,size: 16,),),
                        ),
                      ),


                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.all(8),
                    child: Divider(
                      color: Colors.grey,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Text('Last week winners'),
                    ),
                  ),
                   Container(
                      // margin: EdgeInsets.symmetric(vertical: 20.0),
                       width: double.infinity,
                       height: 125,
                       child: ListView.builder(
                           itemCount: winnner_list_count,
                           shrinkWrap: true,
                           scrollDirection: Axis.horizontal,
                           itemBuilder: (context, index) {
                             return  Padding(
                               padding: EdgeInsets.all(2),
                               child: Column(
                                 children: [
                                   Container(
                                     width: 95.0,
                                     height: 95.0,
                                     decoration: new BoxDecoration(
                                       color: Colors.blue,
                                       border: Border.all(
                                           color: Colors.blue
                                       ),
                                       shape: BoxShape.circle,
                                     ),
                                     child: Column(
                                       mainAxisAlignment: MainAxisAlignment.center,
                                       crossAxisAlignment: CrossAxisAlignment.center,
                                       children: [
                                         Text(winnersList[index].amount+' AED',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 16),)

                                       ],
                                     ),
                                   ),
                                   SizedBox(height: 5,),
                                   Text(winnersList[index].name)
                                 ],
                               ),
                             );
                           })
                   ),
                  Padding(
                    padding: EdgeInsets.all(8),
                    child: Divider(
                      color: Colors.grey,
                    ),
                  ),
                  GridView.count(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    crossAxisCount: 2 ,
                    childAspectRatio: 1.0,
                    padding: const EdgeInsets.all(4.0),
                    mainAxisSpacing: 4.0,
                    crossAxisSpacing: 4.0,
                    children: List.generate(voucher_list_count,(index){
                      return GestureDetector(
                        child: Container(
                            child: Card(
                              elevation: 4,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(15),
                                    topRight: Radius.circular(15),
                                    bottomLeft: Radius.circular(15),
                                    topLeft:  Radius.circular(15)),),
                              child: ClipPath(
                                child: Container(
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: AssetImage("assets/images/voucher_back.png"),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  height: 100,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Align(
                                        child:  Container(
                                          width : 100,
                                          height :18,
                                          decoration: BoxDecoration(
                                            image: DecorationImage(
                                              image: AssetImage("assets/images/copoun_back_sticker.png"),
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                          child: Padding(
                                            padding: EdgeInsets.all(2),
                                            child:  Text(voucherList[index].coupon_status == 1 ?'Select Shop' : voucherList[index].coupon_status == 2 ? 'Waiting for approval' :
                                            voucherList[index].coupon_status == 3 ? 'Approved' :  'Available on '+voucherList[index].date  ,style: TextStyle(color: Colors.white,fontSize: 10),),
                                          ),
                                        ),
                                        alignment: Alignment.topLeft,
                                      ),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center ,
                                        crossAxisAlignment: CrossAxisAlignment.center ,
                                        children: [
                                          new RotatedBox(
                                            quarterTurns: 3,
                                            child: Padding(
                                              padding: EdgeInsets.only(top: 2),
                                              child: Text('AED',style: TextStyle(fontSize: 20,color: Colors.white,fontWeight: FontWeight.bold),),
                                            ),
                                          ),
                                          Text(voucherList[index].amount,style: TextStyle(fontSize: 70,color: Colors.white,fontWeight: FontWeight.bold),),
                                        ],
                                      ),
                                      SizedBox(height: 10,),
                                      Align(
                                        alignment: Alignment.center,
                                        child:  Text(voucherList[index].coupon_status == 1 ? 'Select your favourite shop' :
                                        voucherList[index].coupon_status == 2 ? 'This coupon waiting for approval' :
                                        voucherList[index].coupon_status == 3 ? 'This coupon was approved':'Refer 7 friends and get'+voucherList[index].amount+'AED',style: TextStyle(color: Colors.white,fontSize: 10),),
                                      )
                                    ],
                                  ),

                                ),
                                clipper: ShapeBorderClipper(shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(4))),
                              ),
                            )
                        ),
                        onTap: (){
                          if(voucherList[index].coupon_status == 1){
                             openVoucherDialog(voucherList[index].id);

                          }
                        },
                      );
                    }),
                  )
                ],
              ),
            ),
          ),

        ],
      ),
    );
  }

  Widget CouponDetails(BuildContext context,FoodCopunModel data) {
    showGeneralDialog(
        context: context,
        barrierDismissible: true,
        barrierLabel:
        MaterialLocalizations.of(context).modalBarrierDismissLabel,
        barrierColor: Colors.black45,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (BuildContext buildContext, Animation animation,
            Animation secondaryAnimation) {
          return StatefulBuilder(builder: (context, setState) {
            return Center(
              child: Card(
                  elevation: 6,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        // width: MediaQuery.of(context).size.width - 10,
                        // height: MediaQuery.of(context).size.height - 380,
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.white,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(5),
                                  child: GestureDetector(
                                      onTap: () {
                                        Navigator.pop(context);
                                      },
                                      child: Icon(
                                        Icons.cancel,
                                        color: Colors.blueGrey,
                                      )),
                                )
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.all(25),
                              child: Container(
                                height: 200,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      height: 100,
                                      width: 150,
                                      child: Image.network(
                                        data.image.toString(),
                                        fit: BoxFit.fitWidth,
                                      ),
                                    ),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Expanded(
                                          flex: 0,
                                          child: Text(
                                            data.title,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 15),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 0,
                                          child: Container(
                                            height: 90,
                                            width: 100,
                                            child: Text(
                                              data.descritpion,
                                              maxLines: 5,
                                              style: TextStyle(
                                                  fontSize: 11.5,
                                                  color: Colors.black),
                                            ),
                                          ),
                                        ),
                                        Padding(padding: EdgeInsets.only(left: 5),
                                          child: data.isclipped ? GestureDetector(
                                            child: Container(
                                              height: 25,
                                              decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.all(Radius.circular(20)),
                                                  color: Colors.red
                                              ),
                                              child: Row(
                                                children: [
                                                  Padding(
                                                    padding: EdgeInsets.only(left: 5),
                                                    child: Icon(Icons.attachment,color: Colors.white,size: 20,),
                                                  ),
                                                  Padding(padding: EdgeInsets.only(right: 5),
                                                    child: Text('Un Clip',style: TextStyle(color: Colors.white),),)
                                                ],
                                              ),
                                            ),
                                            onTap: (){
                                              doclipUnclip(data, false);
                                            },
                                          ) : GestureDetector(
                                            child: Container(
                                              height: 25,
                                              decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.all(Radius.circular(20)),
                                                  color: Colors.blue
                                              ),
                                              child: Row(
                                                children: [
                                                  Padding(
                                                    padding: EdgeInsets.only(left: 5),
                                                    child: Icon(Icons.attachment,color: Colors.white,size: 20,),
                                                  ),
                                                  Padding(padding: EdgeInsets.only(right: 5),
                                                    child: Text('Clip',style: TextStyle(color: Colors.white),),)
                                                ],
                                              ),
                                            ),
                                            onTap: (){
                                              doclipUnclip(data, true);
                                            },
                                          ),)                                ],
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  )
              ),
            );
          });
        });
  }

  void getFoodCopuns() async {
    foodcoupnList.clear();
    var response = await http.get(
        "${Urls.baseUrl}${Urls.Coupons}?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=100&locale="+lan+"&page=1",
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print("resp_foodcoupn " + response.body);
    if (response.statusCode == 200) {
      try {
        var data = value['data'];
        var array = data['data'];
        for (int i = 0; i < array.length; i++) {
          var obj = array[i];
          var shop_obj = obj["shop"];
          print("Image link " + obj['image']);
          foodcoupnList.add(FoodCopunModel(
              obj['id'],
              obj['shop_id'],
              obj['title'],
              obj['description'],
              Urls.baseImageUrl + obj['image'],
              obj['view_count'],
              formatter2.format(formatter.parse(obj['valid_from'])),
              formatter2.format(formatter.parse(obj['valid_to'])),
              obj['type'],
              obj['isClipped'],
              Urls.baseImageUrl + shop_obj['logo'],
              shop_obj['name'],
              shop_obj['latitude'],
              shop_obj['longitude']));
          setState(() {
            print(foodcoupnList.length);
          });
        }
      } catch (e) {
        e.toString();
      }
    }
  }

  void doclipUnclip(FoodCopunModel model,bool isclipped)async{
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var response = await http.post(
        "${Urls.baseUrl}${Urls.clipUnclip}?coupon_id="+model.id.toString(),
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    if(response.statusCode == 200){
      dialog.dismissProgressDialog(context);
      print("resp_copun_clip "+response.body);
      var data = value['data'];
      if(data['isClipped']){
        Future token = SharedPrefrence().getCartCount();
        token.then((data) async {
          SharedPrefrence().setCartCount(data +1);
        });
        setState(() {
          model.isclipped = true;
        });
        Navigator.pop(context);
        final snackBar = SnackBar(content: Text("Clipped",style: TextStyle(color: Colors.white)),backgroundColor: Colors.green,);
        _scaffoldKey.currentState.showSnackBar(snackBar);

      }else{
        Future token = SharedPrefrence().getCartCount();
        token.then((data) async {
          if(data>0){
            SharedPrefrence().setCartCount(data  - 1);
          }
        });
        setState(() {
          model.isclipped = false;
        });
        Navigator.pop(context);
        final snackBar = SnackBar(content: Text("Un clipped",style: TextStyle(color: Colors.white)),backgroundColor: Colors.green,);
        _scaffoldKey.currentState.showSnackBar(snackBar);

      }
    }
    else{
      dialog.dismissProgressDialog(context);
      print(response.body);
    }
  }


  void updateIndex() {
    setState(() {
      if(_controller.index == 1){
        isshowFloatingButton = true;
      }else{
        isshowFloatingButton = false;
      }
    });
  }

  void openRefferDialog(){
    showDialog(
      context: context,
      builder: (_) => Material(
        type: MaterialType.transparency,
        child: Center(
          // Aligns the container to center
          child: Container(
            // A simplified version of dialog.
            width: double.infinity,
            height: double.infinity,
            color: Colors.transparent,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Align(
                  alignment: Alignment.topRight,
                  child: IconButton(
                    icon: Icon(Icons.close,color: Colors.white,),
                    onPressed: (){
                      Navigator.pop(context);
                    },
                  ),
                ),
                SizedBox(height: 20,),
                Padding(
                  padding: EdgeInsets.all(10),
                  child: Container(
                      width: 250,
                      height: 450,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(
                            color: Colors.white,
                          ),
                          image: DecorationImage(
                            image: AssetImage("assets/images/refer_back.png"),
                            fit: BoxFit.cover,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(20))
                      ),
                      child: Column(
                        children: [
                          SizedBox(height: 50,),
                          Padding(
                            padding: EdgeInsets.only(left: 25),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Text('Weekly winners get vouchers',style: TextStyle(fontSize: 17,color: Colors.grey),),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 25,top: 8),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Text('Earn Points!!',style: TextStyle(fontSize: 35,color: Colors.black,fontWeight: FontWeight.bold),),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 25,top: 2),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Text('How?',style: TextStyle(fontSize: 35,color: Colors.black,fontWeight: FontWeight.bold),),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 25,top: 8),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Row(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(right: 8),
                                    child: Icon(Icons.share,color: Colors.black54,),
                                  ),
                                  Text('Refer app earn 100 points',style: TextStyle(fontSize: 15),)
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 25,top: 15),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Row(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(right: 8),
                                    child: Icon(Icons.person_sharp,color: Colors.black54,),
                                  ),
                                  Text('Complete profile earn 50 points',style: TextStyle(fontSize: 15),)
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 25,top: 15),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Row(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(right: 8),
                                    child: Icon(Icons.camera_alt_sharp,color: Colors.black54,),
                                  ),
                                  Text('Capture your shopping bill',style: TextStyle(fontSize: 15),)
                                ],
                              ),
                            ),
                          ),
                        ],
                      )
                  ),
                ),
                SizedBox(height: 10,),
                Padding(
                  padding: EdgeInsets.only(left: 75,right: 75),
                  child:  Container(
                      width : 35,
                      height: 45,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(
                            color: Colors.white,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(20))
                      ),
                      child: Center(
                        child:  GestureDetector(
                          child: Text('Refer Friend',style: TextStyle(fontSize: 16),),
                          onTap: (){
                            getReferralLink();
                          },
                        ),
                      )
                  ),
                )


              ],
            ),
          ),
        ),
      ),
    );
  }

  void openVoucherDialog(String copounId){
    showDialog(
      context: context,
      builder: (_) => StatefulBuilder(
        builder: (context,setState){
          return Material(
            type: MaterialType.transparency,
            child: Center(
// Aligns the container to center
              child: Container(
// A simplified version of dialog.
                width: double.infinity,
                height: double.infinity,
                color: Colors.transparent,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Align(
                      alignment: Alignment.topRight,
                      child: IconButton(
                        icon: Icon(Icons.close,color: Colors.white,),
                        onPressed: (){
                          Navigator.pop(context);
                        },
                      ),
                    ),
                    SizedBox(height: 20,),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: Container(
                          width: 250,
                          height: 380,
                          child: Container(
                            width: 250,
                            height: 380,
                            decoration: BoxDecoration(
                                color: Colors.lightBlue[900],
                                border: Border.all(
                                  color: Colors.transparent,
                                ),
                                image: DecorationImage(
                                  image: AssetImage("assets/images/voucher_dialog_back.png"),
                                  fit: BoxFit.cover,
                                ),
                                borderRadius: BorderRadius.all(Radius.circular(20))
                            ),
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 75,
                                ),
                                Text('You won',style: TextStyle(fontSize: 65,color: Colors.white,fontWeight: FontWeight.normal),),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center ,
                                  children: [
                                    new RotatedBox(
                                      quarterTurns: 3,
                                      child: Padding(
                                        padding: EdgeInsets.only(top: 2),
                                        child: Text('AED',style: TextStyle(fontSize: 30,color: Colors.white,fontWeight: FontWeight.normal),),
                                      ),
                                    ),
                                    Text('109',style: TextStyle(fontSize: 90,color: Colors.white,fontWeight: FontWeight.bold),),
                                  ],
                                ),
                                Text('Request your shop',style: TextStyle(fontSize: 18,color: Colors.white,fontWeight: FontWeight.normal),),
                                Text('to redeem the vouchers',style: TextStyle(fontSize: 18,color: Colors.white,fontWeight: FontWeight.normal),),
                                SizedBox(height: 20,),
                                Center(
                                  child:
                                  DropdownButtonHideUnderline(
                                      child: ButtonTheme(
                                          alignedDropdown: false,
                                          child: new DropdownButton<VoucherShopModel>(
                                            hint: new Text(
                                              "Select your shop",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(color: Colors.black),
                                            ),
                                            value: selectedShop,
                                            isExpanded: false,
                                            iconSize: 30,
                                            style: new TextStyle(
                                              color: Colors.white,
                                            ),
                                            onChanged: (VoucherShopModel pt) {
                                              setState(() {
                                                selectedShop = pt;
                                                print("Selected user " + selectedShop.id);
                                                selectedShopId = selectedShop.id;
                                              });
                                            },
                                            items: shops.map((VoucherShopModel p) {
                                              return new DropdownMenuItem<VoucherShopModel>(
                                                value: p,
                                                child: new Text(
                                                  p.name,
                                                  textAlign: TextAlign.center,
                                                  style: new TextStyle(color: Colors.black),
                                                ),
                                              );
                                            }).toList(),
                                          )
                                      )
                                  ),
                                ),
                              ],
                            ),
                          )
                      ),
                    ),
                    Align(
                      alignment: Alignment.center,
                      child:  Text('Congratulation!',style: TextStyle(fontSize: 25,color: Colors.white,fontWeight: FontWeight.normal),),
                    ),
                    SizedBox(height: 3,),
                    Align(
                      alignment: Alignment.center,
                      child:  Text('Reach out to your friends and earn more',style: TextStyle(fontSize: 18,color: Colors.white,fontWeight: FontWeight.normal),),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 75,right: 75,top: 8),
                      child:  GestureDetector(
                        onTap: (){
                          openVoucherRequestConfirmDialog();
                        },
                        child: GestureDetector(
                          child: Container(
                              width : 35,
                              height: 45,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    color: Colors.white,
                                  ),
                                  borderRadius: BorderRadius.all(Radius.circular(20))
                              ),
                              child: Center(
                                child:  Text('Send request',style: TextStyle(fontSize: 16),),
                              )
                          ),
                          onTap: (){
                            if(selectedShopId !=""){
                              claimVoucher(selectedShopId, copounId);
                            }else{
                              showDialog(context: context, child:
                              new AlertDialog(
                                title: new Text("Warning"),
                                content: new Text("Select Your Favourite Shop"),
                                actions: [
                                  FlatButton(child: Text('OK'),onPressed: (){
                                    Navigator.pop(context);
                                  },)
                                ],
                              )
                              );
                            }

                          },
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  void openVoucherRequestConfirmDialog(){
    showDialog(
      context: context,
      builder: (_) => Material(
        type: MaterialType.transparency,
        child: Center(
          // Aligns the container to center
          child: Container(
            // A simplified version of dialog.
            width: 250,
            height: 280,
            color: Colors.white70,
            child: Column(
              children: [
                SizedBox(height: 25,),
                Image.asset("assets/images/alert.png"),
                Padding(
                  padding: EdgeInsets.only(top: 8,right: 8,left: 8),
                  child: Text('once you choose your shop it',style: TextStyle(fontSize: 12),),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 8,right: 8),
                  child: Text('cannot be change',style: TextStyle(fontSize: 12)),
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                  child: Divider(color: Colors.black54,),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 8,right: 8,left: 8),
                  child: Text('Dont share your voucher ID with anyone',style: TextStyle(fontSize: 12)),
                ),
                SizedBox(height: 15,),
                Container(
                    width : 120,
                    height: 45,
                    decoration: BoxDecoration(
                        color: Colors.blue,
                        border: Border.all(
                          color: Colors.blue,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(20))
                    ),
                    child: Center(
                      child:  Text('Confirm',style: TextStyle(fontSize: 16,color: Colors.white),),
                    )
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void getPoints()async{
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var response = await http.get(
        "${Urls.baseUrl}${Urls.Get_Points}",
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    if(response.statusCode == 200){
      dialog.dismissProgressDialog(context);
      print("resp_points "+response.body);
      try{
         setState(() {
           totalPoints = value['data']['points'].toString();
         });
      }catch(e){}
    }else{
      dialog.dismissProgressDialog(context);
    }

  }

  void getWinners()async{
    setState(() {
      winnersList.clear();
      winnner_list_count = 0;
    });
    var response = await http.get(
        "${Urls.baseUrl}${Urls.Get_Last_Week_Winners}",
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    if(response.statusCode == 200){
      print("resp_winners "+response.body);
      try{
          for(int i =0 ; i < value['data'].length ; i++){
            var obj = value['data'][i];
            winnersList.add(WinnersModel(obj['name'].toString(), obj['points'].toString(), obj['amount'].toString()));
          }
          setState(() {
            winnner_list_count = winnersList.length;
          });
      }catch(e){}
    }else{

    }

  }

  void getVouchers()async{

    setState(() {
      voucher_list_count = 0;
      voucherList.clear();
    });
    var response = await http.get(
        "${Urls.baseUrl}${Urls.Get_Vouchers}",
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    if(response.statusCode == 200){
      print("resp_vouchers "+response.body);
      try{
            var obj = value['data'];
            var pending_array = obj ['pending'];
            var claimed_array = obj['claimed'];
            var approved_array = obj['approved'];
            var upcoming_array = obj['upcoming'];
            for(int k = 0; k < pending_array.length ; k++){
              var obj = pending_array[k];

              voucherList.add(VoucherModel(obj['id'].toString(),obj['name'].toString(),formatter2.format(formatter.parse(obj['date'])),obj['user_id'].toString(),
                  obj['image'].toString(),obj['amount'].toString(),obj['barcode_image'].toString(),obj['shop_id'].toString(),obj['status'],
                  "","",1));
            }
            for(int k =0 ; k < claimed_array.length; k++){
              var obj = claimed_array[k];
              voucherList.add(VoucherModel(obj['id'].toString(),obj['name'].toString(),formatter2.format(formatter.parse(obj['date'])),obj['user_id'].toString(),
                  obj['image'].toString(),obj['amount'].toString(),obj['barcode_image'].toString(),obj['shop_id'].toString(),obj['status'],
                 "",obj['expiry'],2));
            }

            for(int k = 0; k < approved_array.length; k++){
              var obj = approved_array[k];
              var shopname = obj['shop']['name'];
              voucherList.add(VoucherModel(obj['id'].toString(),obj['name'].toString(),formatter2.format(formatter.parse(obj['date'])),obj['user_id'].toString(),
                  obj['image'].toString(),obj['amount'].toString(),obj['barcode_image'].toString(),obj['shop_id'].toString(),obj['status'],
                  shopname,obj['expiry'],3));
            }

            for(int k = 0; k < upcoming_array.length; k++){
              var obj = upcoming_array[k];
              voucherList.add(VoucherModel(obj['id'].toString(),obj['name'].toString(),formatter2.format(formatter.parse(obj['date'])),obj['user_id'].toString(),
                  obj['image'].toString(),obj['amount'].toString(),obj['barcode_image'].toString(),obj['shop_id'].toString(),obj['status'],
                  "","",4));
            }

            setState(() {
              if(approved_array.length >0){
                isActiveVouchers = true;
              }else{
                isActiveVouchers = false;
              }
              voucher_list_count = voucherList.length;
            });
      }catch(e){
        e.toString();
      }
    }else{

    }

  }

  void getVoucherShops()async{
    shops.clear();
    var response = await http.get(
        "${Urls.baseUrl}${Urls.Get_Voucher_Shops}?latitude=${Constants.latitude}&longitude=${Constants.longitude}",
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    if(response.statusCode == 200){
      print("resp_shop "+response.body);
      try{
          for(int i =0 ; i < value['data'].length ; i++){
            var obj = value['data'][i];
            shops.add(VoucherShopModel(obj['id'].toString(), obj['name'].toString(), obj['distance'].toString()));
          }
      }catch(e){
        e.toString();
      }
    }
  }

  void claimVoucher(String shopId,String copounId) async{
    print("shopid "+shopId+" copunid "+copounId);
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var response = await http.post(
        "${Urls.baseUrl}${Urls.Claim_Vouchers}${copounId}/claim?shop_id="+shopId,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print("resp_claim_voucher "+response.body);
    print("resp_claim_status "+response.statusCode.toString());
    if(response.statusCode == 200){
      dialog.dismissProgressDialog(context);
     try{
       final snackBar = SnackBar(content: Text(value['message'],style: TextStyle(color: Colors.white)),backgroundColor: Colors.green,);
       _scaffoldKey.currentState.showSnackBar(snackBar);
        Navigator.pop(context);
        getVouchers();
     }catch(e){}

    }else{
      dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(content: Text('Try Again',style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  void getReferralLink()async{
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var response = await http.get(
        "${Urls.baseUrl}${Urls.Get_Referral_Link}",
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    if(response.statusCode == 200){
      dialog.dismissProgressDialog(context);
      print("referral_link "+response.body);
      try{
        Share.share(value['data']['referral_link']);
      }catch(e){}
    }else{
      dialog.dismissProgressDialog(context);
    }
  }

  void UploadShoppingBill(String barcode) async{
    // print("shopid "+shopId+" copunid "+copounId);
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var response = await http.post(
        "${Urls.baseUrl}${Urls.Upload_shopping_bill}?barcode="+barcode,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print("resp_claim_voucher "+response.body);
    print("resp_claim_status "+response.statusCode.toString());
    if(response.statusCode == 200){
      dialog.dismissProgressDialog(context);
      try{
        final snackBar = SnackBar(content: Text(value['message'],style: TextStyle(color: Colors.white)),backgroundColor: Colors.green,);
        _scaffoldKey.currentState.showSnackBar(snackBar);
        Navigator.pop(context);
        getVouchers();
      }catch(e){}

    }else{
      dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(content: Text(value['message'],style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

//scan barcode
  Future<void> scanBarcodeNormal() async {
    String barcodeScanRes;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          "#ff6666", "Cancel", true, ScanMode.BARCODE);
      print(barcodeScanRes);
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _scanBarcode = barcodeScanRes;
      print("barcode "+_scanBarcode);
      if(_scanBarcode=="-1"||_scanBarcode=="0"){

      }
      else{
        UploadShoppingBill(_scanBarcode);
      }

    });
  }

}

