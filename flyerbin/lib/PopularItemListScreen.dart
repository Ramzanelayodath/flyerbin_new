import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_paginator/enums.dart';
import 'package:flutter_paginator/flutter_paginator.dart';
import 'package:http/http.dart' as http;
import 'package:share/share.dart';
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher.dart';

import 'Model/PrefrenceModel.dart';
import 'SimiliarProductScreen.dart';
import 'Utils/Constants.dart';
import 'Utils/CustomDialogs.dart';
import 'Utils/SharedPrefrence.dart';
import 'Utils/Urls.dart';

class PopularItemListScreen extends StatefulWidget {
  String search_slug,title;
  PopularItemListScreen(this.search_slug,this.title);
  @override
  _PopularItemListScreenState createState() => _PopularItemListScreenState(this.search_slug,this.title);
}

class _PopularItemListScreenState extends State<PopularItemListScreen> {
  String search_slug,title;
  String userToken;
  bool loading=true;
  Timer timer;
  bool show_similiar_prdouct = false;
  String related_image = "",related_image_2 = "",lan ='en';
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  List<PrefrenceModel> search_result_list = [];
  _PopularItemListScreenState(this.search_slug,this.title);

  void startTimer() {
    // Start the periodic timer which prints something every 1 seconds
    timer = new Timer(new Duration(seconds: 3), () {
      setState(() {
        loading = false;
        timer.cancel();
      });
    });
  }

  void initState(){
    super.initState();
    Future lng = SharedPrefrence().getLanguage();
    lng.then((data) async {
      lan = data;
    });
    Future token = SharedPrefrence().getToken();
    token.then((data) async {
      userToken = data;
      //getRecommendedItems();
      getSearchResult();

    });

  }



  @override
  Widget build(BuildContext context) {
    startTimer();
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          title,
          style: TextStyle(fontSize: 15, color: Colors.lightBlue),
        ),
        automaticallyImplyLeading: true,
        iconTheme: IconThemeData(
          color: Colors.lightBlue,
        ),
        backgroundColor: Colors.white,
      ),
      body: /*SingleChildScrollView(
        child: Column(
          children: [
            _PopularAreaItemList(),
          ],
        ),
      ),*/
      _PopularAreaItemList(),
    );
  }
  Widget _PopularAreaItemList() {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Container(
        child: GridView.count(
          crossAxisCount: 2 ,
          childAspectRatio: (260 / 370),
          crossAxisSpacing: 4.0,
          mainAxisSpacing: 4.0,
          shrinkWrap: true,
          physics: ScrollPhysics(),
          children: List.generate(search_result_list.length,(index){
            PrefrenceModel model = search_result_list[index];
            return  loading?
            Shimmer.fromColors(
            baseColor: Colors.grey[200],
            highlightColor: Colors.grey,
            child: Padding(
            padding: const EdgeInsets.all(5.0),
            child: Container(
            width: 260,
            height: 230,
            decoration: BoxDecoration(

            color: Color(0xFFFFF29A),
            shape: BoxShape.rectangle,
            /*borderRadius: BorderRadius.all(
                                Radius.circular(15),
                              ),*/
            ),

            ),
            ),
            ):Card(
              elevation: 6,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
              child: Column(
                children: [
                  Expanded(
                    flex: 0,
                    child: GestureDetector(
                      onTap: () {
                        ClipFoodCoupnAlert(context,model);
                      },
                      child: Container(
                        height: 230,
                        width: 260,

                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              flex: 0,
                              child: Container(
                                height: 170,
                                width: 260,
                                child: Image.network(
                                  Urls.baseImageUrl+search_result_list[index].thumb_path,
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Text(
                                  search_result_list[index].name,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 13),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Text(
                                  search_result_list[index].price + " AED",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      color: Colors.lightBlue,
                                      fontSize: 14),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          }),
        ),
      ),
    );
  }

  Future<void> PopularItemView(BuildContext context) async{
    await showGeneralDialog(
        context: context,
        barrierDismissible: true,
        barrierLabel: MaterialLocalizations.of(context)
            .modalBarrierDismissLabel,
        barrierColor: Colors.black45,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (BuildContext buildContext,
            Animation animation,
            Animation secondaryAnimation) {
          return StatefulBuilder(
            builder: (context,setState){
              return Center(
                child: Card(
                  elevation: 6,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  child: Container(
                    width: MediaQuery.of(context).size.width - 10,
                    height: MediaQuery.of(context).size.height -  75,
                    padding: EdgeInsets.all(15),
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0),
                      color: Colors.white,
                  ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: Container(
                                  child: Image.network(
                                    'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
                                    fit: BoxFit.fitWidth,
                                    width: 70,
                                    height: 25,),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: GestureDetector(onTap: (){
                                  Navigator.pop(context);
                                },child: Icon(Icons.cancel,color: Colors.blueGrey,)),
                              ),
                            ),
                          ],
                        ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Expanded(
                          flex: 0,
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Text(
                              "11/9/2020",
                              style: TextStyle(
                                color: Colors.blueGrey,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 13),
                            ),
                          ),
                        ),
                      ],
                    ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Expanded(
                              flex: 0,
                              child: GestureDetector(
                                onTap: () {
                                  PopularItemView(context);
                                },
                                child: Container(
                                  height: 250,
                                  width: 320,

                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),

                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Expanded(
                                        flex: 0,
                                        child: Container(
                                          height: 190,
                                          width: 320,
                                          child: Image.network(
                                            'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
                                            fit: BoxFit.fitWidth,
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 0,
                                        child: Padding(
                                          padding: const EdgeInsets.all(4.0),
                                          child: Text(
                                            "Mango product",
                                            style: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                fontSize: 15),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 0,
                                        child: Padding(
                                          padding: const EdgeInsets.all(4.0),
                                          child: Text(
                                            "10.5" + "AED",
                                            style: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                color: Colors.lightBlue,
                                                fontSize: 15),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Expanded(
                          flex: 0,
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Text(
                              "Related Items",
                              style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  color: Colors.blueGrey,
                                  fontSize: 15),
                            ),
                          ),
                        ),
                        SingleChildScrollView(
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Expanded(
                                    flex: 0,
                                    child: Container(
                                      height: 70,
                                      width: 70,
                                      child: Image.network(
                                        'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
                                        fit: BoxFit.fitHeight,
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 0,
                                    child: Container(
                                      height: 70,
                                      width: 70,
                                      child: Image.network(
                                        'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
                                        fit: BoxFit.fitHeight,
                                      ),
                                    ),
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        flex: 0,
                                        child: Padding(
                                          padding: const EdgeInsets.all(4.0),
                                          child: Text(
                                            "See More >>>",
                                            style: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                color: Colors.blueGrey,
                                                fontSize: 15),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 0,
                                        child: Container(
                                          height: 30,
                                          width: 140,
                                          child: RaisedButton.icon(
                                            onPressed: (){ print('Button Clicked.'); },
                                            shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.all(Radius.circular(5.0))),
                                            label: Text('View Flyer',
                                              style: TextStyle(color: Colors.white,fontSize: 11),),
                                            icon: Icon(Icons.visibility, color:Colors.white,),
                                            textColor: Colors.white,
                                            splashColor: Colors.red,
                                            color: Colors.lightBlue,),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 40,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              height: 40,
                              width: 150,
                              color: Colors.yellow,
                              child: RaisedButton.icon(
                                onPressed: (){ print('Button Clicked.'); },
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(5.0))),
                                label: Text('Share',
                                  style: TextStyle(color: Colors.white),),
                                icon: Icon(Icons.share, color:Colors.white,),
                                textColor: Colors.white,
                                splashColor: Colors.red,
                                color: Colors.lightBlue,),
                            ),

                            Container(
                              height: 40,
                              width: 150,
                              child: RaisedButton.icon(
                                onPressed: (){ print('Clip Button cliecked'); },
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(5.0))),
                                label: Text('Clip',
                                  style: TextStyle(color: Colors.white),),
                                icon: Icon(Icons.shopping_cart, color:Colors.white,),
                                textColor: Colors.white,
                                splashColor: Colors.red,
                                color: Colors.lightBlue,),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              );
            }
          );
        });

  }

 /* Future<CountriesData> sendCountriesDataRequest(int page) async {
    try {
      String url = Uri.encodeFull(
          "${Urls.baseUrl}${Urls.CategorySearch}?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=100&locale=en&keyword=${search_slug}&page=${page}&type=item");
      print(url);
      http.Response response = await http.get(url,headers: {"Content-Type": "application/json",
    "Authorization": "Bearer ${userToken}",
    "Accept" : "application/json"});
      return CountriesData.fromResponse(response);
    } catch (e) {
      if (e is IOException) {
        return CountriesData.withError(
            'Please check your internet connection.');
      } else {
        print(e.toString());
        return CountriesData.withError('Something went wrong.');
      }
    }
  }*/

  void getSearchResult() async {
   // ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);

    var response = await http.get("${Urls.baseUrl}${Urls.CategorySearch}?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=100&locale="+lan+"&keyword=${search_slug}&page=${1}&type=item",
      headers: {"Content-Type": "application/json",
        "Authorization": "Bearer ${userToken}",
        "Accept" : "application/json"},
      /* body: json.encode({
          "email": usermail,
          "password": userpass,
        })*/);

    Map<String, dynamic> value = json.decode(response.body);
    if (response.statusCode == 200) {
      //dialog.dismissProgressDialog(context);
      try {
        print("response search " + response.body.toString());
        Map<String, dynamic> value = json.decode(response.body);

        var status = value['success'];
        var message = value['message'];
        print(message);
        if (status == true) {
          search_result_list.clear();
          try {
            var data = value['data']['data'];
            if (data.length > 0) {
              for (int i = 0; i < data.length; i++) {
                var obj = data[i];
                //var petrol_obj = obj['petrolstation'];
                search_result_list.add(PrefrenceModel(
                    obj['id'].toString(),
                    obj['description'].toString(),
                    obj['valid_from'].toString(),
                    obj['valid_to'].toString(),
                    obj['thumbs']['md'].toString(),
                    obj['status'].toString(),
                    "",
                    "",
                    obj['name'].toString(),
                    "",
                    obj['flyer_slug'].toString(),
                    obj['type'].toString(),
                    obj['isClipped'].toString(),
                    obj['url'].toString(),
                    obj['price'].toString(),
                    obj['flyer_page_id'].toString(),
                    obj['id'].toString(),obj['id'].toString(),obj['id'].toString(),
                  obj['brand_name'].toString(),
                  obj['brand_logo'].toString(),
                ));
              }
              setState(() {
                /*list_count = pending_fuel_list.length;
                print(list_count);*/
                print(search_result_list.length);
              });
            }
            else {
              final snackBar = SnackBar(content: Text("No Data Available"));
              _scaffoldKey.currentState.showSnackBar(snackBar);
            }
          } catch (e) {
            e.toString();
          }

        }
        else {
          print("Error...");
          final snackBar = SnackBar(content: Text(message));
          _scaffoldKey.currentState.showSnackBar(snackBar);
        }
      } catch (e) {
        print(e.toString());
      }
    } else if (response.statusCode == 404) {
     // dialog.dismissProgressDialog(context);
      var message = value['message'];
      final snackBar = SnackBar(content: Text(message));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    } else if (response.statusCode == 208) {
     // dialog.dismissProgressDialog(context);
    } else {
      var message = value['message'];
      //dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(content: Text(message));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  Future<void> ClipFoodCoupnAlert(BuildContext context,PrefrenceModel data) async{
    getSimiliarProduct(data.id.toString());
    await showGeneralDialog(
        context: context,
        barrierDismissible: true,
        barrierLabel: MaterialLocalizations.of(context)
            .modalBarrierDismissLabel,
        barrierColor: Colors.black45,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (BuildContext buildContext,
            Animation animation,
            Animation secondaryAnimation) {
          return StatefulBuilder(
              builder: (context,setState){
                return Center(
                  child: Card(
                    elevation: 6,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                         //width: MediaQuery.of(context).size.width - 10,
                          //height: MediaQuery.of(context).size.height -  110,
                          padding: EdgeInsets.all(0),
                          decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0),
                            color: Colors.white,
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(5),
                                    child: Container(
                                      child:Row(
                                        children: [
                                          Image.network(
                                            " ",
                                            fit: BoxFit.fitWidth,
                                            width: 70,
                                            height: 25,),

                                        ],
                                      ),
                                    ),
                                  ),

                                  Padding(
                                    padding: const EdgeInsets.all(5),
                                    child: GestureDetector(onTap: (){
                                      Navigator.pop(context);
                                    },child: Icon(Icons.cancel,color: Colors.blueGrey,)),
                                  )
                                ],
                              ),
                              Align(
                                alignment: Alignment.topRight,
                                child: Padding(
                                  padding: EdgeInsets.only(right: 8,top: 8),
                                  child: Text('Valid To : '+data.valid_to),
                                ),
                              ),
                              Divider(),
                              Padding(
                                padding: EdgeInsets.all(8),
                                child: Container(
                                  height: 250,
                                  width: 300,
                                  child: Image.network(
                                    Urls.baseImageUrl+data.thumb_path,
                                    fit: BoxFit.fitWidth,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  data.name,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 22),
                                ),
                              ),

                              Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Text(
                                  data.price.toString()+" AED",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.lightBlue,
                                      fontSize: 25),
                                ),
                              ),
                              Visibility(
                                child: Padding(padding: EdgeInsets.all(8),child:  Row(
                                  children: [
                                    Image.network(related_image,width: 50,height: 50,),
                                    Padding(padding: EdgeInsets.only(left: 5,right: 5),
                                      child: Image.network(related_image_2,width: 50,height: 50,),),
                                    GestureDetector(
                                      child:  Column(
                                        children: [
                                          Text('See More >>',style: TextStyle(color: Colors.blue,fontWeight: FontWeight.bold),),
                                          Container(
                                              decoration: BoxDecoration(border: Border.all(color: Colors.green,),
                                                  borderRadius: BorderRadius.all(Radius.circular(20)),color: Colors.green),
                                              child: Padding(padding: EdgeInsets.all(2),
                                                child: Row(
                                                  children: [
                                                    Text('View Flyer',style: TextStyle(color: Colors.white,fontSize: 12),)
                                                  ],
                                                ),)
                                          )
                                        ],
                                      ),
                                      onTap: (){
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => SimiliarProductScreen(data.id.toString()),
                                          ),
                                        );
                                      },
                                    )
                                  ],
                                ),),
                                visible: show_similiar_prdouct,
                              ),
                              Container(
                                width: double.infinity,
                                height: 55,
                                child:Row(
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: Padding(
                                        padding: EdgeInsets.only(right: 2),
                                        child: GestureDetector(
                                          child: Container(
                                            width: double.infinity,
                                            height: 55,
                                            color: Colors.blue,
                                            child: Center(
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                    flex: 1,
                                                    child: Icon(Icons.ios_share,color: Colors.white,size: 20,),
                                                  ),
                                                  Expanded(
                                                    flex: 1,
                                                    child: Text('Share',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 16),),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                          onTap: (){
                                            share(data);
                                          },
                                        ),
                                      ),
                                    ),
                                    Visibility(
                                      visible: data.type == "offline" ||  data.type == "both",
                                      child:  Expanded(
                                        flex: 1,
                                        child: Padding(
                                          padding: EdgeInsets.only(right: 2),
                                          child: GestureDetector(
                                            child: Container(
                                                width: double.infinity,
                                                height: 55,
                                                color: Colors.blue,
                                                child: Center(
                                                  child: Row(
                                                    children: [
                                                      Expanded(
                                                        flex: 1,
                                                        child: Icon(Icons.shopping_bag_outlined,color: Colors.white,size: 20,),
                                                      ),
                                                      Expanded(
                                                        flex: 1,
                                                        child: Text(data.isClipped ? 'Unclip' :'Clip',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 16),),
                                                      )
                                                    ],
                                                  ),
                                                )
                                            ),
                                            onTap: (){
                                              doclipUnclip(data);
                                            },
                                          ),
                                        ),
                                      ),
                                    ),
                                    Visibility(
                                        visible: data.type == "both",
                                        child: Expanded(
                                          flex: 1,
                                          child: Padding(
                                            padding: EdgeInsets.only(right: 2),
                                            child: GestureDetector(
                                              child: Container(
                                                  width: double.infinity,
                                                  height: 55,
                                                  color: Colors.blue,
                                                  child: Center(
                                                    child: Row(
                                                      children: [
                                                        Expanded(
                                                          flex: 1,
                                                          child: Icon(Icons.shopping_basket,color: Colors.white,size: 20,),
                                                        ),
                                                        Expanded(
                                                          flex: 1,
                                                          child: Text('Buy Now',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 16),),
                                                        )
                                                      ],
                                                    ),
                                                  )
                                              ),
                                              onTap: (){
                                                openUrl(data.url);
                                              },
                                            ),
                                          ),
                                        )
                                    ),
                                    Visibility(
                                      visible: data.type == "link",
                                      child:  Expanded(
                                        flex: 1,
                                        child: Padding(
                                          padding: EdgeInsets.only(right: 2),
                                          child: GestureDetector(
                                            child: Container(
                                                width: double.infinity,
                                                height: 55,
                                                color: Colors.blue,
                                                child: Center(
                                                  child: Row(
                                                    children: [
                                                      Expanded(
                                                        flex: 1,
                                                        child: Icon(Icons.public,color: Colors.white,size: 20,),
                                                      ),
                                                      Expanded(
                                                        flex: 1,
                                                        child: Text('Visit',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 16),),
                                                      )
                                                    ],
                                                  ),
                                                )
                                            ),
                                            onTap: (){
                                              openUrl(data.url);
                                            },
                                          ),
                                        ),
                                      ),

                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    )
                  ),
                );
              }
          );
        });

  }

  void doclipUnclip(PrefrenceModel model)async{
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var response = await http.post(
        "${Urls.baseUrl}${Urls.RemoveFlyerFromCart}?cartitem_id="+model.id.toString(),
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    if(response.statusCode == 200){
      dialog.dismissProgressDialog(context);
      print("resp_copun_clip "+response.body);
      var data = value['data'];
      if(data['isClipped']){
        Future token = SharedPrefrence().getCartCount();
        token.then((data) async {
          SharedPrefrence().setCartCount(data +1);
        });
        setState(() {
          model.isClipped = true;
        });
        Navigator.pop(context);
        final snackBar = SnackBar(content: Text("Clipped",style: TextStyle(color: Colors.white)),backgroundColor: Colors.green,);
        _scaffoldKey.currentState.showSnackBar(snackBar);

      }else{
        Future token = SharedPrefrence().getCartCount();
        token.then((data) async {
          if(data>0){
            SharedPrefrence().setCartCount(data  - 1);
          }
        });
        setState(() {
          model.isClipped = false;
        });
        Navigator.pop(context);
        final snackBar = SnackBar(content: Text("Un clipped",style: TextStyle(color: Colors.white)),backgroundColor: Colors.green,);
        _scaffoldKey.currentState.showSnackBar(snackBar);

      }
    }
    else{
      dialog.dismissProgressDialog(context);
      print(response.body);
    }
  }

  void share(PrefrenceModel model){
    var shareMessage = Urls.baseurl+"flyer/"+model.slug+"?item="+model.id.toString();
    Share.share(shareMessage, subject: 'Flyerbin');
  }

  void openUrl(String url) async{
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void getSimiliarProduct(String id) async{
    var response = await http.get(
        "${Urls.baseUrl}${Urls.SimiliarProducts}?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=100&&count=20&locale="+lan+"&page=1&id="+id,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print("resp_similiar "+response.body);
    if(response.statusCode == 200){
      try{
        var array = value['data'];
        if(array.length >0){
          setState(() {
            show_similiar_prdouct = true;
            related_image = Urls.baseImageUrl+array[0]['item_path'].toString();
            related_image_2 =  Urls.baseImageUrl+array[1]['item_path'].toString();
          });
        }else{
          setState(() {
            show_similiar_prdouct = false;
          });
        }
      }catch(e){
        e.toString();
      }

    }
  }

}
