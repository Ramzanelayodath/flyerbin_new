
import 'dart:convert';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;
import 'package:webview_flutter/webview_flutter.dart';

import 'Cart.dart';
import 'Model/InsideFlyerModel.dart';
import 'SimiliarProductScreen.dart';
import 'Utils/Constants.dart';
import 'Utils/CustomDialogs.dart';
import 'Utils/SharedPrefrence.dart';
import 'Utils/Urls.dart';


class FlyerLoadScreen extends StatefulWidget {
  String flyer_id,shopname,valid_from,valid_to,company_logo,item_id;
  bool isfromCartPage = false;
  FlyerLoadScreen(this.flyer_id,this.shopname,this.valid_from,this.valid_to,this.company_logo,this.isfromCartPage,this.item_id);
  @override
  FlyerLoadScreenState createState() => FlyerLoadScreenState(this.flyer_id,this.shopname,this.valid_from,this.valid_to,this.company_logo,this.isfromCartPage,this.item_id);
}

class FlyerLoadScreenState extends State<FlyerLoadScreen>{
 String flyer_id,shopname,valid_from,valid_to,company_logo,item_id;
  WebViewController _controller;
 bool isLoading=true,isfromCartPage = false;
 final _scaffoldKey = GlobalKey<ScaffoldState>();
 String userToken;
 var list = List<InsideFlyerModel>();
 var list_count = 0;
 final DateFormat formatter = DateFormat('yyyy-MM-dd');
 final DateFormat formatter2 = DateFormat('dd-MM-yyyy');
 String related_image = "",related_image_2 = "",lan = 'en';
 bool show_similiar_prdouct = false;
 //InAppWebViewController webView;
 String url="";
 double progress = 0;
  FlyerLoadScreenState(this.flyer_id,this.shopname,this.valid_from,this.valid_to,this.company_logo,this.isfromCartPage,this.item_id);
 //var webView_flutter = FlutterWebviewPlugin();
 Set<JavascriptChannel> jsChannels;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future lng = SharedPrefrence().getLanguage();
    lng.then((data) async {
      lan = data;
    });
    Future token = SharedPrefrence().getToken();
    token.then((data) async {
      userToken = data;
      url = '${Urls.baseUrl}flyer/${flyer_id}';
      print("link "+url);
      //getFlyersInCategory(id, data,slug);
      //  getTab();

    });
    //_webViewHandler();
    if(isfromCartPage){
      getSingleItem(item_id);
    }
  }

  @override
  Widget build(BuildContext context) {
   // webView_flutter.show();
    /*jsChannels = [
      JavascriptChannel(
          name: 'Flutter',
          onMessageReceived: (JavascriptMessage message) {
            print(message.message);
            getSingleItem(message.message);
            //webView_flutter.hide();

          }),
    ].toSet();*/
    return Scaffold(
      resizeToAvoidBottomInset: false,
      key: _scaffoldKey,
      appBar: AppBar(
        centerTitle: false,
        iconTheme: new IconThemeData(color: Color(0xff00ADEE)),
        title: Text(
          shopname+"\n"
              "VALID "+valid_from+" to "+valid_to,
          style: TextStyle(color: Color(0xff00ADEE), fontSize: 13),
        ),
        leading:
        GestureDetector(
          onTap: (){
            Navigator.pop(context,true);
          },
          child:Icon(Icons.arrow_back,color: Color(0xff00ADEE),),
        ),
        backgroundColor: Colors.white,
        actions: [

          Padding(
            padding: const EdgeInsets.all(8.0),
            child: GestureDetector(
               child:  Icon(
                 Icons.shopping_cart,
                 color: Color(0xff00ADEE),
               ),
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Cart(),
                  ),
                );
              },
            ),
          )
        ],
      ),
      body: Stack(
        children: [
              WebView(
                initialUrl: '${Urls.baseUrl}flyer/${flyer_id}',
                javascriptMode: JavascriptMode.unrestricted,
                javascriptChannels: Set.from([
                  JavascriptChannel(
                      name: "Flutter",
                      onMessageReceived: (JavascriptMessage message) {
                        getSingleItem(message.message);
                      }),
                  // we can have more than one channels
                ]),
                onWebViewCreated: (WebViewController webViewController) {
                  _controller = webViewController;
                 // _controller.loadUrl('${Urls.baseUrl}flyer/${flyer_id}');
                },
                onPageFinished: (finish)  {
                  setState(() {
                    isLoading = false;
                  });
                },
                gestureNavigationEnabled: true,

              ),
          /*InAppWebView(
            initialUrl: '${Urls.baseUrl}flyer/${flyer_id}',
            initialHeaders: {},
            initialOptions: InAppWebViewGroupOptions(
                crossPlatform: InAppWebViewOptions(
                  javaScriptCanOpenWindowsAutomatically: true,
                  debuggingEnabled: true,
                  javaScriptEnabled: true,
                  useShouldOverrideUrlLoading: true,
                  useOnLoadResource: true,
                  cacheEnabled: true,
                )),
        onWebViewCreated: (InAppWebViewController controller) {
          webView = controller;
          //webView.evaluateJavascript(source: "Flutter");
          webView.addJavaScriptHandler(
              handlerName: "Flutter",
              callback: (arguments) async {
                print('hello from test');
                print(arguments.length);
                print(arguments);
              });
        },
        onLoadStart: (InAppWebViewController controller, String url) {
          setState(() {
            this.url = url;
          });
        },
        onLoadStop: (InAppWebViewController controller, String url) async {
          setState(() {
            this.url = url;
            isLoading = false;

          });
        },
        onProgressChanged: (InAppWebViewController controller, int progress) {
          setState(() {
            this.progress = progress / 100;

          });
        },
      ),*/

          isLoading ? Center( child: CircularProgressIndicator(),)
              : Stack(),
            ],
      ));
   /* return Scaffold(
      resizeToAvoidBottomInset: false,
      key: _scaffoldKey,
      appBar: AppBar(
        centerTitle: false,
        iconTheme: new IconThemeData(color: Color(0xff00ADEE)),
        title: Text(
          shopname+"\n"
              "VALID "+valid_from+" to "+valid_to,
          style: TextStyle(color: Color(0xff00ADEE), fontSize: 13),
        ),
        leading:
        GestureDetector(
          onTap: (){
            Navigator.pop(context,true);
          },
          child:Icon(Icons.arrow_back,color: Color(0xff00ADEE),),
        ),
        backgroundColor: Colors.white,
        actions: [

          Padding(
            padding: const EdgeInsets.all(8.0),
            child: GestureDetector(
              child:  Icon(
                Icons.shopping_cart,
                color: Color(0xff00ADEE),
              ),
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Cart(),
                  ),
                );
              },
            ),
          )
        ],
      ),
      body: WebviewScaffold(
        url: '${Urls.baseUrl}flyer/${flyer_id}',
        javascriptChannels: jsChannels,
        withJavascript: true,
        withZoom: true,
        hidden: true,
        initialChild: Container(
          child:  Center(
            child: CircularProgressIndicator(),
          ),
        ),
      ),
    );*/
}

 void getSingleItem(String id)async{
   ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
   var slugooo,brandlogo;
   print( "${Urls.baseUrl}${Urls.GetSingleItem}${id}?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=100&locale="+lan);
   var response = await http.get(
       "${Urls.baseUrl}${Urls.GetSingleItem}${id}?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=100&locale="+lan,
       headers: {
         "Content-Type": "application/json",
         "Authorization": "Bearer ${userToken}",
         "Accept": "application/json"
       });
   Map<String, dynamic> value = json.decode(response.body);
   print("resp_catflyer "+response.body);
   if(response.statusCode == 200){
     list.clear();
     dialog.dismissProgressDialog(context);
     try{
       //var data = value['items'];
      // slugooo = value['slug'];
      // brandlogo = value['brand']['thumbs']['md'];
       //for(int i = 0; i < data.length ; i++){
        // var obj = data[i];
         list.add(InsideFlyerModel(value['id'],value['flyer_page_id'],value['category_id'],
             value['type'],value['name'],value['description'],value['url'],value["sku_no"],
             value['sales_details'],value['offer_details'],formatter2.format(formatter.parse(value['valid_from'])),formatter2.format(formatter.parse(value['valid_to'])),
             value['offer_description'],value['view_count'],value['cart_entry_count'],value['thumbs']['md'],
             value['flyer_id'],value['isClipped'],value['price'],"",""));
       //}
     }catch(e){
       print(e.toString());
     }
     setState(() {
       list_count = list.length;
       ClipFoodCoupnAlert(_scaffoldKey.currentContext,list[0]);
      // webView_flutter.hide();
     });

   }else{
     final snackBar = SnackBar(content: Text("Try Again",style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
     _scaffoldKey.currentState.showSnackBar(snackBar);
     dialog.dismissProgressDialog(context);
   }

 }

 Future<void> ClipFoodCoupnAlert(BuildContext context,InsideFlyerModel data) async{
   getSimiliarProduct(data.id.toString());
   await showGeneralDialog(
       context: context,
       barrierDismissible: true,
       barrierLabel: MaterialLocalizations.of(context)
           .modalBarrierDismissLabel,
       barrierColor: Colors.black45,
       transitionDuration: const Duration(milliseconds: 200),
       pageBuilder: (BuildContext buildContext,
           Animation animation,
           Animation secondaryAnimation) {
         return StatefulBuilder(
             builder: (context,setState){
               return Center(
                 child: Card(
                   elevation: 6,
                   shape: RoundedRectangleBorder(
                     borderRadius: BorderRadius.circular(5.0),
                   ),

                   child: Column(
                     mainAxisSize: MainAxisSize.min,
                     children: [
                       Container(
                         //width: MediaQuery.of(context).size.width - 10,
                        // height: MediaQuery.of(context).size.height -  110,
                         padding: EdgeInsets.all(0),
                         decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0),
                           color: Colors.white,
                         ),
                         child: Column(
                           mainAxisAlignment: MainAxisAlignment.start,
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children: [
                             Row(
                               crossAxisAlignment: CrossAxisAlignment.center,
                               mainAxisAlignment: MainAxisAlignment.spaceBetween,
                               children: [
                                 Padding(
                                   padding: const EdgeInsets.all(10),
                                   child: Container(
                                     child:Row(
                                       children: [
                                         Image.network(
                                           Urls.baseImageUrl+company_logo,
                                           fit: BoxFit.fill,
                                           width: 40,
                                           height: 40,),

                                       ],
                                     ),
                                   ),
                                 ),

                                 Padding(
                                   padding: const EdgeInsets.all(5),
                                   child: GestureDetector(onTap: (){
                                     Navigator.pop(context);
                                     //webView_flutter.show();
                                   },child: Icon(Icons.cancel,color: Colors.blueGrey,)),
                                 )
                               ],
                             ),
                             Align(
                               alignment: Alignment.topRight,
                               child: Padding(
                                 padding: EdgeInsets.only(right: 8,top: 8),
                                 child: Text('Valid To : '+data.valid_to),
                               ),
                             ),
                             Divider(),
                             Align(
                               child:Padding(
                                 padding: EdgeInsets.all(8),
                                 child: Container(
                                   height: 250,
                                   width: 300,
                                   child: Image.network(
                                     Urls.baseImageUrl+data.item_path,
                                     fit: BoxFit.fill,
                                   ),
                                 ),
                               ),
                               alignment: Alignment.center,
                             ),

                             Padding(
                               padding: const EdgeInsets.all(8.0),
                               child: Text(
                                 data.name,
                                 style: TextStyle(
                                     fontWeight: FontWeight.bold,
                                     fontSize: 22),
                               ),
                             ),

                             Padding(
                               padding: const EdgeInsets.all(4.0),
                               child: Text(
                                 data.price.toString()+" AED",
                                 style: TextStyle(
                                     fontWeight: FontWeight.bold,
                                     color: Colors.lightBlue,
                                     fontSize: 25),
                               ),
                             ),
                             Visibility(
                               child: Padding(padding: EdgeInsets.all(8),child:  Row(
                                 children: [
                                   Image.network(related_image,width: 50,height: 50,),
                                   Padding(padding: EdgeInsets.only(left: 5,right: 5),
                                     child: Image.network(related_image_2,width: 50,height: 50,),),
                                   GestureDetector(
                                     child:  Column(
                                       children: [
                                         Text('See More >>',style: TextStyle(color: Colors.blue,fontWeight: FontWeight.bold),),
                                         /*Container(
                                             decoration: BoxDecoration(border: Border.all(color: Colors.green,),
                                                 borderRadius: BorderRadius.all(Radius.circular(20)),color: Colors.green),
                                             child: Padding(padding: EdgeInsets.all(2),
                                               child: Row(
                                                 children: [
                                                   Text('View Flyer',style: TextStyle(color: Colors.white,fontSize: 12),)
                                                 ],
                                               ),)
                                         )*/
                                       ],
                                     ),
                                     onTap: (){
                                       Navigator.push(
                                         context,
                                         MaterialPageRoute(
                                           builder: (context) => SimiliarProductScreen(data.id.toString()),
                                         ),
                                       );
                                     },
                                   )
                                 ],
                               ),),
                               visible: true,
                             ),
                             Container(
                               width: double.infinity,
                               height: 55,
                               child:Row(
                                 children: [
                                   Expanded(
                                     flex: 1,
                                     child: Padding(
                                       padding: EdgeInsets.only(right: 2),
                                       child: GestureDetector(
                                         child: Container(
                                           width: double.infinity,
                                           height: 55,
                                           color: Colors.blue,
                                           child: Center(
                                             child: Row(
                                               children: [
                                                 Expanded(
                                                   flex: 1,
                                                   child: Icon(Icons.ios_share,color: Colors.white,size: 20,),
                                                 ),
                                                 Expanded(
                                                   flex: 1,
                                                   child: Text('Share',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 16),),
                                                 )
                                               ],
                                             ),
                                           ),
                                         ),
                                         onTap: (){
                                           share(data);
                                         },
                                       ),
                                     ),
                                   ),
                                   Visibility(
                                     visible: data.type == "offline" ||  data.type == "both",
                                     child:  Expanded(
                                       flex: 1,
                                       child: Padding(
                                         padding: EdgeInsets.only(right: 2),
                                         child: GestureDetector(
                                           child: Container(
                                               width: double.infinity,
                                               height: 55,
                                               color: data.isClipped?Colors.red:Colors.blue,
                                               child: Center(
                                                 child: Row(
                                                   children: [
                                                     Expanded(
                                                       flex: 1,
                                                       child: Icon(Icons.shopping_bag_outlined,color: Colors.white,size: 20,),
                                                     ),
                                                     Expanded(
                                                       flex: 1,
                                                       child: Text(data.isClipped ? 'Unclip' :'Clip',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 16),),
                                                     )
                                                   ],
                                                 ),
                                               )
                                           ),
                                           onTap: (){
                                             doclipUnclip(data);
                                           },
                                         ),
                                       ),
                                     ),
                                   ),
                                   Visibility(
                                       visible: data.type == "both",
                                       child: Expanded(
                                         flex: 1,
                                         child: Padding(
                                           padding: EdgeInsets.only(right: 2),
                                           child: GestureDetector(
                                             child: Container(
                                                 width: double.infinity,
                                                 height: 55,
                                                 color: Colors.blue,
                                                 child: Center(
                                                   child: Row(
                                                     children: [
                                                       Expanded(
                                                         flex: 1,
                                                         child: Icon(Icons.shopping_basket,color: Colors.white,size: 20,),
                                                       ),
                                                       Expanded(
                                                         flex: 1,
                                                         child: Text('Buy Now',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 16),),
                                                       )
                                                     ],
                                                   ),
                                                 )
                                             ),
                                             onTap: (){
                                               openUrl(data.url);
                                             },
                                           ),
                                         ),
                                       )
                                   ),
                                   Visibility(
                                     visible: data.type == "link",
                                     child:  Expanded(
                                       flex: 1,
                                       child: Padding(
                                         padding: EdgeInsets.only(right: 2),
                                         child: GestureDetector(
                                           child: Container(
                                               width: double.infinity,
                                               height: 55,
                                               color: Colors.blue,
                                               child: Center(
                                                 child: Row(
                                                   children: [
                                                     Expanded(
                                                       flex: 1,
                                                       child: Icon(Icons.public,color: Colors.white,size: 20,),
                                                     ),
                                                     Expanded(
                                                       flex: 1,
                                                       child: Text('Visit',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 16),),
                                                     )
                                                   ],
                                                 ),
                                               )
                                           ),
                                           onTap: (){
                                             openUrl(data.url);
                                            // webView_flutter.hide();
                                           },
                                         ),
                                       ),
                                     ),

                                   )
                                 ],
                               ),
                             )
                           ],
                         ),
                       ),
                     ],
                   )
                 ),
               );
             }
         );
       });

 }

 void getSimiliarProduct(String id) async{
   var response = await http.get(
       "${Urls.baseUrl}${Urls.SimiliarProducts}?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=100&&count=20&locale="+lan+"&page=1&id="+id,
       headers: {
         "Content-Type": "application/json",
         "Authorization": "Bearer ${userToken}",
         "Accept": "application/json"
       });
   Map<String, dynamic> value = json.decode(response.body);
   print("resp_similiar "+response.body);
   if(response.statusCode == 200){
     try{
       var array = value['data'];
       if(array.length >0){
         setState(() {
           show_similiar_prdouct = true;
           related_image = Urls.baseImageUrl+array[0]['item_path'];
           related_image_2 =  Urls.baseImageUrl+array[1]['item_path'];
         });
       }else{
         setState(() {
           show_similiar_prdouct = false;
         });
       }
     }catch(e){
       e.toString();
     }

   }
 }

 void share(InsideFlyerModel model){
   var shareMessage = Urls.baseurl+"flyer/"+model.slug+"?item="+model.id.toString();
   Share.share(shareMessage, subject: 'Flyerbin');
 }

 void openUrl(String url) async{
   if (await canLaunch(url)) {
     await launch(url);
   } else {
     throw 'Could not launch $url';
   }
 }

 void doclipUnclip(InsideFlyerModel model)async{
   ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
   var response = await http.post(
       "${Urls.baseUrl}${Urls.RemoveFlyerFromCart}?cartitem_id="+model.id.toString(),
       headers: {
         "Content-Type": "application/json",
         "Authorization": "Bearer ${userToken}",
         "Accept": "application/json"
       });
   Map<String, dynamic> value = json.decode(response.body);
   if(response.statusCode == 200){
     dialog.dismissProgressDialog(context);
     print("resp_copun_clip "+response.body);
     var data = value['data'];
     if(data['isClipped']){
       Future token = SharedPrefrence().getCartCount();
       token.then((data) async {
         SharedPrefrence().setCartCount(data +1);
       });
       setState(() {
         model.isClipped = true;
        // _controller.loadUrl("javascript:setItemClipped("+model.id.toString()+","+"true"+")");
         _controller.evaluateJavascript("javascript:setItemClipped("+model.id.toString()+","+"true"+")");
         //webView_flutter.show();
        // _controller.evaluateJavascript("javascript:setItemClipped("+model.id.toString()+","+"false"+")");
       });
       Navigator.pop(context);
       final snackBar = SnackBar(
           content: Text("Clipped",
               style: TextStyle(color: Colors.white)),
           backgroundColor: Colors.green,
           duration: const Duration(milliseconds: 100));
       _scaffoldKey.currentState.showSnackBar(snackBar);

     }else{
       Future token = SharedPrefrence().getCartCount();
       token.then((data) async {
         if(data>0){
           SharedPrefrence().setCartCount(data  - 1);
         }
       });
       setState(() {
         model.isClipped = false;
         _controller.evaluateJavascript("javascript:setItemClipped("+model.id.toString()+","+"false"+")");
        //webView_flutter.show();
         //_controller.loadUrl("javascript:setItemClipped("+model.id.toString()+","+"false"+")");
         //_controller.injectJavascriptFileFromUrl("javascript:setItemClipped("+model.id.toString()+","+"false"+")");
       });
       Navigator.pop(context);
       final snackBar = SnackBar(
           content: Text("Un Clipped",
               style: TextStyle(color: Colors.white)),
           backgroundColor: Colors.green,
           duration: const Duration(milliseconds: 100));
       _scaffoldKey.currentState.showSnackBar(snackBar);

     }
   }
   else{
     dialog.dismissProgressDialog(context);
     print(response.body);
   }
 }

}

