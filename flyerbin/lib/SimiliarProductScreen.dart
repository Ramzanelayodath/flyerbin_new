import 'dart:convert';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher.dart';
import 'Cart.dart';
import 'FlyerLoadScreen.dart';
import 'Model/SimiliarProductsModel.dart';
import 'Utils/Constants.dart';
import 'Utils/CustomDialogs.dart';
import 'Utils/SharedPrefrence.dart';
import 'Utils/Urls.dart';
import 'package:share/share.dart';

class SimiliarProductScreen extends StatefulWidget {
  String id;

  SimiliarProductScreen(this.id);

  @override
  State<StatefulWidget> createState() {
    return state(id);
  }
}

class state extends State<SimiliarProductScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String id;
  String appbarname = "";
  String userToken, related_image = "", related_image_2 = "", lan = 'en';
  int first_page = 1, last_page = 1, list_count = 0;
  var list = List<SimiliarProdcutModel>();
  bool show_similiar_prdouct = false;
  final DateFormat formatter2 = DateFormat('dd-MM-yyyy');
  final DateFormat formatter = DateFormat('yyyy-MM-dd');
  state(this.id);

  var page = 1;
  ScrollController _sccontroller = new ScrollController();
  bool isLoading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future lng = SharedPrefrence().getLanguage();
    lng.then((data) async {
      lan = data;
    });
    Future token = SharedPrefrence().getToken();
    token.then((data) async {
      userToken = data;
      // getSimilarProduct(id);
      this.getSimilarProduct(id, page);
      _sccontroller.addListener(() {
        if (_sccontroller.position.pixels ==
            _sccontroller.position.maxScrollExtent) {
          if(page==0){

          }
          else{
            getSimilarProduct(id, page);
          }

        }
      });
      //  getTab();
    });
  }

  @override
  void dispose() {
    _sccontroller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        key: _scaffoldKey,
        centerTitle: false,
        iconTheme: new IconThemeData(color: Color(0xff00ADEE)),
        title: Text(
          appbarname,
          style: TextStyle(color: Color(0xff00ADEE), fontSize: 13),
        ),
        backgroundColor: Colors.white,
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: GestureDetector(
              child: Icon(
                Icons.shopping_cart,
                color: Color(0xff00ADEE),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Cart(),
                  ),
                );
              },
            ),
          )
        ],
      ),
      body: isLoading?Shimmer.fromColors(
        baseColor: Colors.grey[200],
        highlightColor: Colors.grey,
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Container(
            height: 250,
            width: 200,
            //width: 30,
            //height: 30,
            decoration: BoxDecoration(
              color: Color(0xFFFFF29A),
              shape: BoxShape.rectangle,
              /*borderRadius: BorderRadius.all(
                                Radius.circular(15),
                              ),*/
            ),

          ),
        ),
      ):list_count==0?Center(child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text("No products to show"),
      )):GridView.builder(
          controller: _sccontroller,
          itemCount: list_count,
          physics: const AlwaysScrollableScrollPhysics(),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(childAspectRatio: 6 / 7.5,crossAxisCount: 2),
          shrinkWrap: true,
          itemBuilder: (BuildContext context, int index) {
            if (index == list_count) {
              if(page==0){
                return Center(child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(""),
                ));
              }
              else{
                return Shimmer.fromColors(
                  baseColor: Colors.grey[200],
                  highlightColor: Colors.grey,
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Container(
                      height: size.height * 0.6,
                      //width: 30,
                      //height: 30,
                      decoration: BoxDecoration(
                        color: Color(0xFFFFF29A),
                        shape: BoxShape.rectangle,
                        /*borderRadius: BorderRadius.all(
                                Radius.circular(15),
                              ),*/
                      ),

                    ),
                  ),
                );//_buildProgressIndicator();
              }

            } else {
              return SingleChildScrollView(
                physics: NeverScrollableScrollPhysics(),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GestureDetector(
                    onTap: () {
                      ClipFoodCoupnAlert(
                          _scaffoldKey.currentContext, list[index]);
                    },
                    child: Container(
                      child: Column(
                        children: [
                          Expanded(
                            flex: 0,
                            child: Card(
                              elevation: 2,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(2),
                              ),
                              child: Container(
                                  child: Stack(
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.all(2),
                                        child: Image.network(
                                          Urls.baseImageUrl +
                                              list[index].item_path,
                                          fit: BoxFit.fill,
                                          height: 180,
                                          width: 180,
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8),
                                        child: Text(
                                          list[index].name,
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Align(
                                    alignment: Alignment.topRight,
                                    child: Visibility(
                                      child: Text(
                                        'Clipped',
                                        style: TextStyle(
                                            backgroundColor: Colors.green,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white),
                                      ),
                                      visible: list[index].isClipped,
                                    ),
                                  )
                                ],
                              )),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            }
          }),
    );
  }

  Widget _buildProgressIndicator() {
    return new Center(
      child: new Opacity(
        opacity: isLoading ? 1.0 : 00,
        child: new CircularProgressIndicator(),
      ),
    );
  }

  void getSimilarProduct(String id, int index) async {
    print("size" + index.toString());
    /*if (!isLoading) {
      setState(() {
        isLoading = true;
      });*/
      List<SimiliarProdcutModel> tList = List();
      // ProgressDialog dialog =
      //     CustomDialogs().showLoadingProgressDialog(context);
      var response = await http.get(
          "${Urls.baseUrl}${Urls.SimiliarProducts}?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=100&&count=20&locale=" +
              lan +
              "&page=${page}&id=" +
              id,
          headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer ${userToken}",
            "Accept": "application/json"
          });
      Map<String, dynamic> value = json.decode(response.body);
      print("resp_similiar " + response.body);
      if (response.statusCode == 200) {
        // dialog.dismissProgressDialog(context);
        var array = value['data'];
        if(array.length>0){
          for (int i = 0; i < array.length; i++) {
            var obj = array[i];
            list.add(SimiliarProdcutModel(
                obj['id'].toString(),
                obj['brand_id'].toString(),
                obj['brand_logo'].toString(),
                obj['brand_name'].toString(),
                obj['description'].toString(),
                obj['flyer_id'].toString(),
                obj['flyer_page_id'].toString(),
                obj['flyer_slug'].toString(),
                obj['image_path'].toString(),
                obj['isClipped'],
                obj['item_path'].toString(),
                obj['name'].toString(),
                obj['price'].toString(),
                obj['sales_details'].toString(),
                obj['shop_id'].toString(),
                obj['shop_logo'].toString(),
                obj['shop_name'].toString(),
                obj['type'].toString(),
                obj['url'].toString(),
                obj['valid_to'].toString(),
                obj['valid_from'].toString()));
            var tag_array = obj['tags'];
            for (int i = 0; i < tag_array.length; i++) {
              var obj = tag_array[i];
              setState(() {
                appbarname = obj['name'].toString();
              });
            }

          }
          setState(() {
            last_page = value['last_page'];
            isLoading=false;
            //print(last_page.toString()+" last page");
            if(last_page==1){
              list_count = list.length;
              isLoading = false;
              list.addAll(tList);
              print(list.length);
              page=0;
            }
            else if(page!=last_page&&page!=0){
              list_count = list.length;
              isLoading = false;
              list.addAll(tList);
              print(list.length);
              page++;
            }
            else if(page==last_page&&page!=0){
              list_count = list.length;
              isLoading = false;
              list.addAll(tList);
              print(list.length);
              page=0;
            }
            else{
              page = 0;
            }

          });
        }
        else{
          setState(() {
            isLoading = false;
          });
        }

      } else {
        final snackBar = SnackBar(
          content: Text("Try Again", style: TextStyle(color: Colors.white)),
          backgroundColor: Colors.red,
        );
        _scaffoldKey.currentState.showSnackBar(snackBar);
        // dialog.dismissProgressDialog(context);
      }
    /*} else {
      setState(() {
        isLoading = false;
      });
    }*/
  }

  Future<void> ClipFoodCoupnAlert(
      BuildContext context, SimiliarProdcutModel data) async {
    setState(() {
      getSimiliarProduct(data.id.toString());
    });

    await showGeneralDialog(
        context: context,
        barrierDismissible: true,
        barrierLabel:
            MaterialLocalizations.of(context).modalBarrierDismissLabel,
        barrierColor: Colors.black45,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (BuildContext buildContext, Animation animation,
            Animation secondaryAnimation) {
          return StatefulBuilder(builder: (context, setState) {
            return Center(
                child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Card(
                  elevation: 6,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  child: Container(
                    // width: MediaQuery.of(context).size.width - 10,
                    // height: MediaQuery.of(context).size.height -  110,
                    padding: EdgeInsets.all(0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      color: Colors.white,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: Container(
                                child: Row(
                                  children: [
                                    Image.network(
                                      Urls.baseImageUrl + data.brand_logo,
                                      fit: BoxFit.fill,
                                      width: 30,
                                      height: 30,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(5),
                              child: GestureDetector(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: Icon(
                                    Icons.cancel,
                                    color: Colors.blueGrey,
                                  )),
                            )
                          ],
                        ),
                        Align(
                          alignment: Alignment.topRight,
                          child: Padding(
                            padding: EdgeInsets.only(right: 8, top: 8),
                            child: Text('Valid To : ' + formatter2.format(formatter.parse( data.validto.toString()))),
                          ),
                        ),
                        Divider(),
                        Align(
                          child: Padding(
                            padding: EdgeInsets.all(8),
                            child: Container(
                              height: 250,
                              width: 300,
                              child: Image.network(
                                Urls.baseImageUrl + data.item_path,
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                          alignment: Alignment.center,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            data.name,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 22),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Text(
                            data.price.toString() + " AED",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.lightBlue,
                                fontSize: 25),
                          ),
                        ),
                        Visibility(
                          child: Padding(
                            padding: EdgeInsets.all(8),
                            child: Row(
                              children: [
                                Image.network(
                                  related_image,
                                  width: 80,
                                  height: 80,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 5, right: 5),
                                  child: Image.network(
                                    related_image_2,
                                    width: 80,
                                    height: 80,
                                  ),
                                ),
                                Column(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.all(5),
                                      child: GestureDetector(
                                        child: Text(
                                          'See More >>',
                                          style: TextStyle(
                                              color: Colors.blue,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        onTap: () {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  SimiliarProductScreen(
                                                      data.id.toString()),
                                            ),
                                          );
                                        },
                                      ),
                                    ),
                                    GestureDetector(
                                      child: Container(
                                          padding: EdgeInsets.all(3),
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                color: Colors.green,
                                              ),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(20)),
                                              color: Colors.green),
                                          child: Padding(
                                            padding: EdgeInsets.all(2),
                                            child: Row(
                                              children: [
                                                Text(
                                                  'View Flyer',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 12),
                                                )
                                              ],
                                            ),
                                          )),
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                FlyerLoadScreen(
                                                    data.flyer_id.toString(),
                                                    data.shop_name.toString(),
                                                    formatter2.format(formatter.parse( data.valid_from.toString())),
                                                    formatter2.format(formatter.parse( data.validto.toString())),
                                                    data.shop_logo.toString(),
                                                    true,
                                                    data.id.toString()),
                                          ),
                                        );
                                      },
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                          visible: true,
                        ),
                        Container(
                          width: double.infinity,
                          height: 55,
                          child: Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: Padding(
                                  padding: EdgeInsets.only(right: 2),
                                  child: GestureDetector(
                                    child: Container(
                                      width: double.infinity,
                                      height: 55,
                                      color: Colors.blue,
                                      child: Center(
                                        child: Row(
                                          children: [
                                            Expanded(
                                              flex: 1,
                                              child: Icon(
                                                Icons.ios_share,
                                                color: Colors.white,
                                                size: 20,
                                              ),
                                            ),
                                            Expanded(
                                              flex: 1,
                                              child: Text(
                                                'Share',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 16),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                    onTap: () {
                                      share(data);
                                    },
                                  ),
                                ),
                              ),
                              Visibility(
                                visible: data.type == "offline" ||
                                    data.type == "both",
                                child: Expanded(
                                  flex: 1,
                                  child: Padding(
                                    padding: EdgeInsets.only(right: 2),
                                    child: GestureDetector(
                                      child: Container(
                                          width: double.infinity,
                                          height: 55,
                                          color: data.isClipped?Colors.red:Colors.blue,
                                          child: Center(
                                            child: Row(
                                              children: [
                                                Expanded(
                                                  flex: 1,
                                                  child: Icon(
                                                    Icons.shopping_bag_outlined,
                                                    color: Colors.white,
                                                    size: 20,
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 1,
                                                  child: Text(
                                                    data.isClipped
                                                        ? 'Unclip'
                                                        : 'Clip',
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 16),
                                                  ),
                                                )
                                              ],
                                            ),
                                          )),
                                      onTap: () {
                                        doclipUnclip(data);
                                      },
                                    ),
                                  ),
                                ),
                              ),
                              Visibility(
                                  visible: data.type == "both",
                                  child: Expanded(
                                    flex: 1,
                                    child: Padding(
                                      padding: EdgeInsets.only(right: 2),
                                      child: GestureDetector(
                                        child: Container(
                                            width: double.infinity,
                                            height: 55,
                                            color: Colors.blue,
                                            child: Center(
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                    flex: 1,
                                                    child: Icon(
                                                      Icons.shopping_basket,
                                                      color: Colors.white,
                                                      size: 20,
                                                    ),
                                                  ),
                                                  Expanded(
                                                    flex: 1,
                                                    child: Text(
                                                      'Buy Now',
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 16),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            )),
                                        onTap: () {
                                          openUrl(data.url);
                                        },
                                      ),
                                    ),
                                  )),
                              Visibility(
                                visible: data.type == "link",
                                child: Expanded(
                                  flex: 1,
                                  child: Padding(
                                    padding: EdgeInsets.only(right: 2),
                                    child: GestureDetector(
                                      child: Container(
                                          width: double.infinity,
                                          height: 55,
                                          color: Colors.blue,
                                          child: Center(
                                            child: Row(
                                              children: [
                                                Expanded(
                                                  flex: 1,
                                                  child: Icon(
                                                    Icons.public,
                                                    color: Colors.white,
                                                    size: 20,
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 1,
                                                  child: Text(
                                                    'Visit',
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 16),
                                                  ),
                                                )
                                              ],
                                            ),
                                          )),
                                      onTap: () {
                                        openUrl(data.url);
                                      },
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ));
          });
        });
  }

  void doclipUnclip(SimiliarProdcutModel model) async {
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var response = await http.post(
        "${Urls.baseUrl}${Urls.RemoveFlyerFromCart}?cartitem_id=" +
            model.id.toString(),
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    if (response.statusCode == 200) {
      dialog.dismissProgressDialog(context);
      print("resp_copun_clip " + response.body);
      var data = value['data'];
      if (data['isClipped']) {
        Future token = SharedPrefrence().getCartCount();
        token.then((data) async {
          SharedPrefrence().setCartCount(data + 1);
        });
        setState(() {
          model.isClipped = true;
        });
        Navigator.pop(context);
        final snackBar = SnackBar(
          content: Text("Clipped", style: TextStyle(color: Colors.white)),
          backgroundColor: Colors.green,duration: Duration(milliseconds: 100),
        );
        _scaffoldKey.currentState.showSnackBar(snackBar);
      } else {
        Future token = SharedPrefrence().getCartCount();
        token.then((data) async {
          if (data > 0) {
            SharedPrefrence().setCartCount(data - 1);
          }
        });
        setState(() {
          model.isClipped = false;
        });
        Navigator.pop(context);
        final snackBar = SnackBar(
          content: Text("Un clipped", style: TextStyle(color: Colors.white)),
          backgroundColor: Colors.green,duration: Duration(milliseconds: 100),
        );
        _scaffoldKey.currentState.showSnackBar(snackBar);
      }
    } else {
      dialog.dismissProgressDialog(context);
      print(response.body);
    }
  }

  void share(SimiliarProdcutModel model) {
    var shareMessage = Urls.baseurl +
        "flyer/" +
        model.flyer_slug +
        "?item=" +
        model.id.toString();
    Share.share(shareMessage, subject: 'Flyerbin');
  }

  void openUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void getSimiliarProduct(String id) async {
    var response = await http.get(
        "${Urls.baseUrl}${Urls.SimiliarProducts}?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=100&&count=20&locale=" +
            lan +
            "&page=1&id=" +
            id,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print("resp_similiar " + response.body);
    if (response.statusCode == 200) {
      try {
        var array = value['data'];
        if (array.length > 0) {
          setState(() {
            show_similiar_prdouct = true;
            related_image = Urls.baseImageUrl + array[0]['item_path'].toString();
            related_image_2 = Urls.baseImageUrl + array[1]['item_path'].toString();
          });
        } else {
          setState(() {
            show_similiar_prdouct = false;
          });
        }
      } catch (e) {
        e.toString();
      }
    }
  }
}
