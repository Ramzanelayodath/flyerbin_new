

import 'dart:convert';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart'as  http;
import 'Utils/Constants.dart';
import 'Utils/CustomDialogs.dart';
import 'Utils/SharedPrefrence.dart';
import 'Utils/Urls.dart';

class PasswordChange extends StatefulWidget{
  bool hasPassword;

  PasswordChange(this.hasPassword);

  @override
  State<StatefulWidget> createState() {
   return state(hasPassword);
  }

}

class state extends State<PasswordChange>{
  bool hasPassword;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String userToken;
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 15.0);
  final TextEditingController _textEditingPasswordController =
  new TextEditingController();
  final TextEditingController _textEditingConfirmPasswordController =
  new TextEditingController();
  final TextEditingController _textEditingCurrentPasswordController =
  new TextEditingController();
  String current_password;
  state(this.hasPassword);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future password = SharedPrefrence().getUserPassword();
    password.then((data) async {
      setState(() {
        current_password = data;
      });
    });
    Future token = SharedPrefrence().getToken();
    token.then((data) async {
      userToken = data;
    });
  }

  @override
  Widget build(BuildContext context) {
   return Scaffold(
     key: _scaffoldKey,
     appBar: AppBar(
       automaticallyImplyLeading: true,
       iconTheme: IconThemeData(
         color: Colors.blue, //change your color here
       ),
       backgroundColor: Colors.white,
       title: Text(
         "Change Password",
         style: TextStyle(fontSize: 15, color: Colors.lightBlue),
       ),
     ),
     body: SingleChildScrollView(
       child: Column(
         children: [
           Visibility(
             child: Padding(
               padding: EdgeInsets.all(8),
               child: TextField(
                 keyboardType: TextInputType.emailAddress,
                 obscureText: true,
                 controller:_textEditingCurrentPasswordController ,
                 decoration: InputDecoration(
                     prefixIcon: Icon(Icons.lock_rounded,color: Colors.blue,),
                     contentPadding: EdgeInsets.fromLTRB(10.0, 15.0, 20.0, 10.0),
                     //hintText: "Email",
                     labelText: "Current Password"
                   //border:
                   // OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))
                 ),
               ),
             ),
             visible: hasPassword,
           ),
           Padding(
             padding: EdgeInsets.only(left: 8,right: 8),
             child: TextField(
               keyboardType: TextInputType.emailAddress,
               obscureText: true,
               controller: _textEditingPasswordController,
               decoration: InputDecoration(
                   prefixIcon: Icon(Icons.lock_rounded,color: Colors.blue,),
                   contentPadding: EdgeInsets.fromLTRB(10.0, 15.0, 20.0, 10.0),
                   //hintText: "Email",
                   labelText: "New Password"
                 //border:
                 // OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))
               ),
             ),
           ),
          Padding(
            padding: EdgeInsets.all(8),
            child:  TextField(
              keyboardType: TextInputType.emailAddress,
              obscureText: true,
              controller: _textEditingConfirmPasswordController,
              decoration: InputDecoration(
                  prefixIcon: Icon(Icons.lock_rounded,color: Colors.blue,),
                  contentPadding: EdgeInsets.fromLTRB(10.0, 15.0, 20.0, 10.0),
                  //hintText: "Email",
                  labelText: "Repeat Password"
                //border:
                // OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8),
            child:  Material(
              elevation: 5.0,
              borderRadius: BorderRadius.circular(30.0),
              color: Color(0xff00ADEE),
              child: MaterialButton(
                minWidth: MediaQuery.of(context).size.width,
                padding: EdgeInsets.fromLTRB(10.0, 15.0, 20.0, 10.0),
                onPressed: () {
                  if(hasPassword &&_textEditingCurrentPasswordController.text.isEmpty){
                    final snackBar = SnackBar(content: Text('Enter Current Password',style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
                    _scaffoldKey.currentState.showSnackBar(snackBar);
                  }else if(hasPassword && _textEditingCurrentPasswordController.text != current_password){
                    final snackBar = SnackBar(content: Text('Current Password Is Incorrect',style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
                    _scaffoldKey.currentState.showSnackBar(snackBar);
                  }else if(_textEditingPasswordController.text.isEmpty){
                    final snackBar = SnackBar(content: Text('Enter New Password',style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
                    _scaffoldKey.currentState.showSnackBar(snackBar);
                  }else if(_textEditingPasswordController.text.length < 8){
                    final snackBar = SnackBar(content: Text('Password Minimum Length 8 Characters',style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
                    _scaffoldKey.currentState.showSnackBar(snackBar);
                  }else if(_textEditingConfirmPasswordController.text.isEmpty){
                    final snackBar = SnackBar(content: Text('Enter Confirm Password',style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
                    _scaffoldKey.currentState.showSnackBar(snackBar);
                  }else if(_textEditingPasswordController.text != _textEditingConfirmPasswordController.text){
                    final snackBar = SnackBar(content: Text('Entered Password and Repeat Password Are Miss Match',style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
                    _scaffoldKey.currentState.showSnackBar(snackBar);
                  }
                  else{
                      if(hasPassword){
                        changePassword(_textEditingCurrentPasswordController.text, _textEditingPasswordController.text);
                      }else{
                        addNewPassword(_textEditingPasswordController.text);
                      }
                  }
                },
                child: Text("Save",
                    textAlign: TextAlign.center,
                    style: style.copyWith(
                        color: Colors.white, fontWeight: FontWeight.bold)),
              ),
            ),
          )
         ],
       ),
     ),
   );
  }

  void changePassword(String oldPassword,String newPassword)async{
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var response = await http.post(
        "${Urls.baseUrl}${Urls.UpdatePasswordandEmail}?current_password="+oldPassword+"&new_password="+newPassword,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    if(response.statusCode == 200){
      dialog.dismissProgressDialog(context);
      if(value['success']){
        _textEditingCurrentPasswordController.clear();
        _textEditingPasswordController.clear();
        _textEditingConfirmPasswordController.clear();
        final snackBar = SnackBar(content: Text(value['message'],style: TextStyle(color: Colors.white)),backgroundColor: Colors.green,);
        _scaffoldKey.currentState.showSnackBar(snackBar);
        SharedPrefrence().saveUserPassword(newPassword);
        setState(() {
          current_password = newPassword;
        });
      }else{
        final snackBar = SnackBar(content: Text(value['message'],style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
        _scaffoldKey.currentState.showSnackBar(snackBar);
      }
    }else{
      dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(content: Text("Try Again..",style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  void addNewPassword(String new_password) async{
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var response = await http.post(
        "${Urls.baseUrl}${Urls.UpdatePasswordandEmail}?new_password="+new_password,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    if(response.statusCode == 200){
      dialog.dismissProgressDialog(context);
      if(value['success']){
        _textEditingPasswordController.clear();
        _textEditingConfirmPasswordController.clear();
        final snackBar = SnackBar(content: Text(value['message'],style: TextStyle(color: Colors.white)),backgroundColor: Colors.green,);
        _scaffoldKey.currentState.showSnackBar(snackBar);
        SharedPrefrence().saveUserPassword(new_password);
        SharedPrefrence().setHasPassword(true);
      }else{
        final snackBar = SnackBar(content: Text(value['message'],style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
        _scaffoldKey.currentState.showSnackBar(snackBar);
      }
    }else{
      dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(content: Text("Try Again..",style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }
}