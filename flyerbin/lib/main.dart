import 'dart:async';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';



import 'BottomnavBarScreen.dart';
import 'IntroScreen.dart';
import 'Loginscreen.dart';
import 'Utils/SharedPrefrence.dart';
FirebaseAnalytics analytics;
Future<void> main()  async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SplashScreen(),
    );
  }
}

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return state();
  }
}

class state extends State<SplashScreen> {
  bool intro_status = false;
  bool isllogged = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    startTime();


  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    Size size = MediaQuery
        .of(context)
        .size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: new Stack(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Center(
                child: SvgPicture.asset(
                  'assets/images/flyerbin_logo.svg', height: 100, width: 100,),
              ),
            ],
          )
        ],
      ),
    );
  }

  startTime() async {
    var _duration = new Duration(seconds: 3);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() {
    Future isLogged = SharedPrefrence().getLogedIn();
    isLogged.then((logged_data) {
      setState(() {
        isllogged = logged_data;
        if (isllogged) {
          Navigator.pop(context, true);
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => DashboardScreen()));
        }
        else {
          Navigator.pop(context, true);
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => LoginScreen()));
        }
      });
    });
  }




//}


}