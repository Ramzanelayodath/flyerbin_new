

import 'dart:convert';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'Loginscreen.dart';
import 'Utils/CustomDialogs.dart';
import 'Utils/SharedPrefrence.dart';
import 'Utils/Urls.dart';

class RegisterScreen extends StatefulWidget{
  String referral;

  RegisterScreen(this.referral);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new RegisterScreenState(referral);
  }
  
}

class RegisterScreenState extends State<RegisterScreen>{
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 15.0);
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String otp;
  String referral = "";

  RegisterScreenState(this.referral);

  final TextEditingController _textEditingNameController =
  new TextEditingController();
  final TextEditingController _textEditingMailController =
  new TextEditingController();
  final TextEditingController _textEditingPhoneController =
  new TextEditingController();
  final TextEditingController _textEditingPasswordController =
  new TextEditingController();
  final TextEditingController _textEditingConfirmPasswordController =
  new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    Future token = SharedPrefrence().getMobileOTP();
    Future mobile_numer = SharedPrefrence().getMobile();
    token.then((data) async {
      mobile_numer.then((mobile_data)async{
        _textEditingPhoneController.text = mobile_data;
        otp = data;
      });

      //getFlyersInCategory(id, data,slug);
      //  getTab();
    });
    //_webViewHandler();
  }

  @override
  Widget build(BuildContext context) {

    final nameField = TextField(
      keyboardType: TextInputType.text,
      obscureText: false,
      style: style,
      controller: _textEditingNameController,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(10.0, 15.0, 20.0, 10.0),
        //hintText: "Name",
        labelText: "Name"
        //border:
        // OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))
      ),
    );

    final emailField = TextField(
      keyboardType: TextInputType.emailAddress,
      obscureText: false,
      style: style,
      controller: _textEditingMailController,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(10.0, 15.0, 20.0, 10.0),
        //hintText: "Email",
        labelText: "Email"
        //border:
        // OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))
      ),
    );

    final phoneField = TextField(
      keyboardType: TextInputType.phone,
      enabled: false,
      obscureText: false,
      style: style,
      controller: _textEditingPhoneController,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(10.0, 15.0, 20.0, 10.0),
       // hintText: "Phone",
        labelText: "Phone"
        //border:
        // OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))
      ),
    );

    final passwordField = TextField(
      keyboardType: TextInputType.visiblePassword,
      obscureText: true,
      style: style,
      controller: _textEditingPasswordController,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(10.0, 15.0, 20.0, 10.0),
       // hintText: "Password",
        labelText: "Password"
        //border:
        // OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))
      ),
    );
    final confirmPasswordField = TextField(
      keyboardType: TextInputType.visiblePassword,
      obscureText: true,
      style: style,
      controller: _textEditingConfirmPasswordController,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(10.0, 15.0, 20.0, 10.0),
        //hintText: "Confirm Password",
        labelText: "Confirm Password"
        //border:
        // OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))
      ),
    );

    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff00ADEE),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(10.0, 15.0, 20.0, 10.0),
        onPressed: () {
          if (_textEditingNameController.text
            .toString()
            .length ==
            0) {
          final snackBar = SnackBar(
              content: Text("Enter Name"));
          _scaffoldKey.currentState
              .showSnackBar(snackBar);
        } else if (_textEditingMailController.text
            .toString()
            .length ==
            0) {
          final snackBar = SnackBar(
              content: Text("Enter Mail ID"));
          _scaffoldKey.currentState
              .showSnackBar(snackBar);
        }
          else if (_textEditingPhoneController.text
              .toString()
              .length ==
              0) {
            final snackBar = SnackBar(
                content: Text("Enter Phone no"));
            _scaffoldKey.currentState
                .showSnackBar(snackBar);
          }
          else if (_textEditingPasswordController.text
              .toString()
              .length ==
              0) {
            final snackBar = SnackBar(
                content: Text("Enter password"));
            _scaffoldKey.currentState
                .showSnackBar(snackBar);
          }
          else if (_textEditingPasswordController.text
              .toString()
              .length < 8) {
            final snackBar = SnackBar(
                content: Text("Enter minimum 8 character"));
            _scaffoldKey.currentState
                .showSnackBar(snackBar);
          }
          else if (_textEditingConfirmPasswordController.text
              .toString()
              .length ==
              0) {
            final snackBar = SnackBar(
                content: Text("Enter confirm password"));
            _scaffoldKey.currentState
                .showSnackBar(snackBar);
          }
          else if (_textEditingConfirmPasswordController.text
              .toString()
              .length < 8) {
            final snackBar = SnackBar(
                content: Text("Enter minimum 8 character"));
            _scaffoldKey.currentState
                .showSnackBar(snackBar);
          }
          else if (_textEditingConfirmPasswordController.text
              .toString() != _textEditingPasswordController.text
              .toString()) {
            final snackBar = SnackBar(
                content: Text("Password mismatch"));
            _scaffoldKey.currentState
                .showSnackBar(snackBar);
          }
          else {
          doRegister(_textEditingNameController.text.toString(),
              _textEditingMailController.text.toString(),
              _textEditingPhoneController.text.toString(),
              otp,
              _textEditingPasswordController.text.toString(),referral);
        }
        },
        child: Text("Register",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
        child: Center(
          child: Container(
            //color: Colors.white,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: const EdgeInsets.all(32.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 30.0,
                    /*child: SvgPicture.asset(
                      "assets/images/flyerbin_logo.svg",
                      fit: BoxFit.contain,
                    ),*/
                    child:Align(
                      child:Text("Sign Up",style: TextStyle(color: Color(0xff00ADEE),fontSize: 20),textAlign: TextAlign.left,), alignment: Alignment.centerLeft,
                    )


                  ),
                  SizedBox(height: 20.0),
                  nameField,
                  SizedBox(height: 20.0),
                  emailField,
                  SizedBox(height: 20.0),
                  phoneField,
                 /* Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text("+971"),
                      phoneField,
                    ],
                  ),*/

                  SizedBox(height: 20.0),
                  passwordField,
                  SizedBox(height: 20.0),
                  confirmPasswordField,
                  SizedBox(
                    height: 30.0,
                  ),
                  GestureDetector(
                    child: loginButon,
                  ),

                  SizedBox(
                    height: 15.0,
                  ),
                 // SocialLoginLayout(),
                 // RegisterLayout()
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void doRegister(String user_name,
      String user_mail,
      String user_phone,
      String user_otp,
      String user_password,String referral) async {
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);

    var response = await http.post(Urls.baseUrl+Urls.Register,
        headers: {"Content-Type": "application/json"},
        body: json.encode({
          "name": user_name,
          "email": user_mail,
          "mobile":user_phone,
          "otp":user_otp,
          "password":user_password,
          "password_confirmation":user_password,
          "referral_code":referral
        }));

    Map<String, dynamic> value = json.decode(response.body);
    if (response.statusCode == 200) {
      dialog.dismissProgressDialog(context);
      try {
        print("response login " + response.body.toString());
        Map<String, dynamic> value = json.decode(response.body);

        var status = value['success'];
        var message = value['message'];
        print(message);
        if (status == true) {
          // SharedPrefrence().setMobile(userMob);
          if(message=='Operation Successfull'){
           /* SharedPrefrence().setToken(value['data']['token'].toString());
            SharedPrefrence().setUserId(value['data']['id'].toString());
            SharedPrefrence().setUserName(value['data']['name'].toString());
            SharedPrefrence().setUserEmail(value['data']['email'].toString());
            SharedPrefrence().setLoggedIn(true);*/
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                    builder: (context) => LoginScreen()),
                ModalRoute.withName("/login"));
          }

        } else if (message == 'Already Registered') {
          final snackBar = SnackBar(content: Text(message));
          _scaffoldKey.currentState.showSnackBar(snackBar);
          // SharedPrefrence().setMobile(userMob);
          //  SharedPrefrence().setLoggedIn(true);
          /* Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => HomeScreen()),
              ModalRoute.withName("/login"));*/
        } else if (message == "NOT REGISTERED") {
          final snackBar = SnackBar(content: Text(message));
          _scaffoldKey.currentState.showSnackBar(snackBar);
        } else if (message == 'Network Error.OTP Not Generated') {
          print("Network Error.OTP Not Generated");
          final snackBar = SnackBar(content: Text(message));
          _scaffoldKey.currentState.showSnackBar(snackBar);
        } else {
          print("Error...");
          final snackBar = SnackBar(content: Text(message));
          _scaffoldKey.currentState.showSnackBar(snackBar);
        }
      } catch (e) {
        print(e.toString());
      }
    } else if (response.statusCode == 404) {
      dialog.dismissProgressDialog(context);
      var message = value['message'];
      final snackBar = SnackBar(content: Text(message));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    } else if (response.statusCode == 208) {
      dialog.dismissProgressDialog(context);
    } else {
      var message = value['message'];
      dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(content: Text(message));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }


}