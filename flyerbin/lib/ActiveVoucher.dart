

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


import 'Model/VoucherModel.dart';
import 'Utils/Urls.dart';

class ActiveVoucher extends StatefulWidget{
  final List<VoucherModel> activeVoucher ;

  ActiveVoucher(this.activeVoucher);

  @override
  State<StatefulWidget> createState() {
    return state(activeVoucher);
  }
}

class state extends State<ActiveVoucher>{
  final List<VoucherModel> activeVoucher ;

  state(this.activeVoucher);

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
   return Scaffold(
     key: _scaffoldKey,
     resizeToAvoidBottomInset: false,
     appBar :AppBar(
       centerTitle: false,
       iconTheme: new IconThemeData(color: Color(0xff00ADEE)),
       title: Text(
         "Active Voucher",
         style: TextStyle(color: Color(0xff00ADEE), fontSize: 13),
       ),
       backgroundColor: Colors.white,
     ),
     body: Container(
       width: double.infinity,
       height: double.infinity,
       decoration: BoxDecoration(
         image: DecorationImage(
           image: AssetImage("assets/images/active_voucher_bg.png"),
           fit: BoxFit.fill,
         ),
       ),
       child: SingleChildScrollView(
         child: Column(
           children: [
             SizedBox(height: 40,),
             Text('You earned',style: TextStyle(color: Colors.black,fontSize: 20),),
             SizedBox(height: 3,),
             Text(activeVoucher[0].amount,style: TextStyle(color: Colors.black,fontSize: 60,fontWeight: FontWeight.bold),),
             SizedBox(height: 1,),
             Text('AED',style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold),),
             SizedBox(height: 10,),
             Image.network(Urls.baseImageUrl+activeVoucher[0].barcode,height: 55,),
             SizedBox(height: 10,),
             Container(
               width: 190,
               height: 190,
               decoration: BoxDecoration(
                   image: DecorationImage(
                       image: NetworkImage(Urls.baseImageUrl+activeVoucher[0].image,),fit: BoxFit.cover),
                   borderRadius: BorderRadius.all(Radius.circular(20))
               ),
               child: Text(''),
             ),
             SizedBox(height: 5,),
             Text('expire in '+activeVoucher[0].expiry,style: TextStyle(color: Colors.black),),
             SizedBox(height: 5,),
             Text(activeVoucher[0].shop,style: TextStyle(color: Colors.black),)
           ],
         ),
       ),
     ),
   );
  }
}