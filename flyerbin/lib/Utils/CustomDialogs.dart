

import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:custom_progress_dialog/custom_progress_dialog.dart';

class CustomDialogs
{
   showLoadingProgressDialog(BuildContext context) {
     ProgressDialog _progressDialog = ProgressDialog();
     _progressDialog.showProgressDialog(context,dismissAfter: Duration(seconds: 5),textToBeDisplayed:'Loading...');
    return _progressDialog;
  }

  showErrorAlert(BuildContext context,String message){
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
            content: Container(
              decoration: BoxDecoration(
              ),
              height: 150,
              width: double.infinity,
              child: Column(
                children: <Widget>[
                  Center(
                    child:  Padding(padding: EdgeInsets.only(top:20),child:
                    Image.asset("assets/images/animation_error.gif",height: 75,width: 75,),),
                  ),
                  Padding(padding: EdgeInsets.only(top: 10),child: Text(message),)
                ],
              ),
            )
         
        );
      },
    );
  }


}