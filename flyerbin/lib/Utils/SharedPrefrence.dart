import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';

class SharedPrefrence{

  Future<bool> setToken(String  token) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString("token", token);
  }

  Future<String> getToken() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("token") ?? '';
  }

  Future<bool> setUserId(String  userId) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString("user_id", userId);
  }

  Future<String> getUserId() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("user_id") ?? '';
  }

  Future<bool> setUserName(String  userName) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString("user_name", userName);
  }

  Future<String> getUserName() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("user_name") ?? '';
  }

  Future<bool> setUserEmail(String  userMail) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString("user_mail", userMail);
  }

  Future<String> getUserEmail() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("user_mail") ?? '';
  }

  Future<bool> setMobile(String  mobile) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString("mobile", mobile);
  }

  Future<String> getMobile() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("mobile") ?? '';
  }
  Future<bool> setLatitude(String  latitude) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString("latitude", latitude);
  }

  Future<String> getLatitude() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("latitude") ?? '';
  }
  Future<bool> setLongitude(String  longitude) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString("longitude", longitude);
  }

  Future<String> getLongitude() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("longitude") ?? '';
  }
  Future<bool> setLoggedIn(bool  status) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool("logged_in", status);
  }

  Future<bool> getLogedIn() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool("logged_in") ?? false;
  }

  Future<bool> setShowIntro(bool  status) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool("intro_screen", status);
  }

  Future<bool> getShowIntro() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool("intro_screen") ?? false;
  }

  Future<bool> setGustLoggedIn(bool  status) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool("gust_logged_in", status);
  }

  Future<bool> getGustLogedIn() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool("gust_logged_in") ?? false;
  }


  Future<bool> setLanguage(String  name) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString("lang", name);
  }

  Future<String> getLanguage() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("lang") ?? 'en';
  }

  //cart list item
  Future<bool> setBoonidCart(List<String> list) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setStringList("boon_id", list);
  }
  Future<List<String>> getBoonidCart() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList("boon_id") ?? [];
  }

  Future<bool> setProductidCart(List<String> list) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setStringList("product_id", list);
  }
  Future<List<String>> getProductidCart() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList("product_id") ?? [];
  }
  Future<bool> setboonnameCart(List<String> list) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setStringList("boon_name", list);
  }
  Future<List<String>> getboonnameCart() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList("boon_name") ?? [];
  }
  Future<bool> setproductnameCart(List<String> list) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setStringList("product_name", list);
  }
  Future<List<String>> getProductnameCart() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList("product_name") ?? [];
  }
  Future<bool> setproductpriceCart(List<String> list) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setStringList("product_price", list);
  }
  Future<List<String>> getProductpriceCart() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList("product_price") ?? [];
  }
  Future<bool> setproductqtyCart(List<String> list) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setStringList("product_qty", list);
  }
  Future<List<String>> getProductqtyCart() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList("product_qty") ?? [];
  }
  Future<bool> setproductSizeCart(List<String> list) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setStringList("product_size", list);
  }
  Future<List<String>> getProductsizeCart() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList("product_size") ?? [];
  }
  Future<bool> setproductColorCart(List<String> list) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setStringList("product_color", list);
  }
  Future<List<String>> getProductcolorCart() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList("product_color") ?? [];
  }
  Future<bool> setproductImageCart(List<String> list) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setStringList("product_image", list);
  }
  Future<List<String>> getProductImageCart() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList("product_image") ?? [];
  }
  Future<bool> setBoonImageCart(List<String> list) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setStringList("boon_image", list);
  }
  Future<List<String>> getBoonImage() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList("boon_image") ?? [];
  }
  Future<bool> setpercentageCart(List<String> list) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setStringList("percentage", list);
  }
  Future<List<String>> getPearcentage() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList("percentage") ?? [];
  }

  Future<bool> setCartCount(int count) async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setInt("cart_count", count);
  }

  Future <int> getCartCount() async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt("cart_count") ?? 0;
  }

  Future<bool> setMobileOTP(String  otp) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString("otp", otp);
  }

  Future<String> getMobileOTP() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("otp") ?? '';
  }

  Future<bool> setRadius(String  longitude) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString("radius", longitude);
  }

  Future<String> getRadius() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("radius") ?? '';
  }

  Future<bool>setHasPassword(bool status) async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool("Haspassword", status);
  }

  Future<bool>getHasPassword() async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool("Haspassword") ?? false;
  }

  Future<bool> saveUserPassword(String password)async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString("password", password);
  }

  Future<String> getUserPassword()async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("password") ?? '';
  }

  Future<bool> setNotificationFLag(bool flag)async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool("not_flag", flag);
  }

  Future<bool> getNotificationFlag()async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool("not_flag") ??true;
  }

  Future<bool> saveUserPhoto(String photo)async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString("user_photo", photo);
  }

  Future<String> getUserPhoto()async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("user_photo") ?? '';
  }

  Future<bool> setReferralcode(String referralcode)async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString("referral_code", referralcode);
  }

  Future <String> getreferralCode()async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("referral_code") ??'';
  }

}