
import 'dart:convert';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart'as http;
import 'MobilUpdateOtp.dart';
import 'Utils/CustomDialogs.dart';
import 'Utils/SharedPrefrence.dart';
import 'Utils/Urls.dart';

class UpdateMobile extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return state();
  }
}
class state extends State<UpdateMobile>{
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 15.0);
  final TextEditingController ctrl_phone =
  new TextEditingController();
  String userToken;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future token = SharedPrefrence().getToken();
    token.then((data) async {
      userToken = data;
    });
  }
  @override
  Widget build(BuildContext context) {
   return Scaffold(
       resizeToAvoidBottomInset: false,
       key: _scaffoldKey,
       appBar: AppBar(
         title: Text(
           "Update Mobile",
           style: TextStyle(fontSize: 15, color: Colors.lightBlue),
         ),
         automaticallyImplyLeading: true,
         iconTheme: IconThemeData(
           color: Colors.lightBlue,
         ),
         backgroundColor: Colors.white,),
     body: SingleChildScrollView(
       child: Column(
         children: [
           SizedBox(height: 30,),
           Padding(
             padding: EdgeInsets.all(10),
             child: TextField(
               keyboardType: TextInputType.phone,
               obscureText: false,
               style: style,
               controller: ctrl_phone,
               decoration: InputDecoration(
                   prefixIcon: Icon(Icons.phone,color: Colors.blue,),
                   contentPadding: EdgeInsets.fromLTRB(10.0, 15.0, 20.0, 10.0),
                   //hintText: "Email",
                   labelText: "Mobile Number Exclude Country Code"
                 //border:
                 // OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))
               ),
             ),

           ),
           Padding(
             padding: EdgeInsets.all(8),
             child: Material(
               elevation: 5.0,
               borderRadius: BorderRadius.circular(30.0),
               color: Color(0xff00ADEE),
               child: MaterialButton(
                 minWidth: MediaQuery.of(context).size.width,
                 padding: EdgeInsets.fromLTRB(10.0, 15.0, 20.0, 10.0),
                 onPressed: () {
                    updateMobile(ctrl_phone.text);
                 },
                 child: Text("Verify",
                     textAlign: TextAlign.center,
                     style: style.copyWith(
                         color: Colors.white, fontWeight: FontWeight.bold)),
               ),
             ),
           )
         ],
       ),

     ),
   );
  }

  void updateMobile(String mobile)async{
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var response = await http.post(
        "${Urls.baseUrl}${Urls.Update_Mobile}?mobile="+mobile,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print("resp_mobile "+response.body);
    if(response.statusCode == 200){
       dialog.dismissProgressDialog(context);
       try{
          if(value['message']['otp_sent']){
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => MobileUpdateOtp(mobile),
              ),
            );
          }
       }catch(e){}
    }else{
      dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(content: Text('Try Again',style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }
}