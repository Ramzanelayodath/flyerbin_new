import 'dart:async';
import 'dart:convert';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_layouts/flutter_layouts.dart';
import 'package:flutter_svg/svg.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;

import 'BottomnavBarScreen.dart';
import 'Model/FavouriteShopPreferenceModel.dart';
import 'Utils/Constants.dart';
import 'Utils/CustomDialogs.dart';
import 'Utils/SharedPrefrence.dart';
import 'Utils/Urls.dart';


class ChangeShopPreference extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ChangeShopPreferenceState();
  }
}

class ChangeShopPreferenceState extends State<ChangeShopPreference> {
  String radioItem = '';
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String latitude;
  String longitude;
  String language = "",lan = "en";
  bool islocateCompelete = false;
  bool _serviceEnabled;
  String userToken;
  var selected_shop_list = List<FavouriteShopPreferenceModel>();
  List<FavouriteShopPreferenceModel> favourite_shop_list = [];
  List<FavouriteShopPreferenceModel> temp_list = [];
  Position _currentPosition;
  String _currentAddress;
  int list_count = 0;
  TextEditingController _controller = new TextEditingController();
  FocusNode _textFocus = new FocusNode();

  @override
  void initState() {
    super.initState();
    _controller.addListener(sortItems);
    _textFocus.addListener(sortItems);
    Future lng = SharedPrefrence().getLanguage();
    lng.then((data) async {
      lan = data;
    });
    Future token = SharedPrefrence().getToken();
    token.then((data) async {
      userToken = data;
      getShopPreference();
    });

   // _GetLocationdata();
   // _getLocation();
    _getCurrentLocation();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    Size size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color.fromRGBO(243, 244, 244, 1),
        key: _scaffoldKey,
        appBar: AppBar(
        /*  leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: Color.fromRGBO(112,112,112,1),
                ),
                tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
              );
            },
          ),*/
          iconTheme: new IconThemeData(color: Color.fromRGBO(34, 83, 148, 1)),
          title: appbarLogo(),
          centerTitle: true,
          backgroundColor: Colors.white,
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: GestureDetector(
                child: Align(
                  child: Text('SKIP',style: TextStyle(color: Color.fromRGBO(0,161,237,1)),textAlign: TextAlign.center,),
                  alignment: Alignment.center,
                ),
                onTap: () {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                          builder: (context) => DashboardScreen()),
                      ModalRoute.withName("/login"));
                },
              ),
            ),
          ],
        ),
       // body: SingleChildScrollView(
          body: Stack(
            children: <Widget>[
              Center(
                child: Column(
                 // mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Expanded(
                      flex: 0,
                      child: Searchbar(),
                    ),
                    Expanded(
                      flex: 1,
                      child:buildBody(size),
                    ),

            ],
          ),
        ),
      ],
      ),
    )
    );
  }

  Widget appbarLogo() {
    return SvgPicture.asset("assets/images/shop_prefernce_indicator.svg",
        //image:SvgPicture.asset("assets/images/shop_prefernce_indicator.svg"),
        height: 10.0,
        width: 120,
        alignment: FractionalOffset.center);
  }

  Widget Searchbar() {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Container(
        width: 400,
        height: 40,
        child: TextField(
          controller: _controller,
          focusNode: _textFocus,
          textAlign: TextAlign.left,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            fillColor: Colors.white,
            prefixIcon: Icon(Icons.search),
            hintText: "Search",
            hintStyle: TextStyle(fontSize: 13),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15),
              borderSide: BorderSide(
                width: 0,
                style: BorderStyle.none,
              ),
            ),
            filled: true,
            contentPadding: EdgeInsets.all(16),
          ),
        ),
      ),
    );
  }

  Widget ShopGrid(Size size){
    return Container(
      height: size.height *1.5,
      child: GridView.count(
        crossAxisCount: 4,
        shrinkWrap: false,
        children: List.generate(list_count, (index) {
        //  FavouriteShopPreferenceModel data = FavouriteShopPreferenceModel[index];
          return Column(
            children: <Widget>[
              //Flexible(
               // flex: 0,
              //  child:
          GestureDetector(
                  onTap: () {
                   var selectedID = favourite_shop_list[index].id;
                    print(selected_shop_list);
                    if(favourite_shop_list[index].isclicked){
                      for(var i = 0 ; i < selected_shop_list.length ; i++){
                         if(selected_shop_list[i].id == selectedID){
                           setState(() {
                             selected_shop_list.removeAt(i);
                             favourite_shop_list[index].isclicked = false;
                           });
                           break;
                         }
                      }
                    }else{
                        setState(() {
                          favourite_shop_list[index].isclicked = true;
                          selected_shop_list.add(favourite_shop_list[index]);
                        });
                    }
                  },
                  child:favourite_shop_list[index].isclicked==false? Container(
                    width: 60,
                    height: 60,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.circle,

                    ),
                    padding: const EdgeInsets.all(3.0),
                    child: Padding(
                      padding: EdgeInsets.all(8),
          child:ClipRRect(
            //borderRadius: BorderRadius.circular(50.0),
              child: FadeInImage(
                image: NetworkImage(
                    Urls.baseImageUrl+favourite_shop_list[index].logo),
                placeholder:
                AssetImage(""),
                height: 35,
                width: 35,
                fit: BoxFit.contain,
              )),
          )
                  ):Container(
                      width: 60,
                      height: 60,
                      decoration: BoxDecoration(
                        //color: Colors.white,
                        shape: BoxShape.circle,
                        image: DecorationImage(image:AssetImage("assets/images/favourite_shop_icon.png")),

                      ),
                      padding: const EdgeInsets.all(3.0),
                      child: Padding(
                        padding: EdgeInsets.all(8),
                        child:ClipRRect(
                          //borderRadius: BorderRadius.circular(50.0),
                            child: FadeInImage(
                              image: NetworkImage(
                                  ""),
                              placeholder:
                              AssetImage(""),
                              height: 35,
                              width: 35,
                              fit: BoxFit.contain,
                            )),
                      )
                  )

                 /* Container(
                    width: 75.0,
                    height: 75.0,
                    decoration: new BoxDecoration(
                      border: Border.all(
                       // color: brand_list[index].isclicked ? Colors.blue : Colors.black12,
                      ),
                      shape: BoxShape.circle,
                    ),child: Padding(
                    padding: EdgeInsets.all(8),
                    child: Image.network(Urls.baseImageUrl+favourite_shop_list[index].logo,width: 40,height: 40,fit:BoxFit.contain,),
                  ),),*/
                ),
            //  ),
             // Flexible(
               // flex: 0,
                //child:
            Container(
                    height: 20,
                    child: Padding(
                      padding: const EdgeInsets.all(0.0),
                      child: Center(
                          child: Text(
                            favourite_shop_list[index].name,
                            style: TextStyle(
                                //fontFamily: 'montessarat',
                                //fontWeight: FontWeight.bold,
                                fontSize: 10,
                                color: Color.fromRGBO(
                                    34, 83, 148, 1)),textAlign: TextAlign.center,
                          )
                              ),
                    )),
            //  ),
            ],
          );
        }),
      ),
    );
  }

  Widget buildFooter() {
    return Container(
      height: 120,
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(color: Colors.white.withOpacity(0.5)),
     /* child: FlatButton(
        onPressed: () {},
        child: Text("Lean more", style: Theme.of(context).textTheme.button.copyWith(
            color: Theme.of(context).colorScheme.onBackground
        ),),
      ),*/
     child: Column(
       mainAxisAlignment: MainAxisAlignment.center,
       children: [
         Text(
           "Choose the stores you shop at.",
           style: TextStyle(color: Color.fromRGBO(0,157,221,1)),
           textAlign: TextAlign.center,
         ),
        GestureDetector(
          onTap: (){
            /*Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                    builder: (context) => ChangeCategoryPreference()),
                ModalRoute.withName("/login"));*/
            if(selected_shop_list.length<3){
              final snackBar = SnackBar(content: Text("Select atleast 3 shops"));
              _scaffoldKey.currentState.showSnackBar(snackBar);
            }
            else{
              saveShops();
            }

          },
          child:   Padding(
            padding: EdgeInsets.all(10),
            child:  Container(
                width: 250,
                height: 40,
                decoration: BoxDecoration(
                    color: Color.fromRGBO(0,157,221,1),
                    borderRadius: BorderRadius.circular(10)
                ),
                child:Padding(
                  padding: EdgeInsets.all(10),
                  child: Text("CONTINUE",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                )
            ),
          ),
        )
       ],
     ),
    );
  }

  Widget buildBody(Size size) {
    return Footer(
      body: ShopGrid(size),
      footer: buildFooter(),
    );
  }

  void getShopPreference() async {
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);

    var response = await http.get("${Urls.baseUrl}${Urls.FavouriteShopList}?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=100&locale="+lan,
        headers: {"Content-Type": "application/json",
                  "Authorization": "Bearer ${userToken}",
                   "Accept" : "application/json"},
       /* body: json.encode({
          "email": usermail,
          "password": userpass,
        })*/);

    Map<String, dynamic> value = json.decode(response.body);
    if (response.statusCode == 200) {
      dialog.dismissProgressDialog(context);
      try {
        print("response shopprefernce " + response.body.toString());
        Map<String, dynamic> value = json.decode(response.body);

        var status = value['success'];
        var message = value['message'];
        print(message);
        if (status == true) {
          favourite_shop_list.clear();
          try {
            var data = value['data'];
            if (data.length > 0) {
              for (int i = 0; i < data.length; i++) {
                var obj = data[i];
                var petrol_obj = obj['petrolstation'];
                if(obj['is_favorited']==1){
                  favourite_shop_list.add(FavouriteShopPreferenceModel(
                      obj['id'].toString(),
                      obj['name'].toString(),
                      obj['thumbs']['md'].toString(),
                      obj['is_favorited'].toString(),true));
                  temp_list.add(FavouriteShopPreferenceModel(
                      obj['id'].toString(),
                      obj['name'].toString(),
                      obj['thumbs']['md'].toString(),
                      obj['is_favorited'].toString(),true));
                  selected_shop_list.add(FavouriteShopPreferenceModel(
                      obj['id'].toString(),
                      obj['name'].toString(),
                      obj['thumbs']['md'].toString(),
                      obj['is_favorited'].toString(),true));
                }
                else{
                  favourite_shop_list.add(FavouriteShopPreferenceModel(
                      obj['id'].toString(),
                      obj['name'].toString(),
                      obj['thumbs']['md'].toString(),
                      obj['is_favorited'].toString(),false));
                  temp_list.add(FavouriteShopPreferenceModel(
                      obj['id'].toString(),
                      obj['name'].toString(),
                      obj['thumbs']['md'].toString(),
                      obj['is_favorited'].toString(),false));
                }

              }
              setState(() {
                /*list_count = pending_fuel_list.length;
                print(list_count);*/
                list_count = favourite_shop_list.length;
          print(favourite_shop_list.length);
        });
    }
    else {
    final snackBar = SnackBar(content: Text("No Data Available"));
    _scaffoldKey.currentState.showSnackBar(snackBar);
    }
    } catch (e) {
    e.toString();
    }

        }
        else {
          print("Error...");
          final snackBar = SnackBar(content: Text(message));
          _scaffoldKey.currentState.showSnackBar(snackBar);
        }
      } catch (e) {
        print(e.toString());
      }
    } else if (response.statusCode == 404) {
      dialog.dismissProgressDialog(context);
      var message = value['message'];
      final snackBar = SnackBar(content: Text(message));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    } else if (response.statusCode == 208) {
      dialog.dismissProgressDialog(context);
    } else {
      var message = value['message'];
      dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(content: Text(message));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  void saveShops() async {
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var array= [];
    for(int i =0 ; i < selected_shop_list.length ; i++){
      array.add(selected_shop_list[i].id);
    }
    var response = await http.post("${Urls.baseUrl}${Urls.SaveFavouriteShops}",
      headers: {"Content-Type": "application/json",
        "Authorization": "Bearer ${userToken}",
        "Accept" : "application/json"},
       body: json.encode({
          "shop_ids": array,
        }));

    Map<String, dynamic> value = json.decode(response.body);
    if (response.statusCode == 200) {
      dialog.dismissProgressDialog(context);
      try {
        print("response shopprefernce " + response.body.toString());
        Map<String, dynamic> value = json.decode(response.body);

        var status = value['success'];
        var message = value['message'];
        print(message);
        if (status == true) {
          //favourite_shop_list.clear();
          try {
            var data = value['data'].toString();
            if(data=="success"){
              setState(() {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DashboardScreen()),
                    ModalRoute.withName("/login"));
              });
            }

          } catch (e) {
            e.toString();
          }

        }
        else {
          print("Error...");
          final snackBar = SnackBar(content: Text(message));
          _scaffoldKey.currentState.showSnackBar(snackBar);
        }
      } catch (e) {
        print(e.toString());
      }
    } else if (response.statusCode == 404) {
      dialog.dismissProgressDialog(context);
      var message = value['message'];
      final snackBar = SnackBar(content: Text(message));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    } else if (response.statusCode == 208) {
      dialog.dismissProgressDialog(context);
    } else {
      var message = value['message'];
      dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(content: Text(message));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  ///Get Current location funtion
 /* Future _getLocation() async {
    setState(() {
      islocateCompelete = true;
    });
    Location location = new Location();

    var _permissionGranted = await location.hasPermission();
    _serviceEnabled = await location.serviceEnabled();`

    if (_permissionGranted != PermissionStatus.granted || !_serviceEnabled) {
      _permissionGranted = await location.requestPermission();

      _serviceEnabled = await location.requestService();

      setState(() {
        islocateCompelete = true;
      });
    } else {
      setState(() {
        islocateCompelete = false;
      });
    }

    LocationData _currentPosition = await location.getLocation();

    SharedPrefrence().setLatitude(_currentPosition.latitude.toString());
    SharedPrefrence().setLongitude(_currentPosition.longitude.toString());

   *//* try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        _currentAddress =
        "${place.locality}, ${place.postalCode}, ${place.country}";
        print(_currentAddress);
      });
    } catch (e) {
      print(e);
    }*//*

    *//*Future loginstatus = SharedPrefrence().getLogedIn();
    Future gust_status = SharedPrefrence().getGustLogedIn();
    gust_status.then((gustdata) {
      print(gustdata);
      loginstatus.then((data) {
        print(data);

        if (data == true || gustdata == true) {
          Navigator.pop(context, true);
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => HomeScreen()),
              ModalRoute.withName("/login"));
        } else {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
          );
        }
      });
    });*//*
  }*/

  _getCurrentLocation() async{
   getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });

      _getAddressFromLatLng();
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        _currentAddress =
        "${place.locality}, ${place.postalCode}, ${place.country}";
        print(_currentAddress);
        if(place.country=="United Arab Emirates"){
          SharedPrefrence().setLatitude(_currentPosition.latitude.toString());
          SharedPrefrence().setLongitude(_currentPosition.longitude.toString());
        }
        else{
          SharedPrefrence().setLatitude(Constants.latitude);
          SharedPrefrence().setLongitude(Constants.longitude);
        }
      });
    } catch (e) {
      print(e);
    }
  }

  void sortItems(){
    print( _controller.text);
    favourite_shop_list.clear();
    if(_controller.text != ""){
      for(var i = 0 ; i < temp_list.length ; i++){
        if(temp_list[i].name.toLowerCase().startsWith(_controller.text.toLowerCase())){
          favourite_shop_list.add(temp_list[i]);
        }
      }
      setState(() {
        list_count = favourite_shop_list.length;
      });

    }else{
      for(var i =0 ; i < temp_list.length ; i++){
        favourite_shop_list.add(temp_list[i]);
      }
      setState(() {
        list_count = favourite_shop_list.length;
      });
    }

  }

}

/*  favourite_shop_list.clear();
    if(_controller.text != ""){
      var isclicked = false;
      for(int i =0 ; i < temp_list.length ; i++){
        if(temp_list[i].name.toLowerCase().startsWith(_controller.text.toLowerCase())){
          for(int k =0 ; k < favourite_shop_list.length ; k++){
            if(favourite_shop_list[k].id == temp_list[i].id){
              setState(() {
                isclicked = favourite_shop_list[k].isclicked;
              });

              break;
            }
          }

          setState(() {
            favourite_shop_list.add(FavouriteShopPreferenceModel(temp_list[i].id,
                temp_list[i].name,
                temp_list[i].logo,
                temp_list[i].is_favourite,isclicked));
            isclicked = false;
            list_count = favourite_shop_list.length;
          });

        }
      }
    }else{
      var isclicked = false;
      for(int i =0 ; i< temp_list.length ; i++){
        for(int k = 0 ; k <favourite_shop_list.length ; k++){
          if(favourite_shop_list[k].id == temp_list[i].id){
           setState(() {
             isclicked = favourite_shop_list[k].isclicked;
           });
            break;
          }
        }

        setState(() {
          favourite_shop_list.add(
              FavouriteShopPreferenceModel(
                  temp_list[i].id,
                  temp_list[i].name,
                  temp_list[i].logo,
                  temp_list[i].is_favourite,isclicked
              ));
          list_count = favourite_shop_list.length;
          isclicked = false;
        });
      }
    }*/

/*  for(int i = 0 ; i< favourite_shop_list.length ; i++) {
              if(selectedID==favourite_shop_list[i].id){
                if (favourite_shop_list[i].isclicked) {
                  setState(() {
                    favourite_shop_list[i].isclicked = false;
                    for(int j = 0;j<selected_shop_list.length;j++){
                      if(selected_shop_list[j]==selectedID){
                        selected_shop_list.removeAt(j);
                      }
                    }
                    //selected_shop_list.remove(favourite_shop_list[i].id);
                  });
                  break;
                }
                else{
                  print(selected_shop_list);
                  setState(() {
                    favourite_shop_list[i].isclicked = true;
                    selected_shop_list.add(favourite_shop_list[i]);


                  });
                  break;
                }
              }

            } */
