

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart'as  http;
import 'Utils/Constants.dart';
import 'Utils/CustomDialogs.dart';
import 'Utils/SharedPrefrence.dart';
import 'Utils/Urls.dart';
import 'dart:convert';
import 'package:custom_progress_dialog/custom_progress_dialog.dart';
class FeedBack extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return state();
  }
}

class state extends State<FeedBack>{
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 15.0);
  String userToken,name,email;
  TextEditingController ctrl_feedback = TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future token = SharedPrefrence().getToken();
    token.then((data) async {
      userToken = data;
    });
    Future sh_name = SharedPrefrence().getUserName();
    sh_name.then((data) async {
      name = data;
    });

    Future sh_email = SharedPrefrence().getUserEmail();
    sh_email.then((data) async {
      email = data;
    });

  }
  @override
  Widget build(BuildContext context) {
     return Scaffold(
         key: _scaffoldKey,
         resizeToAvoidBottomInset: false,
         appBar :AppBar(
           centerTitle: false,
           iconTheme: new IconThemeData(color: Color(0xff00ADEE)),
           title: Text(
             "FeedBack",
             style: TextStyle(color: Color(0xff00ADEE), fontSize: 13),
           ),
           backgroundColor: Colors.white,
         ),
       body: SingleChildScrollView(
         child: Column(
           children: [
             SizedBox(
               height: 25,
             ),
             Padding(padding: EdgeInsets.all(8),
              child:Align(
                alignment: Alignment.topLeft,
                child:  Text('Feel Free To Drop us \nYour Feedback',style: TextStyle(
                    fontSize: 20,color: Colors.blue,fontWeight: FontWeight.bold
                ),),
              ),),
             Padding(
               padding: EdgeInsets.only(left: 8,right: 8,top: 7),
               child: Text('Do you have a Suggestion or Found some Bugs?\nwe would love to hear from you pls let us know what is on your mind as were always looking to improve the app'),
             ),
             Padding(
               padding: EdgeInsets.only(top: 15,left: 8,right: 8),
               child:   Container(
                 margin: EdgeInsets.all(12),
                 height: 5 * 24.0,
                 child: TextField(
                   controller: ctrl_feedback,
                    maxLines: 5,
                    decoration: InputDecoration(
                      labelText: "Describe your issue or idea..",
                      fillColor: Colors.grey,
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8.0),
                        borderSide: BorderSide(
                          color: Colors.grey,
                        ),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8.0),
                        borderSide: BorderSide(
                          color: Colors.grey,
                          width: 1.0,
                        ),
                      ),
                    )
                 )),
             ),
             Padding(
               padding: EdgeInsets.only(top: 15,left: 55,right: 55),
               child: Material(
                 elevation: 5.0,
                 borderRadius: BorderRadius.circular(30.0),
                 color: Color(0xff00ADEE),
                 child: MaterialButton(
                   minWidth: MediaQuery.of(context).size.width,
                   padding: EdgeInsets.fromLTRB(10.0, 15.0, 20.0, 10.0),
                   onPressed: () {
                     if(ctrl_feedback.text.isNotEmpty){
                       sendFeedback(ctrl_feedback.text);
                     }else{
                       final snackBar = SnackBar(content: Text('Enter Feedback',style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
                       _scaffoldKey.currentState.showSnackBar(snackBar);
                     }
                   },
                   child: Text("Send Feedback",
                       textAlign: TextAlign.center,
                       style: style.copyWith(
                           color: Colors.white, fontWeight: FontWeight.bold)),
                 ),
               ),
             )
           ],
         ),
       ),
     );
  }

  void sendFeedback(String feedback)async{
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = name;
    data['email'] = email;
    data['message'] = feedback;
    var bodydata = json.encode(data);//
    var response = await http.post(
        "${Urls.baseUrl}${Urls.Feedback}",
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        },body: bodydata);
    Map<String, dynamic> value = json.decode(response.body);
    if(response.statusCode == 200){
      dialog.dismissProgressDialog(context);
      try{
        final snackBar = SnackBar(content: Text(value['message'],style: TextStyle(color: Colors.white)),backgroundColor: Colors.green,);
        _scaffoldKey.currentState.showSnackBar(snackBar);
        ctrl_feedback.clear();
      }catch(e){}
    }else{
      dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(content: Text('Try Again',style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
      _scaffoldKey.currentState.showSnackBar(snackBar);

    }
  }

}