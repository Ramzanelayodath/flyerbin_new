import 'dart:io';

import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'ChangeLocationScreen.dart';
import 'ChangeShopPreference.dart';
import 'FeedBack.dart';
import 'Languages.dart';
import 'Loginscreen.dart';
import 'MallsListScreen.dart';
import 'ProfileScreen.dart';
import 'Settings.dart';
import 'Utils/SharedPrefrence.dart';

class SideDraweScreen extends StatefulWidget {
  @override
  _SideDraweScreenState createState() => _SideDraweScreenState();
}

class _SideDraweScreenState extends State<SideDraweScreen> {

  String userName = "",userPhoto = "";

  @override
  void initState() {
    super.initState();
    Future username = SharedPrefrence().getUserName();
   // Future longitude = SharedPrefrence().getLongitude();
    username.then((name) async {
        userName = name;
    });
    Future photo = SharedPrefrence().getUserPhoto();
    photo.then((data) async {
      userPhoto = data;
    });

  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Drawer(
        child: ListView(
          children: [
            Stack(
              children: [
                DrawerHeader(
                  decoration: BoxDecoration(
                   // color: Colors.white,
                  ),
                  child: Center(
                    child: Column(
                      children: [
                        Container(
                            width: 70,
                            height: 70,
                            decoration: BoxDecoration(
                              //color: Colors.white,
                              shape: BoxShape.circle,
                             color: Colors.white
                             // image: DecorationImage(image:AssetImage("assets/images/user_palceholder.png")),

                            ),
                            padding: const EdgeInsets.all(3.0),
                            child: Padding(
                              padding: EdgeInsets.all(0),
                              child:ClipRRect(
                                //borderRadius: BorderRadius.circular(50.0),
                                  child: FadeInImage(
                                    image: NetworkImage(
                                        " "),
                                    placeholder:
                                    AssetImage("assets/images/user_palceholder.png"),
                                    height: 70,
                                    width: 70,
                                    fit: BoxFit.fill,
                                  )),
                            )
                        ),
                        Padding(padding: EdgeInsets.all(10),child:Text(userName,style: TextStyle(color: Color(0xff00ADEE)),textAlign:TextAlign.center ,) ,)

                      ],
                    ),
                  ),
                ),
              ],
            ),
           // Divider(),
            GestureDetector(
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ProfileScreen(),
                  ),
                );
              },
              child: Container(
                 child: ListTile(
                       leading: Icon(Icons.person,
                       color:Color(0xff00ADEE), size: 30),
                      title: Text(
                             "Profile",
                       style: TextStyle(fontWeight: FontWeight.bold),
    )),
              ),
            ),
            GestureDetector(
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ChangeLocationScreen(),
                  ),
                );
              },
              child: Container(
                child: ListTile(
                    leading: Icon(Icons.location_on,
                        color:Color(0xff00ADEE), size: 30),
                    title: Text(
                      "Change Location",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )),
              ),
            ),
            GestureDetector(
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MallsListScreen(),
                  ),
                );
              },
              child: Container(
                child: ListTile(
                    leading: Icon(Icons.location_city,
                        color:Color(0xff00ADEE), size: 30),
                    title: Text(
                      "Malls",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )),
              ),
            ),
            GestureDetector(
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ChangeShopPreference(),
                  ),
                );
              },
              child: Container(
                child: ListTile(
                    leading: Icon(Icons.favorite,
                        color:Color(0xff00ADEE), size: 30),
                    title: Text(
                      "Change Preference",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )),
              ),
            ),
            GestureDetector(
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Languages(),
                  ),
                );
              },
              child: Container(
                child: ListTile(
                    leading: Icon(Icons.language,
                        color:Color(0xff00ADEE), size: 30),
                    title: Text(
                      "Language",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )),
              ),
            ),
            GestureDetector(
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Settings(),
                  ),
                );
              },
              child: Container(
                child: ListTile(
                    leading: Icon(Icons.settings,
                        color:Color(0xff00ADEE), size: 30),
                    title: Text(
                      "Settings",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )),
              ),
            ),
            GestureDetector(
              onTap: (){
                share();
              },
              child: Container(
                child: ListTile(
                    leading: Icon(Icons.share,
                        color:Color(0xff00ADEE), size: 30),
                    title: Text(
                      "Share",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )),
              ),
            ),
            GestureDetector(
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => FeedBack(),
                  ),
                );
              },
              child: Container(
                child: ListTile(
                    leading: Icon(Icons.feedback,
                        color:Color(0xff00ADEE), size: 30),
                    title: Text(
                      "Feedback",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )),
              ),
            ),
            GestureDetector(
              onTap: () async {
                SharedPreferences preferences =
                    await SharedPreferences.getInstance();
                await preferences.remove('token');
                SharedPrefrence().setLoggedIn(false);

                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => LoginScreen(),
                  ),
                );
              },
              child: Container(
               // color: Colors.black12,
                child: ListTile(
                    leading: Icon(Icons.exit_to_app,
                        color: Color(0xff00ADEE), size: 30),
                    title: Text(
                      "Logout",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )),
              ),
            ),
          ],
        ),
      ),
    );
  }
  void share(){
    var shareMessage="";
    if(Platform.isIOS){
    shareMessage="https://apps.apple.com/in/app/flyerbin/id1502843770";
    Share.share(shareMessage,subject: "Flyerbin");
    }
    else{
        shareMessage = "https://play.google.com/store/apps/details?id=com.flyerbinapps.flyerbin";
        Share.share(shareMessage,subject: "Flyerbin");
    }
  }

  void showLanguageAlert(){

    showDialog(context: context, child:AlertDialog(
      title: Text(
        ' Select Language',
        textScaleFactor: 1,
      ),
      content: SingleChildScrollView( // won't be scrollable
       // child: Text('Scrollable content', textScaleFactor: 5),
      ),
      actions: <Widget>[
        TextButton(child: Text('English'), onPressed: () {
          SharedPrefrence().setLanguage('en');
          setState(() {

          });
          Navigator.of(context).pop();}),
        TextButton(child: Text('Arabic'), onPressed: () {
          SharedPrefrence().setLanguage('ar');
          setState(() {

          });
          Navigator.of(context).pop();}),
      ],
    ),
    );
  }
}
