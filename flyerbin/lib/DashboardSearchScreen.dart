import 'dart:async';
import 'dart:convert';

import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:shimmer/shimmer.dart';

import 'ListItems.dart';
import 'Model/RecommendedItemModel.dart';
import 'Model/SearchDataModel.dart';
import 'PopularItemListScreen.dart';
import 'SearchResultScreen.dart';
import 'SideDrawerScreen.dart';
import 'package:http/http.dart' as http;

import 'Utils/Constants.dart';
import 'Utils/SharedPrefrence.dart';
import 'Utils/Urls.dart';

class DashboardSearchScreen extends StatefulWidget {
  @override
  _DashboardSearchScreenState createState() => _DashboardSearchScreenState();
}

class _DashboardSearchScreenState extends State<DashboardSearchScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  bool loading=true;
  Timer timer;
  String userToken,lan = 'en';
  List<RecommendedItemModel> recommentedList = [];
  GlobalKey<AutoCompleteTextFieldState<SearchDataModel>> key = new GlobalKey();
  AutoCompleteTextField searchTextField;
   TextEditingController _typeAheadController = TextEditingController();
   List<SearchDataModel> search_data;



  void startTimer() {
    // Start the periodic timer which prints something every 1 seconds
    timer = new Timer(new Duration(seconds: 3), () {
      setState(() {
        loading = false;
        timer.cancel();
      });
    });
  }

  void initState(){
    super.initState();
    Future lng = SharedPrefrence().getLanguage();
    lng.then((data) async {
      lan = data;
    });
    Future token = SharedPrefrence().getToken();
    token.then((data) async {
      userToken = data;
      getRecommendedItems();
    });

  }

  @override
  Widget build(BuildContext context) {
    startTimer();
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomInset: false,
        body: DoubleBackToCloseApp(
        snackBar: const SnackBar(
        content: Text('Tap back again to exit'),
    ),child: SingleChildScrollView(
          child: Column(
            children: [
              Expanded(flex: 0, child: _SearchText()),
              Expanded(
                  flex: 0,
                  child: Center(
                      child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: Text("Popular in you area"),
                  ))),
              Expanded(flex: 0, child: _PopularArea()),
            ],
          ),
        ),
      ),
    ),);
  }

  Widget appbarLogo() {
    return SvgPicture.asset(
      "assets/images/flyerbin_logo.svg",
      height: 40,
    );
  }

  Widget _SearchText() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: /*Container(
        width: 380,
        height: 35,
        child: TextField(
          autofocus: false,
          decoration: InputDecoration(
              prefixIcon: Icon(Icons.search),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(25.0),
                ),
              ),
              hintText: "Search...",
              hintStyle: TextStyle(color: Colors.blueGrey, fontSize: 14),
              filled: true,
              fillColor: Colors.white),
        ),
      ),*/
      Container(
        height: 35,
        child:TypeAheadField(
          textFieldConfiguration: TextFieldConfiguration(
            controller: _typeAheadController,
              textInputAction: TextInputAction.search,
              onSubmitted: (value){
              print("search "+value);
              if(value=="") {
                final snackBar = SnackBar(content: Text("Please enter search item"));
                _scaffoldKey.currentState.showSnackBar(snackBar);
              }
              else{
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) =>  SearchResultScreen(value,value)
                ));
              }

              },
              autofocus: false,
              style: DefaultTextStyle.of(context).style.copyWith(
                  fontStyle: FontStyle.italic
              ),
              decoration: InputDecoration(
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
                  contentPadding: EdgeInsets.all(10),
                  hintText: "Search"

              )
          ),
          suggestionsCallback: (pattern) async {
            return await SearchViewModel.loadPlayers(pattern);
          },
          itemBuilder: (context, suggestion) {
            print("Sug "+suggestion['name']);
            return ListTile(
              // leading: Icon(Icons.shopping_cart),
              title: Text(suggestion['name'],style: TextStyle(fontSize: 12),),
              // subtitle: Text('\$${suggestion['price']}'),
            );
          },
          onSuggestionSelected: (suggestion) {
            print(suggestion);
            setState(() {
              _typeAheadController.text = suggestion['name'];
            });
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) =>  SearchResultScreen(suggestion['name'],suggestion['name'])
            ));
          },
        ) ,
      )

      );

  }

  Widget _PopularArea() {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Center(
          child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              padding: EdgeInsets.all(5.0),
              child: GridView.count(
                crossAxisCount: 3,
                shrinkWrap: true,
                physics: ScrollPhysics(),
                children: List.generate(
                  recommentedList.length,
                  (index) {
                    return GestureDetector(
                      onTap: () {

                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => SearchResultScreen(recommentedList[index].slug,recommentedList[index].name),
                          ),
                        );
                      },
                      child: loading?
                      Shimmer.fromColors(
                        baseColor: Colors.grey[200],
                        highlightColor: Colors.grey,
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Container(
                            height: size.height * 0.6,
                            //width: 30,
                            //height: 30,
                            decoration: BoxDecoration(
                              color: Color(0xFFFFF29A),
                              shape: BoxShape.circle,
                              /*borderRadius: BorderRadius.all(
                                Radius.circular(15),
                              ),*/
                            ),

                          ),
                        ),
                      ):Container(
                        height: size.height * 0.6,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              flex: 0,
                              child: Container(
                                width: 70,
                                height: 70,
                                decoration:
                                    BoxDecoration(shape: BoxShape.circle,color: Colors.white),
                                padding: const EdgeInsets.all(3.0),
                                child: ClipRRect(
                                    borderRadius: BorderRadius.circular(50.0),
                                    child: FadeInImage(
                                      image:
                                          NetworkImage("${Urls.baseImageUrl}${recommentedList[index].icon}"),
                                      placeholder:
                                          AssetImage("assets/images/flyerbin_logo.svg"),
                                      //height: 30,
                                      //width: 30,
                                    )),
                              ),
                            ),
                            Expanded(
                              flex: 0,
                              child: Center(
                                child: Padding(
                                  padding: const EdgeInsets.all(10),
                                  child: Text(
                                    recommentedList[index].name,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              )),
        ],
      )),
    );
  }



  void getRecommendedItems() async {
   // ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
   var name;
    var response = await http.get("${Urls.baseUrl}${Urls.recomentedList}?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=100&locale="+lan,
      headers: {"Content-Type": "application/json",
        "Authorization": "Bearer ${userToken}",
        "Accept" : "application/json"},
      /* body: json.encode({
          "email": usermail,
          "password": userpass,
        })*/);

    Map<String, dynamic> value = json.decode(response.body);
    if (response.statusCode == 200) {
     // dialog.dismissProgressDialog(context);
      try {
        print("response recommendations " + response.body.toString());
        Map<String, dynamic> value = json.decode(response.body);

        var status = value['success'];
        var message = value['message'];
        print(message);
        if (status == true) {
          recommentedList.clear();
          try {
            var data = value['data'];
            if (data.length > 0) {
              for (int i = 0; i < data.length; i++) {
                var obj = data[i];
               // var petrol_obj = obj['petrolstation'];
                if(obj['translated'] != null){
                  name = obj['translated']['name'];
                }else{
                  name = obj['name'].toString();
                }
                recommentedList.add(RecommendedItemModel(
                    obj['id'].toString(),
                    name,
                    obj['thumbs']['md'].toString(),
                    "false",
                    "0",
                    obj['tag_category_id'].toString(),
                    obj['slug'].toString(), "false"));
              }
              setState(() {
                /*list_count = pending_fuel_list.length;
                print(list_count);*/
                print(recommentedList.length);
              });
            }
            else {
              final snackBar = SnackBar(content: Text("No Data Available"));
              _scaffoldKey.currentState.showSnackBar(snackBar);
            }
          } catch (e) {
            e.toString();
          }

        }
        else {
          print("Error...");
          final snackBar = SnackBar(content: Text(message));
          _scaffoldKey.currentState.showSnackBar(snackBar);
        }
      } catch (e) {
        print(e.toString());
      }
    } else if (response.statusCode == 404) {
     // dialog.dismissProgressDialog(context);
      var message = value['message'];
      final snackBar = SnackBar(content: Text(message));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    } else if (response.statusCode == 208) {
      //dialog.dismissProgressDialog(context);
    } else {
      var message = value['message'];
      //dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(content: Text(message));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  Future loadPlayers(String keyword) async {
    try {
      search_data = new List<SearchDataModel>();
      var response = await http.get(
        "${Urls.baseUrl}${Urls.searchAutoComplt}?s=${keyword}&locale="+lan,
        headers: {"Content-Type": "application/json",
          //"Authorization": "Bearer ${userToken}",
          "Accept": "application/json"},
        /* body: json.encode({
          "email": usermail,
          "password": userpass,
        })*/);
      Map<String, dynamic> value = json.decode(response.body);
      var brands = value['data']['brands'] as List;
      var tags = value['data']['tags'] as List;
      for (int i = 0; i < brands.length; i++) {
      // search_data.add(new SearchDataModel.fromJson(brands[i]));
      }
      for (int i = 0; i < tags.length; i++) {
       // search_data.add(new SearchDataModel.fromJson(tags[i]));
      }
    } catch (e) {
      print(e);
    }
  }
}
