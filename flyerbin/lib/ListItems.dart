

import 'dart:convert';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher.dart';
import 'Cart.dart';
import 'FlyerLoadScreen.dart';
import 'Model/OffersInEachItemModel.dart';
import 'SimiliarProductScreen.dart';
import 'Utils/Constants.dart';
import 'Utils/CustomDialogs.dart';
import 'Utils/SharedPrefrence.dart';
import 'Utils/Urls.dart';
import 'package:share/share.dart';
class ListItems extends StatefulWidget{
  String slug,name;

  ListItems(this.slug, this.name);

  @override
  State<StatefulWidget> createState() {
    return state(slug,name);
  }
}

class state extends State<ListItems>{
  String slug,name,related_image = "",related_image_2 = "";
  bool show_similiar_prdouct = false;
  var page = 1,last_page = 1,list_count =0;
  var list = List<OffersInEachItemModel>();
  state(this.slug, this.name);

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  var title = "",userToken="",lan = 'en';
  ScrollController _sccontroller = new ScrollController();
  bool isLoading = true;
  @override
  void initState() {
    super.initState();
    Future lng = SharedPrefrence().getLanguage();
    lng.then((data) async {
      lan = data;
    });
    Future token = SharedPrefrence().getToken();
    token.then((data) async {
      userToken = data;
      // getItems(slug, page.toString());
      this.getItems(slug,page);
      _sccontroller.addListener(() {
        if (_sccontroller.position.pixels ==
            _sccontroller.position.maxScrollExtent) {
          if(page==0){

          }
          else{
            getItems(slug,page);
          }

        }
      });
    });
    setState(() {
      title = name;
    });

  }

  @override
  void dispose() {
    _sccontroller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomInset: false,
      appBar :AppBar(
        centerTitle: false,
        iconTheme: new IconThemeData(color: Color(0xff00ADEE)),
        title: Text(
          title,
          style: TextStyle(color: Color(0xff00ADEE), fontSize: 13),
        ),
        backgroundColor: Colors.white,
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: GestureDetector(
              child: Icon(
                Icons.shopping_cart,
                color: Color(0xff00ADEE),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Cart(),
                  ),
                );
              },
            ),
          )
        ],
      ),
      body:isLoading?Shimmer.fromColors(
        baseColor: Colors.grey[200],
        highlightColor: Colors.grey,
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Container(
            height: 250,
            width: 200,
            //width: 30,
            //height: 30,
            decoration: BoxDecoration(
              color: Color(0xFFFFF29A),
              shape: BoxShape.rectangle,
              /*borderRadius: BorderRadius.all(
                                Radius.circular(15),
                              ),*/
            ),

          ),
        ),
      ):list_count==0?Center(child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text("No products to show"),
      )): GridView.builder(
          controller: _sccontroller,
          itemCount: list_count,
          physics: const AlwaysScrollableScrollPhysics(),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(childAspectRatio: 6 / 7.5,crossAxisCount: 2),
          shrinkWrap: true,
          itemBuilder: (BuildContext context, int index) {
             //  print(list_count);
            if (index == list_count) {
              if(page==0){
                return Center(child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(" "),
                ));
              }
              else{
                return Shimmer.fromColors(
                  baseColor: Colors.grey[200],
                  highlightColor: Colors.grey,
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Container(
                      height: size.height * 0.6,
                      //width: 30,
                      //height: 30,
                      decoration: BoxDecoration(
                        color: Color(0xFFFFF29A),
                        shape: BoxShape.rectangle,
                        /*borderRadius: BorderRadius.all(
                                Radius.circular(15),
                              ),*/
                      ),

                    ),
                  ),
                );
              }
               //_buildProgressIndicator();
            } else {
              return SingleChildScrollView(
                physics: NeverScrollableScrollPhysics(),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GestureDetector(
                    onTap: (){
                      ClipFoodCoupnAlert(context,list[index]);
                    },
                    child: Container(
                      height: 300,
                      child: Card(
                        elevation: 2,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(2),
                        ),
                        child:  Container(
                            width: 300,
                            child:Stack(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.all(2),
                                      child: Image.network(
                                        Urls.baseImageUrl+list[index].item_path,
                                        fit: BoxFit.fill,
                                        height: 180,
                                        width: 180,
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8),
                                      child: Text(list[index].name,style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(3),
                                      child: RichText(
                                        text: TextSpan(
                                            text: 'At ',
                                            style: TextStyle(
                                                color: Colors.blueGrey, fontSize: 10),
                                            children: <TextSpan>[
                                              TextSpan(text: list[index].brand_name_or_shop_name,
                                                style: TextStyle(
                                                    color: Color(0xff00ADEE), fontSize: 10,fontWeight: FontWeight.bold),)
                                            ]
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Align(alignment: Alignment.topRight,child:  Visibility(
                                  child: Text('Clipped',style: TextStyle(backgroundColor: Colors.green,fontWeight: FontWeight.bold,color: Colors.white),),
                                  visible: list[index].isClipped,
                                ),)
                              ],
                            )),
                      ),
                    ),
                  ),
                ),
              );
            }
          })    );
  }
  Widget _buildProgressIndicator() {
    return new Center(
      child: new Opacity(
        opacity: isLoading ? 1.0 : 00,
        child: new CircularProgressIndicator(),
      ),
    );
  }
  void getItems(String slug,int index)async{
    // ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    /*if (!isLoading) {
      setState(() {
        isLoading = true;
      });*/
      List<OffersInEachItemModel> tList = List();
      var brand_or_shop_name = "";
      var brand_or_shop_id = "";
      var brand_or_shop_logo = "";
      print( "${Urls.baseUrl}${Urls.GetOffersInEachItems}${slug}?latitude=${Constants
          .latitude}&longitude=${Constants.longitude}&radius=100&locale=" +
          lan + "&page=${index.toString()}");
      var response = await http.get(
        "${Urls.baseUrl}${Urls.GetOffersInEachItems}${slug}?latitude=${Constants
            .latitude}&longitude=${Constants.longitude}&radius=100&locale=" +
            lan + "&page=${index.toString()}",
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        },
      );
      print("resp_listItem " + response.body);
      Map<String, dynamic> value = json.decode(response.body);
      if (response.statusCode == 200) {
        // dialog.dismissProgressDialog(context);
        try {
          var data = value['data'];
          var items = data['items'];
          var array = items['data'];
          if (array.length > 0) {
            for (int i = 0; i < array.length; i++) {
              var obj = array[i];
              if (obj['brand_id'] == null) {
                brand_or_shop_id = obj['shop_id'].toString();
                brand_or_shop_name = obj['shop_name'].toString();
              }
              else {
                brand_or_shop_id = obj['brand_id'].toString();
                brand_or_shop_name = obj['brand_name'].toString();
              }
              if (obj['shop_logo'] != null) {
                brand_or_shop_logo = obj['shop_logo'].toString();
              }
              else {
                brand_or_shop_logo = obj['brand_logo'].toString();
              }
              list.add(OffersInEachItemModel(
                  obj['id'].toString(),
                  obj['flyer_page_id'].toString(),
                  obj['category_id'].toString(),
                  obj['type'].toString(),
                  obj['name'].toString(),
                  obj['description'].toString(),
                  obj['url'].toString(),
                  obj['sku_no'].toString(),
                  obj['sales_details'].toString(),
                  obj['offer_details'].toString(),
                  obj['valid_from'].toString(),
                  obj['valid_to'].toString(),
                  obj['offer_description'].toString(),
                  obj['thumbs']['md'].toString(),
                  obj['flyer_id'].toString(),
                  obj['flyer_slug'].toString(),
                  brand_or_shop_id,
                  brand_or_shop_name,
                  obj['isClipped'],
                  brand_or_shop_logo,
                  obj['price'].toString()));
            }
            setState(() {
              last_page = items['last_page'];
              isLoading=false;
              if(last_page==1){
                list_count = list.length;
                isLoading = false;
                list.addAll(tList);
                print(list.length);
                page=0;
              }
              if(page!=last_page&&page!=0){
                list_count = list.length;
                isLoading = false;
                list.addAll(tList);
                print(list.length);
                page++;
              }
              else if(page==last_page&&page!=0){
                list_count = list.length;
                isLoading = false;
                list.addAll(tList);
                print(list.length);
                page=0;
              }
              else{
                page=0;
              }

            });
          }
          else{
            setState(() {
              isLoading=false;
            });
          }

        } catch (e) {
          e.toString();
        }
      } else {
        // dialog.dismissProgressDialog(context);
        final snackBar = SnackBar(
          content: Text("Try Again...", style: TextStyle(color: Colors.white)),
          backgroundColor: Colors.red,);
        _scaffoldKey.currentState.showSnackBar(snackBar);
      }
   /* }
    else
      {
        setState(() {
          isLoading = false;
        });
      }*/
  }

  void getTrendingItems(String slug,String page)async{
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var brand_or_shop_name = "";
    var brand_or_shop_id = "";
    var response = await http.get(
        "${Urls.baseUrl}${Urls.TrendindItems}/${slug}?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=100&locale="+lan+"&page="+page,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print("resp_trednding_delas "+response.body);
    if(response.statusCode == 200){
      dialog.dismissProgressDialog(context);
      try{
        var data = value['data'];
        last_page = data['last_page'];
        var array = data['data'];
        for(int i = 0 ; i< array.length ; i++){
          var obj = array[i];
          if(obj['brand_id'] == null){
            brand_or_shop_id = obj['shop_id'].toString();
            brand_or_shop_name = obj['shop_name'].toString();
          }else{
            brand_or_shop_id = obj['brand_id'].toString();
            brand_or_shop_name = obj['brand_name'].toString();
          }
          var thumb = obj['thumbs'];
          list.add(OffersInEachItemModel(obj['id'].toString(),
              obj['flyer_page_id'].toString(),
              obj['category_id'].toString(),
              obj['type'].toString(),
              obj['name'].toString(),
              obj['description'].toString(),
              obj['url'].toString(),
              obj['sku_no'].toString(),
              obj['sales_details'].toString(),
              obj['offer_details'].toString(),
              obj['valid_from'].toString(),
              obj['valid_to'].toString(),
              obj['offer_description'].toString(),
              obj['item_path'].toString(),
              obj['flyer_id'].toString(),
              obj['flyer_slug'].toString(),
              brand_or_shop_id,
              brand_or_shop_name,
              obj['isClipped'],
              obj['brand_logo'].toString(),
              obj['price'].toString()));
        }
        setState(() {
          list_count = list.length;
        });
      }catch(e){
        e.toString();
      }
    }else{
      dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(content: Text("Try Again",style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,);
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  Future<void> ClipFoodCoupnAlert(BuildContext context,OffersInEachItemModel data) async{
    setState(() {
      getSimiliarProduct(data.id.toString());
    });

    await showGeneralDialog(
        context: context,
        barrierDismissible: true,
        barrierLabel: MaterialLocalizations.of(context)
            .modalBarrierDismissLabel,
        barrierColor: Colors.black45,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (BuildContext buildContext,
            Animation animation,
            Animation secondaryAnimation) {
          return StatefulBuilder(
              builder: (context,setState){
                return Center(
                  child: Card(
                    elevation: 6,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                         // width: MediaQuery.of(context).size.width - 10,
                         // height: MediaQuery.of(context).size.height -  110,
                          padding: EdgeInsets.all(0),
                          decoration: BoxDecoration(borderRadius: BorderRadius.circular(10.0),
                            color: Colors.white,
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(10),
                                    child: Container(
                                      child:Row(
                                        children: [
                                          Image.network(
                                            Urls.baseImageUrl+data.brand_logo,
                                            fit: BoxFit.fill,
                                            width: 40,
                                            height: 40,),

                                        ],
                                      ),
                                    ),
                                  ),

                                  Padding(
                                    padding: const EdgeInsets.all(5),
                                    child: GestureDetector(onTap: (){
                                      Navigator.pop(context);
                                    },child: Icon(Icons.cancel,color: Colors.blueGrey,)),
                                  )
                                ],
                              ),
                              Align(
                                alignment: Alignment.topRight,
                                child: Padding(
                                  padding: EdgeInsets.only(right: 8,top: 8),
                                  child: Text('Valid To : '+data.valid_to),
                                ),
                              ),
                              Divider(),
                              Padding(
                                padding: EdgeInsets.all(8),
                                child:Align(
                                  child:Container(
                                    height: 250,
                                    width: 300,
                                    child: Image.network(
                                      Urls.baseImageUrl+data.item_path,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  alignment: Alignment.center,
                                )
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  data.name,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 22),
                                ),
                              ),

                              Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Text(
                                  data.price.toString()+" AED",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.lightBlue,
                                      fontSize: 25),
                                ),
                              ),
                              Visibility(
                                child: Padding(padding: EdgeInsets.all(8),child:  Row(
                                  children: [
                                    Image.network(related_image,width: 50,height: 50,),
                                    Padding(padding: EdgeInsets.only(left: 5,right: 5),
                                      child: Image.network(related_image_2,width: 50,height: 50,),),
                                    Column(
                                      children: [
                                        Padding(padding: EdgeInsets.all(5),
                                          child:GestureDetector(
                                            child:Text('See More >>',style: TextStyle(color: Colors.blue,fontWeight: FontWeight.bold),),
                                            onTap: (){
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) => SimiliarProductScreen(data.id.toString()),
                                                ),
                                              );
                                            },),
                                        ),
                                        GestureDetector(
                                          child: Container(
                                              padding: EdgeInsets.all(3),
                                              decoration: BoxDecoration(border: Border.all(color: Colors.green,),
                                                  borderRadius: BorderRadius.all(Radius.circular(20)),color: Colors.green),
                                              child: Padding(padding: EdgeInsets.all(2),
                                                child: Row(
                                                  children: [
                                                    Text('View Flyer',style: TextStyle(color: Colors.white,fontSize: 12),)
                                                  ],
                                                ),)
                                          ) ,
                                          onTap: (){
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) => FlyerLoadScreen(
                                                    data.flyer_id.toString(),
                                                    data.brand_name_or_shop_name.toString(),
                                                    data.valid_from.toString(),
                                                    data.valid_to.toString(),
                                                    data.brand_logo.toString(),
                                                    true,
                                                    data.id.toString()),
                                              ),
                                            );
                                          },
                                        )

                                      ],
                                    ),
                                  ],
                                ),),
                                visible: true,
                              ),
                              Container(
                                width: double.infinity,
                                height: 55,
                                child:Row(
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: Padding(
                                        padding: EdgeInsets.only(right: 2),
                                        child: GestureDetector(
                                          child: Container(
                                            width: double.infinity,
                                            height: 55,
                                            color: Colors.blue,
                                            child: Center(
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                    flex: 1,
                                                    child: Icon(Icons.ios_share,color: Colors.white,size: 20,),
                                                  ),
                                                  Expanded(
                                                    flex: 1,
                                                    child: Text('Share',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 16),),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                          onTap: (){
                                            share(data);
                                          },
                                        ),
                                      ),
                                    ),
                                    Visibility(
                                      visible: data.type == "offline" ||  data.type == "both",
                                      child:  Expanded(
                                        flex: 1,
                                        child: Padding(
                                          padding: EdgeInsets.only(right: 2),
                                          child: GestureDetector(
                                            child: Container(
                                                width: double.infinity,
                                                height: 55,
                                                color: data.isClipped?Colors.red:Colors.blue,
                                                child: Center(
                                                  child: Row(
                                                    children: [
                                                      Expanded(
                                                        flex: 1,
                                                        child: Icon(Icons.shopping_bag_outlined,color: Colors.white,size: 20,),
                                                      ),
                                                      Expanded(
                                                        flex: 1,
                                                        child: Text(data.isClipped ? 'Unclip' :'Clip',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 16),),
                                                      )
                                                    ],
                                                  ),
                                                )
                                            ),
                                            onTap: (){
                                              doclipUnclip(data);
                                            },
                                          ),
                                        ),
                                      ),
                                    ),
                                    Visibility(
                                        visible: data.type == "both",
                                        child: Expanded(
                                          flex: 1,
                                          child: Padding(
                                            padding: EdgeInsets.only(right: 2),
                                            child: GestureDetector(
                                              child: Container(
                                                  width: double.infinity,
                                                  height: 55,
                                                  color: Colors.blue,
                                                  child: Center(
                                                    child: Row(
                                                      children: [
                                                        Expanded(
                                                          flex: 1,
                                                          child: Icon(Icons.shopping_basket,color: Colors.white,size: 20,),
                                                        ),
                                                        Expanded(
                                                          flex: 1,
                                                          child: Text('Buy Now',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 16),),
                                                        )
                                                      ],
                                                    ),
                                                  )
                                              ),
                                              onTap: (){
                                                openUrl(data.url);
                                              },
                                            ),
                                          ),
                                        )
                                    ),
                                    Visibility(
                                      visible: data.type == "link",
                                      child:  Expanded(
                                        flex: 1,
                                        child: Padding(
                                          padding: EdgeInsets.only(right: 2),
                                          child: GestureDetector(
                                            child: Container(
                                                width: double.infinity,
                                                height: 55,
                                                color: Colors.blue,
                                                child: Center(
                                                  child: Row(
                                                    children: [
                                                      Expanded(
                                                        flex: 1,
                                                        child: Icon(Icons.public,color: Colors.white,size: 20,),
                                                      ),
                                                      Expanded(
                                                        flex: 1,
                                                        child: Text('Visit',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 16),),
                                                      )
                                                    ],
                                                  ),
                                                )
                                            ),
                                            onTap: (){
                                              openUrl(data.url);
                                            },
                                          ),
                                        ),
                                      ),

                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    )
                  ),
                );
              }
          );
        });

  }

  void doclipUnclip(OffersInEachItemModel model)async{
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var response = await http.post(
        "${Urls.baseUrl}${Urls.RemoveFlyerFromCart}?cartitem_id="+model.id.toString(),
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    if(response.statusCode == 200){
      dialog.dismissProgressDialog(context);
      print("resp_copun_clip "+response.body);
      var data = value['data'];
      if(data['isClipped']){
        Future token = SharedPrefrence().getCartCount();
        token.then((data) async {
          SharedPrefrence().setCartCount(data +1);
        });
        setState(() {
          model.isClipped = true;
        });
        Navigator.pop(context);
        final snackBar = SnackBar(content: Text("Clipped",style: TextStyle(color: Colors.white)),backgroundColor: Colors.green,duration: Duration(milliseconds: 100),);
        _scaffoldKey.currentState.showSnackBar(snackBar);

      }else{
        Future token = SharedPrefrence().getCartCount();
        token.then((data) async {
          if(data>0){
            SharedPrefrence().setCartCount(data  - 1);
          }
        });
        setState(() {
          model.isClipped = false;
        });
        Navigator.pop(context);
        final snackBar = SnackBar(content: Text("Un clipped",style: TextStyle(color: Colors.white)),backgroundColor: Colors.green,duration: Duration(milliseconds: 100),);
        _scaffoldKey.currentState.showSnackBar(snackBar);

      }
    }
    else{
      dialog.dismissProgressDialog(context);
      print(response.body);
    }
  }

  void share(OffersInEachItemModel model){
    var shareMessage = Urls.baseurl+"flyer/"+model.flyer_slug+"?item="+model.id.toString();
    Share.share(shareMessage, subject: 'Flyerbin');
  }

  void openUrl(String url) async{
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void getSimiliarProduct(String id) async{
    var response = await http.get(
        "${Urls.baseUrl}${Urls.SimiliarProducts}?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=100&&count=20&locale="+lan+"&page=1&id="+id,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print("resp_similiar "+response.body);
    if(response.statusCode == 200){
      try{
        var array = value['data'];
        if(array.length >0){
          setState(() {
            show_similiar_prdouct = true;
            related_image = Urls.baseImageUrl+array[0]['item_path'];
            related_image_2 =  Urls.baseImageUrl+array[1]['item_path'];
          });
        }else{
          setState(() {
            show_similiar_prdouct = false;
          });
        }
      }catch(e){
        e.toString();
      }

    }
  }
}