import 'dart:async';
import 'dart:convert';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_layouts/flutter_layouts.dart';
import 'package:flutter_svg/svg.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;

import 'BottomnavBarScreen.dart';
import 'BrowseDashboardScreen.dart';
import 'Model/FavouriteCategoryModel.dart';
import 'Utils/Constants.dart';
import 'Utils/CustomDialogs.dart';
import 'Utils/SharedPrefrence.dart';
import 'Utils/Urls.dart';


class ChangeCategoryPreference extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ChangeCategoryPreferenceState();
  }
}

class ChangeCategoryPreferenceState extends State<ChangeCategoryPreference> {
  String radioItem = '',lan = 'en';
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String latitude;
  String longitude;
  String language = "";
  bool islocateCompelete = false;
  bool _serviceEnabled;
  String userToken;
  var selected_category_list = List<String>();
  List<FavouriteCategoryModel> favourite_category_list = [];
  Position _currentPosition;
  String _currentAddress;

  @override
  void initState() {
    super.initState();
    Future lng = SharedPrefrence().getLanguage();
    lng.then((data) async {
      lan = data;
    });
    Future token = SharedPrefrence().getToken();
    token.then((data) async {
      userToken = data;
      getCategoryPreference();
    });


   // _GetLocationdata();
   // _getLocation();
    _getCurrentLocation();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    Size size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color.fromRGBO(243, 244, 244, 1),
        key: _scaffoldKey,
        appBar: AppBar(
        /*  leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: Color.fromRGBO(112,112,112,1),
                ),
                tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
              );
            },
          ),*/
          iconTheme: new IconThemeData(color: Color.fromRGBO(34, 83, 148, 1)),
          title: appbarLogo(),
          centerTitle: true,
          backgroundColor: Colors.white,
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: GestureDetector(
                child: Align(
                  child: Text('SKIP',style: TextStyle(color: Color.fromRGBO(0,161,237,1)),textAlign: TextAlign.center,),
                  alignment: Alignment.center,
                ),
                onTap: () {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                          builder: (context) => DashboardScreen()),
                      ModalRoute.withName("/login"));
                },
              ),
            ),
          ],
        ),
       // body: SingleChildScrollView(
          body: Stack(
            children: <Widget>[
              Center(
                child: Column(
                 // mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Expanded(
                      flex: 0,
                      child: Searchbar(),
                    ),
                    Expanded(
                      flex: 1,
                      child:buildBody(size),
                    ),

            ],
          ),
        ),
      ],
      ),
    )
    );
  }

  Widget appbarLogo() {
    return SvgPicture.asset("assets/images/shop_prefernce_indicator.svg",
        //image:SvgPicture.asset("assets/images/shop_prefernce_indicator.svg"),
        height: 10.0,
        width: 120,
        alignment: FractionalOffset.center);
  }

  Widget Searchbar() {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Container(
        width: 400,
        height: 40,
        child: TextField(
          textAlign: TextAlign.left,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            fillColor: Colors.white,
            prefixIcon: Icon(Icons.search),
            hintText: "Search",
            hintStyle: TextStyle(fontSize: 13),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15),
              borderSide: BorderSide(
                width: 0,
                style: BorderStyle.none,
              ),
            ),
            filled: true,
            contentPadding: EdgeInsets.all(16),
          ),
        ),
      ),
    );
  }

  Widget ShopGrid(Size size){
    return Container(
      height: size.height *1.5,
      child: GridView.count(
        crossAxisCount: 4,
        shrinkWrap: false,
        children: List.generate(favourite_category_list.length, (index) {
        //  FavouriteShopPreferenceModel data = FavouriteShopPreferenceModel[index];
          return Column(
            children: <Widget>[
              //Flexible(
               // flex: 0,
              //  child:
          GestureDetector(
                  onTap: () {
                    var selectedID = favourite_category_list[index].id;
                    print(selected_category_list);
                    for(int i = 0 ; i< favourite_category_list.length ; i++) {
                      if(selectedID==favourite_category_list[i].id){
                        if (favourite_category_list[i].isclicked) {
                          setState(() {
                            favourite_category_list[i].isclicked = false;
                            for(int j = 0;j<selected_category_list.length;j++){
                              if(selected_category_list[j]==selectedID){
                                selected_category_list.removeAt(j);
                              }
                            }
                            //selected_category_list.remove(selected_category_list[i].id);
                          });
                          break;
                        }
                        else{
                          print(selected_category_list);
                          setState(() {
                            favourite_category_list[i].isclicked = true;
                            selected_category_list.add(favourite_category_list[i].id);

                          });
                          break;
                        }
                      }

                    }
                  },
                  child:favourite_category_list[index].isclicked==false? Container(
                    width: 65,
                    height: 65,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.circle
                    ),
                    padding: const EdgeInsets.all(3.0),
                    child: Padding(
                      padding: EdgeInsets.all(10),
          child:ClipRRect(
              //borderRadius: BorderRadius.horizontal(50.0),
              child: FadeInImage(
                image: NetworkImage(
                    Urls.baseImageUrl+favourite_category_list[index].logo),
                fit: BoxFit.fitHeight,
                alignment: Alignment.center,
                placeholder:
                AssetImage(""),
                height: 30,
                width: 30,
              )), 
          )
                  ):Container(
                      width: 60,
                      height: 60,
                      decoration: BoxDecoration(
                        //color: Colors.white,
                        shape: BoxShape.circle,
                        image: DecorationImage(image:AssetImage("assets/images/favourite_shop_icon.png")),

                      ),
                      padding: const EdgeInsets.all(3.0),
                      child: Padding(
                        padding: EdgeInsets.all(8),
                        child:ClipRRect(
                          //borderRadius: BorderRadius.circular(50.0),
                            child: FadeInImage(
                              image: NetworkImage(
                                  ""),
                              placeholder:
                              AssetImage(""),
                              height: 35,
                              width: 35,
                              fit: BoxFit.contain,
                            )),
                      )
                  ),
                ),
            //  ),
             // Flexible(
               // flex: 0,
                //child:
            Expanded(
              flex: 0,
              child: Container(
                      height: 20,
                      child: Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: Center(
                            child: Text(
                              favourite_category_list[index].name,
                              style: TextStyle(
                                  //fontFamily: 'montessarat',
                                  //fontWeight: FontWeight.bold,
                                  fontSize: 10,
                                  color: Color.fromRGBO(
                                      34, 83, 148, 1)),textAlign: TextAlign.center,
                            )
                                ),
                      )),
            ),
            //  ),
            ],
          );
        }),
      ),
    );
  }

  Widget buildFooter() {
    return Container(
      height: 120,
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(color: Colors.white.withOpacity(0.5)),
     /* child: FlatButton(
        onPressed: () {},
        child: Text("Lean more", style: Theme.of(context).textTheme.button.copyWith(
            color: Theme.of(context).colorScheme.onBackground
        ),),
      ),*/
     child: Column(
       mainAxisAlignment: MainAxisAlignment.center,
       children: [
         Text(
           "Choose the stores you shop at.",
           style: TextStyle(color: Color.fromRGBO(0,157,221,1)),
           textAlign: TextAlign.center,
         ),
        GestureDetector(
          onTap: (){

            if(selected_category_list.length<3){
              final snackBar = SnackBar(content: Text("Select at least 3 categories"));
              _scaffoldKey.currentState.showSnackBar(snackBar);
            }
            else{
              saveCategories();
            }
          },
          child:   Padding(
            padding: EdgeInsets.all(10),
            child:  Container(
                width: 250,
                height: 40,
                decoration: BoxDecoration(
                    color: Color.fromRGBO(0,157,221,1),
                    borderRadius: BorderRadius.circular(10)
                ),
                child:Padding(
                  padding: EdgeInsets.all(10),
                  child: Text("CONTINUE",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                )
            ),
          ),
        )
       ],
     ),
    );
  }

  Widget buildBody(Size size) {
    return Footer(
      body: ShopGrid(size),
      footer: buildFooter(),
    );
  }

  void getCategoryPreference() async {
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);

    var response = await http.get("${Urls.baseUrl}${Urls.FavouriteCategoiresList}?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=100&locale="+lan,
        headers: {"Content-Type": "application/json",
                  "Authorization": "Bearer ${userToken}",
                   "Accept" : "application/json"},
       /* body: json.encode({
          "email": usermail,
          "password": userpass,
        })*/);

    Map<String, dynamic> value = json.decode(response.body);
    if (response.statusCode == 200) {
      dialog.dismissProgressDialog(context);
      try {
        print("response shopprefernce " + response.body.toString());
        Map<String, dynamic> value = json.decode(response.body);

        var status = value['success'];
        var message = value['message'];
        print(message);
        if (status == true) {
          favourite_category_list.clear();
          try {
            var data = value['data'];
            if (data.length > 0) {
              for (int i = 0; i < data.length; i++) {
                var obj = data[i];
                var petrol_obj = obj['petrolstation'];
                if(obj['is_favorited'].toString()=="0"){
                  favourite_category_list.add(FavouriteCategoryModel(
                      obj['id'].toString(),
                      obj['slug'].toString(),
                      obj['name'].toString(),
                      obj['app_thumbs']['md'].toString(),
                      obj['is_favorited'].toString(),false));

                }
                else{
                  favourite_category_list.add(FavouriteCategoryModel(
                      obj['id'].toString(),
                      obj['slug'].toString(),
                      obj['name'].toString(),
                      obj['app_thumbs']['md'].toString(),
                      obj['is_favorited'].toString(),true));
                  selected_category_list.add(obj['id'].toString());
                }

              }
              setState(() {
                /*list_count = pending_fuel_list.length;
                print(list_count);*/
          print(favourite_category_list.length);
        });
    }
    else {
    final snackBar = SnackBar(content: Text("No Data Available"));
    _scaffoldKey.currentState.showSnackBar(snackBar);
    }
    } catch (e) {
    e.toString();
    }

        }
        else {
          print("Error...");
          final snackBar = SnackBar(content: Text(message));
          _scaffoldKey.currentState.showSnackBar(snackBar);
        }
      } catch (e) {
        print(e.toString());
      }
    } else if (response.statusCode == 404) {
      dialog.dismissProgressDialog(context);
      var message = value['message'];
      final snackBar = SnackBar(content: Text(message));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    } else if (response.statusCode == 208) {
      dialog.dismissProgressDialog(context);
    } else {
      var message = value['message'];
      dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(content: Text(message));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  void saveCategories() async {
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);

    var response = await http.post("${Urls.baseUrl}${Urls.SaveFavoueiteCategories}",
        headers: {"Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept" : "application/json"},
        body: json.encode({
          "category_ids": selected_category_list,
        }));

    Map<String, dynamic> value = json.decode(response.body);
    if (response.statusCode == 200) {
      dialog.dismissProgressDialog(context);
      try {
        print("response categorypreference " + response.body.toString());
        Map<String, dynamic> value = json.decode(response.body);

        var status = value['success'];
        var message = value['message'];
        print(message);
        if (status == true) {
          //favourite_shop_list.clear();
          try {
            var data = value['data'].toString();
            if(data=="success"){
              setState(() {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DashboardScreen()),
                    ModalRoute.withName("/login"));
              });
            }

          } catch (e) {
            e.toString();
          }

        }
        else {
          print("Error...");
          final snackBar = SnackBar(content: Text(message));
          _scaffoldKey.currentState.showSnackBar(snackBar);
        }
      } catch (e) {
        print(e.toString());
      }
    } else if (response.statusCode == 404) {
      dialog.dismissProgressDialog(context);
      var message = value['message'];
      final snackBar = SnackBar(content: Text(message));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    } else if (response.statusCode == 208) {
      dialog.dismissProgressDialog(context);
    } else {
      var message = value['message'];
      dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(content: Text(message));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  ///Get Current location funtion
 /* Future _getLocation() async {
    setState(() {
      islocateCompelete = true;
    });
    Location location = new Location();

    var _permissionGranted = await location.hasPermission();
    _serviceEnabled = await location.serviceEnabled();`

    if (_permissionGranted != PermissionStatus.granted || !_serviceEnabled) {
      _permissionGranted = await location.requestPermission();

      _serviceEnabled = await location.requestService();

      setState(() {
        islocateCompelete = true;
      });
    } else {
      setState(() {
        islocateCompelete = false;
      });
    }

    LocationData _currentPosition = await location.getLocation();

    SharedPrefrence().setLatitude(_currentPosition.latitude.toString());
    SharedPrefrence().setLongitude(_currentPosition.longitude.toString());

   *//* try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        _currentAddress =
        "${place.locality}, ${place.postalCode}, ${place.country}";
        print(_currentAddress);
      });
    } catch (e) {
      print(e);
    }*//*

    *//*Future loginstatus = SharedPrefrence().getLogedIn();
    Future gust_status = SharedPrefrence().getGustLogedIn();
    gust_status.then((gustdata) {
      print(gustdata);
      loginstatus.then((data) {
        print(data);

        if (data == true || gustdata == true) {
          Navigator.pop(context, true);
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => HomeScreen()),
              ModalRoute.withName("/login"));
        } else {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
          );
        }
      });
    });*//*
  }*/

  _getCurrentLocation() async{
   getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });

      _getAddressFromLatLng();
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {

      List<Placemark> p = await placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        _currentAddress =
        "${place.locality}, ${place.postalCode}, ${place.country}";
      });
    } catch (e) {
      print(e);
    }
  }

}
