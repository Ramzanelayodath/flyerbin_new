import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:package_info/package_info.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher.dart';

import 'Cart.dart';
import 'CategorySlugDetails.dart';
import 'ChangeLocationScreen.dart';
import 'DealsDetails.dart';
import 'FlashOfferFlyer.dart';
import 'FlyerLoadScreen.dart';
import 'Model/BrandModel.dart';
import 'Model/BrowseFlyerModel.dart';
import 'Model/CategoryModel.dart';
import 'Model/FoodCopunModel.dart';
import 'Model/PrefrenceModel.dart';
import 'Model/TrendingDealsModel.dart';
import 'SideDrawerScreen.dart';
import 'Utils/Constants.dart';
import 'Utils/CustomDialogs.dart';
import 'Utils/SharedPrefrence.dart';
import 'Utils/Urls.dart';

class BrowseDashboardScreen extends StatefulWidget {
  String tapped = "";

  BrowseDashboardScreen(this.tapped);

  @override
  _BrowseDashboardScreenState createState() =>
      _BrowseDashboardScreenState(this.tapped);
}

class _BrowseDashboardScreenState extends State<BrowseDashboardScreen>
    with SingleTickerProviderStateMixin {
  final PLAY_STORE_URL =
      'https://play.google.com/store/apps/details?id=com.flyerbinapps.flyerbin';
  final APP_STORE_URL = 'https://apps.apple.com/in/app/flyerbin/id1502843770';
  String device = "";

  PackageInfo _packageInfo = PackageInfo(
    buildNumber: '',
  );

  String tapped = "";
  bool loading = true;
  bool isDealsLoding = true;
  Timer timer;
  static int page = 1;
  ScrollController _sccontroller = new ScrollController();
  ScrollController controller = ScrollController();
  ScrollController _latestflyer_controller = ScrollController();
  ScrollController flyer_list_controller,
      latest_flyer_controller = ScrollController();
  TabController _tabController;
  List<CategoryModel> category_list = [];
  List<PrefrenceModel> prefrence_list = List();
  List<BrandModel> brand_list = List();
  List<BrowseFlyerModel> flyer_list = List();
  List<BrowseFlyerModel> fav_flyer = List();
  List<BrowseFlyerModel> latest_flyer = List();
  List<BrowseFlyerModel> slug_flyer = List();
  List<TrendingDealsModel> trending_deals_list = List();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String userToken,
      userSelectedSlug,
      defult_latitude = "",
      defult_longitude = "",
      defult_radius = "20";
  TabController _controller;
  final List<String> imgList = List();
  final List<FoodCopunModel> foodcoupnList = List();
  String img_link_your_store = "",
      img_link_your_store_compny_logo = "",
      your_store_name = "",
      your_store_valid_from = "",
      your_store_valid_to = "",
      store_flyer_id = "",
      lan = "en";
  final DateFormat formatter = DateFormat('yyyy-MM-dd');
  final DateFormat formatter2 = DateFormat('dd-MM-yyyy');
  bool is_store_fav = false, is_all_flyer_clicked = false;
  int prefrence_page = 1,
      prefrence_last_page = 1,
      preferecne_list_size = 0,
      brand_list_count = 0,
      flyer_page = 1,
      flyer_last_page = 1,
      flyer_list_count = 0,
      categories_count = 3,
      fav_flyer_list_count = 0,
      latest_flyer_count = 0,
      latest_flyer_page = 1,
      latest_flyer_last_page,
      slug_flyer_list_count = 0,
      slug_flyer_page = 1,
      slug_flyer_last_page = 1,
      deals_page = 1,
      deals_last_page = 1,
      deals_list_count = 0;
  int flash_offer_count = 0;
  _BrowseDashboardScreenState(this.tapped);

  void startTimer() {
    // Start the periodic timer which prints something every 1 seconds
    timer = new Timer(new Duration(seconds: 3), () {
      setState(() {
        loading = false;
        timer.cancel();
      });
    });
  }

  @override
  void initState() {
    super.initState();
    //CheckAppUpdate();

    controller.addListener(_scrollListener);
    //  flyer_list_controller.addListener(_flyerlistScrollListener);
    latest_flyer_controller.addListener(latestflyerlistScrollListener);
    Future lng = SharedPrefrence().getLanguage();
    lng.then((data) async {
      lan = data;
    });
    Future token = SharedPrefrence().getToken();
    Future latitude = SharedPrefrence().getLatitude();
    Future longitude = SharedPrefrence().getLongitude();
    Future radius = SharedPrefrence().getRadius();
    token.then((data) async {
      latitude.then((lat_data) async {
        longitude.then((lon_data) async {
          radius.then((radius_data) async {
            userToken = data;
            defult_latitude = lat_data;
            defult_longitude = lon_data;
            defult_radius = radius_data;
            if(defult_radius==""){
              defult_radius="20";
              SharedPrefrence().setRadius(defult_radius);
            }
            print(defult_radius + " radius");
            print(defult_longitude+" Longitude");
            if (defult_latitude == "") {
              showAlertDialog(context);
            } else {
              setState(() {
                getCategories();
                getStore();
                getPrefrence(prefrence_page.toString());
                getFoodCopuns();
                getBrands();
                this.getNearestFlayers(flyer_page.toString());
              });

              // HandleTab(1);
            }
          });
        });
      });
      //  getTab();
      _latestflyer_controller.addListener(() {
        if (_latestflyer_controller.position.pixels ==
            _latestflyer_controller.position.maxScrollExtent) {
          if (latest_flyer_page == 0) {
          } else {
            getLatestFlyers(latest_flyer_page.toString());
          }
        }
      });
    });
    getSliderImage();
    print(tapped);
  }

  List<Tab> getTab() {
    return category_list
        .map((category) => Tab(
              icon: Padding(
                padding: EdgeInsets.all(0),
                child: SvgPicture.asset(
                  category.imagepath,
                  alignment: Alignment.center,
                ),
              ),
              text: category.name.toString(),
            ))
        .toList();
  }

  DateTime now = DateTime.now();
  DateTime currentBackPressTime;
///For closing app and goes to default index on press backbutton
  Future<bool> _onWillPop() async {

    if (_tabController.index == 1) {
      if (currentBackPressTime == null ||
          now.difference(currentBackPressTime) > Duration(seconds: 2)) {
        currentBackPressTime = now;
        final snackBar = SnackBar(
          content: Text('Tap back again to exit'),
        );
        _scaffoldKey.currentState.showSnackBar(snackBar);
        return Future.value(false);
      } else {
        return Future.value(true);
      }
    } else {
      Future.delayed(Duration(milliseconds: 200), () {
        print("set index");
        _tabController.index = 1;
      });

      print("return to index");
      return _tabController.index == 1;
    }
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    startTimer();
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      drawer: SideDraweScreen(),
      appBar: AppBar(
        iconTheme: new IconThemeData(color: Colors.black),
        title: appbarLogo(),
        backgroundColor: Colors.white,
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: GestureDetector(
              child: Icon(
                Icons.shopping_cart,
                color: Color(0xff00ADEE),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Cart(),
                  ),
                );
              },
            ),
          )
        ],
      ),
      body: WillPopScope(
        onWillPop: _onWillPop,
        child: category_list.length != 0
            ? DefaultTabController(
                length: category_list.length,
                initialIndex: 1,
                child: Column(
                  children: <Widget>[
                    Column(
                      children: [
                        Container(
                          constraints: BoxConstraints.expand(height: 40),
                          child: TabBar(
                            controller: _tabController,
                            isScrollable: true,
                            indicatorColor: Color(0xff00ADEE),
                            labelColor: Color(0xff00ADEE),
                            unselectedLabelColor: Colors.black,
                            tabs: getTab(),
                          ),
                        ),
                      ],
                    ),
                    Expanded(
                      child: Container(
                        child: TabBarView(
                            controller: _tabController,
                            children: createDynamicslugWIdget()),
                      ),
                    )
                  ],
                ),
              )
            : Center(
                child: CircularProgressIndicator(),
              ),
      ),
    );
  }

  Widget appbarLogo() {
    return SvgPicture.asset(
      "assets/images/flyerbin_logo.svg",
      height: 40,
    );
  }

  Widget _HomeBanner() {
    return Padding(
      padding: EdgeInsets.only(left: 0, right: 0, top: 0),
      child: Container(
          child: CarouselSlider(
        options: CarouselOptions(
          height: 150,
          autoPlay: true,
          viewportFraction: 1.0,
        ),
        items: imgList
            .map((item) => Container(
                  child: Image.network(item, fit: BoxFit.cover, width: 1000),
                ))
            .toList(),
      )),
    );
  }

  Widget StoreWidget() {
    Size size = MediaQuery.of(context).size;
    return Column(
      children: [
        Column(
          children: [
            StoreItemsNew(
                context: context,
                img_link_your_store: img_link_your_store,
                img_link_your_store_compny_logo:
                    img_link_your_store_compny_logo,
                name: your_store_name,
                valid_from: your_store_valid_from,
                valid_to: your_store_valid_to,
                is_store_fav: is_store_fav,
                flyer_id: store_flyer_id)
          ],
        ),
      ],
    );
  }

  Widget ExploreMenu() {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _HomeBanner(),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 5.0, bottom: 2.0),
            child: Text(
              "Your Store",
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.w600),
            ),
          ),
          Divider(
            color: Color(0xffE0E0E0),
            thickness: 1,
          ),
          SizedBox(
            height: 3,
          ),
          StoreWidget(),
          Padding(
            padding: const EdgeInsets.only(left: 5.0, bottom: 2.0, top: 5.0),
            child: Text(
              "Your Preferences",
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.w600),
            ),
          ),
          Divider(
            color: Color(0xffE0E0E0),
            thickness: 1,
          ),
          /* SizedBox(
            height: 2,
          ),*/
          YourPrefrence(),
          SizedBox(
            height: 10,
          ),
          foodcoupnList.length == 0 ? Container(child: Text("")) : FoodCopuns(),

          /* SizedBox(
            height: 10,
          ),*/
          Expanded(flex: 0, child: BrandList()),
          /* SizedBox(
            height: 10,
          ),*/
          flash_offer_count==0? Container(child: Text("")) :flashOffers(),
          Flyers()
        ],
      ),
    );
  }

  Widget StoreItemsNew(
          {BuildContext context,
          String img_link_your_store,
          String img_link_your_store_compny_logo,
          String name,
          String valid_from,
          String valid_to,
          bool is_store_fav,
          String flyer_id}) =>
      Center(
        child: Padding(
            padding: EdgeInsets.only(left: 3, right: 3, top: 3, bottom: 5),
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => FlyerLoadScreen(
                          store_flyer_id,
                          your_store_name,
                          valid_from,
                          valid_to,
                          img_link_your_store_compny_logo,
                          false,
                          "")),
                );
              },
              child: Container(
                width: 380,
                height: 180,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Color(0xffE0E0E0)),
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(5)),
                child: Row(
                  children: [
                    Expanded(
                      flex: 2,
                      child: Image.network(
                        img_link_your_store,
                        fit: BoxFit.fill,
                        // width: 120,
                        height: 180,
                      ),
                    ),
                    Container(
                      width: 15,
                      child: Text(" "),
                    ),
                    Expanded(
                      flex: 3,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(3),
                            child: Container(
                              child: Image.network(
                                img_link_your_store_compny_logo,
                                fit: BoxFit.fill,
                                width: 80,
                                height: 80,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 5),
                            child: Text(
                              name,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 18),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(5),
                            child: Container(
                              child: Image.asset(
                                'assets/images/new.png',
                                fit: BoxFit.fitHeight,
                                width: 40,
                                height: 15,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(5),
                            child: Text(
                              "Valid from: " + valid_from,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  color: Colors.blueGrey,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 11),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 5),
                            child: Text(
                              "Valid to:" + valid_to,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  color: Colors.blueGrey,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 11),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                        flex: 1,
                        child: Padding(
                          padding: EdgeInsets.only(right: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                flex: 0,
                                child: Padding(
                                    padding: const EdgeInsets.all(5),
                                    child: Text(" ")),
                              ),
                              Expanded(
                                flex: 0,
                                child: Padding(
                                  padding: const EdgeInsets.all(5),
                                  child: GestureDetector(
                                    child: Icon(
                                      Icons.favorite_border,
                                      size: 22,
                                      color: is_store_fav
                                          ? Colors.red
                                          : Colors.blue,
                                    ),
                                    onTap: () {
                                      doFavorUnfav(
                                          isfromlist: false,
                                          id: store_flyer_id);
                                    },
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ))
                  ],
                ),
              ),
            )),
      );

  Widget Storeitems(
          {BuildContext context,
          String img_link_your_store,
          String img_link_your_store_compny_logo,
          String name,
          String valid_from,
          String valid_to,
          bool is_store_fav,
          String flyer_id}) =>
      Center(
        child: Card(
            //elevation: 10,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0.0),
            ),
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => FlyerLoadScreen(
                          store_flyer_id,
                          your_store_name,
                          valid_from,
                          valid_to,
                          img_link_your_store_compny_logo,
                          false,
                          "")),
                );
              },
              child: Container(
                height: 180,
                width: 380,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                          flex: 0,
                          child: Padding(
                            padding: const EdgeInsets.all(0),
                            child: Container(
                              child: Image.network(
                                img_link_your_store,
                                fit: BoxFit.fill,
                                width: 120,
                                height: 180,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Expanded(
                            flex: 0,
                            child: GestureDetector(
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: Container(
                                  child: Image.asset(
                                    "assets/images/sample_logo.png",
                                    fit: BoxFit.fill,
                                    width: 80,
                                    height: 80,
                                  ),
                                ),
                              ),
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => FlyerLoadScreen(
                                            store_flyer_id,
                                            your_store_name,
                                            valid_from,
                                            valid_to,
                                            img_link_your_store_compny_logo,
                                            false,
                                            "")));
                              },
                            )),
                        Expanded(
                          flex: 0,
                          child: Padding(
                            padding: const EdgeInsets.all(5),
                            child: Text(
                              name,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 18),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 0,
                          child: Padding(
                            padding: const EdgeInsets.all(5),
                            child: Container(
                              child: Image.asset(
                                'assets/images/new.png',
                                fit: BoxFit.fitHeight,
                                width: 40,
                                height: 15,
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 0,
                          child: Padding(
                            padding: const EdgeInsets.all(5),
                            child: Text(
                              "Valid from: " + valid_from,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  color: Colors.blueGrey,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 11),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 0,
                          child: Padding(
                            padding: const EdgeInsets.all(5),
                            child: Text(
                              "Valid to:" + valid_to,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  color: Colors.blueGrey,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 11),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          flex: 0,
                          child: Padding(
                            padding: const EdgeInsets.all(5),
                            child: Icon(
                              Icons.shopping_cart,
                              size: 22,
                              color: Colors.blue,
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 0,
                          child: Padding(
                            padding: const EdgeInsets.all(5),
                            child: GestureDetector(
                              child: Icon(
                                Icons.favorite_border,
                                size: 22,
                                color: is_store_fav ? Colors.red : Colors.blue,
                              ),
                              onTap: () {
                                doFavorUnfav(
                                    isfromlist: false, id: store_flyer_id);
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            )),
      );

  Widget YourPreferenceItem(int index) {
    return Center(
      child: Padding(
        padding: EdgeInsets.only(left: 5, right: 5),
        child: GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => FlyerLoadScreen(
                      prefrence_list[index].id.toString(),
                      prefrence_list[index].name,
                      prefrence_list[index].valid_from,
                      prefrence_list[index].valid_to,
                      prefrence_list[index].logo,
                      false,
                      ""),
                ),
              );
            },
            child: Container(
              height: 230,
              width: 140,
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Color(0xffE0E0E0)),
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(5)),
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 5),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Image.network(
                        prefrence_list[index].logo,
                        fit: BoxFit.fill,
                        height: 30,
                      ),
                    ),
                  ),
                  Image.network(
                    prefrence_list[index].thumb_path,
                    fit: BoxFit.fill,
                    width: 140,
                    height: 198,
                  ),
                ],
              ),
            )),
      ),
    );
  }

  Widget YourPrefrence() {
    Size size = MediaQuery.of(context).size;
    return Column(
      children: [
        Column(
          children: [
            Expanded(
              flex: 0,
              child: Container(
                height: 245,
                child: ListView.builder(
                    controller: controller,
                    itemCount:
                        preferecne_list_size > 0 ? preferecne_list_size : 0,
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return YourPreferenceItem(index);
                      /*Card(
                          elevation: 2,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(2),
                          ),
                          child: GestureDetector(
                            onTap: () {
                              */ /*Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FlyerLoadScreen()),
                                ModalRoute.withName("/login"));*/ /*
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => FlyerLoadScreen(
                                      prefrence_list[index].id.toString(),
                                      prefrence_list[index].name,
                                      prefrence_list[index].valid_from,
                                      prefrence_list[index].valid_to,
                                      prefrence_list[index].logo,
                                      false,
                                      ""),
                                ),
                              );
                            },
                            child: Container(
                              height: 195,
                              width: 150,
                              child: Container(
                                  child: Column(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.all(8),
                                    child: Align(
                                      alignment: Alignment.topLeft,
                                      child: Image.network(
                                        prefrence_list[index].logo,
                                        fit: BoxFit.fitHeight,
                                        height: 20,
                                      ),
                                    ),
                                  ),
                                  Image.network(
                                    prefrence_list[index].thumb_path,
                                    fit: BoxFit.fitHeight,
                                    width: 150,
                                    height: 195,
                                  ),
                                ],
                              )),
                            ),
                          ));*/
                    }),
              ),
            )
          ],
        ),
      ],
    );
  }

  Widget BrandList() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      physics: NeverScrollableScrollPhysics(),
      child: Row(
        children: [
          Container(
            width: 75.0,
            height: 75.0,
            decoration: new BoxDecoration(
              color: Colors.white,
              border: Border.all(
                color: is_all_flyer_clicked ? Colors.blue : Colors.black12,
              ),
              shape: BoxShape.circle,
            ),
            child: Padding(
              padding: EdgeInsets.all(8),
              child: GestureDetector(
                child: Container(
                  child: Padding(
                    padding: EdgeInsets.all(10),
                    child: Image.asset(
                      'assets/images/allflyer.png',
                      width: 10,
                      height: 10,
                    ),
                  ),
                  width: 50,
                  height: 50,
                ),
                onTap: () {
                  for (int i = 0; i < brand_list.length; i++) {
                    if (brand_list[i].isclicked) {
                      setState(() {
                        brand_list[i].isclicked = false;
                      });
                      break;
                    }
                  }
                  setState(() {
                    is_all_flyer_clicked = true;
                    flyer_page = 1;
                    flyer_list.clear();
                    flyer_list_count = 0;
                  });
                  getNearestFlayers(flyer_page.toString());
                },
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 100,
            child: ListView.builder(
                itemCount: brand_list_count > 0 ? brand_list_count : 0,
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return brand_list_count > 0
                      ? Padding(
                          padding: EdgeInsets.only(top: 8, left: 8, bottom: 8),
                          child: GestureDetector(
                            child: Container(
                              width: 75.0,
                              height: 75.0,
                              decoration: new BoxDecoration(
                                border: Border.all(
                                  color: brand_list[index].isclicked
                                      ? Colors.blue
                                      : Colors.black12,
                                ),
                                shape: BoxShape.circle,
                              ),
                              padding: const EdgeInsets.all(4.0),
                              child:
                                  /*Padding(
                                padding: EdgeInsets.all(8),
                                child: Image.network(
                                  brand_list[index].logo,
                                  width: 50,
                                  height: 50,
                                ),*/
                                  Padding(
                                padding: EdgeInsets.all(8),
                                child: ClipRRect(
                                    //borderRadius: BorderRadius.circular(50.0),
                                    child: FadeInImage(
                                  image: NetworkImage(brand_list[index].logo),
                                  placeholder: AssetImage(''),
                                  height: 25,
                                  width: 25,
                                  fit: BoxFit.contain,
                                )),
                              ),
                            ),
                            //),
                            onTap: () {
                              for (int i = 0; i < brand_list.length; i++) {
                                if (brand_list[i].isclicked) {
                                  setState(() {
                                    brand_list[i].isclicked = false;
                                  });
                                  break;
                                }
                              }
                              setState(() {
                                is_all_flyer_clicked = false;
                                brand_list[index].isclicked = true;
                                flyer_page = 1;
                                flyer_list.clear();
                                flyer_list_count = 0;
                              });
                              getBrandFlyers(flyer_page.toString(),
                                  brand_list[index].id.toString());
                            },
                          ),
                        )
                      : CircularProgressIndicator();
                }),
          )
        ],
      ),
    );
  }

  Widget flashOffers() {
    return Padding(
      padding: const EdgeInsets.only(left:8.0,right: 8.0,bottom: 10.0),
      child: Container(
        height: 55,
        width: 400,
        decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color(0xFFc1202e),
                Color(0xFFea2127),
              ],
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
            ),
            borderRadius: BorderRadius.all(
              Radius.circular(30),
            ),
            boxShadow: [
              BoxShadow(
                blurRadius: 10,
                offset: Offset(-10, 10),
                color: Colors.black26,
              ),
            ]),
        child: MaterialButton(
            elevation: 8,
            child: Row(children: [
              Expanded(
                  child: Text(
                    'FLASH OFFER',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 20,
                        fontStyle: FontStyle.italic,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  )),
              CircleAvatar(
                  backgroundImage: AssetImage("assets/images/yellow.png"),
                  backgroundColor: Colors.transparent,
                  radius: 25,
                  child: Text(
                    flash_offer_count.toString(),
                    style: TextStyle(fontSize: 15, color: Colors.red,fontWeight: FontWeight.bold),
                  ))
            ]),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => FlashOfferListScreen(),
                ),
              );
            }),
      ),
    );
  }

  Widget Flyers() {
    return ListView.builder(
        controller: flyer_list_controller,
        itemCount: flyer_list_count > 0 ? flyer_list_count : 0,
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return Center(
            child:
                /*Card(
                //elevation: 10,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(0.0),
                ),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => FlyerLoadScreen(
                            flyer_list[index].id.toString(),
                            flyer_list[index].shopName,
                            flyer_list[index].vaild_from,
                            flyer_list[index].valid_to,
                            flyer_list[index].shopLogo,
                            false,
                            ""),
                      ),
                    );
                  },
                  child: Container(
                    height: 180,
                    width: 380,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          // crossAxisAlignment: CrossAxisAlignment.start,
                          // mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(0),
                                child: Container(
                                  child: Image.network(
                                    flyer_list[index].thumb_path,
                                    fit: BoxFit.fill,
                                    width: 120,
                                    height: 180,
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: Container(
                                  child: Image.network(
                                    flyer_list[index].shopLogo,
                                    fit: BoxFit.fill,
                                    width: 50,
                                    height: 50,
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(0),
                                child: Text(
                                  flyer_list[index].name,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: Container(
                                  child: Image.asset(
                                    'assets/images/new.png',
                                    fit: BoxFit.fitHeight,
                                    width: 40,
                                    height: 15,
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: Text(
                                  "Valid from: " + flyer_list[index].vaild_from,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Colors.blueGrey,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 11),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: Text(
                                  "Valid to:" + flyer_list[index].valid_to,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Colors.blueGrey,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 11),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              flex: 0,
                              child: Visibility(
                                child: Padding(
                                  padding: const EdgeInsets.all(5),
                                  child: Icon(
                                    Icons.shopping_cart,
                                    size: 22,
                                    color: Colors.blue,
                                  ),
                                ),
                                visible: false,
                              ),
                            ),
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: GestureDetector(
                                  child: Icon(
                                    Icons.favorite_border,
                                    size: 22,
                                    color: flyer_list[index].isFavourited
                                        ? Colors.red
                                        : Colors.blue,
                                  ),
                                  onTap: () {
                                    doFavorUnfav(
                                        model: flyer_list[index],
                                        isfromlist: true);
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                )),*/
                Padding(
                    padding:
                        EdgeInsets.only(left: 5, right: 5, top: 3, bottom: 20),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => FlyerLoadScreen(
                                flyer_list[index].id.toString(),
                                flyer_list[index].shopName,
                                flyer_list[index].vaild_from,
                                flyer_list[index].valid_to,
                                flyer_list[index].shopLogo,
                                false,
                                ""),
                          ),
                        );
                      },
                      child: Container(
                        width: 380,
                        height: 180,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(color: Color(0xffE0E0E0)),
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(5)),
                        child: Row(
                          children: [
                            Expanded(
                              flex: 2,
                              child: Image.network(
                                flyer_list[index].thumb_path,
                                fit: BoxFit.fill,
                                // width: 120,
                                height: 180,
                              ),
                            ),
                            Container(
                              width: 15,
                              child: Text(" "),
                            ),
                            Expanded(
                              flex: 3,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(3),
                                    child: Container(
                                      child: Image.network(
                                        flyer_list[index].shopLogo,
                                        fit: BoxFit.fill,
                                        width: 70,
                                        height: 70,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 5),
                                    child: Text(
                                      flyer_list[index].name,
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(5),
                                    child: Container(
                                      child: Image.asset(
                                        'assets/images/new.png',
                                        fit: BoxFit.fitHeight,
                                        width: 40,
                                        height: 15,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(5),
                                    child: Text(
                                      "Valid from: " +
                                          flyer_list[index].vaild_from,
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          color: Colors.blueGrey,
                                          fontWeight: FontWeight.w400,
                                          fontSize: 11),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 5),
                                    child: Text(
                                      "Valid to:" + flyer_list[index].valid_to,
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          color: Colors.blueGrey,
                                          fontWeight: FontWeight.w400,
                                          fontSize: 11),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                                flex: 1,
                                child: Padding(
                                  padding: EdgeInsets.only(right: 10),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Expanded(
                                        flex: 0,
                                        child: Padding(
                                            padding: const EdgeInsets.all(5),
                                            child: Text(" ")),
                                      ),
                                      Expanded(
                                        flex: 0,
                                        child: Padding(
                                          padding: const EdgeInsets.all(5),
                                          child: GestureDetector(
                                            child: Icon(
                                              Icons.favorite_border,
                                              size: 22,
                                              color:
                                                  flyer_list[index].isFavourited
                                                      ? Colors.red
                                                      : Colors.blue,
                                            ),
                                            onTap: () {
                                              doFavorUnfav(
                                                  model: flyer_list[index],
                                                  isfromlist: true);
                                            },
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ))
                          ],
                        ),
                      ),
                    )),
          );
        });
  }

  Widget FoodCopuns() {
    return Padding(
      padding: EdgeInsets.only(left: 8, right: 8),
      child: Container(
          child: CarouselSlider(
        options: CarouselOptions(
          autoPlayCurve: Curves.bounceIn,
          height: 200,
          autoPlay: false,
          viewportFraction: 1.0,
        ),
        items: foodcoupnList
            .map(
              (item) => ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                child: GestureDetector(
                  child: Image.network(
                    item.image,
                    fit: BoxFit.fill,
                    width: 1000.0,
                  ),
                  onTap: () {
                    print("ontap_name " + item.image);
                    ClipFoodCoupnAlert(context, item);
                  },
                ),
              ),
            )
            .toList(),
      )),
    );
  }

  Widget FavouriteFlyers() {
    return ListView.builder(
        itemCount: fav_flyer_list_count > 0 ? fav_flyer_list_count : 0,
        // physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return Center(
            child: Padding(
                padding: EdgeInsets.only(left: 3, right: 3, top: 3, bottom: 20),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FlyerLoadScreen(
                              fav_flyer[index].id.toString(),
                              fav_flyer[index].shopName,
                              fav_flyer[index].vaild_from,
                              fav_flyer[index].valid_to,
                              fav_flyer[index].shopLogo,
                              false,
                              "")),
                    );
                  },
                  child: Container(
                    width: 380,
                    height: 180,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: Color(0xffE0E0E0)),
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(5)),
                    child: Row(
                      children: [
                        Expanded(
                          flex: 2,
                          child: Image.network(
                            fav_flyer[index].thumb_path,
                            fit: BoxFit.fill,
                            // width: 120,
                            height: 180,
                          ),
                        ),
                        Container(
                          width: 15,
                          child: Text(" "),
                        ),
                        Expanded(
                          flex: 3,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(3),
                                child: Container(
                                  child: Image.network(
                                    fav_flyer[index].shopLogo,
                                    fit: BoxFit.fill,
                                    width: 70,
                                    height: 70,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 5),
                                child: Text(
                                  fav_flyer[index].name,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(5),
                                child: Container(
                                  child: Image.asset(
                                    'assets/images/new.png',
                                    fit: BoxFit.fitHeight,
                                    width: 40,
                                    height: 15,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(5),
                                child: Text(
                                  "Valid from: " + fav_flyer[index].vaild_from,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Colors.blueGrey,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 11),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 5),
                                child: Text(
                                  "Valid to:" + fav_flyer[index].valid_to,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Colors.blueGrey,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 11),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                            flex: 1,
                            child: Padding(
                              padding: EdgeInsets.only(right: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    flex: 0,
                                    child: Padding(
                                        padding: const EdgeInsets.all(5),
                                        child: Text(" ")),
                                  ),
                                  Expanded(
                                    flex: 0,
                                    child: Padding(
                                      padding: const EdgeInsets.all(5),
                                      child: GestureDetector(
                                        child: Icon(
                                          Icons.favorite_border,
                                          size: 22,
                                          color: fav_flyer[index].isFavourited
                                              ? Colors.red
                                              : Colors.blue,
                                        ),
                                        onTap: () {
                                          doFavorUnfav(
                                              model: fav_flyer[index],
                                              isfromlist: true,
                                              isfromfavflayer: true);
                                        },
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ))
                      ],
                    ),
                  ),
                )),
          );
          /*GestureDetector(
              child: Center(
                child: Card(
                  // elevation: 10,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0.0),
                  ),
                  child: Container(
                    height: 180,
                    width: 380,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          //mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(0),
                                child: Container(
                                  child: Image.network(
                                    fav_flyer[index].thumb_path,
                                    fit: BoxFit.fill,
                                    width: 120,
                                    height: 180,
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: Container(
                                  child: Image.network(
                                    fav_flyer[index].shopLogo,
                                    fit: BoxFit.fill,
                                    width: 50,
                                    height: 50,
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: Text(
                                  fav_flyer[index].name,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: Text(
                                  "Valid from: " + fav_flyer[index].vaild_from,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Colors.blueGrey,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 11),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: Text(
                                  "Valid to:" + fav_flyer[index].valid_to,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Colors.blueGrey,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 11),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              flex: 0,
                              child: Visibility(
                                child: Padding(
                                  padding: const EdgeInsets.all(5),
                                  child: Icon(
                                    Icons.shopping_cart,
                                    size: 22,
                                    color: Colors.blue,
                                  ),
                                ),
                                visible: false,
                              ),
                            ),
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: GestureDetector(
                                  child: Icon(
                                    Icons.favorite_border,
                                    size: 22,
                                    color: fav_flyer[index].isFavourited
                                        ? Colors.red
                                        : Colors.blue,
                                  ),
                                  onTap: () {
                                    doFavorUnfav(
                                        model: fav_flyer[index],
                                        isfromlist: true,
                                        isfromfavflayer: true);
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => FlyerLoadScreen(
                          fav_flyer[index].id.toString(),
                          fav_flyer[index].shopName,
                          fav_flyer[index].vaild_from,
                          fav_flyer[index].valid_to,
                          fav_flyer[index].shopLogo,
                          false,
                          "")),
                );
              });*/
        });
  }

  Widget LatestFlyers() {
    return ListView.builder(
        itemCount: latest_flyer.length + 1,
        // physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        controller: _latestflyer_controller,
        itemBuilder: (context, index) {
          if (index == latest_flyer.length) {
            if (latest_flyer_page == 0) {
              return Center(
                  child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("No more flyers....."),
              ));
            } else {
              return Center(
                  child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircularProgressIndicator(),
              ));
            }
          } else {
            return Center(
              child: Padding(
                  padding:
                      EdgeInsets.only(left: 3, right: 3, top: 3, bottom: 20),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => FlyerLoadScreen(
                                  latest_flyer[index].id.toString(),
                                  latest_flyer[index].shopName,
                                  latest_flyer[index].vaild_from,
                                  latest_flyer[index].valid_to,
                                  latest_flyer[index].shopLogo,
                                  false,
                                  "")));
                    },
                    child: Container(
                      width: 380,
                      height: 180,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(color: Color(0xffE0E0E0)),
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(5)),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 2,
                            child: Image.network(
                              latest_flyer[index].thumb_path,
                              fit: BoxFit.fill,
                              // width: 120,
                              height: 180,
                            ),
                          ),
                          Container(
                            width: 15,
                            child: Text(" "),
                          ),
                          Expanded(
                            flex: 3,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(3),
                                  child: Container(
                                    child: Image.network(
                                      latest_flyer[index].shopLogo,
                                      fit: BoxFit.fill,
                                      width: 70,
                                      height: 70,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 5),
                                  child: Text(
                                    latest_flyer[index].name,
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(5),
                                  child: Container(
                                    child: Image.asset(
                                      'assets/images/new.png',
                                      fit: BoxFit.fitHeight,
                                      width: 40,
                                      height: 15,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(5),
                                  child: Text(
                                    "Valid from: " +
                                        latest_flyer[index].vaild_from,
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: Colors.blueGrey,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 11),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 5),
                                  child: Text(
                                    "Valid to:" + latest_flyer[index].valid_to,
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: Colors.blueGrey,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 11),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                              flex: 1,
                              child: Padding(
                                padding: EdgeInsets.only(right: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      flex: 0,
                                      child: Padding(
                                          padding: const EdgeInsets.all(5),
                                          child: Text(" ")),
                                    ),
                                    Expanded(
                                      flex: 0,
                                      child: Padding(
                                        padding: const EdgeInsets.all(5),
                                        child: GestureDetector(
                                          child: Icon(
                                            Icons.favorite_border,
                                            size: 22,
                                            color:
                                                latest_flyer[index].isFavourited
                                                    ? Colors.red
                                                    : Colors.blue,
                                          ),
                                          onTap: () {
                                            doFavorUnfav(
                                                model: latest_flyer[index],
                                                isfromlist: true,
                                                isfromfavflayer: false);
                                          },
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ))
                        ],
                      ),
                    ),
                  )),
            );
            /*GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FlyerLoadScreen(
                              latest_flyer[index].id.toString(),
                              latest_flyer[index].shopName,
                              latest_flyer[index].vaild_from,
                              latest_flyer[index].valid_to,
                              latest_flyer[index].shopLogo,
                              false,
                              "")));
                },
                child: Center(
                    child: Card(
                  //elevation: 10,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0.0),
                  ),
                  child: Container(
                    height: 180,
                    width: 380,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          //mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(0),
                                child: Container(
                                  child: Image.network(
                                    latest_flyer[index].thumb_path,
                                    fit: BoxFit.fill,
                                    width: 120,
                                    height: 180,
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: Container(
                                  child: Image.network(
                                    latest_flyer[index].shopLogo,
                                    fit: BoxFit.fill,
                                    width: 50,
                                    height: 50,
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: Text(
                                  latest_flyer[index].name,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: Text(
                                  "Valid from: " +
                                      latest_flyer[index].vaild_from,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Colors.blueGrey,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 11),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: Text(
                                  "Valid to:" + latest_flyer[index].valid_to,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Colors.blueGrey,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 11),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              flex: 0,
                              child: Visibility(
                                child: Padding(
                                  padding: const EdgeInsets.all(5),
                                  child: Icon(
                                    Icons.shopping_cart,
                                    size: 22,
                                    color: Colors.blue,
                                  ),
                                ),
                                visible: false,
                              ),
                            ),
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: GestureDetector(
                                  child: Icon(
                                    Icons.favorite_border,
                                    size: 22,
                                    color: latest_flyer[index].isFavourited
                                        ? Colors.red
                                        : Colors.blue,
                                  ),
                                  onTap: () {
                                    doFavorUnfav(
                                        model: latest_flyer[index],
                                        isfromlist: true,
                                        isfromfavflayer: false);
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                )));*/
          }
        });
  }

  Widget SlugFlyers() {
    Size size = MediaQuery.of(context).size;
    return ListView.builder(
        itemCount: slug_flyer_list_count > 0 ? slug_flyer_list_count : 0,
        // physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        //controller: latest_flyer_controller,
        itemBuilder: (context, index) {
          return GestureDetector(
            child: Center(
              child: loading
                  ? Shimmer.fromColors(
                      baseColor: Colors.grey[200],
                      highlightColor: Colors.grey,
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Container(
                          height: size.height * 0.6,
                          //width: 30,
                          //height: 30,
                          decoration: BoxDecoration(
                            color: Color(0xFFFFF29A),
                            shape: BoxShape.rectangle,
                            /*borderRadius: BorderRadius.all(
                                Radius.circular(15),
                              ),*/
                          ),
                        ),
                      ),
                    )
                  : Padding(
                      padding: EdgeInsets.only(
                          left: 3, right: 3, top: 3, bottom: 20),
                      child: GestureDetector(
                        onTap: () {
                          if (slug_flyer[index].catgory_count == 1) {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => FlyerLoadScreen(
                                    slug_flyer[index].id.toString(),
                                    slug_flyer[index].shopName,
                                    slug_flyer[index].vaild_from,
                                    slug_flyer[index].valid_to,
                                    slug_flyer[index].shopLogo,
                                    false,
                                    ""),
                              ),
                            );
                          } else {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => CateogrySlugDetails(
                                    userSelectedSlug,
                                    slug_flyer[index].id.toString(),
                                    slug_flyer[index].shopName,
                                    slug_flyer[index].shopLogo),
                              ),
                            );
                          }
                        },
                        child: Container(
                          width: 380,
                          height: 180,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(color: Color(0xffE0E0E0)),
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(5)),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 2,
                                child: Image.network(
                                  slug_flyer[index].thumb_path,
                                  fit: BoxFit.fill,
                                  // width: 120,
                                  height: 180,
                                ),
                              ),
                              Container(
                                width: 15,
                                child: Text(" "),
                              ),
                              Expanded(
                                flex: 3,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(3),
                                      child: Container(
                                        child: Image.network(
                                          slug_flyer[index].shopLogo,
                                          fit: BoxFit.fill,
                                          width: 70,
                                          height: 70,
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 5),
                                      child: Text(
                                        slug_flyer[index].name,
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(5),
                                      child: Container(
                                        child: Image.asset(
                                          'assets/images/new.png',
                                          fit: BoxFit.fitHeight,
                                          width: 40,
                                          height: 15,
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(5),
                                      child: Text(
                                        "Valid from: " +
                                            slug_flyer[index].vaild_from,
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            color: Colors.blueGrey,
                                            fontWeight: FontWeight.w400,
                                            fontSize: 11),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 5),
                                      child: Text(
                                        "Valid to:" +
                                            slug_flyer[index].valid_to,
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            color: Colors.blueGrey,
                                            fontWeight: FontWeight.w400,
                                            fontSize: 11),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                  flex: 1,
                                  child: Padding(
                                    padding: EdgeInsets.only(right: 10),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          flex: 0,
                                          child: Padding(
                                              padding: const EdgeInsets.all(5),
                                              child: Text(" ")),
                                        ),
                                        Expanded(
                                          flex: 0,
                                          child: Padding(
                                            padding: const EdgeInsets.all(5),
                                            child: GestureDetector(
                                              child: Icon(
                                                Icons.favorite_border,
                                                size: 22,
                                                color: slug_flyer[index]
                                                        .isFavourited
                                                    ? Colors.red
                                                    : Colors.blue,
                                              ),
                                              onTap: () {
                                                doFavorUnfav(
                                                    model: slug_flyer[index],
                                                    isfromlist: true,
                                                    isfromfavflayer: false);
                                              },
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ))
                            ],
                          ),
                        ),
                      )),
            ),
            onTap: () {
              if (slug_flyer[index].catgory_count == 1) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => FlyerLoadScreen(
                        slug_flyer[index].id.toString(),
                        slug_flyer[index].shopName,
                        slug_flyer[index].vaild_from,
                        slug_flyer[index].valid_to,
                        slug_flyer[index].shopLogo,
                        false,
                        ""),
                  ),
                );
              } else {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => CateogrySlugDetails(
                        userSelectedSlug,
                        slug_flyer[index].id.toString(),
                        slug_flyer[index].shopName,
                        slug_flyer[index].shopLogo),
                  ),
                );
              }
            },
          );
        });
  }

  Widget Deals() {
    //startTimer();
    Size size = MediaQuery.of(context).size;
    return isDealsLoding==true?Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: CircularProgressIndicator(),
        )):GridView.count(
      crossAxisCount: 3,
      children: List.generate(deals_list_count, (index) {
        return loading
            ? Shimmer.fromColors(
                baseColor: Colors.grey[200],
                highlightColor: Colors.grey,
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Container(
                    height: size.height * 0.6,
                    //width: 30,
                    //height: 30,
                    decoration: BoxDecoration(
                      color: Color(0xFFFFF29A),
                      shape: BoxShape.circle,
                      /*borderRadius: BorderRadius.all(
                                Radius.circular(15),
                              ),*/
                    ),
                  ),
                ),
              )
            : Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  new GestureDetector(
                    child: Container(
                        width: 70.0,
                        height: 70.0,
                        decoration: new BoxDecoration(
                            shape: BoxShape.circle,
                            image: new DecorationImage(
                                fit: BoxFit.fill,
                                image: new NetworkImage(
                                    trending_deals_list[index].icon)))),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => DealsDetails(
                              trending_deals_list[index].slug,
                              trending_deals_list[index].name),
                        ),
                      );
                    },
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 5),
                    child: Text(
                      trending_deals_list[index].name,
                      style: TextStyle(fontSize: 13, color: Colors.black),
                      textAlign: TextAlign.center,
                    ),
                  )
                ],
              );
      }),
    );
  }

  void getCategories() async {
    // ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    print("Languge " + lan);
    var name = "";
    var response = await http.get(
      "${Urls.baseUrl}${Urls.CategoryList}?latitude=${defult_latitude}&longitude=${defult_longitude}&radius=${defult_radius}&locale=" +
          lan,
      headers: {
        "Content-Type": "application/json",
        //"Authorization": "Bearer ${userToken}",
        "Accept": "application/json"
      },
      /* body: json.encode({
          "email": usermail,
          "password": userpass,
        })*/
    );

    Map<String, dynamic> value = json.decode(response.body);
    if (response.statusCode == 200) {
      // dialog.dismissProgressDialog(context);
      try {
        print("response shopprefernce " + response.body.toString());
        Map<String, dynamic> value = json.decode(response.body);

        var status = value['success'];
        var message = value['message'];
        print(message);
        if (status == true) {
          category_list.clear();
          try {
            var data = value['data'];
            if (data.length > 0) {
              for (int i = 0; i < data.length; i++) {
                var obj = data[i];
                if (obj['translated'] != null) {
                  name = obj['translated']['name'];
                } else {
                  name = obj['name'].toString();
                }
                category_list.add(CategoryModel(
                    name, obj['icon'].toString(), obj['slug'].toString(), " "));
              }
              setState(() {
                /*list_count = pending_fuel_list.length;
                print(list_count);*/
                category_list.insert(
                    0, CategoryModel("♥", "", "favourite", ""));
                category_list.insert(
                    1, CategoryModel("Explore", "", "explore", ""));
                category_list.insert(
                    2, CategoryModel("Latest", "", "latest", ""));
                category_list.insert(
                    3, CategoryModel("Deals", "", "deals", ""));
                print(category_list.length);
                categories_count = category_list.length;
              });
              HandleTab(categories_count);
            } else {
              final snackBar = SnackBar(content: Text("No Data Available"));
              _scaffoldKey.currentState.showSnackBar(snackBar);
            }
          } catch (e) {
            e.toString();
          }
        } else {
          print("Error...");
          final snackBar = SnackBar(content: Text(message));
          _scaffoldKey.currentState.showSnackBar(snackBar);
        }
      } catch (e) {
        print(e.toString());
      }
    } else if (response.statusCode == 404) {
      // dialog.dismissProgressDialog(context);
      var message = value['message'];
      final snackBar = SnackBar(content: Text(message));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    } else if (response.statusCode == 208) {
      // dialog.dismissProgressDialog(context);
    } else {
      var message = value['message'];
      // dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(content: Text(message));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  void getSliderImage() async {
    imgList.clear();
    var response = await http.get(
      "${Urls.baseUrl}${Urls.Slider}",
      headers: {
        "Content-Type": "application/json",
        //"Authorization": "Bearer ${userToken}",
        "Accept": "application/json"
      },
    );

    Map<String, dynamic> value = json.decode(response.body);
    print("resp_slide " + response.body);
    if (response.statusCode == 200) {
      var array = value['data'];
      for (int i = 0; i < array.length; i++) {
        imgList.add(Urls.baseImageUrl + array[i]);
      }
    }
  }

  void getStore() async {
    print(userToken);
    var response = await http.get(
        "${Urls.baseUrl}${Urls.YourStore}?latitude=${defult_latitude}&longitude=${defult_longitude}&radius=${defult_radius}&locale=" +
            lan,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print("resp_yourstore " + response.body);
    if (response.statusCode == 200) {
      try {
        var data = value['data'];
        setState(() {
          store_flyer_id = data['id'].toString();
          img_link_your_store = Urls.baseImageUrl + data['thumbs']['md'];
          your_store_name = data['name'];
          your_store_valid_from =
              formatter2.format(formatter.parse(data['valid_from']));
          your_store_valid_to =
              formatter2.format(formatter.parse(data['valid_to']));
          is_store_fav = data['isFavorited'];
          if (data['shop'] != null) {
            img_link_your_store_compny_logo =
                Urls.baseImageUrl + data['shop']['thumbs']['md'];
          } else {
            img_link_your_store_compny_logo =
                Urls.baseImageUrl + data['brand']['thumbs']['md'];
          }
        });
      } catch (e) {}
    }
  }

  void getPrefrence(String page) async {
    var logo, name;
    print(userToken);
    print(defult_longitude);

    var response = await http.get(
        "${Urls.baseUrl}${Urls.Explore}?latitude=${defult_latitude}&longitude=${defult_longitude}&radius=${defult_radius}&locale=" +
            lan +
            "&page=" +
            page,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print("resp_pref " + response.body);
    if (response.statusCode == 200) {
      try {
        var data = value['data'];
        prefrence_last_page = data['last_page'];
        var array = data['data'];
        for (int i = 0; i < array.length; i++) {
          var obj = array[i];
          var id = obj['id'].toString();
          var desc = obj['name'].toString();
          var validfrom = obj["valid_from"].toString();
          var validto = obj["valid_to"].toString();
          var imagethumbpath = obj["thumb_path"].toString();
          var status = obj["status"];
          var isFavourite = obj["isFavorited"].toString();
          //var thumbs_path = obj["thumbs"];
          var slug = obj['slug'].toString();
          if (obj['shop'] != null) {
            logo = obj["shop"]["thumbs"]["md"].toString();
            name = obj["shop"]["name"].toString();
          } else {
            logo = obj["brand"]["thumbs"]["md"].toString();
            name = obj["brand"]["name"].toString();
          }
          prefrence_list.add(PrefrenceModel(
              id,
              desc,
              validfrom,
              validto,
              Urls.baseImageUrl + imagethumbpath,
              status,
              isFavourite,
              Urls.baseImageUrl + logo,
              name,
              1,
              slug,
              "",
              false,
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              ""));
        }
        setState(() {
          preferecne_list_size = prefrence_list.length;
        });
      } catch (e) {
        print("pref_exception " + e);
      }
    }
  }

  void _scrollListener() {
    if (controller.offset >= controller.position.maxScrollExtent &&
        !controller.position.outOfRange) {
      print("total page : " + prefrence_last_page.toString());
      if (prefrence_page <= prefrence_last_page) {
        print("Now Page " + prefrence_page.toString());
        prefrence_page++;
        getPrefrence(prefrence_page.toString());
      }
    }
  }

  void _flyerlistScrollListener() {
    print(controller.position.extentAfter);
    if (controller.position.pixels == controller.position.maxScrollExtent) {
      final snackBar = SnackBar(
        content: Text("Clipped", style: TextStyle(color: Colors.white)),
        backgroundColor: Colors.green,
      );
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  void latestflyerlistScrollListener() {
    if (latest_flyer_controller.position.extentAfter > 500) {
      print("jhjhjhjhjh");
    }
  }

  void getFoodCopuns() async {
    foodcoupnList.clear();
    var response = await http.get(
        "${Urls.baseUrl}${Urls.FoodCopun}?latitude=${defult_latitude}&longitude=${defult_longitude}&radius=${defult_radius}&locale=" +
            lan +
            "&page=1",
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print("resp_foodcoupn " + response.body);
    if (response.statusCode == 200) {
      try {
        var data = value['data'];
        var array = data['data'];
        for (int i = 0; i < array.length; i++) {
          var obj = array[i];
          var shop_obj = obj["shop"];
          print("Image link " + obj['image']);
          foodcoupnList.add(FoodCopunModel(
              obj['id'],
              obj['shop_id'],
              obj['title'],
              obj['description'],
              Urls.baseImageUrl + obj['image'],
              obj['view_count'],
              formatter2.format(formatter.parse(obj['valid_from'])),
              formatter2.format(formatter.parse(obj['valid_to'])),
              obj['type'],
              obj['isClipped'],
              Urls.baseImageUrl + shop_obj['logo'],
              shop_obj['name'],
              shop_obj['latitude'],
              shop_obj['longitude']));
        }
      } catch (e) {
        e.toString();
      }
    }
  }

  Future<void> ClipFoodCoupnAlert(
      BuildContext context, FoodCopunModel data) async {
    await showGeneralDialog(
        context: context,
        barrierDismissible: true,
        barrierLabel:
            MaterialLocalizations.of(context).modalBarrierDismissLabel,
        barrierColor: Colors.black45,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (BuildContext buildContext, Animation animation,
            Animation secondaryAnimation) {
          return StatefulBuilder(builder: (context, setState) {
            return Center(
                child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Card(
                  elevation: 6,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  child: Container(
                    // width: MediaQuery.of(context).size.width - 10,
                    // height: MediaQuery.of(context).size.height -  75,
                    padding: EdgeInsets.all(15),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      color: Colors.white,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: Container(
                                child: Row(
                                  children: [
                                    Image.network(
                                      data.shop_logo,
                                      fit: BoxFit.fill,
                                      width: 30,
                                      height: 30,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(5),
                                      child: Container(
                                        child: Text(
                                          data.shop_name,
                                          style: TextStyle(fontSize: 13),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(5),
                              child: GestureDetector(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: Icon(
                                    Icons.cancel,
                                    color: Colors.blueGrey,
                                  )),
                            )
                          ],
                        ),
                        Align(
                          child: Container(
                            height: 300,
                            width: 350,
                            child: Image.network(
                              data.image,
                              fit: BoxFit.fitWidth,
                            ),
                          ),
                          alignment: Alignment.center,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Text(
                            data.title,
                            style: TextStyle(
                                fontWeight: FontWeight.w400, fontSize: 18),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Text(
                            data.descritpion,
                            style: TextStyle(
                                fontWeight: FontWeight.w400,
                                color: Colors.lightBlue,
                                fontSize: 15),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Text(
                            "Valid From " + data.valid_from,
                            style: TextStyle(
                                fontWeight: FontWeight.w400, fontSize: 15),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Text(
                            "Valid To " + data.valid_to,
                            style: TextStyle(
                                fontWeight: FontWeight.w400, fontSize: 15),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 8, left: 4),
                          child: Row(
                            children: [
                              GestureDetector(
                                child: Container(
                                  height: 25,
                                  decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(20)),
                                      color: Colors.blue),
                                  child: Row(
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.only(left: 5),
                                        child: Icon(
                                          Icons.map,
                                          color: Colors.white,
                                          size: 20,
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(right: 5),
                                        child: Text(
                                          'Get Directions',
                                          style: TextStyle(color: Colors.white),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  openMap(
                                      double.parse(data.latitude),
                                      double.parse(data.longitude),
                                      Uri.encodeFull(data.shop_name));
                                },
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 5),
                                child: data.isclipped
                                    ? GestureDetector(
                                        child: Container(
                                          height: 25,
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(20)),
                                              color: Colors.red),
                                          child: Row(
                                            children: [
                                              Padding(
                                                padding:
                                                    EdgeInsets.only(left: 5),
                                                child: Icon(
                                                  Icons.attachment,
                                                  color: Colors.white,
                                                  size: 20,
                                                ),
                                              ),
                                              Padding(
                                                padding:
                                                    EdgeInsets.only(right: 5),
                                                child: Text(
                                                  'Un Pin',
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        onTap: () {
                                          doclipUnclip(data, false);
                                        },
                                      )
                                    : GestureDetector(
                                        child: Container(
                                          height: 25,
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(20)),
                                              color: Colors.blue),
                                          child: Row(
                                            children: [
                                              Padding(
                                                padding:
                                                    EdgeInsets.only(left: 5),
                                                child: Icon(
                                                  Icons.attachment,
                                                  color: Colors.white,
                                                  size: 20,
                                                ),
                                              ),
                                              Padding(
                                                padding:
                                                    EdgeInsets.only(right: 5),
                                                child: Text(
                                                  'Pin',
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        onTap: () {
                                          doclipUnclip(data, true);
                                        },
                                      ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ));
          });
        });
  }

  void doclipUnclip(FoodCopunModel model, bool isclipped) async {
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var response = await http.post(
        "${Urls.baseUrl}${Urls.clipUnclip}?coupon_id=" + model.id.toString(),
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    if (response.statusCode == 200) {
      dialog.dismissProgressDialog(context);
      print("resp_copun_clip " + response.body);
      var data = value['data'];
      if (data['isClipped']) {
        Future token = SharedPrefrence().getCartCount();
        token.then((data) async {
          SharedPrefrence().setCartCount(data + 1);
        });
        setState(() {
          model.isclipped = true;
        });
        Navigator.pop(context);
        final snackBar = SnackBar(
          content: Text("Clipped", style: TextStyle(color: Colors.white)),
          backgroundColor: Colors.green,
        );
        _scaffoldKey.currentState.showSnackBar(snackBar);
      } else {
        Future token = SharedPrefrence().getCartCount();
        token.then((data) async {
          if (data > 0) {
            SharedPrefrence().setCartCount(data - 1);
          }
        });
        setState(() {
          model.isclipped = false;
        });
        Navigator.pop(context);
        final snackBar = SnackBar(
          content: Text("Un clipped", style: TextStyle(color: Colors.white)),
          backgroundColor: Colors.green,
        );
        _scaffoldKey.currentState.showSnackBar(snackBar);
      }
    } else {
      dialog.dismissProgressDialog(context);
      print(response.body);
    }
  }

  void getBrands() async {
    brand_list.clear();
    var response = await http.get(
        "${Urls.baseUrl}${Urls.Brands}?latitude=${defult_latitude}&longitude=${defult_longitude}&radius=${defult_radius}&locale=" +
            lan +
            "&page=1",
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    if (response.statusCode == 200) {
      Map<String, dynamic> value = json.decode(response.body);
      print("resp_brand " + response.body);
      try {
        var array = value['data'];
        for (int i = 0; i < array.length; i++) {
          var obj = array[i];
          var thumbs_obj = obj["thumbs"];
          brand_list.add(BrandModel(obj["id"], obj["name"],
              Urls.baseImageUrl + thumbs_obj["md"], true, false));
        }
        setState(() {
          brand_list_count = brand_list.length;
        });
      } catch (e) {}
    } else {
      print("resp_brand " + response.body);
    }
  }

  void getNearestFlayers(String page) async {
    var name, logo;

    var response = await http.get(
        "${Urls.baseUrl}${Urls.NearestFlyers}?latitude=${defult_latitude}&longitude=${defult_longitude}&radius=${defult_radius}&locale=" +
            lan +
            "&page=" +
            page,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });

    if (response.statusCode == 200) {
      var temp_list = List<BrowseFlyerModel>();
      Map<String, dynamic> value = json.decode(response.body);
      print("resp_nearest " + response.body);
      try {
        var data = value['data'];
        flyer_last_page = data['last_page'];
        var array = data['data'];
        for (int i = 0; i < array.length; i++) {
          var obj = array[i];
          if (obj['shop'] != null) {
            name = obj['shop']['name'];
            logo = obj['shop']['thumbs']['md'];
          } else {
            name = obj['brand']['name'];
            logo = obj['brand']['thumbs']['md'];
          }
          var thumbs_path = obj['thumbs'];
          temp_list.add(BrowseFlyerModel(
              obj['id'],
              obj['name'],
              Urls.baseImageUrl + thumbs_path['sm'],
              Urls.baseImageUrl + logo,
              name,
              obj['isFavorited'],
              formatter2.format(formatter.parse(obj['valid_from'])),
              formatter2.format(formatter.parse(obj['valid_to'])),
              ""));
        }
        setState(() {

          if(flyer_page==1){
            if(int.parse(defult_radius)<50){
              flyer_list.addAll(temp_list);
              flyer_list_count = flyer_list.length;
            }
            else{
              flyer_list.addAll(temp_list);
              flyer_list_count = flyer_list.length;
              flyer_page=2;
              getNearestFlayers("2");
            }

          }
          else if(flyer_page==2){
            flyer_list.addAll(temp_list);
            flyer_list_count = flyer_list.length;
            flyer_page=3;
          }
        });
      } catch (e) {}
    }
  }

  void getBrandFlyers(String page, String id) async {
    var name, logo;
    var response = await http.get(
        "${Urls.baseUrl}${Urls.BrandFlyers}/${id}/flyers?latitude=${defult_latitude}&longitude=${defult_longitude}&radius=${defult_radius}&locale=" +
            lan +
            "&page=" +
            page,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    if (response.statusCode == 200) {
      Map<String, dynamic> value = json.decode(response.body);
      print("resp_brand_flyers " + response.body);
      try {
        var data = value['data'];
        flyer_last_page = data['last_page'];
        var array = data['data'];
        if (array.length > 0) {
          for (int i = 0; i < array.length; i++) {
            var obj = array[i];
            if (obj['shop'] != null) {
              name = obj['shop']['name'];
              logo = obj['shop']['thumbs']['md'];
            } else {
              name = obj['brand']['name'];
              logo = obj['brand']['thumbs']['md'];
            }
            var thumbs_path = obj['thumbs'];
            flyer_list.add(BrowseFlyerModel(
                obj['id'],
                obj['name'],
                Urls.baseImageUrl + thumbs_path['sm'],
                Urls.baseImageUrl + logo,
                name,
                obj['isFavorited'],
                formatter2.format(formatter.parse(obj['valid_from'])),
                formatter2.format(formatter.parse(obj['valid_to'])),
                ""));
          }
        } else {
          final snackBar = SnackBar(
              content: Text("No Flyer to show",
                  style: TextStyle(color: Colors.white)),
              backgroundColor: Colors.red,
              duration: const Duration(milliseconds: 100));
          _scaffoldKey.currentState.showSnackBar(snackBar);
        }
        setState(() {
          flyer_list_count = flyer_list.length;
        });
      } catch (e) {
        e.toString();
      }
    } else {
      print("resp_brand_flyers " + response.body);
    }
  }

  void doFavorUnfav(
      {BrowseFlyerModel model,
      bool isfromlist,
      bool isfromfavflayer,
      String id}) async {
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var url;
    if (isfromlist) {
      url = "${Urls.baseUrl}${Urls.AddRemoveFavourite}?flyer_id=" +
          model.id.toString();
    } else {
      url = "${Urls.baseUrl}${Urls.AddRemoveFavourite}?flyer_id=" + id;
    }
    var response = await http.post(url, headers: {
      "Content-Type": "application/json",
      "Authorization": "Bearer ${userToken}",
      "Accept": "application/json"
    });
    Map<String, dynamic> value = json.decode(response.body);
    if (response.statusCode == 200) {
      dialog.dismissProgressDialog(context);
      try {
        if (value['message'] == "Favourited") {
          setState(() {
            if (isfromlist) {
              model.isFavourited = true;
            } else {
              is_store_fav = true;
            }
          });
          final snackBar = SnackBar(
            content: Text("Added into your Favourite list",
                style: TextStyle(color: Colors.white,)),
            backgroundColor: Colors.green,duration: Duration(milliseconds: 100),
          );
          _scaffoldKey.currentState.showSnackBar(snackBar);
        } else if (value['message'] == "Unfavourited") {
          setState(() {
            if (isfromlist) {
              model.isFavourited = false;
              if (isfromfavflayer) {
                fav_flyer.remove(model);
                fav_flyer_list_count = fav_flyer.length;
                for (int i = 0; i < flyer_list.length; i++) {
                  if (flyer_list[i].id == model.id) {
                    flyer_list[i].isFavourited = false;
                  }
                }
              }
            } else {
              is_store_fav = false;
            }
          });
          final snackBar = SnackBar(
            content: Text("Removed From your Favourite list",
                style: TextStyle(color: Colors.white)),
            backgroundColor: Colors.green,duration: Duration(milliseconds: 100),
          );
          _scaffoldKey.currentState.showSnackBar(snackBar);
        }
      } catch (e) {}
    } else {
      dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(
        content: Text(value['message'], style: TextStyle(color: Colors.white)),
        backgroundColor: Colors.red,duration: Duration(milliseconds: 100),
      );
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  void getFavourite() async {
    var name, logo;
    fav_flyer.clear();
    var response = await http
        .post("${Urls.baseUrl}${Urls.GetFavourite}?locale=" + lan, headers: {
      "Content-Type": "application/json",
      "Authorization": "Bearer ${userToken}",
      "Accept": "application/json"
    });
    if (response.statusCode == 200) {
      Map<String, dynamic> value = json.decode(response.body);
      print("resp_favs " + response.body);
      try {
        // var data = value['data'];
        // flyer_last_page = data['last_page'];
        var array = value['data'];
        if (array.length > 0) {
          for (int i = 0; i < array.length; i++) {
            var obj = array[i];
            if (obj['shop'] != null) {
              name = obj['shop']['name'];
              logo = obj['shop']['logo'];
            } else {
              name = obj['brand']['name'];
              logo = obj['brand']['logo'];
            }
            var thumbs_path = obj['thumbs'];
            fav_flyer.add(BrowseFlyerModel(
                obj['id'].toString(),
                obj['name'],
                Urls.baseImageUrl + thumbs_path['sm'],
                Urls.baseImageUrl + logo,
                name,
                obj['isFavorited'],
                formatter2.format(formatter.parse(obj['valid_from'])),
                formatter2.format(formatter.parse(obj['valid_to'])),
                ""));
          }
        } else {
          final snackBar = SnackBar(
            content:
                Text("No Favourites", style: TextStyle(color: Colors.white)),
            backgroundColor: Colors.red,duration: Duration(milliseconds: 100),
          );
          _scaffoldKey.currentState.showSnackBar(snackBar);
        }
        setState(() {
          fav_flyer_list_count = fav_flyer.length;
        });
      } catch (e) {
        print("fav_Ex " + e.toString());
      }
    } else {
      print(response.body);
    }
  }

  void HandleTab(int tabcount) {
    _tabController = new TabController(
        vsync: this, length: categories_count, initialIndex: 1);
    _tabController.addListener(() {
      if (tapped == "tapped") {
        ExploreMenu();
        tapped = "";
      }
      print("${_tabController.index}");
      if (_tabController.index == 0) {
        getFavourite();
        setState(() {
          tapped = "";
        });
      } else if (_tabController.index == 2) {
        this.getLatestFlyers(latest_flyer_page.toString());
        setState(() {
          tapped = "";
        });
      } else if (_tabController.index == 3) {
        getTrendingDeals(deals_page.toString());
        setState(() {
          tapped = "";
        });
      } else if (_tabController.index > 3) {
        print(category_list[_tabController.index].slug);
        setState(() {
          tapped = "";
          slug_flyer_list_count = 0;
          slug_flyer_page = 1;
          slug_flyer_last_page = 1;
          slug_flyer.clear();
          userSelectedSlug = category_list[_tabController.index].slug;
        });
        getSlugFlyers(slug_flyer_page.toString(),
            category_list[_tabController.index].slug);
      }
    });
  }

  List<Widget> createDynamicslugWIdget() {
    var widget_list = List<Widget>();
    widget_list.add(FavouriteFlyers());
    widget_list.add(ExploreMenu());
    widget_list.add(LatestFlyers());
    widget_list.add(Deals());
    for (int i = 0; i < category_list.length - 4; i++) {
      widget_list.add(SlugFlyers());
    }
    print("widget size " + widget_list.length.toString());
    return widget_list;
  }

  void getLatestFlyers(String page) async {
    var name, logo;
    print("latest_flyer_page" + page.toString() + " " + defult_radius);
    List<BrowseFlyerModel> tList = List();
    var response = await http.get(
        "${Urls.baseUrl}${Urls.AllFlyers}?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=${defult_radius}&locale=" +
            lan +
            "&page=" +
            page,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });

    if (response.statusCode == 200) {
      Map<String, dynamic> value = json.decode(response.body);
      print("resp_nearest " + response.body);
      try {
        var data = value['data'];
        latest_flyer_last_page = data['last_page'];
        var array = data['data'];
        for (int i = 0; i < array.length; i++) {
          var obj = array[i];
          if (obj['shop'] != null) {
            name = obj['shop']['name'];
            logo = obj['shop']['thumbs']['md'];
          } else {
            name = obj['brand']['name'];
            logo = obj['brand']['thumbs']['md'];
          }
          var thumbs_path = obj['thumbs'];
          latest_flyer.add(BrowseFlyerModel(
              obj['id'],
              obj['name'],
              Urls.baseImageUrl + thumbs_path['sm'],
              Urls.baseImageUrl + logo,
              name,
              obj['isFavorited'],
              formatter2.format(formatter.parse(obj['valid_from'])),
              formatter2.format(formatter.parse(obj['valid_to'])),
              ""));
        }
        setState(() {
          latest_flyer_last_page = data['last_page'];
          print("lastpage " + data['last_page'].toString());
          print("current pages " + latest_flyer_page.toString());
          if (latest_flyer_page != latest_flyer_last_page) {
            latest_flyer_page++;
            latest_flyer_count = latest_flyer.length;
            latest_flyer.addAll(tList);
            print("latest_flyer_count" + latest_flyer.length.toString());
            print("current page " + latest_flyer_page.toString());
            //latest_flyer_count++;
          } else {
            latest_flyer_page = 0;
          }
        });
      } catch (e) {}
    }
  }

  void getSlugFlyers(String page, String slug) async {
    var name, logo;
    var response = await http.get(
        "${Urls.baseUrl}${Urls.CategoryListSlug}${slug}?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=${defult_radius}&locale=" +
            lan +
            "&page=" +
            page,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });

    if (response.statusCode == 200) {
      Map<String, dynamic> value = json.decode(response.body);
      // print("resp_slug_flyer "+response.body);
      print("resp_slug_flyer " + response.body);
      try {
        var data = value['data'];
        slug_flyer_last_page = data['last_page'];
        var array = data['data'];
        for (int i = 0; i < array.length; i++) {
          var obj = array[i];
          if (obj['shop'] != null) {
            name = obj['shop']['name'];
            logo = obj['shop']['thumbs']['md'];
          } else {
            name = obj['brand']['name'];
            logo = obj['brand']['thumbs']['md'];
          }
          var thumbs_path = obj['thumbs'];
          slug_flyer.add(BrowseFlyerModel(
              obj['id'],
              obj['name'],
              Urls.baseImageUrl + thumbs_path['sm'],
              Urls.baseImageUrl + logo,
              name,
              obj['isFavorited'],
              formatter2.format(formatter.parse(obj['valid_from'])),
              formatter2.format(formatter.parse(obj['valid_to'])),
              obj['categories_count']));
        }
        setState(() {
          slug_flyer_list_count = slug_flyer.length;
        });
      } catch (e) {}
    } else {
      print("resp_slug_flyer " + response.body);
    }
  }

  void getTrendingDeals(String page) async {
    //ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);

    var response = await http.get(
        "${Urls.baseUrl}${Urls.TrendingDeals}?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=${defult_radius}&&count=20&locale=" +
            lan +
            "&page=" +
            page,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print("resp_deals " + response.body);
    if (response.statusCode == 200) {
      // dialog.dismissProgressDialog(context);
      trending_deals_list.clear();
      try {
        // dialog.dismissProgressDialog(context);
        var obj = value['data'];
        deals_last_page = obj['last_page'];
        var array = obj['data'];
        if(array.length>0){
          for (int i = 0; i < array.length; i++) {
            var obj = array[i];
            trending_deals_list.add(TrendingDealsModel(
                obj['name'],
                obj['id'],
                Urls.baseImageUrl + obj['thumbs']['md'],
                obj['slug'],
                obj['slug']));
          }
          setState(() {
            // dialog.dismissProgressDialog(context);
            isDealsLoding = false;
            deals_list_count = trending_deals_list.length;
          });
        }
        else{
          setState(() {
            isDealsLoding=false;
          });
        }

      } catch (e) {
        print("deals_exp" + e.toString());
      }
    } else {
      // dialog.dismissProgressDialog(context);
    }
  }

  static Future<void> openMap(
      double latitude, double longitude, String name) async {
    String googleUrl =
        Urls.GoogleMapNavigationUrl + '$latitude,$longitude?q=$name';
    if (await canLaunch(googleUrl)) {
      await launch(googleUrl);
    } else {
      throw 'Could not open the map.';
    }
  }

  showAlertDialog(BuildContext context) {
    // Create button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {
        //Navigator.of(context).pop();
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => ChangeLocationScreen()),
            ModalRoute.withName("/login"));
      },
    );

    // Create AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Flyerbin"),
      content: Text("Please set your location to show flyers"),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Future<void> CheckAppUpdate() async {
    var alertStyle = AlertStyle(
      isCloseButton: true,
      isOverlayTapDismiss: true,
      descStyle: TextStyle(fontWeight: FontWeight.bold),
      descTextAlign: TextAlign.start,
      animationDuration: Duration(milliseconds: 400),
      alertBorder: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
        side: BorderSide(
          color: Colors.grey,
        ),
      ),
      titleStyle: TextStyle(
        color: Color(0xff00ADEE),
      ),
      alertAlignment: Alignment.center,
    );

    if (Platform.isIOS) {
      setState(() {
        device = "ios";
      });
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      String appversionCode = packageInfo.version;
      print("App=" + appversionCode);

      var response = await http.get(
        Urls.baseUrl + Urls.Appvesion + device,
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json"
        },
      );
      print("Version Update" + response.body);
      Map<String, dynamic> value = json.decode(response.body);
      var data = value['data'];
      var apiappversion = data['version'];
      print(apiappversion);
      if (appversionCode == apiappversion) {
      } else {
        Alert(
          context: context,
          style: alertStyle,
          title: "Update",
          desc: "Update Available",
          buttons: [
            DialogButton(
              child: Text(
                "OK",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () {
                print(APP_STORE_URL);
                launchAppURL(
                    'https://apps.apple.com/in/app/flyerbin/id1502843770');
              },
              color: Color(0xff00ADEE),
            ),
          ],
        ).show();
      }
    } else if (Platform.isAndroid) {
      setState(() {
        device = "android";
        print("Android=" + device);
      });
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      String appversionCode = packageInfo.buildNumber;
      print("App" + appversionCode);

      var response = await http.get(
        Urls.baseUrl + Urls.Appvesion + device,
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json"
        },
      );
      print("Version Update" + response.body);
      Map<String, dynamic> value = json.decode(response.body);
      var data = value['data'];
      var apiappversion = data['version'].toString();
      print(apiappversion);
      if (appversionCode == apiappversion) {
      } else {
        Alert(
          context: context,
          style: alertStyle,
          title: "Update",
          desc: "Update Available",
          buttons: [
            DialogButton(
              child: Text(
                "OK",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () {
                launchAppURL(PLAY_STORE_URL);
              },
              color: Color(0xff00ADEE),
            ),
          ],
        ).show();
      }
    } else {
      print("Error Occurred");
    }
  }

  //trigger App update url
  launchAppURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceWebView: true, universalLinksOnly: true);
    } else {
      Alert(
        context: context,
        type: AlertType.error,
        title: "Update Url is Broken:" + url,
        buttons: [
          DialogButton(
            child: Text(
              "Update",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () => Navigator.pop(context),
            width: 180,
            color: Color(0xff00ADEE),
          ),
        ],
      ).show();
//      throw 'Could not launch $url';
    }
  }

  void getFlashOffers(String page) async {
    var logo, name;
    print(userToken);
    print(defult_longitude);

    var response = await http.get(
        "${Urls.baseUrl}${Urls.Flash_offers}?latitude=${defult_latitude}&longitude=${defult_longitude}&radius=${defult_radius}&locale=" +
            lan +
            "&page=" +
            page,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print("resp_flashoffer " + response.body);
    if (response.statusCode == 200) {
      try {
        var data = value['data'];
        prefrence_last_page = data['last_page'];
        var array = data['data'];
        /* for (int i = 0; i < array.length; i++) {
          var obj = array[i];
          var id = obj['id'].toString();
          var desc = obj['name'].toString();
          var validfrom = obj["valid_from"].toString();
          var validto = obj["valid_to"].toString();
          var imagethumbpath = obj["thumb_path"].toString();
          var status = obj["status"];
          var isFavourite = obj["isFavorited"].toString();
          //var thumbs_path = obj["thumbs"];
          var slug = obj['slug'].toString();
          if (obj['shop'] != null) {
            logo = obj["shop"]["thumbs"]["md"].toString();
            name = obj["shop"]["name"].toString();
          } else {
            logo = obj["brand"]["thumbs"]["md"].toString();
            name = obj["brand"]["name"].toString();
          }
          prefrence_list.add(PrefrenceModel(
              id,
              desc,
              validfrom,
              validto,
              Urls.baseImageUrl + imagethumbpath,
              status,
              isFavourite,
              Urls.baseImageUrl + logo,
              name,
              1,
              slug,
              "",
              false,
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              ""));
        }*/
        setState(() {
          flash_offer_count = array.length;
        });
      } catch (e) {
        print("pref_exception " + e);
      }
    }
  }

}
