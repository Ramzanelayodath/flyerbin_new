import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

import 'BrowseDashboardScreen.dart';
import 'DashboardCouponScreen.dart';
import 'DashboardItemListScreen.dart';
import 'DashboardNearByScreen.dart';
import 'DashboardSearchScreen.dart';

class DashboardScreen extends StatefulWidget {
  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  PersistentTabController _controller =PersistentTabController(initialIndex: 0);
  int currentTabIndex = 0;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
//Screens for each nav items.
  List<Widget> _NavScreens() {
    return [
      BrowseDashboardScreen("tapped"),
      DashboardSearchScreen(),
      DashboardItemListScreen(),
      DashboardNearByScreen(),
      DashboardCouponScreen(),

    ];
  }
  List<PersistentBottomNavBarItem> _navBarsItems() {
    print("index "+_controller.index.toString());
    return [
      PersistentBottomNavBarItem(
        icon:_controller.index==0?SvgPicture.asset("assets/images/bottom_browse_selected.svg"):SvgPicture.asset("assets/images/bottom_browse.svg"),
        title: ("Browse"),
        activeColor: Color.fromRGBO(0, 161, 237, 1),
        inactiveColor: Color.fromRGBO(70, 71, 73, 1),
      ),
      PersistentBottomNavBarItem(
        icon: SvgPicture.asset("assets/images/bottom_search.svg"),
        title: ("Search"),
        activeColor: Color.fromRGBO(0, 161, 237, 1),
        inactiveColor: Color.fromRGBO(70, 71, 73, 1),
      ),
      PersistentBottomNavBarItem(
        icon: SvgPicture.asset("assets/images/bottom_shopping_list.svg"),
        title: ("List"),
        activeColor: Color.fromRGBO(0, 161, 237, 1),
        inactiveColor: Color.fromRGBO(70, 71, 73, 1),
      ),
      PersistentBottomNavBarItem(
        icon: SvgPicture.asset("assets/images/bottom_nearby.svg"),
        title: ("Near By"),
        activeColor: Color.fromRGBO(0, 161, 237, 1),
        inactiveColor: Color.fromRGBO(70, 71, 73, 1),
      ),
      PersistentBottomNavBarItem(
        icon: SvgPicture.asset("assets/images/bottom_coupons.svg"),
        title: ("Coupons"),
        activeColor: Color.fromRGBO(0, 161, 237, 1),
        inactiveColor: Color.fromRGBO(70, 71, 73, 1),
      ),

    ];
  }

  @override
  void initState() {
    super.initState();
  }

  _getDrawerItemWidget(int pos) {
    switch (pos) {
      case 0:
          return new BrowseDashboardScreen("tapped");
      case 1:
        return new DashboardSearchScreen();
      case 2:
        return new DashboardItemListScreen();
      case 3:
        return new DashboardNearByScreen();
      case 4:
        return new DashboardCouponScreen();
      default:
        return new Text("Error");
    }
  }

  onTapped(int index) {
    // ignore: missing_return
    setState(() {
      currentTabIndex = index;
      if (currentTabIndex == 0) {
        print("tapped");
        return new BrowseDashboardScreen("tapped");

      } else if (currentTabIndex == 1) {
        // ignore: missing_return
        return new DashboardSearchScreen();
      } else if (currentTabIndex == 3) {
        return new DashboardItemListScreen();
      } else {
        return new DashboardCouponScreen();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    /*return  Center(
      child: PersistentTabView(
        controller: _controller,
        screens: _NavScreens(),
        items: _navBarsItems(),
        confineInSafeArea: true,
        backgroundColor: Colors.white,
        handleAndroidBackButtonPress: true,
        stateManagement: false,
        resizeToAvoidBottomInset: true,
        hideNavigationBarWhenKeyboardShows: true,
        navBarStyle: NavBarStyle.style6,
      ),
    );*/
    return Scaffold(
      key: _scaffoldKey,
      body: _getDrawerItemWidget(currentTabIndex),
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTapped,
        showSelectedLabels: true, // <-- HERE
        showUnselectedLabels: true,
        selectedLabelStyle: TextStyle(color: Colors.orange),
        unselectedLabelStyle: TextStyle(color: Colors.black),
        currentIndex: currentTabIndex,
        items: [
          BottomNavigationBarItem(
            //icon: SvgPicture.asset("assets/images/ic_home_bottom.svg"),
            //activeIcon: SvgPicture.asset("assets/images/ic_home_select_bottom.svg"),
            icon: SvgPicture.asset("assets/images/bottom_browse.svg"),
            activeIcon: SvgPicture.asset("assets/images/bottom_browse_selected.svg"),
            title: Text("Browse",
                style: TextStyle(color:currentTabIndex==0? Color.fromRGBO(0, 161, 237, 1):Color.fromRGBO(70, 71, 73, 1), fontSize: 13)),
          ),
          BottomNavigationBarItem(
            // icon: SvgPicture.asset("assets/images/send_bottom.svg"),
            //activeIcon: SvgPicture.asset("assets/images/bottom_selected_send.svg"),
            icon: SvgPicture.asset("assets/images/bottom_search.svg"),
            activeIcon: SvgPicture.asset("assets/images/bottom_search_selected.svg"),
            title: Text("Search",
                style: TextStyle(color:currentTabIndex==1? Color.fromRGBO(0, 161, 237, 1):Color.fromRGBO(70, 71, 73, 1), fontSize: 13)),
          ),
          BottomNavigationBarItem(
            //icon: SvgPicture.asset("assets/images/bottom_schedule.svg"),
            //activeIcon: SvgPicture.asset("assets/images/bottom_selected_schedule.svg"),
            icon: SvgPicture.asset("assets/images/bottom_shopping_list.svg"),
            activeIcon: SvgPicture.asset("assets/images/bottom_shopping_list_selected.svg"),
            title: Text("List",
                style: TextStyle(color:currentTabIndex==2? Color.fromRGBO(0, 161, 237, 1):Color.fromRGBO(70, 71, 73, 1), fontSize: 13)),
          ),
          BottomNavigationBarItem(
            //  icon: SvgPicture.asset("assets/images/bottom_rate.svg"),
            //activeIcon: SvgPicture.asset("assets/images/bottom_selected_rate.svg"),
            icon: SvgPicture.asset("assets/images/bottom_nearby.svg"),
            activeIcon: SvgPicture.asset("assets/images/bottom_nearby_selected.svg"),
            title: Text("Nearby",
                style: TextStyle(color:currentTabIndex==3? Color.fromRGBO(0, 161, 237, 1):Color.fromRGBO(70, 71, 73, 1), fontSize: 13)),
          ),
          BottomNavigationBarItem(
            //icon: SvgPicture.asset("assets/images/bottom_branch.svg"),
            //activeIcon: SvgPicture.asset("assets/images/bottom_selected_branch.svg"),
            icon: SvgPicture.asset("assets/images/bottom_coupons.svg"),
            activeIcon: SvgPicture.asset("assets/images/bottom_coupon_selected.svg"),
            title: Text("Coupons",
                style: TextStyle(color:currentTabIndex==4? Color.fromRGBO(0, 161, 237, 1):Color.fromRGBO(70, 71, 73, 1), fontSize: 13)),
          ),
        ],
      ),
    );
  }

}

