import 'dart:async';
import 'dart:convert';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';

import 'Cart.dart';
import 'FlyerLoadScreen.dart';
import 'Model/BrowseFlyerModel.dart';
import 'Model/NearbyListitemModel.dart';
import 'Model/NearbyMallListItemModel.dart';
import 'Utils/Constants.dart';
import 'Utils/CustomDialogs.dart';
import 'Utils/SharedPrefrence.dart';
import 'Utils/Urls.dart';
import 'package:http/http.dart' as http;

// ignore: must_be_immutable
class FlashOfferListScreen extends StatefulWidget {
 // String shop_id;
 // String mall_id;
 // String namep;

  //NearbyListItemDetailsScreen(this.shop_id, this.mall_id, this.namep);

  @override
  FlashOfferListScreenScreenState createState() =>
      FlashOfferListScreenScreenState();
}

class FlashOfferListScreenScreenState
    extends State<FlashOfferListScreen> {
  String shop_id;
  String mall_id;
  String namep,lan = 'en';
  final List<NearbyListitemModel> nearbylistitems = List();
  final List<NearbyMallListItemModel> nearbymalllistitems = List();
  double latti = 25.2048493, long = 55.2707828;
  Completer<GoogleMapController> _controller = Completer();
  Iterable markers = [];
  String userToken,
      userSelectedSlug,
      defult_latitude = "",
      defult_longitude = "",
      defult_radius = "20";
  bool shopflyershow = false;
  bool mallflyershow = false;
  final DateFormat formatter = DateFormat('yyyy-MM-dd');
  final DateFormat formatter2 = DateFormat('dd-MM-yyyy');
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  List<BrowseFlyerModel>flash_offer_flyer = List();
  int fav_flyer_list_count=0,flash_offer_last_page = 1,flash_offer_page=1;
  ScrollController _flash_offer_controller = ScrollController();

  //_NearbyListItemDetailsScreenState(this.shop_id, this.mall_id, this.namep);



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future lng = SharedPrefrence().getLanguage();
    lng.then((data) async {
      lan = data;
    });
    Future token = SharedPrefrence().getToken();
    Future latitude = SharedPrefrence().getLatitude();
    Future longitude = SharedPrefrence().getLongitude();
    Future radius = SharedPrefrence().getRadius();
    token.then((data) async {
      latitude.then((lat_data) async {
        longitude.then((lon_data) async {
          radius.then((radius_data) async {
            userToken = data;
            defult_latitude = lat_data;
            defult_longitude = lon_data;
            defult_radius = radius_data;
            if(defult_radius==""){
              defult_radius="20";
              SharedPrefrence().setRadius(defult_radius);
            }
            print(defult_radius + " radius");
            print(defult_longitude+" Longitude");
              setState(() {
                getFlashOffersList(flash_offer_page.toString());
              });

          });
        });
      });
  });

    _flash_offer_controller.addListener(() {
      if (_flash_offer_controller.position.pixels ==
          _flash_offer_controller.position.maxScrollExtent) {
        if (flash_offer_page == 0) {
        } else {
          getFlashOffersList(flash_offer_page.toString());
        }
      }
    });
        }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          centerTitle: false,
          iconTheme: new IconThemeData(color: Color.fromRGBO(34, 83, 148, 1)),
          title: Text(
            "Flash Offers",
            style:
                TextStyle(fontSize: 13, color: Color.fromRGBO(34, 83, 148, 1)),
          ),
          backgroundColor: Colors.white,
          actions: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: GestureDetector(
                child:  Icon(
                  Icons.shopping_cart,
                  color: Color(0xff00ADEE),
                ),
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Cart(),
                    ),
                  );
                },
              ),
            )
          ],
        ),
        body:FlashOfferFlyer());/* SingleChildScrollView(
          child: Column( 
            children: [

              //Visibility(visible: mallflyershow, child: MallFlyer()),
              //Visibility(visible: shopflyershow, child: ShopFlyer())
            ],
          ),
        ));*/
  }

  Widget MallFlyer() {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListView.builder(
            shrinkWrap: true,
            itemCount: nearbymalllistitems.length,
            itemBuilder: (context, index) {
              NearbyMallListItemModel data = nearbymalllistitems[index];
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                      flex: 0,
                      child: Container(
                        height: 70,
                        width: 100,
                        child: Image.network(
                          data.original_logo.toString(),
                          height: 70,
                          width: 100,
                          fit: BoxFit.fill,
                        ),
                      )),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        data.name.toString(),
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.w500),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Container(
                          width: 250,
                          child: Text(data.address.toString(),
                              style: TextStyle(
                                  fontSize: 13, color: Colors.blueGrey))),
                    ],
                  )
                ],
              );
            },
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 150,
            child: GoogleMap(
              markers: Set.from(markers),
              initialCameraPosition: CameraPosition(
                  target: LatLng(latti, long), tilt: 30, zoom: 14),
              mapType: MapType.normal,
              onMapCreated: (GoogleMapController controller) {
                _controller = controller as Completer<GoogleMapController>;
              },
            ),
          ),
         /* Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Flyers",
              style: TextStyle(fontSize: 15, color: Colors.blueAccent[400]),
            ),
          ),*/
        ],
      ),
    );
  }

  Widget ShopFlyer() {
    return nearbylistitems.length==0?Center(child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Image.asset("assets/images/no_flyer.png"))
    ):
     ListView.builder(
      shrinkWrap: true,
      itemCount: nearbylistitems.length,
      itemBuilder: (context, index) {
        NearbyListitemModel data = nearbylistitems[index];
        return Center(
          /*child: GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => FlyerLoadScreen(
                        data.id.toString(),
                        data.name.toString(),
                        data.valid_from.toString(),
                        data.valid_to.toString(),
                        data.image_path.toString(),false,"")),
              );
            },
            child: Container(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    height: 160,
                    width: 120,
                    child: Image.network(
                      data.image_path.toString(),
                      height: 160,
                      width: 120,
                      fit: BoxFit.fill,
                    ),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: [
                          Container(
                              width: 150,
                              child: Text(
                                data.name,
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold),
                              )),
                          SizedBox(
                            height: 20,
                          ),
                          Text("Valid From " +
                             data.valid_from),
                          Text("Valid To  " +
                              data.valid_to),
                        ],
                      ),
                      GestureDetector(
                        onTap: (){
                          doFavorUnfav(model :data,isfromlist : true);
                        },
                        child: Icon(Icons.favorite_border,
                            color: data.isFavorited ? Colors.red : Colors.blue),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),*/
          child:Padding(
              padding:
              EdgeInsets.only(left: 5, right: 5, top: 3, bottom: 20),
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => FlyerLoadScreen(
                          data.id.toString(),
                          data.name.toString(),
                          data.valid_from.toString(),
                          data.valid_to.toString(),
                          data.image_path.toString(),false,""),
                    ),
                  );
                },
                child: Container(
                  width: 380,
                  height: 180,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(color: Color(0xffE0E0E0)),
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(5)),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 2,
                        child: Image.network(
                          data.image_path.toString(),
                          fit: BoxFit.fill,
                          // width: 120,
                          height: 180,
                        ),
                      ),
                      Container(
                        width: 15,
                        child: Text(" "),
                      ),
                      Expanded(
                        flex: 3,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(3),
                              child: Container(
                                child: Image.network(
                                  "",
                                  fit: BoxFit.fill,
                                  width: 70,
                                  height: 70,
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 5),
                              child: Text(
                                data.name,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(5),
                              child: Container(
                                child: Image.asset(
                                  'assets/images/new.png',
                                  fit: BoxFit.fitHeight,
                                  width: 40,
                                  height: 15,
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(5),
                              child: Text(
                                "Valid from: " +
                                    data.valid_from.toString(),
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: Colors.blueGrey,
                                    fontWeight: FontWeight.w400,
                                    fontSize: 11),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 5),
                              child: Text(
                                "Valid to:" + data.valid_to.toString(),
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: Colors.blueGrey,
                                    fontWeight: FontWeight.w400,
                                    fontSize: 11),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                          flex: 1,
                          child: Padding(
                            padding: EdgeInsets.only(right: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisAlignment:
                              MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  flex: 0,
                                  child: Padding(
                                      padding: const EdgeInsets.all(5),
                                      child: Text(" ")),
                                ),
                                Expanded(
                                  flex: 0,
                                  child: Padding(
                                    padding: const EdgeInsets.all(5),
                                    child: GestureDetector(
                                      child: Icon(
                                        Icons.favorite_border,
                                        size: 22,
                                        color:
                                        data.isFavorited
                                            ? Colors.red
                                            : Colors.blue,
                                      ),
                                      onTap: () {
                                        doFavorUnfav(
                                            model: flash_offer_flyer[index],
                                            isfromlist: true);
                                      },
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ))
                    ],
                  ),
                ),
              )),
        );
      },
    );
  }

  Widget FlashOfferFlyer() {
    return ListView.builder(
        itemCount: flash_offer_flyer.length + 1,
        // physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        controller: _flash_offer_controller,
        itemBuilder: (context, index) {
          if (index == flash_offer_flyer.length) {
            if (flash_offer_page == 0) {
              return Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("No more flyers....."),
                  ));
            } else {
              return Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: CircularProgressIndicator(),
                  ));
            }
          } else {
            return Center(
              child: Padding(
                  padding:
                  EdgeInsets.only(left: 3, right: 3, top: 3, bottom: 20),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => FlyerLoadScreen(
                                  flash_offer_flyer[index].id.toString(),
                                  flash_offer_flyer[index].shopName,
                                  flash_offer_flyer[index].vaild_from,
                                  flash_offer_flyer[index].valid_to,
                                  flash_offer_flyer[index].shopLogo,
                                  false,
                                  "")));
                    },
                    child: Container(
                      width: 380,
                      height: 180,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(color: Color(0xffE0E0E0)),
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(5)),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 2,
                            child: Image.network(
                              flash_offer_flyer[index].thumb_path,
                              fit: BoxFit.fill,
                              // width: 120,
                              height: 180,
                            ),
                          ),
                          Container(
                            width: 15,
                            child: Text(" "),
                          ),
                          Expanded(
                            flex: 3,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(3),
                                  child: Container(
                                    child: Image.network(
                                      flash_offer_flyer[index].shopLogo,
                                      fit: BoxFit.fill,
                                      width: 70,
                                      height: 70,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 5),
                                  child: Text(
                                    flash_offer_flyer[index].name,
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(5),
                                  child: Container(
                                    child: Image.asset(
                                      'assets/images/new.png',
                                      fit: BoxFit.fitHeight,
                                      width: 40,
                                      height: 15,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(5),
                                  child: Text(
                                    "Valid from: " +
                                        flash_offer_flyer[index].vaild_from,
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: Colors.blueGrey,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 11),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 5),
                                  child: Text(
                                    "Valid to:" + flash_offer_flyer[index].valid_to,
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: Colors.blueGrey,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 11),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                              flex: 1,
                              child: Padding(
                                padding: EdgeInsets.only(right: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      flex: 0,
                                      child: Padding(
                                          padding: const EdgeInsets.all(5),
                                          child: Text(" ")),
                                    ),
                                    Expanded(
                                      flex: 0,
                                      child: Padding(
                                        padding: const EdgeInsets.all(5),
                                        child: GestureDetector(
                                          child: Icon(
                                            Icons.favorite_border,
                                            size: 22,
                                            color:
                                            flash_offer_flyer[index].isFavourited
                                                ? Colors.red
                                                : Colors.blue,
                                          ),
                                          onTap: () {
                                            doFavorUnfav(
                                                model: flash_offer_flyer[index],
                                                isfromlist: true);
                                          },
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ))
                        ],
                      ),
                    ),
                  )),
            );
            /*GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FlyerLoadScreen(
                              latest_flyer[index].id.toString(),
                              latest_flyer[index].shopName,
                              latest_flyer[index].vaild_from,
                              latest_flyer[index].valid_to,
                              latest_flyer[index].shopLogo,
                              false,
                              "")));
                },
                child: Center(
                    child: Card(
                  //elevation: 10,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0.0),
                  ),
                  child: Container(
                    height: 180,
                    width: 380,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          //mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(0),
                                child: Container(
                                  child: Image.network(
                                    latest_flyer[index].thumb_path,
                                    fit: BoxFit.fill,
                                    width: 120,
                                    height: 180,
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: Container(
                                  child: Image.network(
                                    latest_flyer[index].shopLogo,
                                    fit: BoxFit.fill,
                                    width: 50,
                                    height: 50,
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: Text(
                                  latest_flyer[index].name,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: Text(
                                  "Valid from: " +
                                      latest_flyer[index].vaild_from,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Colors.blueGrey,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 11),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: Text(
                                  "Valid to:" + latest_flyer[index].valid_to,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Colors.blueGrey,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 11),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              flex: 0,
                              child: Visibility(
                                child: Padding(
                                  padding: const EdgeInsets.all(5),
                                  child: Icon(
                                    Icons.shopping_cart,
                                    size: 22,
                                    color: Colors.blue,
                                  ),
                                ),
                                visible: false,
                              ),
                            ),
                            Expanded(
                              flex: 0,
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: GestureDetector(
                                  child: Icon(
                                    Icons.favorite_border,
                                    size: 22,
                                    color: latest_flyer[index].isFavourited
                                        ? Colors.red
                                        : Colors.blue,
                                  ),
                                  onTap: () {
                                    doFavorUnfav(
                                        model: latest_flyer[index],
                                        isfromlist: true,
                                        isfromfavflayer: false);
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                )));*/
          }
        });
  }

  Future<void> ShopFlyerData(String shop_id) async {
    print(shop_id);
    var response = await http.get(
        "${Urls.baseUrl}${Urls.ShopFlyers}/$shop_id/flyers?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=100&locale="+lan+"&page=1",
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print("response " + response.body);
    if (response.statusCode == 200) {
      try {
        var data = value['data'];
        var current_page = value['current_page'];
        var array = data['data'];
        for (int i = 0; i < array.length; i++) {
          var obj = array[i];
          print("Image link " + obj['name']);

          nearbylistitems.add(NearbyListitemModel(
            obj['id'],
            obj['shop_id'],
            obj['brand_id'],
            obj['name'],
            formatter2.format(formatter.parse(obj['valid_from'])),
            formatter2.format(formatter.parse(obj['valid_to'])),
            obj['favourite_count'],
            Urls.baseImageUrl + obj['image_path'],
            obj['isFavorited'],
          ));

          setState(() {
            nearbylistitems.length;
            print(nearbylistitems.length);
          });
        }
      } catch (e) {
        e.toString();
      }
    }
  }

  Future<void> MallFlyerData(String mall_id) async {
    print(mall_id);
    var response = await http.get(
        "${Urls.baseUrl}${Urls.GetMalldetails}/$mall_id?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=100&locale="+lan+"&page=1",
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print("response mall" + response.body);
    if (response.statusCode == 200) {
      try {
        var data = value['data'];
        print("Mall name " + data['name']);
        nearbymalllistitems.add(NearbyMallListItemModel(
          data['id'],
          data['name'],
          Urls.baseImageUrl + data['original_logo'],
          data['address'],
          data['pincode'],
          data['latitude'],
          data['longitude'],
        ));
        setState(() {
          nearbymalllistitems.length;
          this.latti = latti;
          this.long = long;
          latti = double.parse(data["latitude"]);
          long = double.parse(data["longitude"]);
          LatLng latLngMarker = LatLng(
              double.parse(data["latitude"]), double.parse(data["longitude"]));

          return Marker(
              markerId: MarkerId("marker$data"),
              position: latLngMarker,
              infoWindow:
                  InfoWindow(title: data["name"], snippet: data["address"]));
        });
      } catch (e) {
        e.toString();
      }
    }
  }

  void doFavorUnfav({BrowseFlyerModel model,bool isfromlist, String id})async{
    ProgressDialog dialog = CustomDialogs().showLoadingProgressDialog(context);
    var url;
    if(isfromlist){
      url =  "${Urls.baseUrl}${Urls.AddRemoveFavourite}?flyer_id="+model.id.toString();
    }else{
      url =  "${Urls.baseUrl}${Urls.AddRemoveFavourite}?flyer_id="+model.id;
    }
    var response = await http.post(
        url,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });
    Map<String, dynamic> value = json.decode(response.body);
    print("response mall" + response.body);
    if(response.statusCode == 200){
      dialog.dismissProgressDialog(context);
      try{
        if(value['message'] == "Favourited"){
          setState(() {
            if(isfromlist) {
              model.isFavourited = true;
            }else{
              model.isFavourited = true;
            }
          });
          final snackBar = SnackBar(content: Text("Added into your Favourite list",style: TextStyle(color: Colors.white)),backgroundColor: Colors.green,duration: Duration(milliseconds: 100),);
          _scaffoldKey.currentState.showSnackBar(snackBar);

        }else if(value['message'] == "Unfavourited"){
          setState(() {
            if(isfromlist) {
              model.isFavourited = false;
              // if(isfromfavflayer){
              //   fav_flyer.remove(model);
              //   fav_flyer_list_count = fav_flyer.length;
              //
              // }
            }else{
              model.isFavourited = false;
            }
          });
          final snackBar = SnackBar(content: Text("Removed From your Favourite list",style: TextStyle(color: Colors.white)),backgroundColor: Colors.green,duration: Duration(milliseconds: 100),);
          _scaffoldKey.currentState.showSnackBar(snackBar);
        }
      }catch(e){}
    }else{
      dialog.dismissProgressDialog(context);
      final snackBar = SnackBar(content: Text(value['message'],style: TextStyle(color: Colors.white)),backgroundColor: Colors.red,duration: Duration(milliseconds: 100),);
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  void getFlashOffersList(String page) async {
    var name, logo;
    // print("latest_flyer_page" + page.toString() + " " + defult_radius);
    List<BrowseFlyerModel> tList = List();
    var response = await http.get(
        "${Urls.baseUrl}${Urls.Flash_offers}?latitude=${Constants.latitude}&longitude=${Constants.longitude}&radius=${defult_radius}&locale=" +
            lan +
            "&page=" +
            page,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${userToken}",
          "Accept": "application/json"
        });

    if (response.statusCode == 200) {
      Map<String, dynamic> value = json.decode(response.body);
      print("resp_nearest " + response.body);
      try {
        var data = value['data'];
        flash_offer_last_page = data['last_page'];
        var array = data['data'];
        for (int i = 0; i < array.length; i++) {
          var obj = array[i];
          if (obj['shop'] != null) {
            name = obj['shop']['name'];
            logo = obj['shop']['thumbs']['md'];
          } else {
            name = obj['brand']['name'];
            logo = obj['brand']['thumbs']['md'];
          }
          var thumbs_path = obj['thumbs'];
          flash_offer_flyer.add(BrowseFlyerModel(
              obj['id'],
              obj['name'],
              Urls.baseImageUrl + thumbs_path['sm'],
              Urls.baseImageUrl + logo,
              name,
              obj['isFavorited'],
              formatter2.format(formatter.parse(obj['valid_from'])),
              formatter2.format(formatter.parse(obj['valid_to'])),
              ""));
        }
        setState(() {
          flash_offer_last_page = data['last_page'];
          print("lastpage " + data['last_page'].toString());
          print("current pages " + flash_offer_page.toString());
          if (flash_offer_page != flash_offer_last_page) {
            flash_offer_page++;
            fav_flyer_list_count = flash_offer_flyer.length;
            flash_offer_flyer.addAll(tList);
            print("latest_flyer_count" + flash_offer_flyer.length.toString());
            print("current page " + flash_offer_page.toString());
            //latest_flyer_count++;
          } else {
            flash_offer_page = 0;
          }
        });
      } catch (e) {}
    }
  }
}
