import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart'
    as google_place;
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart' as map_service;
import 'package:seekbar/seekbar.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'BottomnavBarScreen.dart';
import 'Utils/Constants.dart';
import 'Utils/SharedPrefrence.dart';

class ChangeLocationScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ChangeLocationScreenState();
  }
}

const kGoogleApiKey = "AIzaSyCvj5T9L3_ew9VgUaLCmYlm0MYI9lBSMLA";
map_service.GoogleMapsPlaces _places =
    map_service.GoogleMapsPlaces(apiKey: kGoogleApiKey);

class ChangeLocationScreenState extends State<ChangeLocationScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final TextEditingController _textEditSearchControl =
      new TextEditingController();
  Position _currentPosition;
  String _currentAddress = "";
  int valueHolder = 20;
  String def_lat, def_lon;
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 15.0);

  @override
  void initState() {
    super.initState();
    Future latitude = SharedPrefrence().getLatitude();
    Future longitude = SharedPrefrence().getLongitude();
    latitude.then((lat) async {
      longitude.then((lon) async {
        def_lat = lat;
        def_lon = lon;
        print(def_lat + " " + def_lon);
        if (def_lat == "") {
        } else {
          _getAddressFromLatLng(double.parse(def_lat), double.parse(def_lon));
        }
      });
    });

    // _GetLocationdata();
    // _getLocation();
    //  _getCurrentLocation();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      resizeToAvoidBottomInset: false,
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          "Change Location",
          style: TextStyle(fontSize: 15, color: Colors.lightBlue),
        ),
        automaticallyImplyLeading: true,
        iconTheme: IconThemeData(
          color: Colors.lightBlue,
        ),
        backgroundColor: Colors.white,
        /* actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(
              Icons.done,
              color: Color(0xff00ADEE),
            ),
          )
        ],*/
      ),
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Expanded(
                    flex: 0,
                    child: Padding(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        "Location",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.black),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 0,
                    child: Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: [
                          Icon(
                            Icons.location_on,
                            color: Colors.black,
                          ),
                          Text(
                            _currentAddress,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                            ),
                            textAlign: TextAlign.right,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Searchbar(),
              Expanded(
                flex: 0,
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Row(
                    children: [
                      Icon(
                        Icons.location_searching,
                        color: Colors.black,
                      ),
                      GestureDetector(
                        onTap: () {
                          _getCurrentLocation();
                        },
                        child: Text(
                          "Locate Me",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                          textAlign: TextAlign.right,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 0,
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      //Icon(Icons.location_searching, color: Colors.black,),
                      Expanded(
                        child: Text(
                          "Radius",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                          textAlign: TextAlign.left,
                        ),
                        flex: 1,
                      ),

                      Expanded(
                        child: Text(
                          valueHolder.toString(),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color(0xff00ADEE),
                          ),
                          textAlign: TextAlign.right,
                        ),
                        flex: 1,
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                  padding: EdgeInsets.all(10),
                  child: Slider(
                      value: valueHolder.toDouble(),
                      min: 20,
                      max: 100,
                      divisions: 100,
                      activeColor: Color(0xff00ADEE),
                      inactiveColor: Colors.grey,
                      label: '${valueHolder.round()}',
                      onChanged: (double newValue) {
                        setState(() {
                          valueHolder = newValue.round();
                        });
                      },
                      semanticFormatterCallback: (double newValue) {
                        return '${newValue.round()}';
                      })),
              Padding(
                padding: EdgeInsets.all(20),
                child: loginButon(),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget loginButon() {
    return Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff00ADEE),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(10.0, 15.0, 20.0, 10.0),
        onPressed: () {
          //print(valueHolder.toString());
          SharedPrefrence().setRadius(valueHolder.toString());
          /*Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => DashboardScreen()),
              ModalRoute.withName("/login"));*/
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DashboardScreen()));

        },
        child: Text("Save",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );
  }

  Widget Searchbar() {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Container(
          width: 400,
          height: 40,
          child: GestureDetector(
            onTap: () async {
              map_service.Prediction p =
                  await google_place.PlacesAutocomplete.show(
                      mode: google_place.Mode.overlay,
                      context: context,
                      apiKey: kGoogleApiKey);
              SearchLocation(p);
            },
            child: TextField(
              controller: _textEditSearchControl,
              textAlign: TextAlign.left,
              enabled: false,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                fillColor: Colors.white,
                prefixIcon: Icon(Icons.search),
                hintText: "Search Location",
                hintStyle: TextStyle(fontSize: 12),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15),
                  borderSide: BorderSide(
                    width: 0,
                    style: BorderStyle.none,
                  ),
                ),
                filled: true,
                contentPadding: EdgeInsets.all(12),
              ),
            ),
          )),
    );
  }

  ///Search location function when press the Textfield
  Future<Null> SearchLocation(map_service.Prediction p) async {
    if (p != null) {
      map_service.PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(p.placeId);

      var placeId = p.placeId;
      var latitude = detail.result.geometry.location.lat;
      var longitude = detail.result.geometry.location.lng;

      var address = detail.result.formattedAddress;
      // SharedPrefrence().setLatitude(latitude.toString());
      //SharedPrefrence().setLongitude(longitude.toString());
      print(latitude);
      print(longitude);
      print(address);
      setState(() {
        _textEditSearchControl.text = address;
        _getAddressFromLatLng(latitude, longitude);
      });
    }
  }

  _getCurrentLocation() async {
    final snackBar = SnackBar(
      content: Row(
        children: [
          Text("Locating....    ",style: TextStyle(fontSize: 16),),
          Container(height: 20, width: 20, child: CircularProgressIndicator()),
        ],
      ),
      backgroundColor: Colors.greenAccent[700],
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);

    await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _getAddressFromLatLng(position.latitude, position.longitude);
        final snackBar = SnackBar(
          content: Text("Located Successfully"),
          backgroundColor: Colors.greenAccent[700],
        );
        _scaffoldKey.currentState.showSnackBar(snackBar);
      });
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng(double latitude, double longitude) async {
    try {
      List<Placemark> p = await placemarkFromCoordinates(latitude, longitude);

      Placemark place = p[0];

      setState(() {
        _currentAddress = "${place.name},${place.locality}";
        print(_currentAddress);
        // _textEditSearchControl.text = place.locality;
        if (place.country == "United Arab Emirates") {
          SharedPrefrence().setLatitude(latitude.toString());
          SharedPrefrence().setLongitude(longitude.toString());
        } else {
          SharedPrefrence().setLatitude(Constants.latitude);
          SharedPrefrence().setLongitude(Constants.longitude);
        }
      });
    } catch (e) {
      print(e);
    }
  }
}
